<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => []], function () {
    Route::post('/server/token','API\Auth\TokenController@token');
    Route::post('/auth/token','API\Auth\TokenController@accessToken');
    Route::post('/auth/token/refresh','API\Auth\TokenController@refreshAccessToken');
    Route::post('/auth/check','API\Auth\TokenController@check');
    Route::post('/auth/register','API\Auth\RegisterController@register');

	Route::post('user/streams/data','API\StreamController@stream');

	Route::get('/user','API\UserController@show');
	Route::get('/user/{id}/ecgs','API\ECGController@listById');
});

Route::group(['middleware' => ['client_credentials']], function () {
	Route::get('/server/ecgs','API\ECGController@allEcgs');
	Route::get('/server/ecgs/{id}','API\ECGController@show');
	Route::post('/server/ecgs','API\ECGController@store');
	Route::get('/server/ecgs/{id}/z2b/download','API\FileController@z2bFile');
	Route::get('/server/ecgs/{id}/info/download','API\FileController@infoFile');
	Route::get('/server/ecgs/{id}/rredited/download','API\FileController@rrEditedFile');
});
Route::group(['middleware' => []], function () {
	Route::post('/ecgs/upload','API\ECGController@upload');
});

Route::group(['middleware' => ['auth:api','auth.checked', 'auth.expiration_checked']], function () {
	Route::resource('/user/ecgs','API\ECGController');
	Route::get('/user/profile','API\ProfileController@show');
	Route::put('/user/profile','API\ProfileController@update');
	Route::get('/user/ecgs/{id}/report','API\ECGController@report');
	Route::post('/user/ecgs/transfer','API\TransferController@run');
	Route::get('/user/ecgs/{id}/report/download','API\ECGController@reportDownload');
	Route::resource('/user/patients','API\PatientController');
	Route::get('/data/users','API\UserController@index');
	Route::get('/data/users/{id}/ecgs','API\ECGController@userData');

	Route::get('user/ecgs/{id}/ecg','API\ECGController@getResult');
	Route::get('user/ecgs/{id}/sleep','API\SleepController@getResult');

	Route::post('user/ecgs/{id}/hrv','API\HRVController@runAnalysis');
	Route::get('user/ecgs/{id}/hrv','API\HRVController@getResult');

	Route::get('user/services','API\ServiceController@index');

	Route::post('user/streams/start','API\StreamController@start');
	Route::post('user/streams/stop','API\StreamController@stop');


	Route::get('/user/ecgs/{id}/info/download','API\FileController@infoFile');
	Route::get('/user/ecgs/{id}/rredited/download','API\FileController@rrEditedFile');
	
	Route::resource('/user/activities','API\ActivityController');
});
