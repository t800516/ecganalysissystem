<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', function(){
	if(Auth::check()){
        if(Auth::user()->can('poc_manager')){
            return redirect('/data/pocs/manager');
        }
		if((Auth::user()->cannot('data_type') && Auth::user()->cannot('manager_type'))){
            return redirect('/ecg/manager');
        }else{
            return redirect('/data/users');
        }
	}else{
		return redirect()->route('login');
	}
})->name('home');

Route::group(['middleware' => ['web']], function () {
    Route::post('/sendmail','Message\MailController@send');
    Route::get('/apiexplorer','API\ExplorerController@index');
    Route::get('/survey/{id}','Survey\SurveyController@showWeb')->where('id', '[0-9]+');
    Route::post('/survey/{id}/save','Survey\SurveyController@store');
    Route::get('/survey/result','Survey\SurveyController@result');

    Route::get('/apks/{apk}','Apk\ApkController@show');
    Route::get('/lang/set/{locale}','Lang\LocaleController@setLang');
    Route::get('/privacy','PageController@privacy');
    Route::get('/authexpired','Auth\LoginController@authexpired');
    Route::get('/reports/{slug}','API\ECGController@openReportDownloadBySlug');
});

Route::group(['middleware' => ['guest']], function () {
    Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login','Auth\LoginController@login'); 
    //Route::get('/register','Auth\RegisterController@showRegistrationForm');
    //Route::post('/register','Auth\RegisterController@register');
    Route::get('/forgotpassword','Auth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/sendresetlinkemail', 'Auth\ResetPasswordController@showResetForm');
    Route::post('/sendresetlinkemail', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.reset');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/authchecking','Auth\LoginController@showChecking');
    Route::post('/logout','Auth\LoginController@logout');
    Route::get('/password/resetsuccess', 'Auth\ResetPasswordController@resetSuccess');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','auth.data', 'can:edit_profile']], function () {
    
    Route::get('/resetpw','Auth\ResetPasswordLoginController@showResetForm');
    Route::post('/resetpw','Auth\ResetPasswordLoginController@reset');

    //Route::get('/userprofile','User\UserController@showWeb');
    // Route::post('/user/account','User\UserController@updateaccount');
    // Route::post('/user/profile','User\UserController@updateprofile');
});
Route::group(['middleware' => ['auth', 'auth.checked', 'auth.expiration_checked','auth.data']], function () {
    Route::get('/guide','User\GuideController@getGuide');
    Route::post('/guide','User\GuideController@setGuide');

    Route::get('/holter/add','Holter\AddHolterController@showWeb');
    Route::get('/holter/manager','Holter\ManagerController@showWeb');

    Route::get('/ecg/manager','File\ManagerController@showWeb');
    Route::post('/ecg/manager/delete','File\ManagerController@del');
    Route::post('/ecg/manager/rename','File\ManagerController@setFilename');
    Route::get('/api/v0/ecgfiles','File\ManagerController@getECGs');
    Route::get('/api/v0/ecgs/data','File\ManagerController@data');
    Route::get('/api/v0/ecgs/status','ECG\StatusController@sse');

    Route::get('/monitor/manager','Monitor\ManagerController@showWeb');
    Route::get('/monitor/{holter}','Monitor\MonitorController@showWeb');
    Route::get('/monitor/{holter}/backplay','Monitor\HrsController@showWeb');

});
Route::group(['middleware' => ['auth', 'auth.checked', 'auth.expiration_checked']], function () {
    Route::get('/api/v0/ecgs/status','ECG\StatusController@sse');

});

Route::group(['middleware' => []], function () {
    Route::get('/monitor/{holter}/streams/data','Monitor\StreamController@data');
    Route::post('/monitor/holters/data','Monitor\HolterController@data');
    Route::get('/monitor/{holter}/hrs/data','Monitor\HrsController@data');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','auth.data','can:upload_ekg']], function () {
    Route::get('/ecg/upload','File\UploadController@showWeb');
    Route::post('/ecg/upload','File\UploadController@save');
    Route::get('/ecgs/{id}/report/download','API\ECGController@openReportDownload');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:all_records']], function () {
    Route::get('/data/ecgs','Data\ECGController@index');
    Route::get('/data/ecgs/data','Data\ECGController@allData');
   
});
/*
Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','auth.data:4,5,8,10']], function () {
    //Route::get('/data/users','Data\ManagerController@showWeb');
    Route::get('/data/users/{id}/ecgs','Data\ECGController@showWeb');
    Route::get('/data/users/{id}/ecgs/data','Data\ECGController@data');
    Route::post('/data/users/{id}/ecgs/delete','Data\ECGController@delete');
    Route::get('/data/analysis/z2b/{id}/report','Analysis\Z2BController@getReport');
    Route::get('/data/analysis/z2b/{id}/report/download', 'Analysis\Z2BController@downloadReport');
});*/
Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:ecg_data']], function () {
    Route::get('/data/users','Data\PocController@showWeb');
    Route::get('/data/users/data','Data\PocController@getlist');
    Route::get('/data/users/{id}/ecgs','Data\ECGController@showWeb');
    Route::get('/data/users/{id}/ecgs/data','Data\ECGController@data');
    Route::post('/data/users/{id}/ecgs/delete','Data\ECGController@delete');
    Route::get('/data/analysis/z2b/{id}/report','Analysis\Z2BController@getReport');
    Route::get('/data/analysis/z2b/{id}/report/qtc','Analysis\Z2BController@getQTCReport');
    Route::get('/data/analysis/z2b/{id}/report/download', 'Analysis\Z2BController@downloadReport');
   
});
Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','auth.data:4,5,8']], function () {
    Route::get('/data/users/{user}/ecg/upload','Data\UploadController@showWeb');
    Route::post('/data/users/{user}/ecg/upload','Data\UploadController@save');


    Route::get('/data/users/{user}/ecg/export','Data\ExportController@showWeb');
    Route::get('/data/users/{user}/ecg/export/data','Data\ExportController@getECGs');
    Route::get('/data/users/{user}/ecg/export/csv','Data\ExportController@export');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked']], function () {
    Route::get('/analysis/z2b/{id}/report','Analysis\Z2BController@getReport');
    Route::get('/analysis/z2b/{id}/report/exists','Analysis\Z2BController@reportExists');
    Route::get('/analysis/z2b/{id}/report/download', 'Analysis\Z2BController@downloadReport');
    Route::post('/ecg/reports/download', 'Analysis\ReportController@download');

    Route::post('/data/users/{user}/reports/download', 'Data\ReportController@download');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:ecg_assigned']], function () {
    Route::get('/ecg/assigned','ECG\AssignedController@index');
    Route::get('/data/assigned_users/{user}/ecgs','ECG\AssignedController@ecgData');
    Route::get('/data/doctors/{user}/ecgs','ECG\AssignedController@doctorEcgData');
    Route::post('/data/doctors/{user}/ecgs/assigned','ECG\AssignedController@assigned');
    Route::post('/data/doctors/ecgs/unassigned','ECG\AssignedController@unassigned');
});
Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:ecg_analysis']], function () {
    Route::post('/ecg/manager/update_info','File\ManagerController@setInfo');
    Route::post('/ecg/rerun','File\UploadController@rerun');

    Route::get('/ecg/export','File\ExportController@showWeb');
    Route::get('/ecg/export/data','File\ExportController@getECGs');
    Route::get('/ecg/export/csv','File\ExportController@export');

    Route::post('/ecg/transfer','File\TransferController@run');

    Route::get('/analysis/ecgs/{id}/checked','Analysis\ECGController@checked');
    Route::get('/analysis/ecgs/{id}/unchecked','Analysis\ECGController@unchecked');


    Route::middleware('can:ecg_analysis_switch')->get('/analysis/ecg/mode','Analysis\ECGController@mode');

    Route::get('/analysis/ecg/{id}','Analysis\ECGController@showWeb');
    Route::get('/analysis/ecg/exportRR/{id}','Analysis\ECGController@exportRR');
    Route::post('/analysis/ecg/exportRR','Analysis\ECGController@exportRR');
    Route::get('/analysis/ecg/info/{id}','Analysis\ECGController@getInfo');
    Route::get('/analysis/qrs/{id}','Analysis\ECGController@runQRS');
    Route::get('/analysis/rr_align/{id}','Analysis\ECGController@runRRAlign');
    
    Route::get('/analysis/ecg/batchpeak/{id}','Analysis\BatchPeakController@showWeb');
    Route::post('/analysis/ecg/batchpeak/setpeaks/{id}','Analysis\BatchPeakController@setPeaks');

    Route::get('/analysis/hrv/{id}','Analysis\HRVController@showWeb');
    Route::post('/analysis/hrv/export','Analysis\HRVController@export');

    Route::get('/analysis/ecg/report/{id}','Analysis\ReportController@showWeb');
    Route::post('/analysis/ecg/report/{id}','Analysis\ReportController@reportExport');
    Route::get('/analysis/ecg/summary/{id}','Analysis\ReportController@getSummary');

    Route::post('/analysis/ecg/report/email','Analysis\Z2BController@sendReport');
    Route::get('/analysis/ecg/{id}/peak/check','Analysis\ECGController@checkPeakN');

    Route::get('/analysis/sleep/{id}','Analysis\SleepController@showWeb');
    Route::get('/analysis/sleep/{id}/run','Analysis\SleepController@runSleep');
    Route::get('/analysis/sleep/{id}/result/{type}','Analysis\SleepController@getResult');
    Route::get('/analysis/sleep/{id}/tag','Analysis\SleepController@getTag');
    Route::get('/analysis/sleep/{id}/addTag','Analysis\SleepController@addTag');
    Route::get('/analysis/sleep/{id}/delTag','Analysis\SleepController@delTag');
    Route::get('/analysis/sleep/{id}/report','Analysis\SleepController@getReport');
    Route::post('/analysis/sleep/{id}/report','Analysis\SleepController@createReport');
    Route::get('/analysis/sleep/{id}/addPeak','Analysis\SleepController@addPeak');
    Route::get('/analysis/sleep/{id}/delPeak','Analysis\SleepController@delPeak');
    
    Route::get('/analysis/z2b/{id}/report','Analysis\Z2BController@getReport');
    Route::get('/analysis/z2b/{id}/report/qtc','Analysis\Z2BController@getQTCReport');


    Route::get('/api/v0/analysis/hrv/{id}/{interval}','Analysis\HRVController@runAnalysis');
    Route::get('/api/v0/analysis/ecg/{id}','Analysis\ECGController@getECGContent');
    Route::get('/api/v0/analysis/rr/{id}','Analysis\ECGController@getRRContent');
    Route::get('/api/v0/analysis/event/{id}','Analysis\ECGController@getEventContent');
    Route::get('/api/v0/analysis/rr/addPeak/{id}', 'Analysis\ECGController@addPeakRecord');
    Route::get('/api/v0/analysis/rr/delPeak/{id}', 'Analysis\ECGController@delPeakRecord');
    Route::post('/api/v0/analysis/rr/delPeaks/{id}', 'Analysis\ECGController@delPeaksRecord');

    Route::middleware('can:ecg_analysis_confirm')->post('/api/v0/analysis/reports/{id}/confirmed','Analysis\ECGController@confirmed');
    Route::middleware('can:ecg_analysis_confirm')->get('/ecg/confirmed/check','Analysis\ECGController@allConfirmedCheck');

    Route::get('/survey/manager','Survey\ManagerController@showWeb');
});
Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:user_manager']], function () {/*
    Route::get('/data/users/manager','Data\UserController@showWeb');
    Route::get('/data/users/manager/list','Data\UserController@getList');
    Route::get('/data/users/manager/edit/{id}','Data\UserController@edit');
    Route::get('/data/users/manager/del/{id}','Data\UserController@delete');
    Route::post('/data/users/manager/del','Data\UserController@delete');
    Route::post('/data/users/manager/edit','Data\UserController@save');

    Route::get('/data/users/manager/create','Data\UserController@create');
    Route::post('/data/users/manager/create','Data\UserController@save');*/
    
    //Route::get('/data/users','Data\PocController@showWeb');
    Route::get('/data/users/manager','Data\PocController@showWeb');
    Route::get('/data/users/manager/list','Data\PocController@getList');
    Route::get('/data/users/manager/edit/{id}','Data\UserController@edit');
    Route::get('/data/users/manager/del/{id}','Data\UserController@delete');
    Route::post('/data/users/manager/del','Data\UserController@delete');
    Route::post('/data/users/manager/edit','Data\UserController@save');

    Route::get('/data/users/manager/create','Data\UserController@create');
    Route::post('/data/users/manager/create','Data\UserController@save');
    
    //Route::get('/data/pocs','Data\PocController@index');
    Route::get('/data/pocs/manager','Data\PocController@showWeb');
    Route::get('/data/pocs/manager/list','Data\PocController@getList');
    Route::get('/data/pocs/manager/edit/{id}','Data\PocController@edit');
    Route::get('/data/pocs/manager/del/{id}','Data\PocController@delete');
    Route::post('/data/pocs/manager/del','Data\PocController@delete');
    Route::post('/data/pocs/manager/edit','Data\PocController@save');

    Route::get('/data/pocs/manager/create','Data\PocController@create');
    Route::post('/data/pocs/manager/create','Data\PocController@save');

    Route::get('/data/users/{user}/users','Data\UserController@showWeb');
    Route::get('/data/users/{user}/users/list','Data\UserController@getList');
    Route::get('/data/users/{user}/users/edit/{id}','Data\UserController@edit');
    Route::get('/data/users/{user}/users/del/{id}','Data\UserController@destroy');
    Route::post('/data/users/{user}/users/del','Data\UserController@destroy');
    Route::post('/data/users/{user}/users/edit','Data\UserController@save');

    Route::get('/data/users/{user}/users/create','Data\UserController@create');
    Route::post('/data/users/{user}/users/create','Data\UserController@save');

    Route::get('/data/users/{user}/users/{id}/ecgs','Data\ECGController@showWeb');
    Route::get('/data/users/{user}/users/{id}/ecgs/data','Data\ECGController@data');
    Route::post('/data/users/{user}/users/{id}/ecgs/delete','Data\ECGController@data');

    Route::get('/data/users/{user}/users/{id}/ecg/upload','Data\UploadController@showWeb');
    Route::post('/data/users/{user}/users/{id}/ecg/upload','Data\UploadController@save');

    Route::get('/data/users/{user}/users/{id}/ecg/export','Data\ExportController@showWeb');
    Route::get('/data/users/{user}/users/{id}/ecg/export/data','Data\ExportController@getECGs');
    Route::get('/data/users/{user}/users/{id}/ecg/export/csv','Data\ExportController@export');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:service_read']], function () {
    Route::get('/services','Service\ServiceController@index');
    Route::get('/services/data','Service\ServiceController@data');
    Route::get('/services/{id}/summary','Service\ServiceController@summary');
    Route::post('/services/{id}/summary/export','Service\ServiceController@summaryExport');

    Route::post('/services/{id}/summary/comment','Service\ServiceController@updateComment');

    Route::get('/data/pocs/{poc}/users/{id}/services','Service\UserServiceController@index');

    Route::get('/data/users/manager/{id}/services','Service\UserServiceController@index');
    Route::get('/data/pocs/manager/{id}/services','Service\UserServiceController@index');
    Route::get('/data/services/{id}/summary','Service\UserServiceController@summary');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:service_export']], function () {
    Route::get('/services/{id}/export','Service\ServiceController@exportList');
    Route::get('/services/{id}/export/data','Service\ServiceController@exportData');
    Route::post('/services/{id}/export','Service\ServiceController@export');

    Route::get('/data/services/{id}/export','Service\UserServiceController@exportList');
    Route::get('/data/services/{id}/data','Service\UserServiceController@exportData');
    Route::post('/data/services/{id}/export','Service\UserServiceController@export');

     Route::get('/data/services/{poc}/users/{id}/export','Service\UserServiceController@exportList');
    Route::get('/data/services/{poc}/users/{id}/data','Service\UserServiceController@exportData');
    Route::post('/data/services/{poc}/users/{id}/export','Service\UserServiceController@export');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:service_manager,service_read']], function () {
    Route::get('/services/create','Service\ServiceController@create');
    Route::get('/services/{service}/edit','Service\ServiceController@edit');
    Route::post('/services','Service\ServiceController@save');
    Route::post('/services/del','Service\ServiceController@delete');
    Route::post('/services/{service}/del','Service\ServiceController@delete');

    Route::get('/data/services/create','Service\UserServiceController@create');
    Route::get('/data/services/{service}/edit','Service\UserServiceController@edit');
    Route::post('/data/{id}/services/','Service\UserServiceController@save');
    Route::post('/data/services/del','Service\UserServiceController@delete');
    Route::post('/data/services/{service}/del','Service\UserServiceController@delete');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:notifiable_module']], function () {
    Route::get('/notifiable_modules','NotifiableModule\ManagerController@index');
    Route::get('/notifiable_modules/data','NotifiableModule\ManagerController@data');
    Route::post('/notifiable_modules/{id}/notified','NotifiableModule\ManagerController@notified');
    Route::get('/notifiable_modules/{id}/notified','NotifiableModule\ManagerController@notified');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:poc_manager']], function () {
    
    Route::get('/data/pocs/{poc}/users','Data\UserController@showWeb');
    Route::get('/data/pocs/{poc}/users/list','Data\UserController@getList');
    Route::get('/data/pocs/{poc}/users/edit/{id}','Data\UserController@edit');
    Route::get('/data/pocs/{poc}/users/del/{id}','Data\UserController@destroy');
    Route::post('/data/pocs/{poc}/users/del','Data\UserController@destroy');
    Route::post('/data/pocs/{poc}/users/edit','Data\UserController@save');

    Route::get('/data/pocs/{poc}/users/create','Data\UserController@create');
    Route::post('/data/pocs/{poc}/users/create','Data\UserController@save');

    Route::get('/data/pocs/{poc}/users/{id}/ecgs','Data\ECGController@showWeb');
    Route::get('/data/pocs/{poc}/users/{id}/ecgs/data','Data\ECGController@data');
    Route::post('/data/pocs/{poc}/users/{id}/ecgs/delete','Data\ECGController@data');
});

Route::group(['middleware' => ['auth','auth.checked', 'auth.expiration_checked','can:report_summary']], function () {
    Route::get('/data/users/{user}/reports/summary', 'Data\ReportController@summary');
    Route::post('/data/users/{user}/reports/summary/export', 'Data\ReportController@summaryExport');
});


//Administrator
Route::group(['middleware' => ['web','guest:admin']], function () {
    Route::get('/admin/login','Admin\LoginController@showLoginForm');
    Route::post('/admin/login','Admin\LoginController@login');
});

Route::group(['middleware' => ['web','auth:admin','admin_can:0']], function () {
    Route::get('/admin','Admin\MemberController@showWeb')->name('admin.home');

    Route::get('/admin/users','Admin\UserController@index');
    Route::get('/admin/users/data','Admin\UserController@data');
    Route::get('/admin/users/create','Admin\UserController@create');
    Route::get('/admin/users/{id}','Admin\UserController@show');
    Route::get('/admin/users/{id}/edit','Admin\UserController@edit');
    Route::post('/admin/users','Admin\UserController@save');
    Route::post('/admin/users/delete','Admin\UserController@delete');

    Route::get('/admin/member','Admin\MemberController@showWeb');
    Route::get('/admin/member/list','Admin\MemberController@getUserList');
    Route::post('/admin/member/checked','Admin\MemberController@userChecked');
    Route::get('/admin/member/edit/{id}','Admin\MemberController@userEdit');
    Route::get('/admin/member/del/{id}','Admin\MemberController@userDelete');
    Route::post('/admin/member/del','Admin\MemberController@userDelete');
    Route::post('/admin/member/edit','Admin\MemberController@userSave');

    Route::get('/admin/member/register','Admin\RegisterController@showWeb');
    Route::post('/admin/member/register','Admin\RegisterController@register');

    Route::get('/admin/holter','Admin\HolterController@showWeb');
    Route::get('/admin/holter/list','Admin\HolterController@getHolterList');
    Route::get('/admin/holter/list/{mode}','Admin\HolterController@getHolterList');

    Route::get('/admin/holter/create','Admin\HolterController@showCreateWeb');
    Route::post('/admin/holter/create','Admin\HolterController@addHolter');
    Route::get('/admin/holter/edit/{id}','Admin\HolterController@showEditWeb');
    Route::post('/admin/holter/del','Admin\HolterController@delHolters');

    Route::get('/admin/department','Admin\DepartmentController@index');
    Route::get('/admin/department/list','Admin\DepartmentController@getList');
    Route::get('/admin/department/create','Admin\DepartmentController@showForm');
    Route::post('/admin/department','Admin\DepartmentController@save');
    Route::get('/admin/department/edit/{id}','Admin\DepartmentController@showForm');
    Route::post('/admin/department/del','Admin\DepartmentController@delete');

    Route::get('/admin/priority','Admin\PriorityController@index');
    Route::get('/admin/priority/list','Admin\PriorityController@getList');
    Route::get('/admin/priority/create','Admin\PriorityController@showForm');
    Route::post('/admin/priority','Admin\PriorityController@save');
    Route::get('/admin/priority/edit/{id}','Admin\PriorityController@showForm');
    Route::post('/admin/priority/del','Admin\PriorityController@delete');

    Route::get('/admin/apks','Admin\ApkController@index');
    Route::get('/admin/apks/list','Admin\ApkController@getList');
    Route::get('/admin/apks/create','Admin\ApkController@showForm');
    Route::post('/admin/apks','Admin\ApkController@save');
    Route::get('/admin/apks/edit/{id}','Admin\ApkController@showForm');
    Route::post('/admin/apks/del','Admin\ApkController@delete');

    Route::get('/admin/programs','Admin\ProgramController@index');
    Route::post('/admin/programs','Admin\ProgramController@save');
});

Route::group(['middleware' => ['web','auth:admin','admin_can:0,10,11']], function () {
    Route::post('/admin/logout','Admin\LoginController@logout');

    Route::get('/admin/files','Admin\FileController@index');
    Route::get('/admin/files/downloads','Admin\FileController@downloads');
    Route::post('/admin/files/downloads','Admin\FileController@downloads');
});