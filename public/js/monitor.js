var smallGridSize = 4;//5.12;
var singleLineYOffsetUV = 3500; // The offset of the y-axis of a single ECG line (uV)
var viewableWidth;
var sample_rate = 128;
var sampleMs = 2000 / sample_rate;
var xScaleRatio = sampleMs / 40 * smallGridSize;
var yScaleRatio = 1 / 100 * smallGridSize;
var svgHeight = singleLineYOffsetUV * yScaleRatio;
var yOffset = 4 * smallGridSize;
var prevLpEcgPoint = [0, 0, 0];
var prevLpEcgOutput = [0, 0, 0];
var prevHpEcgPoint = [0, 0, 0];
var prevHpEcgOutput = [0, 0, 0];
var prevLpPpgPoint = [0, 0];
var prevLpPpgOutput = [0, 0];
var prevHpPpgPoint = [0, 0];
var prevHpPpgOutput = [0, 0];

function init_chart(container_id, data, exData){
	var chartData = data;
	var chart = $(container_id);
	var container = chart.find('.chartbox');
  var margin = {top: 5, right: 0, bottom: 0, left: 0};
  var padding = {top: 0, right: 0, bottom: 0, left: 0};
	var width = container.width() - margin.left - margin.right;
	var chartYScaleRatio = parseFloat(chart.attr('data-y-scale-ratio'));
	var chartSvgHeight = typeof chart.attr('data-svg-height')!== 'undefined' && chart.attr('data-svg-height') !== false && chart.attr('data-svg-height')!='' ? parseFloat(chart.attr('data-svg-height')) : svgHeight;
	
	var height = chartSvgHeight;
  var chartIndex = 9;

	heightScale = height * chartYScaleRatio;

	svg_widh = width * data.length / chartData.length;
	var chart_svg = d3.select(container[0])
   			.select('svg.chart_svg')
   			.attr("width", svg_widh)
				.attr("height", height);
	var updatebar = chart_svg.select("g.updatebar");
	updatebar.select('rect.current').attr("width", 2 + width/10).attr("height", height).attr("transform", "translate("+(margin.left + ((chartIndex) * width/10) )+", 0)");
	updatebar.select('rect.circle').attr("width", 2 + width/10).attr("height", height).attr("transform", "translate("+(margin.left+ (-1 * (width)/10))+", 0)");
	
}
function draw_chart(container_id, data, exData){
	var chartData = data;
	var filterData = chartData ;
	var chart = $(container_id);
	var container = chart.find('.chartbox');
  var margin = {top: 5, right: 0, bottom: 0, left: 0};
  var padding = {top: 0, right: 0, bottom: 0, left: 0};
	var width = container.width() - margin.left - margin.right;
	var chartYScaleRatio = parseFloat(chart.attr('data-y-scale-ratio'));
	var chartSvgHeight = typeof chart.attr('data-svg-height')!== 'undefined' && chart.attr('data-svg-height') !== false && chart.attr('data-svg-height')!='' ? parseFloat(chart.attr('data-svg-height')) : svgHeight;
	var height = chartSvgHeight;

  var chartIndex = exData.chartIndex % 10;
  var lastChartIndex = exData.lastChartIndex
  var nextChartIndex = chartIndex + 1;

  var waveDataIndex = exData.waveDataIndex;
  var updateIndex = exData.updateIndex;

	heightScale = height * chartYScaleRatio;

	svg_widh = width * data.length / chartData.length;
	var chart_svg = d3.select(container[0])
   			.select('svg.chart_svg')
   			.attr("width", svg_widh)
				.attr("height", height);
	
	var svg = chart_svg.select('svg.line_'+updateIndex)
					.attr("width", svg_widh)
					.attr("height", height)
   				.attr("x", ((-10+chartIndex)*width/10))
   				.attr("viewBox",""+(-1*(10-chartIndex)*width/10)+",0,"+svg_widh+","+height);

 	var oppo_updateIndex = (updateIndex +1) % 2;

 	var oppo_svg = chart_svg.select('svg.line_'+oppo_updateIndex)
					.attr("width", svg_widh)
					.attr("height", height)
					.attr("x", ((chartIndex)*width/10))
					.attr("viewBox",""+((chartIndex)*width/10)+",0,"+svg_widh+","+height);

	var g = svg.select('g.pathbox').attr("transform", "translate( 0, 0 )");
	if(exData.animation && (lastChartIndex == (chartIndex-1) || (chartIndex==0 && lastChartIndex==9))){
		var updatebar = chart_svg.select("g.updatebar").transition();
		if(chartIndex != 0){
			updatebar.select('rect.current').attr("width", 2 + width/10).attr("height", height).duration(1000).ease(d3.easeLinear).attr("transform", "translate("+(margin.left + (chartIndex * width/10) )+", 0)");
			updatebar.select('rect.circle').attr("width", 2 + width/10).attr("height", height).duration(0).attr("transform", "translate("+(margin.left+ (-1 * (width)/10))+", 0)");
		}
		if(chartIndex == 0){
			updatebar.select('rect.current').attr("width", 2 + width/10).attr("height", height).duration(1000).ease(d3.easeLinear).attr("transform", "translate("+(margin.left + ((10) * width/10) )+", 0)");
			updatebar.select('rect.circle').attr("width", 2 + width/10).attr("height", height).duration(1000).ease(d3.easeLinear).attr("transform", "translate("+(margin.left+ 0)+", 0)");
		}
		if(chartIndex == 1){
			updatebar.select('rect.current').attr("width", 2 + width/10).attr("height", height).duration(0).delay(1000).ease(d3.easeLinear).attr("transform", "translate("+(margin.left+( width/10) )+", 0)");
			updatebar.select('rect.circle').attr("width", 2 + width/10).attr("height", height).duration(1000).ease(d3.easeLinear).attr("transform", "translate("+(margin.left+ (width)/10)+", 0)");
		}
		if(chartIndex == 2){
			updatebar.select('rect.current').attr("width", 2 + width/10).attr("height", height).duration(1000).ease(d3.easeLinear).attr("transform", "translate("+(margin.left+( chartIndex * width/10))+", 0)");
		}
	}else{
		var updatebar = chart_svg.select("g.updatebar");
		updatebar.select('rect.current').attr("width", 2 + width/10).attr("height", height).attr("transform", "translate("+(margin.left + ((chartIndex) * width/10) )+", 0)");
		updatebar.select('rect.circle').attr("width", 2 + width/10).attr("height", height).attr("transform", "translate("+(margin.left+ (-1 * (width)/10))+", 0)");
	}
	if(waveDataIndex < 3){
		var meanY = 0;
		var minY = -1 * 15 * smallGridSize;
		var maxY =  1 * 15 * smallGridSize;
	  var axisYHeight = maxY;
	  var chartYOffset = yOffset;
		var yDomainMin = (meanY + axisYHeight);
		var yDomainMax = (meanY - axisYHeight + chartYOffset);

		var axisYPadding = Math.abs(yDomainMax - yDomainMin)/20;
	}else{
			var meanY = d3.median(filterData, function (d) { return d/ 150; });
			var minY = d3.min(filterData, function (d) { return d/ 150; });
			var maxY = d3.max(filterData, function (d) { return d/ 150; });
		// var axisYHeight = Math.max(Math.abs(maxY - meanY), Math.abs(minY - meanY)) + 1;
	 //  var yDomainMin = (meanY + axisYHeight) / chartYScaleRatio;
		// var yDomainMax = (meanY - axisYHeight) / chartYScaleRatio;
		// if(yDomainMin == yDomainMax && yDomainMin==0){
			var meanY = 0;
			var axisYHeight = height/2;
			yDomainMin = (meanY - axisYHeight) / chartYScaleRatio;
			yDomainMax = (meanY + axisYHeight) / chartYScaleRatio;
		// } 
		var axisYPadding = 0;
  }

  var minX = 0;
  var maxX = filterData.length - 1;

	var x = d3.scaleLinear().range([0,  width]).domain([minX, maxX]);

	var yRange = [ 0, height ];

	var y = d3.scaleLinear().range(yRange).domain([yDomainMin + axisYPadding, yDomainMax - axisYPadding]);
	
	var interpolate = chart.data('line-interpolate');
	
	var line = d3.line().x(function (d,i) { return x(i); }).y(function (d) {
		var dataY;
		if(waveDataIndex < 3){
			dataY = d * yScaleRatio;// filterWaveData(d, waveDataIndex) * yScaleRatio;
		}else{
			dataY = d / 150; // (filterWaveData(d, waveDataIndex) / 150);
		}
		var pointY = y(dataY); 
		return $.isNumeric(pointY) ? pointY : 0; 
	}).curve(getLineInterpolate(interpolate));


	var path = g.select('path');
	if(!path.empty()){
		var pathboxg = svg.select('g.pathbox');
		pathboxg.select('.line').attr("d", line(filterData))
			.attr("transform", "translate("+ (margin.left )+", 0)");
	}else{
		g.append("path")
			.attr("d", line(filterData))
			.attr("transform", "translate(" + (margin.left )+", 0)")
			.attr("class", "line");
	}
}
function getLineInterpolate(type){
	switch(type){
		case 'linear': return d3.curveLinear;
		case 'step':return d3.curveStepAfter;
		default : return d3.curveLinear;
	}
}
function filterWaveData(raw, date_type){
	switch(date_type){
		default:return raw;
		case 0:case '0':return ecgFilter(raw, 0);
		case 1:case '1':return ecgFilter(raw, 1);
		case 2:case '2':return ecgFilter(raw, 2);
		case 3:case '3':return ppgFilter(raw, 0);
		case 4:case '4':return ppgFilter(raw, 1);
	}
}
function drawGrid(container_id) {
	// Get the size of the container
	// The grid will have the same size as the container
	var chart = $(container_id);
	var container = chart.find('.chartbox');
	var height = container.height();
	var width = container.width();

	// Add a new SVG group for the grid
	var graph = d3.select(container[0]).select('svg.chart_svg').select('g.grid_box');

	// The x-axis grid lines
	var xAxisLines = graph.selectAll("line.x-grid").data(d3.range(0, width, smallGridSize));
	xAxisLines.attr("x1", function(d) { return d ; })
		.attr("x2", function(d) { return d ; })
		.attr("y1", 0)
		.attr("y2", height)
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; });

	xAxisLines.enter()
		.append("line")
		.classed("x-grid", true)
		.attr("x1", function(d) { return d ; })
		.attr("x2", function(d) { return d ; })
		.attr("y1", 0)
		.attr("y2", height)
		.attr("stroke", "gray")
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; })
		.attr("stroke-opacity", 0.2)
		.attr("shape-rendering", "crispEdges");

	xAxisLines.exit().remove();

	// The y-axis grid lines
	var yAxisLines = graph.selectAll("line.y-grid").data(d3.range(0, height, smallGridSize));
	yAxisLines.attr("x1", 0)
		.attr("x2", width)
		.attr("y1", function(d) { return d ; })
		.attr("y2", function(d) { return d ; })
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; });

	yAxisLines.enter()
		.append("line")
		.classed("y-grid", true)
		.attr("x1", 0)
		.attr("x2", width)
		.attr("y1", function(d) { return d ; })
		.attr("y2", function(d) { return d ; })
		.attr("stroke", "gray")
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; })
		.attr("stroke-opacity", 0.15)
		.attr("shape-rendering", "crispEdges");

	yAxisLines.exit().remove();
}
// raw is each point of the raw signal value
// type means the specific signal
// Ex: ecgL1 is type 0, ecgL2 is type 1
//     ppgir is type 0, ppgr is type 1
function ecgFilter(raw, type) {
    // high pass filter
    var switchHpTemp = raw;
    raw = raw - this.prevHpEcgPoint[type] + 0.996 * this.prevHpEcgOutput[type];
    this.prevHpEcgOutput[type] = raw;
    this.prevHpEcgPoint[type] = switchHpTemp;
    
    // low pass filter
    var switchLpTemp = raw;
    raw = 
      0.319988923452122 * raw + 0.319988923452122 * this.prevLpEcgPoint[type] +
      0.360022153095757 * this.prevLpEcgOutput[type];
    this.prevLpEcgOutput[type] = raw;
    this.prevLpEcgPoint[type] = switchLpTemp;

    return Math.floor(raw);
}

function ppgFilter(raw, type) {
    // high pass filter
    var switchHpTemp = raw;
    raw = raw - this.prevHpPpgPoint[type] + 0.99 * this.prevHpPpgOutput[type];
    this.prevHpPpgOutput[type] = raw;
    this.prevHpPpgPoint[type] = switchHpTemp;
    
    // low pass filter
    var switchLpTemp = raw;
    raw =
      0.0468264154904264 * raw +
      0.0468264154904264 * this.prevLpPpgPoint[type] +
      0.906347169019147 * this.prevLpPpgOutput[type];
    this.prevLpPpgOutput[type] = raw;
    this.prevLpPpgPoint[type] = switchLpTemp;

    return Math.floor(raw);
  }