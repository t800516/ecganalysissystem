function get_year_options(selected){
	var year_options=''
	var current_year=new Date().getFullYear();
	for(var year=parseInt(current_year)-18;year>=1900;year--){
		var selected_str='';
		if(selected==year)selected_str='selected';
		year_options+='<option value="'+year+'" '+selected_str+'>'+year+'</option>';
	}
	return year_options;
}
function get_month_options(selected){
	var month_options='';
	for(var month=1;month<=12;month++){
		var selected_str='';
		if(selected==month)selected_str='selected';
		month_options+='<option value="'+fillzero(month,2)+'" '+selected_str+'>'+fillzero(month,2)+'</option>';
	}
	return month_options;
}
function get_day_options(month,selected){
	var day_options='';
	var maxday=31;
	console.log(month);
	switch(month){default:maxday=31;break;
		case '04':case '06':case '09':case '11':maxday=30;break;
		case '02':maxday=29;break;
	}
	for(var day=1;day<=maxday;day++){
		var selected_str='';
		if(selected==day)selected_str='selected';
		day_options+='<option value="'+fillzero(day,2)+'" '+selected_str+'>'+fillzero(day,2)+'</option>';
	}
	return day_options;
}
function fillzero(num, size){
    	var s = num+"";
    	while (s.length < size) s = "0" + s;
    	return s;
	}
