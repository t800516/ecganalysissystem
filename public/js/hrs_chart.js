var smallGridSize = 4;//5.12;
var singleLineYOffsetUV = 3500; // The offset of the y-axis of a single ECG line (uV)
var graphHeightUV = 10000; // The height of the ECG graph (uV)
var viewableWidth;
var sample_rate = 256;
var sampleMs = 2000 / sample_rate;
var xScaleRatio = sampleMs / 40 * smallGridSize;
var yScaleRatio = 1 / 100 * smallGridSize;
var yOffset = 4 * smallGridSize;
var scrollbarWidth = getBrowserScrollbarWidth();

function draw_chart(container_id, data, range, exData){
	var chartData = data.slice(range.start, range.end + 1);
	var filterValue = getFilterValue(container_id, chartData.length);
	var filterData = filterValue == 1 ? chartData : chartData.filter(function(d, i){ return i % filterValue == 0;} );
	var chart = $(container_id);
	var container = chart.find('.chartbox');
    var margin = {top: 5, right: scrollbarWidth, bottom: 0, left: 0};
    var padding = {top: 0, right: 0, bottom: 0, left: 0};
	var width = container.width() - margin.left - margin.right;
    var height = container.height() - margin.top - margin.bottom;
    var yScaleRatio = chart.attr('data-y-scale-ratio');
    var lineColor = chart.attr('data-line-color');
	yScaleRatio = yScaleRatio ? parseFloat(yScaleRatio) : 1 ;
	heightScale = height * yScaleRatio;
	var rangeType = exData.rangeType;
	var svg = d3.select(container[0])
   				.select('svg')
   				.attr("width", width * data.length / chartData.length)
				.attr("height", heightScale);

	var positionX = exData.positionX;

    var g = svg.select("g.pathbox").attr("transform", "translate( 0, 0)");

	var meanY = exData.meanY ? exData.meanY : d3.mean(filterData, function (d) { return d[1]; });
	var minY = exData.minY ? exData.minY : d3.min(filterData, function (d) { return d[1]; });
	var maxY = exData.maxY ? exData.maxY : d3.max(filterData, function (d) { return d[1]; });
	
    var minX = d3.min(filterData, function (d) { return d[0]; });
	var maxX = d3.max(filterData, function (d) { return d[0]; });
	var dataX = filterData.map(function(d){return d[0];});
	var dataX = filterData.map(function(d){return d[0];}); 
    var axisYHeight = Math.max(Math.abs(maxY - meanY), Math.abs(minY - meanY)) + 1;
    var yOffset = chart.data('y-offset');
    yOffset = yOffset ? parseFloat(eval(yOffset)) * axisYHeight : 0 ; 
	var x = d3.scalePoint().range([0,  width]).domain(dataX);

	var yRange = [ 0,heightScale];

	var yDomainMin = chart.data('y-scale-up-bound') ? 
						parseFloat(chart.data('y-scale-up-bound')) :
						(meanY + axisYHeight);
	var yDomainMax = chart.data('y-scale-low-bound') ? 
						parseFloat(chart.data('y-scale-low-bound')) :
						(meanY - axisYHeight + yOffset);
	var axisYPadding = Math.abs(yDomainMax - yDomainMin)/20;
	var y = d3.scaleLinear().range(yRange).domain([yDomainMin + axisYPadding, yDomainMax - axisYPadding]);
	var interpolate = chart.data('line-interpolate');

	var line = d3.line().x(function (d,i) { 
							return x(d[0]); 
						}).y(function (d) {
							var pointY = y(d[1]); 
							return $.isNumeric(pointY)?pointY:0; 
						}).curve(getLineInterpolate(interpolate));


	var path = g.select('path');
	if(!path.empty()){
		path.attr("d", line(filterData))
			.attr("transform", "translate(" + (margin.left + positionX)+", 0)");
	}else{
		g.append("path")
			.attr("d", line(filterData))
			.attr("transform", "translate(" + (margin.left + positionX)+", 0)")
			.attr("class", "line line-"+lineColor);
	}

	var dontSetScrollLeft = chart.find('.chartbox').data('dont-set-scroll-left');
	if(!dontSetScrollLeft){
		chart.find('.chartbox').data('prevent-scroll-event',true);
		chart.find('.chartbox').scrollLeft(positionX);
		chart.find('.chartbox').data('last-scroll-left',positionX);
	}
	
	var start_time = minX;
	var end_time = maxX;
	chart.find('.start_at').html(dateAxisXText(minX, rangeType)+' '+timeAxisXText(minX, rangeType));
	chart.find('.end_at').html(dateAxisXText(minX, rangeType)+' '+timeAxisXText(maxX, rangeType));
	var ticks = filterData.length ? x.domain().filter((d, i) => i % Math.round(filterData.length/20) === 0) : [];
	var axisX = d3.axisBottom(x).tickValues(ticks).tickFormat("");

	var axis_container = chart.find('.axisbox');
	var axis_svg = d3.select(axis_container[0]).select('svg').attr("height", height);
	var axisY = d3.axisLeft(y).ticks(5*yScaleRatio);

	var scrollTop = container.data('last-scroll-top');
	scrollTop = scrollTop ? scrollTop : (container.find('svg').height()-container.height())/2 + 3;
	container.scrollTop(scrollTop);

	axis_svg.select("g.axisY")
      	.attr("transform", "translate( " +axis_container.width() + ", "+ (-scrollTop)+" )")
		.call(axisY);

	var time_container = chart.find('.timebar');
	var time_svg = d3.select(time_container[0]).select('svg').attr("width", width).attr("height", 40);
	
	time_svg.select("g.axisX")
      	.attr("transform", "translate( 0, 0 )")
		.call(axisX)
		.selectAll('text')
		.selectAll('tspan')  
		.data(function(d){return [dateAxisXText(d, rangeType), timeAxisXText(d, rangeType)];}) // Returns two vals
		.enter()
		.append('tspan')
		.attr('x', 0)
		.attr('dy', '1em')
		.text(function(d){return d;});

	var gridY = d3.select(container[0])
					.select('svg')
					.select('g.y-grid')
					.attr("transform", "translate(" + (margin.left + positionX)+", 0)")
					.call(axisY.tickSize(-width).tickFormat(""));

	var gridX = d3.select(container[0])
					.select('svg')
					.select('g.x-grid')
					.attr("transform", "translate(" + (margin.left + positionX)+", 0)")
					.call(axisX.tickSize(heightScale).tickFormat(""));
}

function updateAxisYPosition(container_id, y){
	var chart = $(container_id);
	var axis_container = chart.find('.axisbox');
	var axis_svg = d3.select(axis_container[0]).select('svg');
	axis_svg.select("g.axisY").attr("transform", "translate( " +axis_container.width() + ", "+ -y +" )");
}

function getBrowserScrollbarWidth() { 
	    var scroll_div = $('<div style="width:50px;height:50px;visibility: hidden;overflow:scroll;position:absolute;top:-50px;left:-50px;"></div>'); 
	    $('body').append(scroll_div); 
	    var scroll_width = $(scroll_div).prop("clientWidth");
	   	var no_scroll_width = $(scroll_div).css('overflow','hidden').prop("clientWidth");
	   	$(scroll_div).remove(); 
	    return (no_scroll_width-scroll_width); 
	}

function getLineInterpolate(type){
	switch(type){
		case 'linear': return d3.curveLinear;
		case 'step':return d3.curveStepAfter;
		default : return d3.curveLinear;
	}
}

function getFilterValue(container_id, dataLen){
	var sleepIndex = $(container_id).data('sleep-index');
	var filterSetting = [
			[],//0
			[],//1
			[],//2
			["3839/4","7679/6","38399/16","230399/48","460799/64","ALL/128"],//3
			["3839/1","7679/1","38399/1","230399/4","460799/8","ALL/32"],//4
			[],//5
			[],//6
			[],//7
			[],//8
			[] //9
		];
	var filterValue = 1 ;
	for (var i = 0; i < filterSetting[sleepIndex].length; i++) {
		var settings = filterSetting[sleepIndex][i].split('/');
		var condition = settings[0];
		condition = condition == 'ALL' ? dataLen : parseInt(condition,10);
		var value = parseInt(settings[1],10);
		if(dataLen < condition ){
			break;
		}
		filterValue = value;
	}
	return filterValue;
}
function dateAxisXText(time, type){
	var datetime = moment(time*1000);
	return datetime.format("YYYY/MM/DD");
}
function timeAxisXText(time, type){
	var datetime = moment(time*1000);
	return datetime.format("HH:mm:ss");
}