$(function(){
	showCookiePrivacyAlert();
	$("#cookie-privacy-accept").click(function(event){
		Cookies.set('cookie-privacy-accept', true);
		showCookiePrivacyAlert();
	});
	function showCookiePrivacyAlert(){
		if(!Cookies.get('cookie-privacy-accept')){
			$(".cookie-privacy").removeClass("hide");
		}else{
			$(".cookie-privacy").addClass("hide");
		}
	}
})