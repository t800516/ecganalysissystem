$.get(url('/guide'),function(data){
	if(data.guide!=''){
		$("#content").append(data.guide);
		$('#guide_modal .guide_page').eq(0).show().siblings().hide();

		$('#guide_modal').modal('show');
		$(document).on('change','#guide_off',function(event){
			var guide=$(this).prop('checked')?'0':'1';
			$.post(url('/guide'),{_token:window.Laravel.csrfToken,guide:guide},function(data){

			});
		});
		$(document).on('click','#guide_modal .next_btn',function(event){
			$(this).parent().parent().next().show().siblings().hide();
		});
		$(document).on('click','#guide_modal .prev_btn',function(event){
			$(this).parent().parent().prev().show().siblings().hide();
		});
	}
});