var scrollbarWidth = getBrowserScrollbarWidth();
function draw_chart(container_id, data, range, exData){
	var chartData = data.slice(range.start, range.end + 1);
	//var filterData = chartData.length>27499 ? chartData.filter(function(d,i){return i%512==0;}):chartData;
	var filterValue = getFilterValue(container_id, chartData.length);
	var filterData = filterValue == 1 ? chartData : chartData.filter(function(d, i){ return i % filterValue == 0;} );
	/*var meanY = d3.mean(chartData, function (d) { return d; });
	var minY = d3.min(chartData, function (d) { return d; });
	var maxY = d3.max(chartData, function (d) { return d; });
*/
	var chart = $(container_id);
	var container = chart.find('.chartbox');
    var margin = {top: 5, right: scrollbarWidth, bottom: 0, left: 0};
    var padding = {top: 0, right: 0, bottom: 0, left: 0};
	var width = container.width() - margin.left - margin.right;
    var height = container.height() - margin.top - margin.bottom;
    var yScaleRatio = chart.attr('data-y-scale-ratio');
	yScaleRatio = yScaleRatio ? parseFloat(yScaleRatio) : 1 ;
	heightScale = height * yScaleRatio;
	var svg = d3.select(container[0])
   				.select('svg')
   				.attr("width", width * data.length / chartData.length)
				.attr("height", heightScale);

	var positionX = exData.notFromStage ? exData.positionX : range.start * width / chartData.length;

    var g = svg.select("g.pathbox").attr("transform", "translate( 0, 0)");

	var meanY = exData.meanY ? exData.meanY : d3.mean(filterData, function (d) { return d; });
	var minY = exData.minY ? exData.minY : d3.min(filterData, function (d) { return d; });
	var maxY = exData.maxY ? exData.maxY : d3.max(filterData, function (d) { return d; });
	
    var minX = 0;
    var maxX = filterValue == 1 ? range.end - range.start : filterData.length - 1;
    //yScaleRatio = height > (maxY - minY) ? height / ((maxY - minY)*2) : height/((maxY - minY)/2);
    //var yOffset = height/2 + meanY * yScaleRatio ;
    //console.log('yOffset: ' + yOffset);
    var axisYHeight = Math.max(Math.abs(maxY - meanY), Math.abs(minY - meanY)) + 1;
    var yOffset = chart.data('y-offset');
    yOffset = yOffset ? parseFloat(eval(yOffset)) * axisYHeight : 0 ; 
	var x = d3.scaleLinear().range([0,  width]).domain([minX, maxX]);
	var yRange = chart.data('sleep-index')=='0'? [heightScale, 0]:[ 0,heightScale];

	var yDomainMin = chart.data('y-scale-up-bound') ? 
						parseFloat(chart.data('y-scale-up-bound')) :
						(meanY + axisYHeight);
	var yDomainMax = chart.data('y-scale-low-bound') ? 
						parseFloat(chart.data('y-scale-low-bound')) :
						(meanY - axisYHeight + yOffset);
	var axisYPadding = Math.abs(yDomainMax - yDomainMin)/20;
	var y = d3.scaleLinear().range(yRange).domain([yDomainMin + axisYPadding, yDomainMax - axisYPadding]);
	//var line = d3.line().x(function (d,i) { return x(i); }).y(function (d) {var pointY = -d * yScaleRatio + yOffset; return $.isNumeric(pointY)?pointY:0; });
	var interpolate = chart.data('line-interpolate');
	var line = d3.line().x(function (d,i) { return x(i); }).y(function (d) {var pointY = y(d); return $.isNumeric(pointY)?pointY:0; }).curve(getLineInterpolate(interpolate));


	var path = g.select('path');
	if(!path.empty()){
		path.attr("d", line(filterData))
			.attr("transform", "translate(" + (margin.left + positionX)+", 0)");
	}else{
		g.append("path")
			.attr("d", line(filterData))
			.attr("transform", "translate(" + (margin.left + positionX)+", 0)")
			.attr("class", "line");
	}

	var dontSetScrollLeft = chart.find('.chartbox').data('dont-set-scroll-left');
	if(!dontSetScrollLeft){
		chart.find('.chartbox').data('prevent-scroll-event',true);
		chart.find('.chartbox').scrollLeft(positionX);
		chart.find('.chartbox').data('last-scroll-left',positionX);
	}
	var sampleRate = chart.data('sample-rate');
	sampleRate = sampleRate ? parseFloat(eval(sampleRate)): 1 ;
    
	//var maxDataLenSampleRate = exData.maxDataLenSampleRate? exData.maxDataLenSampleRate : 1;
	//exData.maxDataLenSampleRate / sampleRate 
	var end_time_add = data.length/chartData.length == 1 ? exData.maxDataSecond : (range.end)/sampleRate;
	var recorded_at = exData.recorded_at;
	var start_time = moment(recorded_at).add(range.start/sampleRate,'seconds');
	var end_time = moment(recorded_at).add(end_time_add,'seconds');

	chart.find('.start_at').html(start_time.hour()+":"+start_time.minute()+":"+start_time.second());
	chart.find('.end_at').html(end_time.hour()+":"+end_time.minute()+":"+end_time.second());
	var time_container = chart.find('.timebar');
	var time_svg = d3.select(time_container[0]).select('svg').attr("width", width).attr("height", 20);
	var axisX = d3.axisBottom(x).ticks(10).tickFormat(function(d){return timeAxisXText(recorded_at,range.start + d, sampleRate);});

	var axis_container = chart.find('.axisbox');
	var axis_svg = d3.select(axis_container[0]).select('svg').attr("height", height);
	var axisY = d3.axisLeft(y).ticks(5*yScaleRatio);

	if(chart.data('sleep-index')=='0'){
		axisY.tickValues([-1,0,1,2]).tickFormat(function(d){return stageAxisYText(d);});
	}else if(chart.data('sleep-index')=='1'){
		axisY.tickValues([0,1]);
	}else if(chart.data('sleep-index')=='8'){
		axisY.tickValues([-1,0,1,2,3]).tickFormat(function(d){return positionAxisYText(d);});
	}

	var scrollTop = container.data('last-scroll-top');
	scrollTop = scrollTop ? scrollTop : (container.find('svg').height()-container.height())/2 + 3;
	container.scrollTop(scrollTop);

	axis_svg.select("g.axisY")
      	.attr("transform", "translate( " +axis_container.width() + ", "+ (-scrollTop)+" )")
		.call(axisY);
	time_svg.select("g.axisX")
      	.attr("transform", "translate( 0, 0 )")
		.call(axisX);
	if(chart.data('sleep-index')=='1'){
		markPeak(container, exData.peak, range, {x: positionX, y: scrollTop});
	}
	if(chart.data('sleep-index')=='2'){
		markTag(container, exData.tag, range, {x: positionX, y: scrollTop});
	}
	var gridY = d3.select(container[0])
					.select('svg')
					.select('g.y-grid')
					.attr("transform", "translate(" + (margin.left + positionX)+", 0)")
					.call(axisY.tickSize(-width).tickFormat(""));
	var gridX = d3.select(container[0])
					.select('svg')
					.select('g.x-grid')
					.attr("transform", "translate(" + (margin.left + positionX)+", 0)")
					.call(axisX.tickSize(height).tickFormat(""));
}
function updateAxisYPosition(container_id, y){
	var chart = $(container_id);
	var axis_container = chart.find('.axisbox');
	var axis_svg = d3.select(axis_container[0]).select('svg');
	axis_svg.select("g.axisY").attr("transform", "translate( " +axis_container.width() + ", "+ -y +" )");
}
function updatePeakPositionY(container_id, y){
	var chart = $(container_id);
	var container = chart.find('.chartbox');
	var svg = d3.select(container[0]).select('svg');
	svg.select("g.peakMarker").attr("transform", "translate(0 , "+ (y-20) +" )");
}
function updateTagPositionY(container_id, y){
	var chart = $(container_id);
	var container = chart.find('.chartbox');
	var svg = d3.select(container[0]).select('svg');
	svg.select("g.tagMarker").attr("transform", "translate(0 , "+ (y) +" )");
}
function stageAxisYText(d){
	switch(d){
		case 0:
			return 'Wake';
		case 1:
			return 'Light';
		case 2:
			return 'Deep';
		case 3:
			return 'S3';
		case 4:
			return 'S4';
		case 5:case -1:
			return 'REM';
	}
}
function positionAxisYText(d){
	switch(d){
		default:
		case -1:
			return 'U';
		case 0:
			return 'P';
		case 1:
			return 'S';
		case 2:
			return 'L';
		case 3:
			return 'R';
	}
}
function updateSlideWindow(container_id, range, position,callback){

	var chart = $(container_id);
	var container = chart.find('.chartbox');
	var margin = {top: 0, right: scrollbarWidth, bottom: 0, left: 0};
    var width = container.width() - margin.left - margin.right;
    var height = container.height() - margin.top - margin.bottom;
	d3.select(container_id).select('.chartbox').select('svg').select('rect')
		.attr("id", "slidewindow")
		.attr("width", range)
		.attr("height", height)
		.attr("transform", "translate(" + position.x + ","+(container.scrollTop())+" )");
	chart.find('.slidewindow').data('x',position.x);
	callback(position.x);
}

function getBrowserScrollbarWidth() { 
	    var scroll_div = $('<div style="width:50px;height:50px;visibility: hidden;overflow:scroll;position:absolute;top:-50px;left:-50px;"></div>'); 
	    $('body').append(scroll_div); 
	    var scroll_width = $(scroll_div).prop("clientWidth");
	   	var no_scroll_width = $(scroll_div).css('overflow','hidden').prop("clientWidth");
	   	$(scroll_div).remove(); 
	    return (no_scroll_width-scroll_width); 
	}
function getLineInterpolate(type){
	switch(type){
		case 'linear': return d3.curveLinear;
		case 'step':return d3.curveStepAfter;
		default : return d3.curveLinear;
	}
}
function markPeak(container, peakDate, range, position) {
	var margin = {top: 0, right: scrollbarWidth, bottom: 0, left: 0};
	var width = container.width() - margin.left - margin.right;
	var svg = d3.select(container[0]).select('svg');
	var g = svg.select("g.peakMarker").attr("transform", "translate( 0 "+(position.y-20)+")");
	var peakPositionData = peakDate.filter(function(d){
		return d.time >=range.start && d.time <= (range.end-1);
	});
    var minX = 0;
    var maxX = range.end - range.start;
    var x = d3.scaleLinear().range([0,  width]).domain([minX, maxX]);
	// Add the peak marker icons
	var icons = g.selectAll('path.peak-marker-icon')
		.data(peakPositionData)
		.attr("transform", function(d,i) {
			return "translate(" + ((margin.left +  position.x) + x(d.time - range.start) - 5 + (x(1)-x(0))/2 ) + " 0)"; 
		}).attr("pos",  function(d) { return d.time; });
	icons.enter()
		.append("path")
		.attr("class", function(d) { return "peak-marker-icon peak-"+d.time; })
		.attr("d", "M 0 40 L 5 30 L 10 40 L 5 50 Z")
		.attr("fill", "red")
		.style("stroke", "white")
		.style("stroke-width", "1")
		// 5px is the half width of the marker
		.attr("transform", function(d,i) {
			return "translate(" + ((margin.left +  position.x) + x(d.time - range.start) - 5 + (x(1)-x(0))/2 ) + " 0)"; 
		})
		.attr("pos",  function(d) { return d.time; });
		//.on("click", function(d) { d3.event.stopPropagation(); deletePeak(d.sleepIndex, d.time); });
	icons.exit().remove();
}
function addPeak(id, time) {
	$.get(url("/analysis/sleep/"+id+"/addPeak"),
		{
			peak : time
		},
		function(responseData){
			console.log(responseData);
			});
}

function deletePeak(id, start, end){
	$.get(url("/analysis/sleep/"+id+"/delPeak"),
		{
			start:start,
			end:end
		},
		function(responseData){
			console.log(responseData);
	});
}
function getFilterValue(container_id, dataLen){
	var sleepIndex = $(container_id).data('sleep-index');
	var filterSetting = [
			[],//0
			[],//1
			[],//2
			["3839/4","7679/6","38399/16","230399/48","460799/64","ALL/128"],//3
			["3839/1","7679/1","38399/1","230399/4","460799/8","ALL/32"],//4
			[],//5
			[],//6
			[],//7
			[],//8
			[] //9
		];
	var filterValue = 1 ;
	for (var i = 0; i < filterSetting[sleepIndex].length; i++) {
		var settings = filterSetting[sleepIndex][i].split('/');
		var condition = settings[0];
		condition = condition == 'ALL' ? dataLen : parseInt(condition,10);
		var value = parseInt(settings[1],10);
		if(dataLen < condition ){
			break;
		}
		filterValue = value;
	}
	return filterValue;
}
function markTag(container, tagDate, range, position) {
	var margin = {top: 0, right: scrollbarWidth, bottom: 0, left: 0};
	var width = container.width() - margin.left - margin.right;
	var svg = d3.select(container[0]).select('svg');
	var g = svg.select("g.tagMarker").attr("transform", "translate( 0 "+(position.y)+")");
	var tagPositionData = tagDate.filter(function(d){
		return (range.start < d.end) && (range.end > d.start);
	});
    var minX = 0;
    var maxX = range.end - range.start;
    
    var x = d3.scaleLinear().range([0,  width]).domain([minX, maxX]);
	// Add the tag marker icons
	var icons = g.selectAll('rect.tag-marker-icon')
		.data(tagPositionData)
		.attr("width", function(d){
			return x(d.end) - x(d.start);})
		.attr("transform", function(d,i) {
			return "translate(" + ((margin.left +  position.x) + x(d.start) - x(range.start)) + " 0)"; 
		}).attr("pos",  function(d) { return d.time; });

	icons.enter()
		.append("rect")
		.attr("class", function(d) { return "tag-marker-icon tag-"+d.time; })
		.attr("width", function(d){
			return x(d.end) - x(d.start);})
		.attr("height", "20")
		.attr("fill", "green")
		.style("stroke", "white")
		.style("stroke-width", "1")
		.attr("transform", function(d,i) {
			return "translate(" + ((margin.left +  position.x) + x(d.start) - x(range.start)) + " 0)"; 
		})
		.attr("pos",  function(d) { return d.time; });
	icons.exit().remove();
}
function addTag(id, start, end) {
	$.get(url("/analysis/sleep/"+id+"/addTag"),
		{
			start:start,
			end:end
		},
		function(responseData){
			console.log(responseData);
			});
}

function deleteTag(id, start, end){
	$.get(url("/analysis/sleep/"+id+"/delTag"),
		{
			start:start,
			end:end
		},
		function(responseData){
			console.log(responseData);
	});
}
function timeAxisXText(recorded_at, time, sampleRate){
	var datetime = moment(recorded_at).add(time/sampleRate,'seconds');
	return datetime.hour()+":"+datetime.minute()+":"+datetime.second();
}