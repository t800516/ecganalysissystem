var smallGridSize = (ecgType=='EKG' || ecgType=='ekg') ? 5 : 5.12; // width for 40ms, height for 0.1mV
var singleLineYOffsetUV = 2000; // The offset of the y-axis of a single ECG line (uV)
var graphHeightUV = 10000; // The height of the ECG graph (uV)
var viewableWidth;
var L2Average;
var line1, line2, line3;
var sample_rate = (ecgType=='EKG' || ecgType=='ekg') ? 250:256;
var sampleMs = 2000 / sample_rate;
var xScaleRatio = (sampleMs / 40) * smallGridSize;
var yScaleRatio = (1 / 100) * smallGridSize;
var scrollbarWidth = getBrowserScrollbarWidth();
var dataSmoothRate = 8;
var overview_sample_rate = $('#ecg_overview_chart').width() * 5;
var origin_width = ecgType=='z2b' || ecgType=='xml' ? 960 : 1280;
var peakPermisson = true;
var invertedECG = false;
var hrMode = false;

function initECGGraph(data, container) {
	// Properties of the ECG graph
	singleLineYOffsetUV = (mode == 'holter' ? 2000 : 2500);
	graphHeightUV = (mode == 'holter' ? 10000 : 7395);
	var containerInstance = $(container);
	var svgWidth = data.L2.length;
	var svgHeight = graphHeightUV * yScaleRatio;
	if(mode=='holter'){
		viewableWidth = ($(container).width() - scrollbarWidth);
	}else{
		viewableWidth = $(container).width();
	}
	L2Average = d3.mean(data.L2.slice(0, sample_rate));
	if(mode=='holter'){
		svgWidth = data.L2.length;
	}else{
		var viewableLenNum = Math.floor(data.L2.length / viewableWidth) - 1;
		viewableLenNum = viewableLenNum < 1 ? 1 : viewableLenNum;
		svgWidth = viewableLenNum * viewableWidth;
	}

	containerInstance.find("svg").width(svgWidth).height(svgHeight);

	// Prepare the lines
	if(mode=='holter'){
		var yOffset = { "L2": graphHeightUV / 2};// { "L2": graphHeightUV / 2 + L2Average};
		yOffset.L1 = yOffset.L2 - singleLineYOffsetUV;
		yOffset.L3 = yOffset.L2 + singleLineYOffsetUV;
	}else{
		var yOffset = { "L1": singleLineYOffsetUV / 2};
		yOffset.L2 = yOffset.L1 + singleLineYOffsetUV;
		yOffset.L3 = yOffset.L2 + singleLineYOffsetUV;
	}

	line1 = d3.line()
		.x(function(d, i) { return i * xScaleRatio * scale; })
		.y(function(d) { return (((invertedECG ? 1 : -1) * d) + yOffset.L1) * yScaleRatio * scale; });

	line2 = d3.line()
		.x(function(d, i) { return i * xScaleRatio * scale; })
		.y(function(d) { return (((invertedECG ? 1 : -1) * d) + yOffset.L2) * yScaleRatio * scale; });

	line3 = d3.line()
		.x(function(d, i) { return i * xScaleRatio * scale; })
		.y(function(d) { return (((invertedECG ? 1 : -1) * d) + yOffset.L3) * yScaleRatio * scale; });

	// Draw the graph
	drawECGLines(data, 0);
	drawECGGrid(container);
	drawCurrentTime(container, data, 0);

	// Center the L2 in the graph
	var scrollDistance = ((svgHeight - containerInstance.height()) / 2) + scrollbarWidth;
	containerInstance.scrollTop(scrollDistance);
	// When the ECG graph scrolls
	containerInstance.scroll(function() {
		// Redraw the ECG lines
		drawECGLines(data, $(this).scrollLeft() / scale);

		// Update current time
		drawCurrentTime(container, data, $(this).scrollLeft() / scale);

		// Redraw the peak markers
		if (rrData && rrData.length != 0) {
			markPeak(rrData, $(this).scrollLeft() / scale);
		}
		if (eventData && eventData.length != 0) {
			markEventPeak(eventData, $(this).scrollLeft() / scale);
		}

		// Make the grid fix on the screen when the ECG view is scrolling
		var ecgGrid = $("#ecgGrid");
		var newTranform = ecgGrid.attr("transform")
			.replace(/translate\([^)]*\)/, "translate(" + $(this).scrollLeft() + " " + $(this).scrollTop() + ")");
		ecgGrid.attr("transform", newTranform);

		// Make the peak markers fix on the screen when the ECG view is scrolling up or down
		if(mode=='holter'){
			$("#peakMarker1").attr("transform", "translate(0 " + $(this).scrollTop() + ")");
			$("#peakEventMarker1").attr("transform", "translate(0 " + $(this).scrollTop() + ")");
		}else{
			$("#peakMarker1").attr("transform", "translate(0 " + $(this).scrollTop() + ")");
			$("#peakMarker2").attr("transform", "translate(0 " + ($(this).scrollTop() ) + ")");
			$("#peakMarker3").attr("transform", "translate(0 " + ($(this).scrollTop() ) + ")");
			$("#peakEventMarker1").attr("transform", "translate(0 " + $(this).scrollTop() + ")");
			$("#peakEventMarker2").attr("transform", "translate(0 " + $(this).scrollTop() + ")");
			$("#peakEventMarker3").attr("transform", "translate(0 " + $(this).scrollTop() + ")");
		
		}
	});
}

function drawECGLines(data, start) {
	start = parseInt(start);
	var end = start + viewableWidth;
	if(mode=='holter'){
		var L1Path = line1(data.L1.slice(start, end));
		var L2Path = line2(data.L2.slice(start, end));
		var L3Path = line3(data.L3.slice(start, end));	
		d3.select("#ecgLineGroup").select(".line1")
			.attr("transform", "translate(" + start * xScaleRatio * scale + ")")
			.attr("d", L1Path);
		d3.select("#ecgLineGroup").select(".line2")
			.attr("transform", "translate(" + start * xScaleRatio * scale + ")")
			.attr("d", L2Path);
		d3.select("#ecgLineGroup").select(".line3")
			.attr("transform", "translate(" + start * xScaleRatio * scale + ")")
			.attr("d", L3Path);
	}else{
		var L1Path = line1(data.L2.slice(start, end));
		var g_start = end;
		end = g_start + viewableWidth;
		var L2Path = line2(data.L2.slice(g_start, end));
		g_start = end;
		end = g_start + viewableWidth;
		var L3Path = line3(data.L2.slice(g_start, end));

		d3.select("#ecgLineGroup").select(".line2")
			.attr("transform", "translate(" + start * xScaleRatio * scale + ")")
			.attr("d", L1Path);
		d3.select("#ecgLineGroup2").select(".line2")
			.attr("transform", "translate(" + start * xScaleRatio * scale + ")")
			.attr("d", L2Path);
		d3.select("#ecgLineGroup3").select(".line2")
			.attr("transform", "translate(" + start * xScaleRatio * scale + ")")
			.attr("d", L3Path);
	}
}


function drawCurrentTime(container, data, start) {
	var containerInstance = $(container);
	var height = containerInstance.height();
	var scollY = containerInstance.scrollTop();
	start = parseInt(start);
	var end = start + viewableWidth;

	var calender = new Date(data.recorded_at)
	var recorded_at = calender.getTime();
	
	var start_time = moment(data.recorded_at).add(start * sampleMs / 1000,'seconds');
	var end_time = moment(data.recorded_at).add(end * sampleMs / 1000,'seconds');

	d3.select("#current_start_time")
		.attr("x", start + 10)
		.attr("y", height - scrollbarWidth - 5 + scollY)
		.attr("font-size", "20px")
		//.text(start_time.getHours()+":"+start_time.getMinutes()+":"+start_time.getSeconds());
		.text(start_time.hour()+":"+start_time.minute()+":"+start_time.second());
	d3.select("#current_end_time")
		.attr("x", end - 80)
		.attr("y", height - scrollbarWidth - 5 + scollY)
		.attr("font-size", "20px")
		//.text(end_time.getHours()+":"+end_time.getMinutes()+":"+end_time.getSeconds());
		.text(end_time.hour()+":"+end_time.minute()+":"+end_time.second());
}

function drawECGGrid(container) {
	// Get the size of the container
	// The grid will have the same size as the container
	var containerInstance = $(container);
	var height = containerInstance.height();
	var width = containerInstance.width();

	// Add a new SVG group for the grid
	var graph = d3.select("#ecgGrid");

	// The x-axis grid lines
	var xAxisLines = graph.selectAll("line.x-grid").data(d3.range(0, width, smallGridSize));
	xAxisLines.attr("x1", function(d) { return d * scale; })
		.attr("x2", function(d) { return d * scale; })
		.attr("y1", 0)
		.attr("y2", height)
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; });

	xAxisLines.enter()
		.append("line")
		.classed("x-grid", true)
		.attr("x1", function(d) { return d * scale; })
		.attr("x2", function(d) { return d * scale; })
		.attr("y1", 0)
		.attr("y2", height)
		.attr("stroke", "red")
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; })
		.attr("stroke-opacity", 0.25)
		.attr("shape-rendering", "crispEdges");

	xAxisLines.exit().remove();

	// The y-axis grid lines
	var yAxisLines = graph.selectAll("line.y-grid").data(d3.range(0, height, smallGridSize));
	yAxisLines.attr("x1", 0)
		.attr("x2", width)
		.attr("y1", function(d) { return d * scale; })
		.attr("y2", function(d) { return d * scale; })
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; });

	yAxisLines.enter()
		.append("line")
		.classed("y-grid", true)
		.attr("x1", 0)
		.attr("x2", width)
		.attr("y1", function(d) { return d * scale; })
		.attr("y2", function(d) { return d * scale; })
		.attr("stroke", "red")
		.attr("stroke-width", function(d, i) { return i % 5 != 0 ? 1 : 2; })
		.attr("stroke-opacity", 0.1)
		.attr("shape-rendering", "crispEdges");

	yAxisLines.exit().remove();
}

function markPeak(data, start) {
	if(mode=='holter'){
		_markPeak(1, data, start)
	}else{
		_markPeak(1, data, start);
		_markPeak(2, data, start);
		_markPeak(3, data, start);
	}
}
function _markPeak(group_id, data, o_start){
	var x_offset = (group_id - 1) * viewableWidth;
	var p_start = parseInt(o_start) + x_offset;
	var end = p_start + viewableWidth;
	var y_pos = (group_id - 1) * singleLineYOffsetUV * yScaleRatio * scale;
	var peakInRange = data.filter(function (d) { return d.time >= p_start && d.time <= end; });
	var intervalInRange = data.slice(1).filter(function (d) { return d.intervalPos >= p_start && d.intervalPos <= end; });

	// The SVG group for the peak markers
	var graph = d3.select("#peakMarker"+group_id);

	// Add the peak marker icons
	var icons = graph.selectAll('path.peak-marker-icon')
		.data(peakInRange);

	icons.attr("class", function(d) { return "peak-marker-icon peak-" + d.peak + " peak-" + d.time; })
		// 5px is the half width of the marker
		.attr("transform", function(d) { return "translate(" + ((d.time-x_offset) * xScaleRatio * scale - 5) + " "+y_pos+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; })
		.on("click", function(d) {d3.event.stopPropagation(); deletePeak(d.time); });

	icons.enter()
		.append("path")
		.attr("class", function(d) { return "peak-marker-icon peak-" + d.peak + " peak-" + d.time; })
		.attr("d", "M 0 40 L 5 30 L 10 40 L 5 50 Z")
		.attr("fill", "red")
		.style("stroke", "black")
		.style("stroke-width", "1")
		// 5px is the half width of the marker
		.attr("transform", function(d) { return "translate(" + ((d.time-x_offset) * xScaleRatio * scale - 5) + " "+y_pos+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; })
		.on("click", function(d) {d3.event.stopPropagation(); deletePeak(d.time); });

	icons.exit().remove();

	// Add the peak type texts
	var peakTypeTexts = graph.selectAll('text.peak-marker-type')
		.data(peakInRange);

	peakTypeTexts.attr("class", function(d) { return "peak-marker-type peak-" + d.peak + " peak-" + d.time; })
		.attr("fill", function(d) { return d.peak == "N" ? "black" : d.peak == "E" ? "blue" : "red"; })
		.attr("transform", function(d) { return "translate(" + ((d.time-x_offset) * xScaleRatio * scale) + " "+(y_pos+24)+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; })
		.text(function(d) { return d.peak; });

	peakTypeTexts.enter()
		.append("text")
		.attr("class", function(d) { return "peak-marker-type peak-" + d.peak + " peak-" + d.time; })
		.style("font-size", "20px")
		.style("font-weight", "bold")
		.attr("text-anchor", "middle")
		.attr("fill", function(d) { return d.peak == "N" ? "black" : d.peak == "E" ? "blue" : "red"; })
		.style("stroke", "black")
		.style("stroke-width", "0.5")
		.attr("transform", function(d) { return "translate(" + (d.time-x_offset) * xScaleRatio * scale + " "+(y_pos+24)+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; })
		.text(function(d) { return d.peak; });

	peakTypeTexts.exit().remove();

	// Add the peak interval texts
	var intervalTexts = graph.selectAll("text.peak-marker-interval")
		.data(intervalInRange);

	intervalTexts.attr("id", function(d) { return "peak-interval-" + d.prevTime + "-" + d.time; })
		.attr("transform", function(d) { return "translate(" + (d.intervalPos-x_offset) * xScaleRatio * scale + " "+(y_pos+24)+")"; })
		.attr("pos",  function(d) { return d.intervalPos * xScaleRatio; })
		.text(function(d) { return d.interval ? (hrMode ? Math.round(60000/d.interval) : d.interval) : ''; });

	intervalTexts.enter()
		.append("text")
		.attr("class", "peak-marker-interval")
		.attr("id", function(d) { return "peak-interval-" + d.prevTime + "-" + d.time; })
		.style("font-size", "12px")
		.style("font-weight", "bold")
		.attr("fill", "black")
		.attr("text-anchor", "middle")
		.attr("transform", function(d) { return "translate(" + (d.intervalPos-x_offset) * xScaleRatio * scale + " "+(y_pos+24)+")"; })
		.attr("pos",  function(d) { return d.intervalPos * xScaleRatio; })
		.text(function(d) { return d.interval ? (hrMode ? Math.round(60000/d.interval) : d.interval) : ''; });

	intervalTexts.exit().remove();
}
function markEventPeak(data, start) {
	if(mode=='holter'){
		_markEventPeak(1, data, start)
	}else{
		_markEventPeak(1, data, start);
		_markEventPeak(2, data, start);
		_markEventPeak(3, data, start);
	}
}

function _markEventPeak(group_id, data, o_start) {
	var x_offset = (group_id - 1) * viewableWidth;
	p_start = parseInt(o_start) + x_offset;
	var end = p_start + viewableWidth;
	var y_pos = (group_id - 1) * singleLineYOffsetUV * yScaleRatio * scale;
	var peakInRange = data.filter(function (d) { return d.time >= p_start && d.time <= end; });
	
	// The SVG group for the peak markers
	var graph = d3.select("#peakEventMarker"+group_id);

	// Add the peak marker icons
	var icons = graph.selectAll('path.peak-marker-icon')
		.data(peakInRange);

	icons.attr("class", function(d) { return "peak-marker-icon peak-" + d.peak + " peak-" + d.time; })
		// 5px is the half width of the marker
		.attr("transform", function(d) { return "translate(" + ((d.time-x_offset) * xScaleRatio * scale - 5) + " "+y_pos+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; });

	icons.enter()
		.append("path")
		.attr("class", function(d) { return "peak-marker-icon peak-" + d.peak + " peak-" + d.time; })
		.attr("d", "M 0 40 L 5 30 L 10 40 L 5 50 Z")
		.attr("fill", "blue")
		.style("stroke", "black")
		.style("stroke-width", "1")
		// 5px is the half width of the marker
		.attr("transform", function(d) { return "translate(" + ((d.time-x_offset) * xScaleRatio * scale - 5) + " "+y_pos+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; });
	icons.exit().remove();

	// Add the peak type texts
	var peakTypeTexts = graph.selectAll('text.peak-marker-type')
		.data(peakInRange);

	peakTypeTexts.attr("class", function(d) { return "peak-marker-type peak-" + d.peak + " peak-" + d.time; })
		.attr("fill", function(d) { return d.peak == "N" ? "black" : d.peak == "E" ? "blue" : "red"; })
		.attr("transform", function(d) { return "translate(" + (d.time-x_offset) * xScaleRatio * scale + " "+(y_pos+24)+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; })
		.text(function(d) { return d.peak; });

	peakTypeTexts.enter()
		.append("text")
		.attr("class", function(d) { return "peak-marker-type peak-" + d.peak + " peak-" + d.time; })
		.style("font-size", "20px")
		.style("font-weight", "bold")
		.attr("text-anchor", "middle")
		.attr("fill", function(d) { return d.peak == "N" ? "black" : d.peak == "E" ? "blue" : "red"; })
		.style("stroke", "black")
		.style("stroke-width", "0.5")
		.attr("transform", function(d) { return "translate(" + (d.time-x_offset) * xScaleRatio * scale + " "+(y_pos+24)+")"; })
		.attr("pos",  function(d) { return d.time * xScaleRatio; })
		.text(function(d) { return d.peak; });
	peakTypeTexts.exit().remove();
}

function deletePeak(time) {
	if(!peakPermisson){
		return ;
	}
	// Find the index of the peak to be deleted in the RR data
	var peakIndex = rrData.findIndex(function (element) {
		return element.time == time;
	});

	if (peakIndex == -1) { return; } // The peak is not found, abort

	// Adjust the RR interval and the position of the interval on graph if the peak to be deleted is not the last peak
	//
	// Note:
	// If the peak to be deleted is the last peak, the interval between this peak and the previous peak
	// will also be deleted
	var prevTime = peakIndex != 0 ? rrData[peakIndex - 1].time : 0;
	var nextTime = peakIndex != rrData.length - 1 ? rrData[peakIndex + 1].time : 0;
	var nextType = peakIndex != rrData.length - 1 ? rrData[peakIndex + 1].peak : "";
	var nextInterval = peakIndex != rrData.length - 1 ? (nextTime - prevTime) * 8 : 0;
	if (peakIndex != rrData.length - 1) {
		rrData[peakIndex + 1].interval = nextInterval;
		rrData[peakIndex + 1].intervalPos = prevTime + (nextTime - prevTime) / 2;
	}

	// Delete the peak from the RR data array
	rrData.splice(peakIndex, 1);

	// Delete the peak from the RR file on server
	$.get("/api/v0/analysis/rr/delPeak/" + ecgID,
		{
			delPeak: {time: time},
			nextPeak: {time: nextTime, type: nextType, interval: nextInterval}
		});

	// Update the peak markers
	markPeak(rrData, $('#ecgView').scrollLeft()/scale);

	// Update the RR interval Chart
	update_rr_interval(rrData, ecgData);

	//Delete the peak from ECG overview
	del_peak_overview(time);
}

function deletePeaks(startTime, endTime) {
	if(!peakPermisson){
		return ;
	}
	var peaks = rrData.filter(function(d){ return (d.time * xScaleRatio) >= startTime &&  (d.time * xScaleRatio) <= endTime});
	var peakStartIndex;// = rrData.length;
	var peakEndIndex;// = -1;
	if(peaks.length>0){
		peakStartIndex = rrData.findIndex(function (element) {
			return element.time == peaks[0].time;
		});
		peakEndIndex = peakStartIndex + peaks.length - 1 ;
	}else{
		return ;
	}/*
	for (var i = 0; i < peaks.length; i++) {
		var tempIndex = rrData.findIndex(function (element) {
			return element.time == peaks[i].time;
		});
		peakStartIndex = tempIndex <= peakStartIndex ? tempIndex : peakStartIndex;
		peakEndIndex = tempIndex >= peakEndIndex ? tempIndex : peakEndIndex;
	}*/

	var prevTime = peakStartIndex != 0 ? rrData[peakStartIndex - 1].time : 0;
	var nextTime = peakEndIndex != rrData.length - 1 ? rrData[peakEndIndex + 1].time : 0;
	var nextType = peakEndIndex != rrData.length - 1 ? rrData[peakEndIndex + 1].peak : "";
	var nextInterval = peakEndIndex != rrData.length - 1 ? (nextTime - prevTime) * 8 : 0;
	if (peakEndIndex != rrData.length - 1) {
		rrData[peakEndIndex + 1].interval = nextInterval;
		rrData[peakEndIndex + 1].intervalPos = prevTime + (nextTime - prevTime) / 2;
	}

	// Delete the peak from the RR data array
	rrData.splice(peakStartIndex, peaks.length);

	// Delete the peak from the RR file on server
	$.post("/api/v0/analysis/rr/delPeaks/" + ecgID,
		{
			delPeaks: peaks.map(function(d , i){return d.time}),
			nextPeak: {time: nextTime, type: nextType, interval: nextInterval}
		},function(response){
	});
	
	// Update the peak markers
	markPeak(rrData, $('#ecgView').scrollLeft()/scale);

	// Update the RR interval Chart
	update_rr_interval(rrData);

	//Delete the peak from ECG overview
	for (var i = 0; i < peaks.length; i++) {
		del_peak_overview(peaks[i].time);
	}
}

function addPeak(peakTime, peakType) {

	// Find the index of the peak after the peak to be add in the RR data
	var nextPeakIndex = rrData.findIndex(function (element) {
		return element.time > peakTime;
	});

	// The attributes about the peaks
	var isInsertAsFirstPeak = nextPeakIndex <= 0 && rrData.length == 0;

	var prevTime = !isInsertAsFirstPeak && nextPeakIndex != 0 ? ( nextPeakIndex != -1 ? rrData[nextPeakIndex - 1].time : rrData[ rrData.length - 1 ].time ) : 0;
	var nextTime = !isInsertAsFirstPeak ? ( nextPeakIndex != -1 ? rrData[nextPeakIndex].time : peakTime ) : peakTime;
	var nextType = !isInsertAsFirstPeak ? ( nextPeakIndex != -1 ? rrData[nextPeakIndex].peak : peakType ): peakType;
	if(peakTime==prevTime){
		return false;
	}

	// The interval and the interval text position on screen before and after the inserted peak
	var beforePeakInterval = (peakTime - prevTime) * 8;
	var beforePeakIntervalPos = prevTime + (peakTime - prevTime) / 2;
	var afterPeakInterval = (nextTime - peakTime) * 8;
	var afterPeakIntervalPos = peakTime + (nextTime - peakTime) / 2;
	
	// Update the RR data array
	var peak = {time: peakTime, peak: peakType, interval: beforePeakInterval, intervalPos: beforePeakIntervalPos};
	if(nextPeakIndex != -1){
		rrData[nextPeakIndex].interval = afterPeakInterval;
		rrData[nextPeakIndex].intervalPos = afterPeakIntervalPos;
		rrData.splice(nextPeakIndex, 0, peak);
	}else{
		rrData.push(peak);
	}

	var peakData = {
			insertPeak: {time: peakTime, type: peakType, interval: beforePeakInterval},
			nextPeak: nextPeakIndex != -1 ? {time: nextTime, type: nextType, interval: afterPeakInterval}:{}
		}
	// Add the peak to the RR file on server
	$.get("/api/v0/analysis/rr/addPeak/" + ecgID, peakData);

	// Update the peak markers
	markPeak(rrData, $('#ecgView').scrollLeft()/scale);
	
	// Update the RR
	update_rr_interval(rrData);
}

function draw_rr_interval(data,recorded_at, container, selector) {
	var sliceData = data.slice(1);
	var margin = {top: 0, right: scrollbarWidth, bottom: 0, left: 0};
	var width = $(container).width() - margin.left - margin.right;
	var height = $(selector).height() - margin.top - margin.bottom;

	var maxX = sliceData.length < 2 ?  0 : d3.max(sliceData, function (d) { return d.time; });
	var meanY = sliceData.length < 2 ?  0 : d3.mean(sliceData, function (d) { return d.interval; });
	var minY = sliceData.length < 2 ?  0 : d3.min(sliceData, function (d) { return d.interval; });
	var maxY = sliceData.length < 2 ?  0 : d3.max(sliceData, function (d) { return d.interval; });
	var heightY = (maxY - minY)*75/800;
	heightY = heightY < 75 ? 75:heightY;
	var svg = d3.select(selector)
		.select("svg")
		.attr("width", '100%')
		.attr("height",  heightY + margin.top + margin.bottom)
		.select(".rr_row")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
	var y = d3.scaleLinear().range([ heightY, 0]).domain([minY, maxY]);
	var line = d3.line().x(function (d) { return x(d.time); }).y(function (d) { return y(d.interval); });
	
	var g = svg.append("g")
			.attr("transform", "scale(" + (width / origin_width) + ",1)");
	var path = g.append("path").attr("class", "line");
	if(sliceData.length >= 2){
		path.attr("d", line(sliceData));

		//var calender = new Date(recorded_at);
		//var recorded_at = calender.getTime();
		
		//var start_time = new Date(recorded_at+sliceData[0].time*3.90625*2);
		//var end_time = new Date(recorded_at+sliceData[sliceData.length - 1].time*3.90625*2);
		
		var start_time = moment(recorded_at).add(sliceData[0].time * 0.0078125,'seconds');
		var end_time = moment(recorded_at).add(sliceData[sliceData.length - 1].time * 0.0078125,'seconds');

		var text_box=svg.append("g")
			.attr("transform", "scale(" + (width / origin_width) + ",1)");

		//$('#rr_time_row .start_time').html(start_time.getHours()+":"+start_time.getMinutes()+":"+start_time.getSeconds());
		//$('#rr_time_row .end_time').html(end_time.getHours()+":"+end_time.getMinutes()+":"+end_time.getSeconds());
		$('#rr_time_row .start_time').html(start_time.hour()+":"+start_time.minute()+":"+start_time.second());
		$('#rr_time_row .end_time').html(end_time.hour()+":"+end_time.minute()+":"+end_time.second());

	}
	$(selector).scrollTop(y(meanY)-$(selector).height()/2);
}
function label_event_peek_rr_interval(rr_data,event_data,container, selector) {
	var sliceData = rr_data.slice(1);
	var margin = {top: 0, right: scrollbarWidth, bottom: 0, left: 0};
	var width = $(container).width() - margin.left - margin.right;
	var height = $(selector).height() - margin.top - margin.bottom;
	var event_row = d3.select(selector)
		.select("svg")
		.append("g")
		.classed("event_row", true)
		.attr("transform", "translate(" + margin.left + "," + (margin.top+$(selector).scrollTop()) + ") scale(" + (width / origin_width) + ",1)");

	if(sliceData.length >= 2){
	var maxX = d3.max(sliceData, function (d) { return d.time; });
	var minY = d3.min(sliceData, function (d) { return d.interval; });
	var maxY = d3.max(sliceData, function (d) { return d.interval; });
	var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
	var y = d3.scaleLinear().range([height, 0]).domain([minY, maxY]);
	var eventText=event_row.selectAll('circle').data(event_data);
	eventText.enter()
			.append('circle')
				.attr("cx", function (d) { return x(d.time)-1; })
            	.attr("cy", y(maxY)+4)
				.attr("r", "2")
				.style("fill", "blue");
	eventText.exit().remove();
	}
}

function draw_ecg_overview(data, overview_sample_rate, container, selector) {
	var down_data = []
	for(var i = 0; i < data.length ; i+=dataSmoothRate ){
		var maxindex = i;
		var max_md = 0;
		for(var j = i; j < i + dataSmoothRate ; j++){
			if( data[j] >= max_md){
				max_md = data[j];
				maxindex = j;
			}
		}
		down_data.push(data[maxindex]);
	}

	var sample_rate = overview_sample_rate / dataSmoothRate;
	var chart_num = parseInt(down_data.length / sample_rate);
	var margin = {top: 0, right: scrollbarWidth, bottom: 0, left: 0};
	var width = $(container).width() - margin.right;
	var height = 50 - margin.top - margin.bottom;

	var svg = d3.select(selector)
		.append("svg")
		.attr("width", '100%')
		.attr("height", height * (chart_num + 1) + margin.top + margin.bottom)
		.append('g')
		.classed('ecg_overview_box', true);
	var x = d3.scaleLinear().range([0, origin_width]).domain([0, sample_rate - 1]);
	var y = d3.scaleLinear().range([height, 0]).domain(d3.extent(down_data, function (d) { return d; }));
	var line = d3.line().x(function (d, i) { return x(i); }).y(function (d) { return y(((invertedECG ? -1 : 1) * d)); });

	for (var index = 0; index <= chart_num; index++) {
		var lineData = index == chart_num ?
			down_data.slice(index * sample_rate, down_data.length) :
			down_data.slice(index * sample_rate, (index + 1) * sample_rate);

		svg.append("g")
			.classed('ecg_overview_row', true)
			.classed('row_'+index, true)
			.attr("transform", "translate(" + margin.left + " " + (margin.top + index * height) + ") scale(" + (width / origin_width) + " 1)")
			.append("path")
			.datum(lineData)
			.attr("class", "line")
			.attr("d", line);
	}
}
function update_ecg_overview(data, overview_sample_rate, container, selector) {
	var down_data = []
	for(var i = 0; i < data.length ; i+=dataSmoothRate ){
		var maxindex = i;
		var max_md = 0;
		for(var j = i; j < i + dataSmoothRate ; j++){
			if( data[j] >= max_md){
				max_md = data[j];
				maxindex = j;
			}
		}
		down_data.push(data[maxindex]);
	}

	var sample_rate = overview_sample_rate / dataSmoothRate;
	var chart_num = parseInt(down_data.length / sample_rate);
	var margin = {top: 0, right: scrollbarWidth, bottom: 0, left: 0};
	var width = $(container).width() - margin.right;
	var height = 50 - margin.top - margin.bottom;

	var svg = d3.select(selector)
		.select("g.ecg_overview_box");

	var x = d3.scaleLinear().range([0, origin_width]).domain([0, sample_rate - 1]);
	var y = d3.scaleLinear().range([height, 0]).domain(d3.extent(down_data, function (d) { return d; }));
	var line = d3.line().x(function (d, i) { return x(i); }).y(function (d) { return y(((invertedECG ? -1 : 1) * d)); });

	for (var index = 0; index <= chart_num; index++) {
		var lineData = index == chart_num ?
			down_data.slice(index * sample_rate, down_data.length) :
			down_data.slice(index * sample_rate, (index + 1) * sample_rate);

		svg.select('.row_'+index).selectAll('.line')
			.datum(lineData)
			.attr("d", line);
	}
}


function label_peek_ecg_overview(data,overview_sample_rate,container,selector) {
	var sample_rate = overview_sample_rate;
	var margin = {left: 0, right: scrollbarWidth, top: 0};
	var width = $(container).width() - margin.right - margin.left;

	var rrdata = d3.map(data, function (d) { return d.time; });
	var x = d3.scaleLinear().range([0, origin_width]).domain([0, sample_rate - 1]);
	$(selector + " svg .rr_peak_row").remove();
	d3.select(selector + " svg").selectAll('path').each(function (p, i) {
		var peekarearow = d3.select(selector + " svg").append('g')
			.classed('rr_peak_row', true)
			.attr("transform", "translate(" + margin.left + "," + (i * 50) + ") scale(" + (width / origin_width) + ",1)");
		var index = p.length * dataSmoothRate;
		while (index >= 0) {
			var rtime = index + i * sample_rate;
			if (rrdata.has(rtime)) {
				peekarearow.append('text')
					.attr('time', rtime)
					.attr('x', x(index) - 6)
					.attr('y', 15)
					.text(rrdata.get(rtime).peak);
				index -= Math.floor(rrdata.get(rtime).interval / 8);
			} else {
				index--;
			}
		}
	});
}
function label_event_peek_ecg_overview(data,overview_sample_rate,container,selector) {
	var sample_rate = overview_sample_rate;
	var margin = {left: 0, right: scrollbarWidth, top: 0};
	var width = $(container).width() - margin.right - margin.left;

	var eventdata = d3.map(data, function (d) { return d.time; });
	var x = d3.scaleLinear().range([0, origin_width]).domain([0, sample_rate - 1]);
	$(selector + " svg .event_peak_row").remove();
	d3.select(selector + " svg").selectAll('path').each(function (p, i) {
		var peekarearow = d3.select(selector + " svg").append('g')
			.classed('event_peak_row', true)
			.attr("transform", "translate(" + margin.left + "," + (i * 50) + ") scale(" + (width / origin_width) + ",1)");
		var index = p.length * dataSmoothRate;
		while (index >= 0) {
			var rtime = index + i * sample_rate;
			if (eventdata.has(rtime)) {
				peekarearow.append('text')
					.attr("fill", "blue")
					.attr('time', rtime)
					.attr('x', x(index) - 6)
					.attr('y', 15)
					.text(eventdata.get(rtime).peak);
				index -= Math.floor(eventdata.get(rtime).interval / 8);
			} else {
				index--;
			}
		}
	});
}

function update_rr_interval(data){
	var sliceData = data.slice(1);
	var selector='#rr_line_chart';
	var margin = {top: 0, right: scrollbarWidth, bottom: 17, left: 0};
	var height = $(selector).height() - margin.top - margin.bottom;
	var maxX = sliceData.length < 2 ?  0 : d3.max(sliceData, function (d) { return d.time; });
	var meanY = sliceData.length < 2 ?  0 : d3.mean(sliceData, function (d) { return d.interval; });
	var minY = sliceData.length < 2 ?  0 : d3.min(sliceData, function (d) { return d.interval; });
	var maxY = sliceData.length < 2 ?  0 : d3.max(sliceData, function (d) { return d.interval; });
	var heightY = (maxY - minY)*75/800;
	heightY = heightY < 75 ? 75:heightY;
	var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
	var y = d3.scaleLinear().range([heightY, 0]).domain([minY, maxY]);
	var line = d3.line().x(function (d) { return x(d.time); }).y(function (d) { return y(d.interval); });

	var svg = d3.select(selector)
		.select("svg")
		.attr("height",  heightY + margin.top + margin.bottom);
		
	if(sliceData.length > 2){
		d3.select(selector + ' svg .rr_row g path').attr('d', line(sliceData));
		$(selector).scrollTop(y(meanY)-$(selector).height()/2);
	}else{
		d3.select(selector + ' svg .rr_row g path').attr('d', line([]));
	}
	if(ecgType=='z2b' || ecgType=='xml'){
		tachogram_update(sliceData, '.rr_beat_chart','#rr_beat_chart');
		histogram_update(sliceData, '.rr_histogram','#rr_histogram');
		avgBeatChart_update(ecgData256, sliceData, '.rr_avgBeatChart','#rr_avgBeatChart');
	}
}

function add_peak_overview(peak_time,peak) {
	var index = peak_time % overview_sample_rate;
	var row_index = Math.floor(peak_time / overview_sample_rate);
	var x = d3.scaleLinear().range([0, origin_width]).domain([0, overview_sample_rate - 1]);
	d3.selectAll('#ecg_overview_chart svg .rr_peak_row').each(function(p,i){
		if(i==row_index){
			d3.select(this).append('text')
				.attr('time', peak_time)
				.attr('x', x(index) - 6)
				.attr('y', 15)
				.text(peak);
		}
	});
}

function del_peak_overview(peak_time){
	$('#ecg_overview_chart').find('svg .rr_peak_row text[time='+peak_time+']').remove();
}

function rr_slide_window(container, selector) {
	var margin = {left: 0, right: scrollbarWidth, top: 0};
	var width = $(container).width() - margin.right - margin.left;
	var height = $(selector).height();

	var ecgView = $("#ecgView");
	var x = ecgView.scrollLeft() * width / ecgView.find('svg').width();
	var rr_size = origin_width * (ecgView.width() - margin.right) / ecgView.find("svg").width();
	
	d3.select(selector + " svg").select('#rr_slidewindow')
		.attr("width", rr_size)
		.attr("height", height)
		.attr("transform", "translate(" + x + " " +($(selector).scrollTop())+ ") scale(" + (width / origin_width) + " 1)");
}

function ecg_slide_window(data,container,selector) {
	var chart_num = parseFloat(data.length / overview_sample_rate);

	var margin = {left: 0, right: scrollbarWidth, top: 0};
	var width = $(container).width() - margin.right - margin.left;
	var height = 50;

	var ecgView = $("#ecgView");
	var ecg_view_size = chart_num * origin_width * (ecgView.width() - margin.right) / ecgView.find("svg").width();

	var slidewindow_box = d3.select(selector + " svg").append("g");
	slidewindow_box.append('rect')
		.attr("id", "ecg_slidewindow")
		.classed("ecg_slidewindow", true)
		.attr("width", ecg_view_size)
		.attr("height", height)
		.attr("transform", "translate(" + margin.left + " 0) scale(" + (width / origin_width) + " 1)");
	slidewindow_box.append('rect')
		.attr("id", "ecg_slidewindow_wrap")
		.classed("ecg_slidewindow", true)
		.attr("width", ecg_view_size)
		.attr("height", height)
		.attr("transform", "translate(" + margin.left + " 0) scale(" + (width / origin_width) + " 1)");
}

function ecg_current_peak_window(selector) {
	var margin = {left: 0, right: scrollbarWidth, top: 0};
	var current_peak_window_size = 30;

	d3.select(selector + " svg").append("g")
		.append('rect').attr("id", "current_peak_window")
		.classed("current_peak_window", true)
		.attr("width", current_peak_window_size)
		.attr("height", "500")
		.attr("transform", "translate(" + margin.left + " " + $("#ecgView").scrollTop() +")");
}

function tachogram_init(data,container,selector){
		data = data.slice(1);
		var margin = {top: 20, right: 0, bottom: 20, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 65;
		var maxX = data.length;
		var origin_width = width;
		var maxY = d3.max(data, function (d) { return (d.interval/128)*125;});
		maxY = maxY > 1200 ? 1600 : 1200;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
		var y = d3.scaleLinear().range([height,0]).domain([400, maxY]);
		
		var xAxis = d3.axisBottom().scale(x).ticks(data.length/10);

		var yAxis = d3.axisLeft().scale(y).ticks(5).tickFormat(function(d){
				return parseFloat(d)/1000;
			});

		var xAxisT = d3.axisBottom().scale(x).ticks(data.length/10).tickFormat(function(d){
				return '';
			});

		var yAxisR = d3.axisLeft().scale(y).ticks(5).tickFormat(function(d){
				return'';
			});


		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var line = d3.line().x(function (d , i) { return x(i); }).y(function (d) { return y((d.interval/128)*125); });
		
		var chart = svg.append("g").attr("class","chart")
		
		var axis = svg.append("g")
					.attr("class", "axis");
		
		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);
  		axis.append("g")
      		.attr("class", "axisy")
      		.call(yAxis);
      	axis.append("g")
      		.attr("class", "axisxT")
      		.attr("transform", "translate(0, 0)")
      		.call(xAxisT);
  		axis.append("g")
      		.attr("class", "axisyR")
      		.attr("transform", "translate("+(width-1)+", 0)")
      		.call(yAxisR);

		chart.append("path")
			.attr("class", "tacholine")
			.attr("d", line(data));

		var dot = chart.selectAll(".dot")
			.data(data);
		dot.enter()
		 	.append("circle") // Uses the enter().append() method
			.attr("class", "dot") // Assign a class for styling
			.attr("cx", function(d, i) { return x(i) })
			.attr("cy", function(d) { return y((d.interval/128)*125)})
			.attr("r", 5)
			    .on("mouseover", function(a, b, c) { 
			  			var circle=d3.select(this);
						circle.style("fill","black");
					})
			    .on("mouseout", function(a, b, c) { 
			  			var circle=d3.select(this);
						circle.style("fill","none");
				});
		dot.exit().remove();
	}

	function histogram_init(data,container,selector){
		data = data.slice(1);
		var margin = {top: 20, right: 0, bottom: 20, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 65;
		var origin_width = width;
		var procData = data.map(function(d,i){return d.interval;});//Math.floor(d.interval/20)*20; });
		var maxX = 1600;		
		var x = d3.scaleLinear().rangeRound([0, origin_width]).domain([400, maxX]);
		var histogram = d3.histogram().value( function(d) { return d; }).domain(x.domain()).thresholds(x.ticks(60));
		
      	var bins = histogram(procData);
		var y = d3.scaleLinear().range([height,0]).domain([0, d3.max(bins, function(d) { return d.length; })]);
		
		var xAxis = d3.axisBottom().scale(x).ticks(15).tickFormat(function(d){
				return parseFloat(d)/1000;
			});

		var yAxis = d3.axisLeft().scale(y).ticks(4);

		var xAxisT = d3.axisBottom().scale(x).ticks(15).tickFormat(function(d){
				return '';
			});

		var yAxisR = d3.axisLeft().scale(y).ticks(5).tickFormat(function(d){
				return'';
			});


		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var chart = svg.append("g").attr("class","chart")
		
		var axis = svg.append("g")
					.attr("class", "axis");
		
		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);
  		axis.append("g")
      		.attr("class", "axisy")
      		.call(yAxis);
      	axis.append("g")
      		.attr("class", "axisxT")
      		.attr("transform", "translate(0, 0)")
      		.call(xAxisT);
  		axis.append("g")
      		.attr("class", "axisyR")
      		.attr("transform", "translate("+(width-1)+", 0)")
      		.call(yAxisR);

      	var bars = chart.selectAll(".bar")
			.data( bins)
			.enter()
			 	.append("g") // Uses the enter().append() method
				.attr("class", "bar") // Assign a class for styling
				.attr("transform", function(d, i) { return "translate(" + x(d.x0) + "," + y(d.length) + ")"; })
				.append("rect")
		        .attr("x", 1)
		        .attr("width", x(bins[0].x1) - x(bins[0].x0) - 1)
		        .attr("height", function(d) { return height - y(d.length); });

	}

function avgBeatChart_init(data, rr_data, container, selector){
		const drawData = avgBeat(data, rr_data);
		console.log(data, rr_data)
		var margin = {top: 20, right: 2, bottom: 2, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 65;
		var grid_width = Math.min(width, height)
		var adjustLeft = width > grid_width ? (width - grid_width - margin.left)/2 : margin.left;
		var maxX = drawData.length;
		var origin_width = grid_width;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
		var y = d3.scaleLinear().range([grid_width, 0]).domain([-750, 1000]);
		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", grid_width+ margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + (adjustLeft+margin.left) + "," + margin.top + ") scale(1 1)");

		var line = d3.line().x(function (d , i) { return x(i); }).y(function (d) { return y(d); });
		
		var chart = svg.append("g").attr("class","chart")

		var axis = d3.scaleLinear().range([0,  origin_width]).domain([0, 25]);
		var xAxisLines = chart.selectAll("line.x-grid").data(d3.range(0, 26, 1));
	
		xAxisLines.attr("x1", function(d) { return xAxis(d); })
		.attr("x2", function(d) { return xAxis(d); })
		.attr("y1", 0)
		.attr("y2", grid_width)
		.attr("stroke-width", function(d, i) { return i % 5 != 0 && i != 25 ? 1 : 3; });

		xAxisLines.enter()
			.append("line")
			.classed("x-grid", true)
			.attr("x1", function(d) { return axis(d); })
			.attr("x2", function(d) { return axis(d); })
			.attr("y1", 0)
			.attr("y2", grid_width)
			.attr("stroke", "#ff8a8a")
			.attr("stroke-width", function(d, i) { return i % 5 != 0 && i != 25 ? 1 : 3; })
			.attr("stroke-opacity", 1)
			.attr("shape-rendering", "crispEdges");

		xAxisLines.exit().remove();

	// The y-axis grid lines
	var yAxisLines = chart.selectAll("line.y-grid").data(d3.range(0, 26, 1));
	yAxisLines.attr("x1", 0)
		.attr("x2", grid_width)
		.attr("y1", function(d) { return axis(d); })
		.attr("y2", function(d) { return axis(d); })
		.attr("stroke-width", function(d, i) { return i % 5 != 0 && i != 25 ? 1 : 3; });

	yAxisLines.enter()
		.append("line")
		.classed("y-grid", true)
		.attr("x1", 0)
		.attr("x2", grid_width)
		.attr("y1", function(d) { return axis(d); })
		.attr("y2", function(d) { return axis(d); })
		.attr("stroke", "#ff8a8a")
		.attr("stroke-width", function(d, i) { return i % 5 != 0 && i != 25 ? 1 : 3; })
		.attr("stroke-opacity", 1)
		.attr("shape-rendering", "crispEdges");

	yAxisLines.exit().remove();

	chart.append("path")
		.attr("class", "beat-line")
		.attr("d", line(drawData));
}
function tachogram_update(data, container, selector){
		data = data.slice(1);
		var margin = {top: 20, right: 0, bottom: 20, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 65;
		var maxX = data.length;
		var origin_width = width;
		var maxY = d3.max(data, function (d) { return (d.interval/128)*125;});
		maxY = maxY > 1200 ? 1600 : 1200;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
		var y = d3.scaleLinear().range([height,0]).domain([400, maxY]);
		
		var xAxis = d3.axisBottom().scale(x).ticks(data.length/10);

		var yAxis = d3.axisLeft().scale(y).ticks(4).tickFormat(function(d){
				return parseFloat(d)/1000;
			});

		var xAxisT = d3.axisBottom().scale(x).ticks(data.length/10).tickFormat(function(d){
				return '';
			});

		var yAxisR = d3.axisLeft().scale(y).ticks(4).tickFormat(function(d){
				return'';
			});


		var svg = d3.select(selector)
			.select("svg").select("g");
		var line = d3.line().x(function (d , i) { return x(i); }).y(function (d) { return y((d.interval/128)*125); });
		
		var chart = svg.select("g.chart");
		
		var axis = svg.select("g.axis");
		
		axis.select("g.axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);
  		axis.select("g.axisy")
      		.call(yAxis);
      	axis.select("g.axisxT")
      		.attr("transform", "translate(0, 0)")
      		.call(xAxisT);
  		axis.select("g.axisyR")
      		.attr("transform", "translate("+(width-1)+", 0)")
      		.call(yAxisR);

		chart.select("path.tacholine")
			.attr("d", line(data));

		var dot = chart.selectAll(".dot")
		    .data([]);
		dot.exit().remove();
		dot = chart.selectAll(".dot")
			.data(data);
		 	dot.enter()
		 		.append("circle") // Uses the enter().append() method
			    .attr("class", "dot") // Assign a class for styling
			    .attr("cx", function(d, i) { return x(i) })
			    .attr("cy", function(d) { return y((d.interval/128)*125)})
			    .attr("r", 5)
			      .on("mouseover", function(a, b, c) { 
			  			var circle=d3.select(this);
						circle.style("fill","black");
					})
			      .on("mouseout", function(a, b, c) { 
			  			var circle=d3.select(this);
						circle.style("fill","none");
					})
			dot.exit().remove();
	}
function histogram_update(data,container,selector){
		data = data.slice(1);
		var margin = {top: 20, right: 0, bottom: 20, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 65;
		var origin_width = width;
		var procData = data.map(function(d,i){ return d.interval; });
		var maxX = 1600;		
		
		var x = d3.scaleLinear().rangeRound([0, origin_width]).domain([400, maxX]);
		var histogram = d3.histogram().value( function(d) { return d; }).domain(x.domain()).thresholds(x.ticks(60));
		
      	var bins = histogram(procData);
		var y = d3.scaleLinear().range([height,0]).domain([0, d3.max(bins, function(d) { return d.length; })]);
		
		var xAxis = d3.axisBottom().scale(x).ticks(15).tickFormat(function(d){
				return parseFloat(d)/1000;
			});

		var yAxis = d3.axisLeft().scale(y).ticks(4);

		var xAxisT = d3.axisBottom().scale(x).ticks(15).tickFormat(function(d){
				return '';
			});

		var yAxisR = d3.axisLeft().scale(y).ticks(5).tickFormat(function(d){
				return'';
			});


		var svg = d3.select(selector)
			.select("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.select("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var chart = svg.select("g.chart");
		
		var axis = svg.select("g.axis");
		
		axis.select("g.axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);
  		axis.select("g.axisy")
      		.call(yAxis);
      	axis.select("g.axisxT")
      		.attr("transform", "translate(0, 0)")
      		.call(xAxisT);
  		axis.select("g.axisyR")
      		.attr("transform", "translate("+(width-1)+", 0)")
      		.call(yAxisR);
      	var bars = chart.selectAll(".bar")
			.data([]).exit().remove();
      	bars = chart.selectAll(".bar")
			.data( bins)
			.enter()
			 	.append("g") // Uses the enter().append() method
				.attr("class", "bar") // Assign a class for styling
				.attr("transform", function(d, i) { return "translate(" + x(d.x0) + "," + y(d.length) + ")"; })
				.append("rect")
		        .attr("x", 1)
		        .attr("width", x(bins[0].x1) - x(bins[0].x0) - 1)
		        .attr("height", function(d) { return height - y(d.length); });

	}

function avgBeatChart_update(data, rr_data, container, selector){
		const drawData = avgBeat(data, rr_data);
		var margin = {top: 20, right: 2, bottom: 2, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 65;
		var grid_width = Math.min(width, height)
		var adjustLeft = width > grid_width ? (width - grid_width - margin.left)/2 : margin.left;
		var maxX = drawData.length;
		var origin_width = grid_width;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
		var y = d3.scaleLinear().range([grid_width, 0]).domain([-750, 1000]);
		var svg = d3.select(selector)
			.select("svg").select("g");
		var line = d3.line().x(function (d , i) { return x(i); }).y(function (d) { return y((invertedECG ? -1 : 1) * d); });
		
		var chart = svg.select("g.chart");

		chart.select("path.beat-line")
			.attr("d", line(drawData));
}

function filterECG(data){
	const b = [0.0102094807912031,	0.0408379231648125,	0.0612568847472188,	0.0408379231648125,	0.0102094807912031];
	const a = [1,	-1.96842778693852,	1.73586070920889,	-0.724470829507363,	0.120389599896245];
	const zi = [0.989790519208796,	-1.01947519089453,	0.655128633567133,	-0.110180119105041,0.0];
	const nfilt = 5;
	const dataL1 = reSample(filtfilt(data, a, b, nfilt, zi));
	const dataL2 = reSample(filtfilt(data, a, b, nfilt, zi));
	const dataL3 = reSample(filtfilt(data, a, b, nfilt, zi));
	return {...data, L1:dataL1, L2:dataL2, L3:dataL3};
}

//1.1 單向濾波
function myfilter(x, xlen, a, b, nfilt, zi){
	m=0;
	i=0;
	data = 0.0;
	for(m=0;m<xlen;m++)
	{
		data = x[m];
		x[m]=(b[0]*data+zi[0]);
		for(i=1;i<nfilt;i++)
		{
			zi[i-1]=b[i]*data+zi[i]-a[i]*x[m];
		}
	}
	return [x, zi];
}
//1.2 雙向濾波
function filtfilt(x, a, b, nfilt, zi){
	let xlen = x.length;
	let nfact=(nfilt+1)*2;
	let i = 0;
	let ylen = xlen+nfact*2;
	let temp1=0.0;
		
	let zi1 = Array(nfilt).fill(0.0);
	let ytemp =  Array(ylen).fill(0.0);
	let y = Array(xlen).fill(0.0);

	for(i = 0; i < nfact; i++){
		ytemp[i] = 2*x[0]-x[nfact-i];
		ytemp[xlen+nfact+i]=2*x[xlen-1]-x[xlen-1-i-1];
	}
		
	for(i = 0; i < xlen; i++){
		ytemp[i+nfact]=x[i];
	}
		
	for (i=0;i<nfilt-1;i++){
		zi1[i]=zi[i]*ytemp[0];
	}

	[ytemp, zi1] = myfilter(ytemp,ylen,a,b,nfilt,zi1);

	for(i = 0; i < ylen/2; i++)
	{
		temp1 = ytemp[i];
		ytemp[i] = ytemp[ylen-1-i];
		ytemp[ylen-1-i]=temp1;
	}

	for (i=0;i<nfilt;i++){
			zi1[i]=zi[i]*ytemp[0];
	}

	[ytemp, zi1] =  myfilter(ytemp,ylen,a,b,nfilt,zi1);

	for(i = nfact; i < ylen-nfact; i++){
		y[ylen-nfact-i-1] = ytemp[i];
	}
	return y;
}	
function reSample(data){
	var reSampleData = [];
	var n = 0
	for(var i in data){
		if(i % 2 == 0){
			reSampleData.push(data[i]);
		}
	}
	return reSampleData;
}

// 依據beat位置 去找最大值或最小值進行平均
function avgBeat(data, rr_data)	{
	let beat = rr_data.map(d=>d.time); 
	let type = rr_data.map(d=>d.peak); 
	const fs = 256;
	//1. 修正到ECG peak的位置 fs = 256
	let L = data.length;
	let MaxVal=0;
	let MinVal=0;
	let beatMax = [];
	let beatMin = [];
	for(let i=0;i<beat.length;i++){
		let beat256 = beat[i]/128 * fs;
		let	LL = beat256 - 11;  //beat左邊寬度
		let	RR = beat256 + 8;	//beat右邊寬度
		if (LL<0){
			LL = 0;
		}
		if (RR>=L){
			RR=L;
		}
		let max_val = data[LL];
		let min_val = data[LL];
		let maxidx = LL;
		let minidx = LL;
		for (let kk=LL+1; kk<RR; kk++){
			if (data[kk]> max_val ){
				max_val = data[kk];
				maxidx = kk;
			}else if (data[kk]< min_val ){
				min_val = data[kk];
				minidx = kk;
			}				
		}	
		beatMax[i] = maxidx;
		beatMin[i] = minidx;    
		MaxVal += data[maxidx]; //判斷選上面的peak還是下面的peak
		MinVal += Math.abs(data[minidx]);
	}		

		let beat1 =  MaxVal>=MinVal ? beatMax : beatMin;

		// 2. 將ECG平均
		MaxVal = 0;
		MinVal = 0;
		let avgECG = Array(fs).fill(0);
		let num = 0;
		for(let i=0; i<beat1.length; i++)
		{
			if (type[i]=='N' && (beat1[i]-fs/2) >0 && (beat1[i]+ fs) <=L)
			{
				num=num+1;
				for(kk=0; kk<fs; kk++)
					avgECG[kk]= avgECG[kk] + data[beat1[i]- Math.round(fs*2/5) + kk];
			}
		}
		if (num>0){
			for(kk=0; kk<fs; kk++){
				avgECG[kk]=avgECG[kk]/num;
			}
		}
	return avgECG;	
}		

function getBrowserScrollbarWidth() { 
	    var scroll_div = $('<div style="width:50px;height:50px;visibility: hidden;overflow:scroll;position:absolute;top:-50px;left:-50px;"></div>'); 
	    $('body').append(scroll_div); 
	    var scroll_width = $(scroll_div).prop("clientWidth");
	   	var no_scroll_width=$(scroll_div).css('overflow','hidden').prop("clientWidth");
	   	$(scroll_div).remove(); 
	    return (no_scroll_width-scroll_width); 
	}