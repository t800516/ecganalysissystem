<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login_title'=>'管理登入',

    'name_label'=>'帳號',
    'password_label'=>'密碼',

    'login'=>'登入',
    'logout'=>'登出',
    
    'failed' => '登入錯誤!',
    'throttle' => '登入次數過多，請於 :seconds 秒後再次嘗試登入。',
    
    'password_reset_success' =>'密碼重設成功',
    'permission_denied' =>'尚無存取權限。',
    'old_password_error'=>'原密碼錯誤!',
    'invalid_credential'=>'密碼錯誤',
    'permission_denied'=>'權限錯誤',
];
