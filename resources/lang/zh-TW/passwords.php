<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密碼長度必須至少為6個字元。',
    'reset' => '密碼已重設',
    'sent' => '密碼重設連結已寄出',
    'token' => '密碼重設連結已失效',
    'user' => "此信箱不存在",

];
