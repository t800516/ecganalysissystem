@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/css/register.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('thirdpart/multi-select/css/multi-select.css')}}">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div class="container-fluid">
				<form id="register_form" action="{{ isset($id) ? url('/admin/member/edit'):url('/admin/member/register')}}" method="post" class="form-horizontal">
					<fieldset class="row">
						<div>
						    <legend class="">{{isset($id) ? '編輯':'新增' }}使用者</legend>
				    	</div>
				    	<ul class="errormsg list-group">
		 					@foreach($errors->all() as $key=>$error)
								<li class="list-group-item list-group-item-danger">{{$error}}</li>
							@endforeach
						</ul>
						<div class="form-group row">
							<label for="email" class="control-label col-sm-2">E-mail</label>
							<div class="controls col-sm-8">
								<input type="email" name="email" id="email" class="form-control" value="{{ $email or '' }}" required>
							</div>
							<div class="col-sm-2 email_msg"> </div>
						</div>
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">名稱</label>
							<div class="controls col-sm-8">
								<input type="text" name="name" id="name" class="form-control" value="{{ $name or '' }}" required>
							</div>
							<div class="col-sm-2 name_msg"> </div>
						</div>
						@if(!isset($id))
							<div class="form-group row">
								<label for="password" class="control-label col-sm-2">密碼</label>
								<div class="controls col-sm-8">
									<input type="password" name="password" id="password" value="{{ $password or '' }}" class="form-control" required>
								</div>
								<div class="col-sm-2 password_msg"> </div>
							</div>
							<div class="form-group row">
								<label for="password_confirmation" class="control-label col-sm-2">確認密碼</label>
								<div class="controls col-sm-8">
									<input type="password" name="password_confirmation" value="{{ $password or '' }}" id="password_confirmation" class="form-control" required>
								</div>
								<div class="col-sm-2 password_confirmation_msg"></div>
							</div>
						@endif
					</fieldset>
					<fieldset class="row">
						<div>
						    <legend class="">使用設定</legend>
				    	</div>
						<div class="form-group row">
							<label for="holters" class="control-label col-sm-2">Holter分配</label>
							<div class="col-sm-8">
								<select id="select_holter" class="form-control" name="holter[]" multiple="multiple">
									@if(isset($holters))
										@foreach($holters as $key=>$holter)
											<option value="{{$holter['id']}}" selected>{{$holter['IDNumber']}}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>						
						<div class="form-group row">
							<label for="storage" class="control-label col-sm-2">額外使用容量</label>
							<div class="controls col-sm-8">
								<input type="Number" name="storage" id="storage" class="form-control" min="0" value="{{ $storage or '0' }}" required>
							</div>
							<div class="col-sm-2 storage_msg">GB</div>
						</div>
						<div class="form-group row">
							<label for="remark" class="control-label col-sm-2">備註</label>
							<div class="controls col-sm-8">
								<textarea name="remark" id="remark" class="form-control">{{ $remark or '' }}</textarea>
							</div>
						</div>
						<div class="form-group row">
							<label for="role_id" class="control-label col-sm-2">類別</label>
							<div class="controls col-sm-8">
								<select id="role_id" name="role_id" class="form-control">
									@foreach($roles as $role_data)
										<option value="{{$role_data->id}}" {{( isset($role) && $role->id==$role_data->id )? 'selected' : ''}}>{{$role_data->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="department_id" class="control-label col-sm-2">報告</label>
							<div class="controls col-sm-8">
								<select id="department_id" name="department_id" class="form-control">
									<option value="0" hidden selected>請選擇</option>
									@foreach( $departments as $department)
										<option value="{{$department->id}}" {{(isset($department_id) && $department_id == $department->id )? 'selected' : ''}}>{{$department->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row {{( !isset($role) || ( isset($role) && !in_array($role->id, [4, 5, 8, 13])))? 'hide' : ''}}">
							<label for="user_ids" class="control-label col-sm-2">可查看之使用者</label>
							<div class="controls col-sm-8">
								<select id="user_ids" name="user_ids[]" class="form-control" multiple>

									@foreach( $allUsers as $allUser)
										<option value="{{$allUser->id}}" {{(isset($users) && $users->where('id', $allUser->id)->count()!=0) ? 'selected' : ''}}>{{$allUser->name}}, {{$allUser->email}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="auto_transfer" class="control-label col-sm-2">自動轉檔</label>
							<div class="controls col-sm-8">
								<input type="checkbox" id="auto_transfer" name="auto_transfer" class="check_input" value="1" {{( isset($auto_transfer) && $auto_transfer==1 )? 'checked' : ''}}>
							</div>
						</div>
						<div class="form-group row">
							<label for="auto_report" class="control-label col-sm-2">自動產生報表</label>
							<div class="controls col-sm-8">
								<input type="checkbox" id="auto_report" name="auto_report" class="check_input" value="1" {{( isset($auto_report) && $auto_report==1 )? 'checked' : ''}}>
							</div>
						</div>
						<div class="form-group row">
							<label for="checked" class="control-label col-sm-2">狀態</label>
							<div class="controls col-sm-8">
								<select id="checked" name="checked" class="form-control">
									<option value="0" {{( isset($checked) && $checked==0 )? 'selected' : ''}}>未啟用</option>
									<option value="1" {{( isset($checked) && $checked==1 )? 'selected' : ''}}>啟用</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="expiration_date" class="control-label col-sm-2">使用期限</label>
							<div class="controls col-sm-8">
								<input type="text" name="expiration_date" id="expiration_date" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" placeholder="YYYY-MM-DD" value="{{isset($expiration_date) && $expiration_date ? $expiration_date:''}}" />
							</div>
						</div>
					</fieldset>
					{!! isset($id) ? '<input type="hidden" name="id" value="'.$id.'">' : '' !!}
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<div class="col-sm-4 col-sm-offset-4">
						<input type="submit" class="btn btn-primary form-control" value="{{isset($id) ? '修改':'新增' }}">
					</div>
				</form>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')

<script src="{{asset('thirdpart/jquery.quicksearch/jquery.quicksearch.js')}}"></script>
<script src="{{asset('thirdpart/multi-select/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
	$(function(){
		$('#account').focus();
		$('#password_confirmation').blur(function(){
			if($(this).val()!=$('#password').val()){
				$('.password_confirmation_msg').html('password confirmation not match.');
			}else{
				$('.password_confirmation_msg').html('');
			}
		});

		$("#register_form").submit(function(){
			@if(!isset($id))
				if($('#password_confirmation').val()!=$('#password').val()){
					$('.password_confirmation_msg').html('password confirmation not match.');
					return false;
				}
			@endif
		});

		load_holter();

		function load_holter(){
			$.get(url('/admin/holter/list/1'),function(data){
				var holter_list=data.holterList;
				var options_str='';
				for(var i=0;i<holter_list.length;i++){
					options_str+='<option value="'+holter_list[i].id+'">'+holter_list[i].IDNumber+'</option>';
				}
				$("#select_holter").append(options_str);
				$('#select_holter').multiSelect('refresh');
			});
		}
		
		$('#select_holter').multiSelect({
	        selectableHeader: "<div class='text-center'>可分配</div><input type='text' class='form-control' autocomplete='off' placeholder='Search'>",
	        selectionHeader: "<div class='text-center'>已分配</div><input type='text' class='form-control' autocomplete='off' placeholder='Search'>",
	        afterInit: function(ms){
	            var that = this,
	                $selectableSearch = that.$selectableUl.prev(),
	                $selectionSearch = that.$selectionUl.prev(),
	                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
	                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

	            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
	                .on('keydown', function(e){
	                    if (e.which === 40){
	                        that.$selectableUl.focus();
	                        return false;
	                    }
	                });

	            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
	                .on('keydown', function(e){
	                    if (e.which == 40){
	                        that.$selectionUl.focus();
	                        return false;
	                    }
	                });
	        },
	        afterSelect: function(){
	            this.qs1.cache();
	            this.qs2.cache();
	        },
	        afterDeselect: function(){
	            this.qs1.cache();
	            this.qs2.cache();
	        }
	    });
	    $('#role_id').change(function(event){
	    	if($(this).val() != 4 && $(this).val() != 5 && $(this).val() != 8 && $(this).val() != 13){
	    		$('#user_ids').parent().parent().addClass('hide');
	    	}else{
	    		$('#user_ids').parent().parent().removeClass('hide');
	    	}

	    	if($(this).val()==7 || $(this).val()==8 || $(this).val()==10){
	    		$('#auto_transfer').prop('checked',true);
	    		$('#auto_transfer').change();
	    		$('#auto_report').prop('checked',true);
	    	}

	    });
	    $('#user_ids').multiSelect({
	        selectableHeader: "<div class='text-center'>可分配</div><input type='text' class='form-control' autocomplete='off' placeholder='Search'>",
	        selectionHeader: "<div class='text-center'>已分配</div><input type='text' class='form-control' autocomplete='off' placeholder='Search'>",
	        afterInit: function(ms){
	            var that = this,
	                $selectableSearch = that.$selectableUl.prev(),
	                $selectionSearch = that.$selectionUl.prev(),
	                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
	                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

	            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
	                .on('keydown', function(e){
	                    if (e.which === 40){
	                        that.$selectableUl.focus();
	                        return false;
	                    }
	                });

	            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
	                .on('keydown', function(e){
	                    if (e.which == 40){
	                        that.$selectionUl.focus();
	                        return false;
	                    }
	                });
	        },
	        afterSelect: function(){
	            this.qs1.cache();
	            this.qs2.cache();
	        },
	        afterDeselect: function(){
	            this.qs1.cache();
	            this.qs2.cache();
	        }
	    });
	    $('#auto_transfer').change(function(event){
	    	if($(this).prop('checked')){
	    		$('#auto_report').prop('disabled',false);
	    	}else{
	    		$('#auto_report').prop('checked',false);
	    		$('#auto_report').prop('disabled',true);
	    	}

	    });
	});
</script>
@endsection