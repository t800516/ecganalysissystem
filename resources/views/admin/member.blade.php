@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">會員管理</h4>
				<div class="tools">
					<ul class="pull-right">
						<li class="">
							<a href="{{url('/admin/member/register')}}" class="btn btn-default" id="new_user">
								<span class="glyphicon glyphicon-plus"></span>
								新增使用者
							</a>
						</li>
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_remove">
								<span class="glyphicon glyphicon-trash"></span>
								刪除
							</a>
						</li>
						<!--
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_pass">
								<span class="glyphicon glyphicon-check"></span>
								審核通過
							</a>
						</li>-->
					</ul>
				</div>
			</div>
			<div id="member_manager">
				<table data-toggle="table" id="member_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-single-select="false" data-checkbox-header="true" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="email" data-sortable="true" data-halign="center" data-align="center">E-mail</th>
						<th data-field="name" data-sortable="true" data-halign="center" data-align="center" data-width="15%">名稱</th>
						<th data-field="department" data-sortable="true" data-halign="center" data-align="center" data-formatter="department_formatter">部門</th>
						<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center">註冊時間</th>
						<th data-field="role" data-sortable="true" data-halign="center" data-align="center" data-formatter="role_formatter">類別</th>
						<th data-field="checked" data-sortable="true" data-halign="center" data-align="center" data-formatter="cheched_formatter">狀態</th>
						<th data-field="id" data-halign="center" data-align="center" data-formatter="function_formatter">操作</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
	$("#func_remove").click(function(event){
		var selections=$('#member_manager_table').bootstrapTable('getSelections');
		if(selections.length>0){
			if(confirm('確定刪除此 '+selections.length+' 筆帳號?')){
				var ids=selections.map(function(d){return d.id});
				$.post(url('/admin/member/del'),{_token:window.Laravel.csrfToken,ids:ids},function(data){
					$('#member_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
				});
			}
		}
	});
	$('#member_manager tbody').on('click','.remove',function(event){
		return confirm('確定刪除此帳號?');
	});
	$('#func_pass').click(function(event){
		var selections = $('#member_manager_table').bootstrapTable('getSelections');
		if(selections.length>0){
			$.post(url('/admin/member/checked'),{ids:selections.map(function(d){return d['id']}),_token:window.Laravel.csrfToken},function(data){
				window.location.replace("{{url('/admin/member')}}");

			});
		}
	});
});
function ajaxRequest(params) {
    $.get(url('/admin/member/list') + '?' + $.param(params.data)).then(function (res) {
      params.success(res);
    })
}
function queryParams(params){
	return params;
}

function department_formatter(value, row, index){
	return value ? value.name:'';
}
function role_formatter(value, row, index){
	return value ? value.name:'';
}
function cheched_formatter(value, row, index){
	return (value==1)?'啟用':'未啟用';
}
function function_formatter(value, row, index){
	return '<a href="'+url("/admin/member/edit/"+value)+'" class="btn btn-default" >編輯</a>';
}
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}
</script>
@endsection