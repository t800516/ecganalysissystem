@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/priority.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('thirdpart/multi-select/css/multi-select.css')}}">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
			<div id="tool_bar">
				<h4 class="pull-left title"> 優先權管理</h4>
				<div class="tools">
					<ul class="pull-right">
						<li>
							<a href="#" class="btn btn-default" id="priority_add">
							<span class="glyphicon glyphicon-plus" ></span>
							優先權
							</a>
						</li>
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_remove">
								<span class="glyphicon glyphicon-trash"></span>
								刪除
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="priority_manager">
				<table data-toggle="table" id="priority_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-single-select="false" data-checkbox-header="true" data-unique-id="id" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="id" data-sortable="true" data-halign="center" data-align="center" data-width="10%">ID</th>
						<th data-field="value" data-sortable="true" data-halign="center" data-align="center" data-width="20%">優先權</th>
						<th data-field="start_hr" data-sortable="true" data-halign="center" data-align="center" data-width="20%" data-formatter="setting_formatter">時間設定</th>
						<th data-field="email" data-sortable="true" data-halign="center" data-align="center" data-width="20%"
						data-formatter="emails_formatter">通知者</th>
						<th data-field="id" data-halign="center" data-align="center" data-formatter="function_formatter">Action</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
<div id="priority_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="priority_modal_label">
  	<div class="modal-dialog" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="priority_modal_label">設定</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<form id="priority_form" action="{{url('admin/priority')}}" method="POST">
	  			{{csrf_field()}}
	  			<input type="hidden" name="id" id="priority_id" value="0">
	  			<div class="row">
					<div class="col-sm-12">
						優先權
						<select id="priority_value" name="value">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
					<div class="col-sm-12">
					從 <select id="priority_start_hr" name="start_hr">
						@for($i=0;$i<24;$i++)
							<option value="{{$i}}">{{$i}}</option>
						@endfor
					</select>：<select id="priority_start_min" name="start_min">
						@for($i=0;$i<60;$i++)
							<option value="{{$i}}">{{$i}}</option>
						@endfor
					</select> 開始至 <select id="priority_end_hr" name="end_hr">
						@for($i=0;$i<24;$i++)
							<option value="{{$i}}">{{$i}}</option>
						@endfor
					</select>：<select id="priority_end_min" name="end_min">
						@for($i=0;$i<60;$i++)
							<option value="{{$i}}">{{$i}}</option>
						@endfor
					</select> ，期間每 <select id="priority_period_hr" name="period_hr" >
						@for($i=0;$i<24;$i++)
							<option value="{{$i}}">{{$i}}</option>
						@endfor
					</select> 時 <select id="priority_period_min" name="period_min">
						@for($i=0;$i<60;$i++)
							<option value="{{$i}}">{{$i}}</option>
						@endfor
					</select> 分檢查是否有優先權為 <span class="priority_value">1</span> 的檔案上傳
	  				</div>
					<hr/>	
	  				<div class="form-group row">
							<label for="priority_exceptions" class="control-label col-sm-2">分析設定</label>
							<div class="controls col-sm-8">
								<select id="priority_exceptions" name="priority_exceptions[]" class="form-control" multiple>
									@foreach( $users as $select_user)
										<option value="{{$select_user->id}}">{{$select_user->email}}</option>
									@endforeach
								</select>
							</div>
						</div>
					<hr/>	
	  				<div class="col-sm-12">
	  				<div class="text">
						若有則寄信通知
					</div>
					<div class="priority_email_list">
						<div>
							<input type="email" class="priority_emails" name="emails[]">
						</div>
					</div>
					<div class="priority_email_add">
						<button id="priority_email_btn" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></button>
					</div>
					</div>
	  			</div>
	  			<div class="row">
	  				<button id="priority_create" class="btn btn-info col-md-4 col-md-offset-1" type="submit">OK</button>
	  				<button id="priority_cancel" class="btn btn-default col-md-3 col-md-offset-3" data-dismiss="modal">Cancel</button>
	  			</div>
	  		</form>
			</div>
	  	</div>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script src="{{asset('thirdpart/jquery.quicksearch/jquery.quicksearch.js')}}"></script>
<script src="{{asset('thirdpart/multi-select/js/jquery.multi-select.js')}}"></script>
<script>
$(function(){
	var priorities = {!! json_encode($priorities) !!};
	$('#priority_manager_table').bootstrapTable('load',priorities);	
	$("#priority_add").click(function(event){
		event.preventDefault();
		$('#priority_id').val(0);
		$('#priority_value').val(1);
		$('#priority_start_hr').val(0);
		$('#priority_start_min').val(0);
		$('#priority_end_hr').val(0);
		$('#priority_end_min').val(0);
		$('#priority_period_hr').val(0);
		$('#priority_period_min').val(0);
		$('.priority_email_list .addition').parent().remove();
		$('.priority_email_list .priority_emails').val('');
		$('#priority_modal').modal('show');
		$('#priority_exceptions').multiSelect('deselect_all');
	});
	$('#priority_value').change(function(event){
		$('.priority_value').html($(this).val());
	});
	$('#priority_email_btn').click(function(event){
		event.preventDefault();
		$('.priority_email_list').append(priority_email_html(''));
	});
	$('#priority_create').click(function(event){
		/*event.preventDefault();
		var data = {
			value:$('#priority_value').val(),
			start_hr:$('#priority_start_hr').val(),
			start_min:$('#priority_start_min').val(),
			period_hr:$('#priority_period_hr').val(),
			period_min:$('#priority_period_min').val(),
			emails:$('.priority_email_list .priority_emails').map(function(d){return $(this).val()}).toArray()
		};
		$('#priority_form').submit();*/
	});
	$('.priority_email_list').on('click','.remove',function(event){
		event.preventDefault();
		$(this).parent().remove();
	});
	$('#priority_manager_table').on('click','.edit',function(event){
		event.preventDefault();
		var row = $('#priority_manager_table').bootstrapTable('getRowByUniqueId',$(this).data('id'));
		$('#priority_id').val(row.id);
		$('#priority_value').val(row.value);
		$('#priority_start_hr').val(row.start_hr);
		$('#priority_start_min').val(row.start_min);
		$('#priority_end_hr').val(row.end_hr);
		$('#priority_end_min').val(row.end_min);
		$('#priority_period_hr').val(row.period_hr);
		$('#priority_period_min').val(row.period_min);
		$('.priority_email_list .addition').parent().remove();
		for (var i = 0; i < row.emails.length; i++) {
			if(i==0){
				$('.priority_email_list .priority_emails').val(row.emails[i].email);
			}else{
				$('.priority_email_list').append(priority_email_html(row.emails[i].email));
			}
		}
		$('#priority_modal').modal('show');
		$('#priority_exceptions').multiSelect('select', row.exception_users.map(function(d){return d.id.toString();}));
	});
	$("#func_remove").click(function(event){
		var selections=$('#priority_manager_table').bootstrapTable('getSelections');
		if(selections.length>0){
			if(confirm('確定刪除此 '+selections.length+' 筆資料?')){
				var ids=selections.map(function(d){return d.id});
				$.post(url("/admin/priority/del"),{_token:window.Laravel.csrfToken,ids:ids},function(data){
					$('#priority_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
				});
			}
		}
	});
	$('#priority_exceptions').multiSelect({
	        selectableHeader: "<div class='text-center'>寄信</div><input type='text' class='form-control' autocomplete='off' placeholder='Search'>",
	        selectionHeader: "<div class='text-center'>不寄信</div><input type='text' class='form-control' autocomplete='off' placeholder='Search'>",
	        afterInit: function(ms){
	            var that = this,
	                $selectableSearch = that.$selectableUl.prev(),
	                $selectionSearch = that.$selectionUl.prev(),
	                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
	                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

	            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
	                .on('keydown', function(e){
	                    if (e.which === 40){
	                        that.$selectableUl.focus();
	                        return false;
	                    }
	                });

	            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
	                .on('keydown', function(e){
	                    if (e.which == 40){
	                        that.$selectionUl.focus();
	                        return false;
	                    }
	                });
	        },
	        afterSelect: function(){
	            this.qs1.cache();
	            this.qs2.cache();
	        },
	        afterDeselect: function(){
	            this.qs1.cache();
	            this.qs2.cache();
	        }
	    });
});
function priority_email_html(email){
	return '<div><input type="email" class="priority_emails addition" name="emails[]" value="'+email+'"> <span class="glyphicon glyphicon-remove remove"></span></div>';
}
function function_formatter(value, row, index){
	return '<a href="#" class="btn btn-default edit" data-id="'+value+'">編輯</a>';
}
function setting_formatter(value, row, index){
	return row.start_hr+'：'+row.start_min+'開始至'+row.end_hr+'：'+row.end_min+'，期間每'+row.period_hr+'小時'+row.period_min+'分檢查';
}
function emails_formatter(value, row, index){
	var html = '';
	for (var i = 0; i < row.emails.length; i++) {
		html+='<div>'+row.emails[i].email+'</div>';
	}
	return html;
}
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}
</script>
@endsection