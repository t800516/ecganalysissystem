@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">Apk管理</h4>
				<div class="tools">
					<ul class="pull-right">
						<li class="">
							<a href="{{url('/admin/apks/create')}}" class="btn btn-default" id="new_apk">
								<span class="glyphicon glyphicon-plus"></span>
								新增Apk
							</a>
						</li>
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_remove">
								<span class="glyphicon glyphicon-trash"></span>
								刪除
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="apk_manager">
				<table data-toggle="table" id="apk_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-single-select="false" data-checkbox-header="true" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="name" data-sortable="true" data-halign="center" data-align="center" data-width="50%">Name</th>
						<th data-field="department" data-sortable="true" data-halign="center" data-align="center" data-width="20%" data-formatter="department_formatter" >Department</th>
						<th data-field="name" data-sortable="true" data-halign="center" data-align="center" data-width="20%" data-formatter="link_formatter" >連結</th>
						<th data-field="id" data-halign="center" data-align="center" data-formatter="function_formatter">Action</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
	var apks = {!! json_encode($apks) !!};
	$('#apk_manager_table').bootstrapTable('load',apks);	
	$("#func_remove").click(function(event){
		var selections=$('#apk_manager_table').bootstrapTable('getSelections');
		if(selections.length>0){
			if(confirm('確定刪除此 '+selections.length+' 筆資料?')){
				var ids=selections.map(function(d){return d.id});
				$.post(url("/admin/apks/del"),{_token:window.Laravel.csrfToken,ids:ids},function(data){
					$('#apk_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
				});
			}
		}
	});
});
function link_formatter(value, row, index){
	return '<a target="_blank" href="'+ url('apks/'+encodeURIComponent(row.name))+'">'+value+'</a>';
}
function function_formatter(value, row, index){
	return '<a href="'+url("/admin/apks/edit/"+value)+'" class="btn btn-default" >編輯</a>';
}
function department_formatter(value, row, index){
	return value?.name ?? '';
}
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}
</script>
@endsection