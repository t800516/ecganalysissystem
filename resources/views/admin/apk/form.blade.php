
@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/apk.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">Apk管理 / {{ $apk ? '編輯': '新增' }}Apk</h4>
			</div>
			<div id="apk_manager">
				<form id="apk_form" action="{{url('/admin/apks')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
				<fieldset>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<label for="name" class="control-label col-md-2">Name</label>
						<div class="controls col-md-8">
							<input type="text" name="name" id="name" value="{{$errors->count()!=0 ? old('name') : ($apk ? $apk->name:'')}}" class="form-control" required>
						</div>
						<div class="col-md-2 name_msg"> </div>
					</div>
					<div class="form-group">
						<label for="department" class="control-label col-md-2">Department</label>
						<div class="controls col-md-8">
							<select class="form-control" name="department_id" id="department_id">
								@foreach($departments as $department)
									<option value="{{ $department->id }}">{{ $department->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2 name_msg"> </div>
					</div>
					<div class="form-group">
						<label for="apk_file" class="control-label col-md-2">Apk</label>
						<div class="controls col-md-8">
							<div class="btn-file">
								<label>
										<span class="btn btn-info fileinput-exists {{ $apk ? '' : 'hide'}}">更換檔案</span>
										<span class="btn btn-success fileinput-new {{ $apk ? 'hide' : ''}}">選擇檔案</span>
										<span class="filename">{{$apk ? basename($apk->path):''}}</span>
									<input type="file" name="apk_file" id="apk_file" class="form-control hide">
								</label>
                            </div>
						</div>
						<div class="col-md-2 email_msg"> </div>
					</div>
					{!! $apk ? '<input type="hidden" name="id" value="'.$apk->id.'">': '' !!}
					{{ csrf_field() }}
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-primary form-control" value="{{ $apk ? '修改': '新增' }}">
				</div>
			</form>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
	
	$('#apk_file').on('change', function(event){
		if(event.target.files.length>0){
			$('.filename').html(event.target.files[0].name);
			$('.fileinput-exists').removeClass('hide');
			$('.fileinput-new').addClass('hide');
		}else{
			$('.fileinput-exists').addClass('hide');
			$('.fileinput-new').removeClass('hide');
		}
	});
	$('#name').focus();

	$("#register_form").submit(function(){
			
	});
});

</script>
@endsection