@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">Holter管理 / {{ isset($id) ? '編輯': '新增' }}Holter</h4>
			</div>
			<div id="holter_manager">
				<form id="holter_form" action="{{url('/admin/holter/create')}}" method="post" class="form-horizontal">
				<fieldset>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<label for="IDNumber" class="control-label col-md-2">Holter ID</label>
						<div class="controls col-md-8">
							<input type="text" name="IDNumber" id="IDNumber" value="{{$IDNumber or ''}}" class="form-control" required>
						</div>
						<div class="col-md-2 IDNumber_msg"> </div>
					</div>
					{!! isset($id) ? '<input type="hidden" name="id" value="'.$id.'">': '' !!}
					{{ csrf_field() }}
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-primary form-control" value="{{ isset($id) ? '修改': '新增' }}">
				</div>
			</form>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
	
	$('#IDNumber').focus();

	$("#register_form").submit(function(){
			
	});
});

</script>
@endsection