@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/thirdpart/bootstraptable/extensions/page-jumpto/bootstrap-table-jumpto.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<h4 class="title">檔案管理</h4>
			<div id="tool_bar">
				<div class="tools">
					<ol class="breadcrumb">
						
						@foreach($layers as $key => $layer)
							@if($key != count($layers)-1)
								<li><a href="{{url('admin/files?layer='.join('/',array_slice($layers,0, $key+1)))}}">{{$key == 1 ? $user_name.'('.$layer.')' : $layer}}</a></li>
							@else
								<li>{{$key == 1 ? $user_name.'('.$layer.')' : $layer}}</li>
							@endif
						@endforeach
					</ol>
					<ul class="pull-right">
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_download">
								<span class="glyphicon glyphicon-download"></span>
								下載
							</a>
							<form id="download_form" class="hide" method="POST" action="{{url('admin/files/downloads')}}" target="_blank">
								{{csrf_field()}}

							</form>
						</li>
					</ul>
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-single-select="false" data-checkbox-header="true" data-maintain-selected="true" data-pagination="true" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]" data-show-jumpto="true" data-pagination-v-align="both">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="name" data-sortable="true" data-halign="center" data-align="center" data-width="60%" data-formatter="name_formatter">File</th>
						<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center" data-width="35%">Created</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/extensions/page-jumpto/bootstrap-table-jumpto.min.js')}}"></script>
<script>
$(function(){
	var files = {!! json_encode($files) !!};
	$('#file_manager_table').bootstrapTable('load',files);	
	$('#func_download').click(function(event){
		var selections = $('#file_manager_table').bootstrapTable('getSelections');
		$('#download_form input[name="path[]"]').remove();
		
		for (var i = 0; i < selections.length; i++) {
			$('#download_form').append('<input type="hidden" name="path[]" value="'+selections[i].filepath+'" />');
		}

		if($('#download_form input[name="path[]"]').length > 0){
			$('#download_form').submit();
		}else{
			alert('無檔案下載');
		}
	});
});
function link_formatter(value, row, index){
	return '<a target="_blank" href="'+ url('files/'+encodeURIComponent(row.name))+'">'+value+'</a>';
}
function name_formatter(value, row, index){
	return '<a '+(row.dir ? '':'target="_blank"')+' href="'+ row.link +'">'+value+'</a>';
}
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}
</script>
@endsection