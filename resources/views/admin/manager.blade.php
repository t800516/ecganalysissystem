@extends('layouts.admin')
@section('css_file')
	<link href="/thirdpart/bootstraptable/bootstrap-table.min.css" rel="stylesheet">
	<link href="/css/sidebar.css" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<div class="row">
			
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection