@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">後台使用者管理</h4>
				<div class="tools">
					<ul class="pull-right">
						<li class="">
							<a href="{{url('/admin/users/create')}}" class="btn btn-default" id="new_user">
								<span class="glyphicon glyphicon-plus"></span>
								新增後台使用者
							</a>
						</li>
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_remove">
								<span class="glyphicon glyphicon-trash"></span>
								刪除
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="user_manager">
				<table data-toggle="table" id="user_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-single-select="false" data-checkbox-header="true" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="account" data-sortable="true" data-halign="center" data-align="center" data-width="50%">Account</th>
						<th data-field="auth" data-sortable="true" data-halign="center" data-align="center" data-width="20%" data-formatter="auth_formatter">Auth</th>
						<th data-field="id" data-halign="center" data-align="center" data-formatter="function_formatter">Action</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
	var users = {!! json_encode($users) !!};
	$('#user_manager_table').bootstrapTable('load',users);	
	$("#func_remove").click(function(event){
		var selections=$('#user_manager_table').bootstrapTable('getSelections');
		if(selections.length>0){
			if(confirm('確定刪除此 '+selections.length+' 筆資料?')){
				var ids = selections.map(function(d){return d.id});
				$.post(url("/admin/users/delete"),{_token:window.Laravel.csrfToken,ids:ids},function(data){
					$('#user_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
				});
			}
		}
	});
});
function auth_formatter(value, row, index){
	switch(value){
		case 0:return '管理者';
		case 10:return '檔案作業';
		case 11:return 'Activity管理';
		default:return '';
	}
	return value;
}
function function_formatter(value, row, index){
	return '<a href="'+url("/admin/users/"+value+"/edit")+'" class="btn btn-default" >編輯</a>';
}
</script>
@endsection