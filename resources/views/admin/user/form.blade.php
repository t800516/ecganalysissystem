
@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">後台使用者管理 / {{ $user ? '編輯': '新增' }}後台使用者</h4>
			</div>
			<div id="user_manager">
				<form id="user_form" action="{{url('/admin/users')}}" method="post" class="form-horizontal">
				<fieldset>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<label for="name" class="control-label col-md-2">Account</label>
						<div class="controls col-md-8">
							<input type="text" name="account" id="account" value="{{$errors->count()!=0 ? old('account'):($user ? $user->account:'')}}" class="form-control" required>
						</div>
						<div class="col-md-2 account_msg"> </div>
					</div>
					<div class="form-group row">
						<label for="password" class="control-label col-sm-2">{{$user ? "修改":""}}密碼</label>
						<div class="controls col-sm-8">
							<input type="password" name="password" id="password" value="{{ $password or '' }}" class="form-control" {{$user ? '':'required'}}>
						</div>
						<div class="col-sm-2 password_msg"> </div>
					</div>
					<div class="form-group row">
						<label for="password_confirmation" class="control-label col-sm-2">確認密碼</label>
						<div class="controls col-sm-8">
							<input type="password" name="password_confirmation" value="{{ $password or '' }}" id="password_confirmation" class="form-control" {{$user ? '':'required'}}>
						</div>
						<div class="col-sm-2 password_confirmation_msg"></div>
					</div>
					<hr/>
					<div class="form-group">
						<label for="select_type" class="control-label col-md-2">權限類型</label>
						<div class="controls col-md-8">
							<select class="form-control" name="auth">
								<option value="0" {{$user && $user->auth == 0 ? 'selected':''}} >管理者</option>
								<option value="10" {{$user && $user->auth == 10 ? 'selected':''}} >檔案作業</option>
								<option value="11" {{$user && $user->auth == 10 ? 'selected':''}} >Activity管理</option>
							</select>
						</div>
						<div class="col-md-2 select_type_msg"> </div>
					</div>
					<hr/>
					{!! $user ? '<input type="hidden" name="id" value="'.$user->id.'">': '' !!}
					{{ csrf_field() }}
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-primary form-control" value="{{ $user ? '修改': '新增' }}">
				</div>
			</form>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
});

</script>
@endsection