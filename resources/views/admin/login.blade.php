@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/css/login.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		<div class="row">
			<form action="{{url('/admin/login')}}" method="post" class="form-horizontal">
				<fieldset>
				    <div>
				      <legend class="">後台登入</legend>
		    		</div>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<div class="controls col-md-12">
							<input type="text" name="account" id="account" class="form-control" placeholder="帳號" >
						</div>
					</div>
					<div class="form-group">
						<div class="controls col-md-12">
							<input type="password" name="password" id="password" class="form-control" placeholder="密碼" >
						</div>
					</div>
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-default form-control" value="立刻登入">
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script>
$(function(){
	$('#account').focus();
});
</script>
@endsection