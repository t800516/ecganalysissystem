@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/apk.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">程式管理</h4>
			</div>
			<div id="program_manager">
				<form id="form" class="form-horizontal mt-5 justify-content-center" action="{{url('/admin/programs')}}" method="POST" enctype="multipart/form-data">
        		{{ csrf_field() }}
				<fieldset class="col-12">
						<label for="z2b_report" class="col-form-label col-sm-2 label-file">z2b報表程式</label>
						<div class="col-sm-10">
							<label class="cursor-pointer" >
								<div class="form-control filename {{$programs['z2b_report'] ? '':'hide'}}">z2b_report</div>
								<input type="file" name="z2b_report" id="z2b_report" class="form-control fileinput {{$programs['z2b_report'] ? 'hide':''}}">
							</label>
						</div>
				</fieldset>
				<fieldset class="col-12">
						<label for="qrs_redetect" class="col-form-label col-sm-2 label-file">QRS Redetect程式</label>
						<div class="col-sm-10">
							<label class="cursor-pointer" >
								<div class="form-control filename {{$programs['qrs_redetect'] ? '':'hide'}}">qrs_redetect</div>
								<input type="file" name="qrs_redetect" id="qrs_redetect" class="form-control fileinput {{$programs['qrs_redetect'] ? 'hide':''}}">
							</label>
						</div>
				</fieldset>
				<button type="submit" name="action" value="save" class="btn btn-info">儲存</button>
				</form>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
	$('#z2b_report').on('change', function(event){
		if(event.target.files.length>0){
			$(this).siblings().html(event.target.files[0].name);
			$(this).siblings().addClass('hide');
			$(this).removeClass('hide');
		}else{
			$(this).siblings().addClass('hide');
			$(this).removeClass('hide');
		}
	});

	$('#qrs_redetect').on('change', function(event){
		if(event.target.files.length>0){
			$(this).siblings().html(event.target.files[0].name);
			$(this).siblings().addClass('hide');
			$(this).removeClass('hide');
		}else{
			$(this).siblings().addClass('hide');
			$(this).removeClass('hide');
		}
	});
});
</script>
@endsection