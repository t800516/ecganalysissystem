
@extends('layouts.admin')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/adminsidebar.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.adminSidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">報告管理 / {{ $department ? '編輯': '新增' }}報告</h4>
			</div>
			<div id="department_manager">
				<form id="department_form" action="{{url('/admin/department')}}" method="post" class="form-horizontal">
				<fieldset>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<label for="name" class="control-label col-md-2">Name</label>
						<div class="controls col-md-8">
							<input type="text" name="name" id="name" value="{{$errors->count()!=0 ? old('name'):($department ? $department->name:'')}}" class="form-control" required>
						</div>
						<div class="col-md-2 name_msg"> </div>
					</div>
					<div class="form-group">
						<label for="email" class="control-label col-md-2">Email</label>
						<div class="controls col-md-8">
							<input type="email" name="email" id="email" value="{{$errors->count()!=0 ? old('email'): ($department ? $department->email:'')}}" class="form-control" required>
						</div>
						<div class="col-md-2 email_msg"> </div>
					</div>
					<div class="form-group">
						<label for="phone" class="control-label col-md-2">Phone</label>
						<div class="controls col-md-8">
							<input type="text" name="phone" id="phone" value="{{$errors->count()!=0 ? old('phone') : ($department ? $department->phone:'')}}" class="form-control">
						</div>
						<div class="col-md-2 phone_msg"> </div>
					</div>
					<div class="form-group row">
							<label for="module_id" class="control-label col-sm-2">Module</label>
							<div class="controls col-sm-8">
								<select id="module_id" name="module_id" class="form-control">
									<option value="0" selected>No module</option>
									@foreach( $modules as $module)
										<option value="{{$module->id}}" {{($department && $department->module_id == $module->id )? 'selected' : ''}}>{{$module->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
					<hr/>
					<div class="form-group">
						<label for="select_type" class="control-label col-md-2">Parameter Type</label>
						<div class="controls col-md-8">
							<select class="form-control" name="select_type">
								<option value="0" {{$department && $department->select_type == 0 ? 'selected':''}} >Single</option>
								<option value="1" {{$department && $department->select_type == 1 ? 'selected':''}} >Multiple</option>
							</select>
						</div>
						<div class="col-md-2 select_type_msg"> </div>
					</div>
					<div class="form-group">
						<label for="report_type" class="control-label col-md-2">Report type</label>
						<div class="controls col-md-8 mt">
							<table class="table table-bordered table-striped " id="report_type_table">
								<thead>
									<tr>
										<th></th>
										<th class="text-center">名稱</th>
										<th class="text-center" width="15%">值</th>
										<th class="text-center">持續時間</th>
										<th class="text-center" width="30%">持續時間限制</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@if($department)
									@foreach($department->report_types as $key => $report_type)
										<tr>
											<td>
												<input type="hidden" name="report_types[{{$key}}][id]" value="{{$report_type->id}}" />
											</td>
											<td>
												<input type="text" name="report_types[{{$key}}][name]" class="form-control" value="{{$report_type->name}}" />
											</td>
											<td>
												<input type="text" name="report_types[{{$key}}][value]" class="form-control" value="{{$report_type->value}}" />
											</td>
											<td>
												<input type="checkbox" name="report_types[{{$key}}][duration]" class="form-control check_duration" value="1" {{$report_type->duration==1 ? 'checked':''}} />
											</td>
											<td>
												<div class="duration_box {{$report_type->duration==1 ? '':'hide'}}">
													<input type="number" min=0 name="report_types[{{$key}}][duration_low]" class="form-control col-md-6 duration" value="{{$report_type->duration_low}}" />
													<div class="duration-dash"> - </div>
													<input type="number" min=0 name="report_types[{{$key}}][duration_up]" class="form-control col-md-6 duration" value="{{$report_type->duration_up}}" />
												</div>
											</td>
											<td class="text-center"><span class="glyphicon glyphicon-remove remove"></span></td>
										</tr>
									@endforeach
									@endif
								</tbody>
							</table>
						</div>
						<div class="col-md-8 col-md-offset-2 text-center"><a href="#" class="btn btn-success" id="add_report_type" ><span class="glyphicon glyphicon-plus"></span></a></div>
					</div>
					<hr/>
					{!! $department ? '<input type="hidden" name="id" value="'.$department->id.'">': '' !!}
					{{ csrf_field() }}
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-primary form-control" value="{{ $department ? '修改': '新增' }}">
				</div>
			</form>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(function(){
	var report_type_index=0;
	$('#add_report_type').click(function(event){
		event.preventDefault();
		tr_html='<tr>'+
			'<td>'+
			'<input type="hidden" name="report_types[new_'+report_type_index+'][id]" value="0" />'+
			'</td>'+
			'<td>'+
			'<input type="text" class="form-control" name="report_types[new_'+report_type_index+'][name]" value="" />'+
			'</td>'+
			'<td>'+
			'<input type="text" class="form-control" name="report_types[new_'+report_type_index+'][value]" value="" />'+
			'</td>'+
			'<td>'+
				'<input type="checkbox" name="report_types[new_'+report_type_index+'][duration]" class="form-control check_duration" value="1" />'+
			'</td>'+
			'<td>'+
				'<div class="duration_box hide">'+
				'<input type="number" min=0 name="report_types[new_'+report_type_index+'][duration_low]" class="form-control col-md-6 duration"  />'+
				'<div class="duration-dash"> - </div>'+
				'<input type="number" min=0 name="report_types[new_'+report_type_index+'][duration_up]" class="form-control col-md-6 duration" />'+
				'</td>'+
				'</div>'+
			'<td class="text-center"><span class="glyphicon glyphicon-remove remove"></span></td>'+
		'</tr>';
		$('#report_type_table tbody').append(tr_html);
		report_type_index++;
	});
	$('#report_type_table').on('click', '.remove', function(event){
		event.preventDefault();
		if(confirm("確定刪除？")){
			$(this).parent().parent().remove();
		}
	});
	$('#report_type_table').on('change', '.check_duration', function(event){
		if($(this).prop('checked')){
			$(this).parent().parent().find('.duration_box').removeClass('hide');
		}else{
			$(this).parent().parent().find('.duration_box').addClass('hide');
		}
	});
	$('#IDNumber').focus();

	$("#register_form").submit(function(){
			
	});
});

</script>
@endsection