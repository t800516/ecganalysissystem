@extends('layouts.front')
@section('css_file')
    <link href="{{url('/css/apk.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="content">
        <article id="main" class="apk_penal">
            <h1>@lang("physiolguard.apk_title")</h1>
            <div class="row">
                 <div class="col-sm-12">
                    @if($apk && $apk->department )
                        @lang("physiolguard.apk_msg", ['name'=>$apk->department->name])
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                </div>
            	@if($apk)
                    <div class="col-sm-4 col-md-4">
                        <a href="{{$apk->url}}" class="download_a">
                            <div class="download_box">
                                <span class="glyphicon glyphicon-cloud-download"></span> 
                                <div>{{$apk->name}}</div>
                            </div>
                        </a>
                    </div>
            	@else
                    <div class="col-sm-4">
                        @lang("physiolguard.no_file")
                    </div>
            	@endif
            </div>
            <div class="row">
                 <div class="col-sm-12">
                    @if($apk)
                        @lang("physiolguard.upload_date"): {{$apk->updated_at}}
                    @endif
                </div>
            </div>
        </article>
    </div>
</div>
@endsection
@section('javascript')
<script>

</script>
@endsection