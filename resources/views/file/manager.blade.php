@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap4-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
	<link href="{{url('/css/guide.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/filemanager.css')}}" rel="stylesheet">
	<link href="{{url('/css/report_message.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			@include('layouts.model_select')
			<div id="tool_bar">
				<h4 class="pull-left title">{{Auth::user()->can('explorer_type') ? trans('physiolguard.report_title'):trans('physiolguard.ecg_title')}}</h4>
				<div class="tools">
					<div id="debug"></div>
					@cannot('explorer_type')
					<ul class="pull-right" data-spy="affix" data-offset-top="100">
						@if(Auth::user()->can('upload_ekg'))
						<li >
							<a href="{{url('/ecg/upload')}}" class="btn btn-default">
								<span class="glyphicon glyphicon-plus-sign"></span> 
								@lang('physiolguard.ecg_upload')
							</a>
						</li>
						@endif
						@if(Auth::user()->cannot('ecg_manager_model') && Auth::user()->cannot('data_manager_model'))
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="ecg_trans_btn">
								<span class="glyphicon glyphicon-retweet"></span> 
								@lang('physiolguard.ecg_translate')
							</a>
						</li>
						@endif
						<li class="dropdown selectedfunc funcs_btn">
							<a href="#" class="dropdown-toggle btn btn-default" data-toggle="dropdown" role="button" aria-expanded="false">
								<span class="glyphicon glyphicon-stats"></span> @lang('physiolguard.analysis')
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li class="selectedonefunc analysis_item">
									<a href="#" class="" id="ecg_analysis_btn" target="_blank">
										<span class="glyphicon glyphicon-stats"></span> 
										@lang('physiolguard.ecg_analysis')
									</a>
								</li>

								@if(Auth::user()->cannot('ecg_explorer_type'))
									<li class="selectedonefunc analysis_item">
										<a href="#" class="" id="hrv_analysis_btn">
											<span class="glyphicon glyphicon-stats"></span> 
											@lang('physiolguard.hrv_analysis')
										</a>
									</li>
								@endif
								
								@can('sleep_analysis')
									<li class="selectedonefunc analysis_item">
										<a href="#" class="" id="sleep_analysis_btn">
											<span class="glyphicon glyphicon-stats"></span> 
											@lang('physiolguard.sleep_analysis')
										</a>
									</li>
								@endcan
							</ul>
						</li>
						@if(Auth::user()->can('edit_ecg_info'))
							<li class="selectedonefunc">
								<a href="#" class="btn btn-default" id="func_editinfo"  data-toggle="modal" data-target="#editinfo_modal">
									<span class="glyphicon glyphicon-edit"></span>  
									@lang('physiolguard.edit_info')
								</a>
							</li>
						@endif
						@if(Auth::user()->cannot('ecg_explorer_type'))
						<li class="dropdown selectedfunc funcs_btn" id="funcs_btn">
							<a href="#" class="dropdown-toggle btn btn-default" data-toggle="dropdown" role="button" aria-expanded="false">
								<span class="glyphicon glyphicon-cog"></span> @lang('physiolguard.options')
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								@can('rr_download')
									<li class="selectedfunc">
										<a href="#" id="func_exportrr">
											<span class="glyphicon glyphicon-download-alt"></span> 
											@lang('physiolguard.rr_download')
										</a>
										<form action="{{url('/analysis/ecg/exportRR/')}}" id="export_rr_form" method="POST" target="_blank">
											{{csrf_field()}}
											<div>
												
											</div>
										</form>
									</li>
								@endcan

								<li class="selectedonefunc">
									<a href="#" id="analysis_report_btn" target="_blank">
										<span class="glyphicon glyphicon-list-alt"></span> 
										@lang('physiolguard.analysis_report')
									</a>
								</li>

								<li class="selectedfunc" id="report_download_btn_box">
									<a href="#" id="report_download_btn">
										<span class="glyphicon glyphicon-save"></span> 
										@lang('physiolguard.report_download')
									</a>
									<form action="{{url('/ecg/reports/download')}}" id="report_download_form" method="POST" target="_blank">
										{{csrf_field()}}
										<div>
											
										</div>
									</form>
								</li>

								@can('rename_ecg')
								<li class="selectedonefunc">
									<a href="#" id="func_rename" data-toggle="modal" data-target="#rename_modal">
										<span class="glyphicon glyphicon-text-background"></span> 
										@lang('physiolguard.rename')
									</a>
								</li>
								@endcan

								@can('sleep_analysis')
									<li class="selectedonefunc">
										<a href="#" id="func_getanalysisinfo" data-toggle="modal" data-target="#analysisinfo_modal">
											<span class="glyphicon glyphicon-list-alt"></span> 
											@lang('physiolguard.analysis_info')
										</a>
									</li>
								@endcan
								@can('QTC_report_download')
									<li class="selectedonefunc status_option">
										<a href="#" id="func_QTC_report">
											<span class="glyphicon glyphicon-download-alt"></span>
											@lang('physiolguard.QTC_report')
										</a>
									</li>
								@endcan
								<li class="selectedfunc">
									<a href="#" id="func_delete">
										<span class="glyphicon glyphicon-trash"></span> 
										@lang('physiolguard.delete')
									</a>
								</li>
							</ul>
						</li>
						@endif
					</ul>
					@endcannot
				</div>
				<div class="storage_box">
					<div class="progress">
  						<div class="progress-bar" role="progressbar" style="width: {{$use_storage_rate}}%;" aria-valuenow="{{$use_storage_rate}}" aria-valuemin="0" aria-valuemax="100">
  							{{$use_storage_rate}}%
  						</div>
					</div>
					<div class="storage">
						<span class="use">{{$use_storage}}</span> MB/
						<span class="total">{{$storage*1024}}</span> MB
					</div>
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-single-select="false" data-unique-id="id" data-show-header="true"  data-pagination="true" data-page-size="50" data-page-list="[25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams" data-group-by="true" data-group-by-field="upload_date" data-group-order-reverse="true" data-search-time-out="1000" {!!/*data-group-order-reverse="true" data-sort-name="created_at" data-sort-order="desc"*/'';!!} {{ Auth::user()->cannot('explorer_type') ? 'data-click-to-select="true" data-checkbox-header="true"':'' }}>
					<thead>
						@cannot('explorer_type')
							<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true" data-width="3%"></th>
						@elsecan('explorer_type')
							<th data-halign="center" data-align="center" data-width="3%" data-formatter="selection_formatter"></th>
						@endcannot

						@if(Auth::user()->role_id == 12)
							<th data-field="filename" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="86%">
								@lang('physiolguard.filename')</th>
						@elseif(Auth::user()->role_id != 7)
							<th data-field="filename" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="{{(Auth::user()->can('data_type') || Auth::user()->can('explorer_type') || Auth::user()->can('prescreening_report')) && Auth::user()->can('report_download') ?'20%':'30%'}}">
							@lang('physiolguard.filename')</th>
						@else
							<th data-field="filename" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="{{(Auth::user()->can('data_type') || Auth::user()->can('explorer_type') || Auth::user()->can('prescreening_report')) && Auth::user()->can('report_download') ?'35%':'45%'}}">
								@lang('physiolguard.filename')</th>
						@endif

						@if(Auth::user()->role_id != 12)
							<th data-field="patient_IDNumber" data-halign="center" data-align="center" data-width="{{Auth::user()->can('ecg_table_conversion') && Auth::user()->can('ecg_table_priority') ?'15%':'30%'}}">@lang('physiolguard.patient_IDNumber')</th>
						@endif
						
						@if(Auth::user()->role_id != 12)
							<th data-field="report_status" data-halign="center" data-align="center" data-formatter="report_status_formatter" data-width="10%">@lang('physiolguard.report_status')</th>
						@endif

						@if(Auth::user()->role_id != 7 && Auth::user()->role_id != 12)
							<th data-field="report_type" data-halign="center" data-align="center" data-width="15%" data-formatter="report_type_formatter">@lang('physiolguard.report_type')</th>
						@endif

						<th data-field="created_at" data-halign="center" data-align="center" data-width="10%">@lang('physiolguard.created_at')</th>

						@can('ecg_table_conversion')
							<th data-field="analysis_status" data-sortable="true" data-halign="center" data-align="center" data-formatter="analysis_status_formatter" data-width="10%">@lang('physiolguard.analysis_status')</th>
						@endcan
						@can('ecg_table_priority')
							<th data-field="priority" data-halign="center" data-align="center" data-formatter="priority_formatter" data-width="5%">@lang('physiolguard.priority')</th>
						@endcan

						@if((Auth::user()->can('data_type') || Auth::user()->can('explorer_type') || Auth::user()->can('prescreening_report')) && Auth::user()->can('report_download'))
							<th data-field="id" data-halign="center" data-align="center" data-formatter="action_formatter" data-width="10%">@lang('physiolguard.action')</th>
						@endif
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@can('rename_ecg')
<div id="rename_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rename_modal_label">
  	<div class="modal-dialog modal-sm" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="rename_modal_label">@lang('physiolguard.rename')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<h5>@lang('physiolguard.input_new_filename')</h5>
	  			<input id="rename_input" type="text" class="form-control">
	  			<div class="row">
	  				<button id="rename_cancel" class="btn btn-default col-md-3 col-md-offset-4" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="rename_save" class="btn btn-info col-md-3 col-md-offset-1">@lang('physiolguard.confirm')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
@endcan
<div id="editinfo_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editinfo_modal_label">
  	<div class="modal-dialog" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="editinfo_modal_label">@lang('physiolguard.edit_info')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			@cannot('ecg_manager_model')
	  				<h5>@lang('physiolguard.filename_ext'):</h5>
		  			<div id="editinfo_filename_ext" class="form-control">
		  			</div>
	  			@endcan
	  			<h5>@lang('physiolguard.patient_name'):</h5>
	  			<input id="editinfo_patient_name" type="text" class="form-control">
	  			<h5>@lang('physiolguard.description'):</h5>
	  			<textarea id="editinfo_description"  class="form-control">
	  			</textarea>
	  			<h5>@lang('physiolguard.birthday'):</h5>
	  			<input id="editinfo_birthday" type="text" class="form-control">
	  			<h5>@lang('physiolguard.gender'):</h5>
	  			<select id="editinfo_sex" class="form-control">
	  				<option value="M">@lang('physiolguard.gender_M')</option>
	  				<option value="F">@lang('physiolguard.gender_F')</option>
	  			</select>
	  			@cannot('ecg_manager_model')
	  				<h5>@lang('physiolguard.comment'):</h5>
		  			<textarea id="editinfo_comment" class="form-control">
		  			</textarea>
	  			@endcan
	  			<h5>@lang('physiolguard.patient_IDNumber'):</h5>
	  			<input id="editinfo_patient_IDNumber" type="text" class="form-control">

	  			@cannot('ecg_manager_model')
		  			<h5>@lang('physiolguard.holter_IDNumber'):</h5>
		  			<div id="editinfo_holter_IDNumber" class="form-control">
		  			</div>
	  			@endcan
	  			@cannot('ecg_manager_model')
	  			<h5>@lang('physiolguard.report_status'):</h5>
	  			<div id="editinfo_report_status" class="form-control">
	  			</div>
	  			@endcan
	  			<h5>@lang('physiolguard.report_type'):</h5>
	  			<div id="editinfo_report_type" class="form-control">
	  			</div>
	  			<div class="row mt-3">
	  				<button id="editinfo_cancel" class="btn btn-default col-md-3 col-md-offset-4" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="editinfo_save" class="btn btn-info col-md-3 col-md-offset-1">@lang('physiolguard.confirm')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
<div id="analysisinfo_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="analysisinfo_modal_label">
  	<div class="modal-dialog" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="analysisinfo_modal_label">@lang('physiolguard.analysis_info')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<div class="row">
	  				<div id="analysisinfo_data">
	  				</div>
	  				<button id="analysisinfo_close" class="btn btn-default col-md-4 col-md-offset-4" data-dismiss="modal">@lang('physiolguard.close')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
<div id="report_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="report_modal_label">
  	<div class="modal-dialog modal-md" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="report_modal_label">@lang('physiolguard.analysis_report')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<h5>@lang('physiolguard.do_select'):</h5>
	  			<div class="report_type_list" id="report_type_list">
	  				@if($department)
	  					@can('report_type_by_comment')
	  						<div class="comment">
								<h4 class="section_title">@lang("physiolguard.comment")</h4>
								<div id="comment" class="comment">
									<textarea id="report_type_comment_input" class="form-control" style="min-height: 240px;resize: vertical"></textarea>
								</div>
							</div>
	  					@else
			  				@foreach($department->report_types as $key => $report_type)
					  			<div class="row">
					  				<label class="col-md-5">
					  					<input type="{{ $department->select_type==0 ? 'radio':'checkbox' }}" value="{{$report_type->value}}" name="report_tpye" class="report_tpye"  data-name="{{$report_type->name}}">{{$report_type->name}} 
					  				</label>
					  				@if($report_type->duration==1)
						  				<div class="col-md-7">
							  				<div class="col-md-6 text-right">
							  					@lang('physiolguard.duration_time')[{{$report_type->duration_low.'-'.$report_type->duration_up}}]:
							  				</div>
							  				<label class="col-md-6">
							  					<input type="number" min="{{$report_type->duration_low}}" value="{{$report_type->duration_low}}" max="{{$report_type->duration_up}}" name="report_tpye_duration" class="report_tpye_duration form-control" data-name="{{$report_type->name}}">
							  				</label>
						  				</div>
					  				@endif
					  			</div>
				  			@endforeach
				  		@endcan
		  			@endif
	  			</div>
	  			<div class="row">
	  				<button id="report_cancel" class="btn btn-default col-md-3 col-md-offset-3" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="report_create" class="btn btn-info col-md-4 col-md-offset-1">@lang('physiolguard.create_report')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
<div id="report_download_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="report_modal_download_label">
  	<div class="modal-dialog modal-md" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="report_modal_download_label">@lang('physiolguard.analysis_report')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<div class="row">
	  				<div class="col-md-12" style="margin-bottom: 2em">
	  				@lang('physiolguard.create_report_msg_1')<span class="report_type"></span>@lang('physiolguard.create_report_msg_2')?
	  				</div>
	  			</div>
	  			@can('send_report')
		  			<div class="row">
		  				<div class="col-md-12" style="margin-bottom: 2em">
		  					<form id="report_email_form" action="{{url('/analysis/ecg/report/email')}}" method="POST">
		  					{{csrf_field()}}
		  					<input type="hidden" name="report_ecg_id" id="report_ecg_id">
		  					<div class="col-sm-3 title">
		  						@lang('physiolguard.send_report_to')
		  					</div>
		  					<div class="col-sm-6">
		  						<input type="email" class="form-control" name="report_email" id="report_email"  placeholder="E-mail" required>
		  					</div>
		  					<button id="report_email_send" class="btn btn-success col-sm-2" type="submit">@lang('physiolguard.send_report')</button>
		  					</form>
		  				</div>
		  				<div class="col-sm-12" id="send_message">
							<div id="send_message_content" class="title text-center">

							</div>
		  					<div id="send_message_loading" class="ajax_loading text-center">
								<img src="/assets/img/loading.gif">
							</div>
		  				</div>
		  			</div>
	  			@endcan
	  			<div class="row">
	  				<button id="report_download_cancel" class="btn btn-default col-md-2 col-md-offset-1" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="report_download" class="btn btn-info col-md-2 col-md-offset-1">@lang('physiolguard.download')</button>
	  				<button id="report_create_show" class="btn btn-info col-md-4 col-md-offset-1">@lang('physiolguard.recreate_report')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
<div id="report_creating_msg" class="hide">
	<div class="text-center msg_box">
		<div class="icon">
			<span class="glyphicon glyphicon-repeat"></span>
		</div>
		<span class="text">@lang('physiolguard.report_generating')</span>
	</div>
</div>
@endsection
@section('javascript')

<script src="{{asset('thirdpart/moment/moment.js')}}"></script>
<script src="{{asset('thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script src="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.js')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}"></script>
<script src="{{asset('thirdpart/bootstrap4-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{url('/js/guide.js')}}"></script>
<script>
var department = {!! json_encode($department) !!};
var evtSource = null;
$(document).ready(function() {
	$('#tool_bar .tools .selectedfunc').hide();
	
	$('#file_manager_table').on('check.bs.table uncheck.bs.table uncheck-all.bs.table check-all.bs.table',function(event, row, $element){
			var selections=$('#file_manager_table').bootstrapTable('getSelections');
			set_analysis_url(selections);
			selected_funcs_show(selections);
			selected_show_report_download(selections);
	});
	$('#file_manager_table').on('load-success.bs.table', function(data){
		updateECGStatus();
	});
	selected_funcs_show([]);
	selected_show_report_download([]);
	$('#func_delete').on('click',function(event){
		ecg_del($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#rename_modal').on('shown.bs.modal',function(event){
		show_rename($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#rename_save').on('click',function(event){
		ecg_rename($('#rename_input').val(),$('#rename_input').data('ecg_id'));
	});

	$('#editinfo_modal').on('shown.bs.modal',function(event){
		show_editinfo($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#analysisinfo_modal').on('shown.bs.modal',function(event){
		show_analysisinfo($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#file_manager_table').on('click', '.prescreening_report_btn',function(event){
		event.preventDefault();
		analysis_report_handler($(this).data('ecg_id'));
	});

	$('#editinfo_save').on('click',function(event){
		var info_data={
			'description':$('#editinfo_description').val(),
			'patient_name':$('#editinfo_patient_name').val(),
			'patient_IDNumber':$('#editinfo_patient_IDNumber').val(),
			'birthday':$('#editinfo_birthday').val(),
			'sex':$('#editinfo_sex').val(),
			'comment':$('#editinfo_comment').val()
		};
		$('#file_manager_table').bootstrapTable('updateByUniqueId',{
			id: $(this).data('ecg_id'),
			row: info_data
		});
		ecginfo_update(info_data,$(this).data('ecg_id'));
	});

	$('#func_exportrr').on('click',function(event){
		event.preventDefault();
		var ecg_ids = $(this).data('ecg_ids');
		export_rr(ecg_ids);
	});

	$('.analysis_item').on('click', 'a', function(event){
		event.preventDefault();
		var href = $(this).attr('href');
		$('#file_manager_table').bootstrapTable('uncheckAll');
		$('#file_manager_table input[name=btSelectGroup]').prop('checked', false);
		window.open(href)
		return false
	});

	$('#ecg_trans_btn').click(function(event){
		event.preventDefault();
		var ecg_ids = $(this).data('ecg_ids');
		var lastResponseLength = false;
		$.ajax({
			type: 'POST',
			url: url('ecg/transfer'),
			data: JSON.stringify({"ecg_ids":ecg_ids}), 
			dataType: 'json',
			contentType: "application/json",
			processData: false,
			xhrFields: {
				onprogress: function(e){
					var progressResponse;
					var response = e.currentTarget.response;
					if(lastResponseLength === false){
						progressResponse = response;
						lastResponseLength = response.length;
					}else{
						progressResponse = response.substring(lastResponseLength);
						lastResponseLength = response.length;
					}
					var data = JSON.parse(progressResponse);
					var rowData = $('#file_manager_table').bootstrapTable('getRowByUniqueId',data.id);
					if(rowData && 'analysis_status' in rowData){
						rowData.analysis_status = data.status;
						$('#file_manager_table').bootstrapTable('updateByUniqueId', data.id, rowData);
						if(rowData.analysis_status==2){
							selected_funcs_show([rowData]);
						}
					}
				}
			}
		}).done(function( data ) {
			console.log(data)
		});
	});
	
	$('#analysis_report_btn').click(function(event){
		event.preventDefault();
		if($(this).data('filename_ext')=='z2b' || $(this).data('filename_ext')=='xml'){
			analysis_report_handler($(this).data('ecg_id'));
		}else{
			show_report_modal($(this).data('ecg_id'));
		}
	});

	$('#func_QTC_report').click(function(event){
		event.preventDefault();
		if(	$(this).data('filename_ext')=='psg' ||
			$(this).data('filename_ext')=='z2b' || 
			$(this).data('filename_ext')=='xml'){
			var row = $('#file_manager_table').bootstrapTable('getRowByUniqueId', $(this).data('ecg_id'));
			var ecg_id = row.id;
			window.open(url('analysis/z2b/'+ecg_id+'/report/qtc'));
		}
	});

	$('#report_create').click(function(event){
		if(!check_report_duration()){
			return false;
		}
		var report_department = $(this).data('department');
		var report_type=$('#report_modal input[name="report_tpye"]:checked').map(function(index, item){
			var report_tpye_duration = $(item).parent().parent().find('.report_tpye_duration');
			report_tpye_duration = report_tpye_duration.length > 0 ? report_tpye_duration.val() : '';
			return $(item).val()+report_tpye_duration;}).toArray().join(',');
		var ecg_id = $(this).data('ecg_id');
		var report_comment_input = $('#report_type_comment_input');
		var report_comment = report_comment_input.length ? report_comment_input.val() : '';
		$('#report_creating_msg').removeClass('hide');
		if( report_department == "QTC"){
			window.open(url('analysis/z2b/'+ecg_id+'/report?type='+report_type+(report_department ? ('&department='+report_department):'')+(report_comment_input.length ? ('&comment='+report_comment):'')));
			$('#report_modal').modal('hide');
			$('#report_creating_msg').addClass('hide');
		}else{
			$.get(url('analysis/z2b/'+ecg_id+'/report?type='+report_type+(report_department ? ('&department='+report_department):'')+(report_comment_input.length ? ('&comment='+report_comment):'')),function(data){
				var rowData = $('#file_manager_table').bootstrapTable('getRowByUniqueId',ecg_id);
				rowData.report_status = data.report_status;
				$('#file_manager_table').bootstrapTable('updateByUniqueId', data.id, rowData);
				$('#report_modal').modal('hide');
				$('#report_creating_msg').addClass('hide');
			});
		}
	});

	$('#report_download').click(function(event){
		if(!check_report_duration()){
			return false;
		}
		var report_department = $(this).data('department');
		var report_type=$('#report_modal input[name="report_tpye"]:checked').map(function(index, item){
			var report_tpye_duration = $(item).parent().parent().find('.report_tpye_duration');
			report_tpye_duration = report_tpye_duration.length > 0 ? report_tpye_duration.val() : '';
			return $(item).val()+report_tpye_duration;}).toArray().join(',');
		window.open(url('analysis/z2b/'+($(this).data('ecg_id'))+'/report/download?type='+report_type+(report_department ? ('&department='+report_department):'')));
		$('#report_download_modal').modal('hide');
	});

	$('#report_download_btn').click(function(event){
		var rows = $('#file_manager_table').bootstrapTable('getSelections');
		if(rows.length){
			var query_string = '';
			$('#report_download_form div').empty();
			for (var i = 0; i < rows.length; i++) {
				$('#report_download_form div').append('<input type="hidden" name="ids[]" value="'+rows[i].id+'" />');
			}
			$('#report_download_form').submit();
		}
	});

	$('#report_create_show').click(function(event){
		event.preventDefault();
		$('#send_message_content').html('');
		if($(this).data('filename_ext')=='z2b' || $(this).data('filename_ext')=='xml'){
			show_report_modal($(this).data('ecg_id'), $(this).data('department'));
		}
	});

	function analysis_report_handler(ecg_id){
		var row = $('#file_manager_table').bootstrapTable('getRowByUniqueId', ecg_id);
		
		$.get(url('/analysis/z2b/'+row.id+'/report/exists'),function(response){
				if($('#report_type_list').data('QTC')){
					$('#report_type_list').html(response.report_type_inputs);
					$('#report_type_list').data('QTC',false);
				}else{
					if(!$('#report_type_list > .row').length){
						$('#report_type_list').html(response.report_type_inputs);
						$('#report_type_list').data('QTC',false);
					}
				}
				if(response.exists==1){
					$('#report_download_modal').modal('show');
					$('#report_download_modal .report_type').html('');
					$('#report_download').data('ecg_id', row.id);
					$('#report_download').data('department', false);
					$('#report_ecg_id').val(row.id);
					$('#report_create_show').data('ecg_id', row.id).data('filename_ext', row.filename_ext).data('report_type',row.report_type);
				}else{
					show_report_modal(row.id);
				}
			});
	}

	function check_report_duration(){
		var report_type_duration = $('#report_modal input[name="report_tpye_duration"]');
		for (var i = 0; i <report_type_duration.length; i++) {
			var duration = report_type_duration.eq(i).val();
			var min = parseInt(report_type_duration.eq(i).attr('min'), 10);
			var max = parseInt(report_type_duration.eq(i).attr('max'), 10);
			if(duration < min || duration > max){
				bootbox.alert({
					message: '{{trans("physiolguard.out_of_range")}}',
					locale: lang
				});
				return false;
			}
		}
		return true;
	}

	function show_report_modal(ecg_id, report_department)
	{
		$.get(url('/analysis/ecg/'+ecg_id+'/peak/check'),function(data){
			var result = data.data;
			$('#report_download_modal').modal('hide');
			$('#report_modal').modal('show');
			$('#report_modal input[name="report_tpye"]').each(function(i,d){
				if(result && result.peak_AN==0){
					if($(d).data('name')=='Normal sinus rhythm'){
						$(d).prop('checked',true);
					}else{
						$(d).prop('checked',false);
					}
				}else{
					if($(d).data('name')=='Abnormal rhythm or beat'){
						$(d).prop('checked',true);
					}else{
						$(d).prop('checked',false);
					}
				}
			});
			$('#report_create').data('ecg_id', ecg_id);
			if(report_department){
				$('#report_create').data('department', report_department);
			}
		});
	}
	$('#report_email_form').submit(function(event){
		var ecg_id = $('#report_ecg_id').val();
		var report_email = $('#report_email').val();
		$('#send_message_content').html('@lang("physiolguard.sending")');
		$('#send_message_loading').show();
		$.post(url('/analysis/ecg/report/email'),{ecg_id:ecg_id,report_email:report_email},function(data){
			$('#send_message_loading').hide();
			if(data.send==1){
				$('#send_message_content').html('<span class="text-success">@lang("physiolguard.send_success")</span>');
			}else{
				$('#send_message_content').html('<span class="text-danger">@lang("physiolguard.send_failed")</span>');
			}
		});
		return false;
	});
	$('#editinfo_birthday').datetimepicker({
	format:'YYYY-MM-DD',
	showTodayButton:true

	});
});
function updateECGStatusHandler(event){
	if(evtSource){
		try {
			var ecgStatus = JSON.parse(event.data)
		} catch (e) {
			evtSource.removeEventListener("message", updateECGStatusHandler)
			evtSource.close();
			evtSource = null
			return false
		}
		if(ecgStatus.length === 0){
			evtSource.removeEventListener("message", updateECGStatusHandler)
			evtSource.close();
			evtSource = null
			return false
		}else{
			for(const item of ecgStatus){
				var rowData = $('#file_manager_table').bootstrapTable('getRowByUniqueId', item.id);
				if(rowData){
					for(const key in item ){
						rowData[key] = item[key]
					}
					$('#file_manager_table').bootstrapTable('updateByUniqueId', item.id, rowData);
				}
			}
		}
	}
}
function updateECGStatus(ecg_ids=[]){
	if(evtSource){
		evtSource.removeEventListener("message", updateECGStatusHandler)
		evtSource.close();
	}
	var rows = $('#file_manager_table').bootstrapTable('getData');
	evtSource = new EventSource(url('/api/v0/ecgs/status?ecg_ids='+rows.map(d=>d.id).join(',')), {
		withCredentials: true,
	});
	evtSource.addEventListener("message", updateECGStatusHandler)
}
function queryParams(params) {
	return params
}

function ajaxRequest(params) {
	$.get(url('/api/v0/ecgs/data') + '?' + $.param(params.data)).then(function (res) {
	  params.success(res);
	})
}
 
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}

function set_analysis_url(selections){
	if(selections.length>0){
		$('#ecg_trans_btn').data('ecg_ids',selections.map(function(d){return d.id;}));
		$('#func_exportrr').data('ecg_ids',selections.map(function(d){return d.id;}));
		$('#ecg_analysis_btn').attr('href',url('analysis/ecg/'+selections[0].id));
		$('#hrv_analysis_btn').attr('href',url('analysis/hrv/'+selections[0].id));
		$('#sleep_analysis_btn').attr('href',url('analysis/sleep/'+selections[0].id));
		if(selections[0].filename_ext=='psg'){
			$('#analysis_report_btn').attr('href',url('analysis/ecg/report/'+selections[0].id)).data('filename_ext','psg');
			$('#func_QTC_report').attr('href',url('analysis/ecg/report/'+selections[0].id)).data('filename_ext','psg').data('department','QTC');
		
		}else{
			$('#analysis_report_btn').attr('href','#').data('filename_ext','z2b').data('ecg_id',selections[0].id).data('report_type',selections[0].report_type);
			$('#func_QTC_report').attr('href','#').data('filename_ext','z2b').data('ecg_id',selections[0].id).data('department','QTC');
		}
	}else{
		$('#ecg_trans_btn').attr('href','#');
		$('#ecg_analysis_btn').attr('href','#');
		$('#hrv_analysis_btn').attr('href','#');
		$('#sleep_analysis_btn').attr('href','#');
	$('#analysis_report_btn').attr('href','#').data('filename_ext',false);
	$('#func_QTC_report').attr('href','#').data('filename_ext',false);
	
	}
}
function selection_formatter(value, row, index){
	return '';
}
function filename_formatter(value, row, index){
	var icon='<span class="glyphicon glyphicon-file"></span> ';
	var memo= row['analysis_status'] == 10 ? '<span class="analysis_status_memo">@lang("physiolguard.transfer_failed")</span>' : '';
	var desc='<span class="file_desc pull-right">'+row['description']+'</span>';
	return icon+'<span>'+value+'</span>'+memo+desc;
}
function filesize_formatter(value, row, index){
	return (value/(1024*1024)).toFixed(2)+' MB';
}
function report_status_formatter(value, row, index){
	return getReportStatus(value)
}

function report_type_formatter(value, row, index){
	var type_string = '';
	var report_type = value || '';
	var report_types = department ? department.report_types : [];
	if(report_type=='-1'){
		return '@lang("physiolguard.report_type_-1")';
	}
	for (var i = 0; i < report_types.length; i++) {
		if(report_type.indexOf(report_types[i].value) != -1){
			type_string += type_string == '' ? report_types[i].name : ', '+report_types[i].name;
		}
	}
	return type_string;
}
function analysis_status_formatter(value, row, index){
	switch(value){default:
		case 0:return '@lang("physiolguard.analysis_status_0")';
		case 9:return '@lang("physiolguard.analysis_status_9")';
		case 1:return '@lang("physiolguard.analysis_status_1")';
		case 92:return '@lang("physiolguard.analysis_status_92")';
		case 93:return '@lang("physiolguard.analysis_status_93")';
		case 82:return '@lang("physiolguard.analysis_status_82")';
		case 2:return '@lang("physiolguard.analysis_status_2")';
		case 10:return '@lang("physiolguard.analysis_status_10")';
		case 11:return '@lang("physiolguard.analysis_status_11")';
	}
}
function priority_formatter(value, row, index){
	return value == '1' ? '<p class="text-danger">real-time</p>':'';
}
function action_formatter(value, row, index){
	var button_html='';
	@if(Auth::user()->can('prescreening_report'))
		button_html += '<a href="#" class="btn btn-info prescreening_report_btn" '+(row.report_status==1 || row.report_status==3 ? 'disabled':'')+' data-ecg_id="'+value+'">{{trans('physiolguard.report_download')}}</a>';
	@else
		var report_type=row.report_type;
		button_html += '<a class="btn btn-info" '+(row.report_status==1 || row.report_status==3 ? 'disabled':'')+' target="_blank" href="'+url('analysis/z2b/'+(value))+'/report/download?report_type='+report_type+'">@lang("physiolguard.download_report")</a>';
	@endif
	return button_html;
}

function selected_funcs_show(selections){
	if(selections.length>0){
		$('#tool_bar .tools .selectedfunc').show();
		selected_onefunc_show(selections);
		return true;
	}else{
		$('#tool_bar .tools .selectedfunc').hide();
		$('#tool_bar .tools .selectedonefunc').hide();
		return false;
	}
}

function selected_onefunc_show(selections){
	if(selections.length==1){
		if(selections[0].analysis_status==2){
			$('#tool_bar .tools .selectedonefunc').show();
			if(selections[0].filename_ext!='z2b' && selections[0].filename_ext!='xml'){
				$('#analysis_report_btn').parent().hide();
			}
		}else{
			$('#tool_bar .tools .selectedonefunc').hide();
		}
		//$('#funcs_btn').show();
		return true;
	}else{
		$('#tool_bar .tools .selectedonefunc').hide();
		//$('#funcs_btn').hide();
		return false;
	}
}

function selected_show_report_download(selections){
	if(selections.length){
		var pass = true;
		for (var i = 0; i < selections.length; i++) {
			if(selections[i].report_status != 2){
				pass= false;
			}
		}
		if(pass){
			$('#report_download_btn_box').show();
			return true;
		}else{
			$('#report_download_btn_box').hide();
			return false;
		}
	}else{
		$('#report_download_btn_box').hide();
		return false;
	}
}
@can('rename_ecg')
function ecg_rename(filename,ecg_id){
	$.post(url('/ecg/manager/rename'),{_token:window.Laravel.csrfToken,filename:filename,id:ecg_id},function(result){
		if(result.response=="ok"){
			var index=$('#file_manager_table tr.selected').data('index');
			$('#file_manager_table').bootstrapTable('updateCell',{index:index,field:'filename',value:result.filename});
			$('#rename_modal').modal('hide');
		}else{
			bootbox.alert({
					message: result.msg,
					locale: lang
				});
		}
	});
}
function show_rename(selections){
	if(selections.length==1){
		$('#rename_input').val(selections[0].filename).select().data('ecg_id',selections[0].id);
	}else{
	}
}
@endcan


function export_rr(selections){
	if(selections.length){
		$('#export_rr_form div').empty();
		for (var i = 0; i < selections.length; i++) {
			$('#export_rr_form div').append('<input type="hidden" name="ids[]" value="'+selections[i]+'" />');
		}
		$('#export_rr_form').submit();
	}
}

function show_analysisinfo(selections){
	if(selections.length==1){
		$('#analysisinfo_data').empty();
		$('#analysisinfo_data').append('<h5>@lang("physiolguard.ecg_analysis_time"):</h5><div>'+selections[0].ecganalysis_time+' 秒'+'</div>');
		$('#analysisinfo_data').append('<h5>@lang("physiolguard.ecg_analysis_time"):</h5><div>'+selections[0].rranalysis_time+' @lang("physiolguard.second")'+'</div>');
		$.get(url('/analysis/ecg/info/'+selections[0].id),function(result){
			$('#analysisinfo_data').append('<h5>@lang("physiolguard.record_total_time"):</h5><div>'+result.data.total_time+' @lang("physiolguard.second")'+'</div>');
			var peak_number_str='';
			for(var index in result.data.peaks){
				peak_number_str+='<div>'+index+' : '+result.data.peaks[index]+'</div>'
			}
			$('#analysisinfo_data').append('<h5>@lang("physiolguard.second"):</h5><div>'+peak_number_str+'</div>');
		});
	}else{

	}
}
function show_editinfo(selections){
	if(selections.length==1){
		$('#editinfo_filename_ext').html(selections[0].filename_ext);
		$('#editinfo_patient_name').val(selections[0].patient_name);
		$('#editinfo_description').val(selections[0].description);
		$('#editinfo_birthday').val(selections[0].birthday);
		$('#editinfo_sex').val(selections[0].sex);
		$('#editinfo_comment').val(selections[0].comment);
		$('#editinfo_patient_IDNumber').val(selections[0].patient_IDNumber);
		$('#editinfo_holter_IDNumber').html(selections[0].holter_IDNumber);
		$('#editinfo_report_status').html(getReportStatus(selections[0].report_status));
		$('#editinfo_report_type').html(getReportType(selections[0].report_type));
		$('#editinfo_save').data('ecg_id',selections[0].id);
	}else{

	}
}
function ecginfo_update(update_data,ecg_id){
	update_data['_token']=window.Laravel.csrfToken;
	update_data['id']=ecg_id;
	$.post(url('/ecg/manager/update_info'),update_data,function(result){
		$('#editinfo_modal').modal('hide');
	});
}
function ecg_del(selections){
		if(selections.length==0){
			bootbox.alert({
					message: '@lang("physiolguard.select_file")',
					locale: lang
				});
		}else{
			var del_msg='';
			if(selections.length==1){
				del_msg='@lang("physiolguard.to_delete") '+selections[0].filename+' ?';
			}else{
				del_msg='@lang("physiolguard.to_delete_all") '+selections.length+' ?';
			}
			bootbox.confirm({
				message: del_msg,
				locale: lang,
				size: 'small',
				callback: function (result) {
					if(result){
						$.post(url('/ecg/manager/delete'),{_token:window.Laravel.csrfToken,ecgs:selections},function(result){
							if(result.del_success_count>0){
								var ids=$.map(selections, function (row){
									return row.id;
								});
								$('#file_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
								$('#tool_bar .tools .selectedfunc').hide();
							}
						});
					}
				}
			});
		}
}
function getReportType(type){
	return report_type_formatter(type, [], 0)
	switch(type){
		case '-1' :case -1 :return '@lang("physiolguard.report_type_other")';
		case '0' :return '@lang("physiolguard.report_type_0")';
		case '1' :return '@lang("physiolguard.report_type_1")';
		case '2' :return '@lang("physiolguard.report_type_2")';
		case '3' :return '@lang("physiolguard.report_type_3")';
		default:return type;
	}
}
function getReportStatus(status){
	switch(status){
		case 0 :return '@lang("physiolguard.report_status_0")';
		case 1 :return '@lang("physiolguard.report_status_1")';
		case 2 :return '@lang("physiolguard.report_status_2")';
		case 3 :return '@lang("physiolguard.report_status_3")';
		default:return status;
	}
}
</script>
@endsection