@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/export.css')}}" rel="stylesheet">

@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">@lang("physiolguard.export_title")</h4>
				<div class="tools">
					<ul class="pull-right">
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="ecg_export_btn">
								<span class="glyphicon glyphicon-export"></span>
								@lang("physiolguard.export")
							</a>
						</li>
					</ul>
				</div>
				<div class="date-rang-select input-group date-input col-sm-6 col-md-5 col-sx-5">
					<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_start" name="date_start" value="{{date('Y-m-d', strtotime(date('Y-m-d').' -1 month'))}}">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-minus"></span>
					</span>
					<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_end" name="date_end" value="{{date('Y-m-d')}}">
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed table-bordered" data-toolbar="#tool_bar" data-unique-id="id" data-show-header="true">
					<thead>
						<th data-field="filename" data-sortable="true" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="{{Auth::user()->can('data_type')?'20%':'30%'}}">@lang("physiolguard.filename")</th>
						<th data-field="filename_ext" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.filename_ext")</th>
						<th data-field="patient_IDNumber" data-sortable="true" data-halign="center" data-align="center" data-width="15%">@lang("physiolguard.patient_IDNumber")</th>
						<th data-field="report_status" data-sortable="true" data-halign="center" data-align="center" data-formatter="report_status_formatter" data-width="10%">@lang("physiolguard.report_status")</th>
						<th data-field="report_type" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.report_type")</th>
						<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.upload_date")</th>
						<th data-field="analysis_status" data-sortable="true" data-halign="center" data-align="center" data-formatter="analysis_status_formatter" data-width="10%">@lang("physiolguard.analysis_status")</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')

<script src="{{asset('thirdpart/moment/moment.js')}}"></script>
<script src="{{asset('thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
$(document).ready(function() {
	loadECG();
	$('#date_start').datepicker().on("changeDate", function(e) {
        loadECG();
    });
	$('#date_end').datepicker().on("changeDate", function(e) {
		loadECG();
	});
	$('#ecg_export_btn').click(function(event){
		event.preventDefault();
		var query = {
			start : $('#date_start').val(),
			end : $('#date_end').val()
		};
		window.open(url("{{isset($type) ? '/data'.(isset($poc) ? '/users/'.$poc->id : '').'/users/'.$user->id : ''}}/ecg/export/csv")+'?'+$.param( query));
	});
});
function loadECG(){
	var query = {
		start : $('#date_start').val(),
		end : $('#date_end').val()
	};
	$.get(url("{{isset($type) ? '/data'.(isset($poc) ? '/users/'.$poc->id : '').'/users/'.$user->id:''}}/ecg/export/data"),query,function(data){
		var files = data.ECGs;
		$('#file_manager_table').bootstrapTable('load',files);
	});
}
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}

function filename_formatter(value, row, index){
	var icon='<span class="glyphicon glyphicon-file"></span> ';
	var desc='<span class="file_desc pull-right">'+row['description']+'</span>'
	return icon+value+desc;
}
function filesize_formatter(value, row, index){
	return (value/(1024*1024)).toFixed(2)+' MB';
}
function report_status_formatter(value, row, index){
	return getReportStatus(value)
}
function analysis_status_formatter(value, row, index){
	switch(value){default:
		case 0:return '@lang("physiolguard.analysis_status_0")';
		case 9:return '@lang("physiolguard.analysis_status_9")';
		case 1:return '@lang("physiolguard.analysis_status_1")';
		case 92:return '@lang("physiolguard.analysis_status_92")';
		case 82:return '@lang("physiolguard.analysis_status_82")';
		case 2:return '@lang("physiolguard.analysis_status_2")';
		case 10:return '@lang("physiolguard.analysis_status_10")';
		case 11:return '@lang("physiolguard.analysis_status_11")';
	}
}

function getReportType(type){
	switch(type){
		default:return '@lang("physiolguard.report_type_other")';
		case -1 :return '@lang("physiolguard.report_type_-1")';
		case 0 :return '@lang("physiolguard.report_type_0")';
		case 1 :return '@lang("physiolguard.report_type_1")';
		case 2 :return '@lang("physiolguard.report_type_2")';
		case 3 :return '@lang("physiolguard.report_type_3")';
	}
}
function getReportStatus(status){
	switch(status){
		default:return '@lang("physiolguard.report_status_other")';
		case 0 :return '@lang("physiolguard.report_status_0")';
		case 1 :return '@lang("physiolguard.report_status_1")';
		case 2 :return '@lang("physiolguard.report_status_2")';
	}
}
</script>
@endsection