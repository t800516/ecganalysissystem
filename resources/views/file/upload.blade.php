@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<!-- blueimp Gallery styles -->
	<link rel="stylesheet" href="{{url('/thirdpart/blueimp/blueimp-gallery.min.css')}}">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="{{url('/css/jquery.fileupload.css')}}">
	<link rel="stylesheet" href="{{url('/css/jquery.fileupload-ui.css')}}">
	<!-- CSS adjustments for browsers with JavaScript disabled -->
	<noscript>
		<link rel="stylesheet" href="{{url('/css/jquery.fileupload-noscript.css')}}">
	</noscript>
	<noscript>
		<link rel="stylesheet" href="{{url('/css/jquery.fileupload-ui-noscript.css')}}">
	</noscript>
	<link rel="stylesheet" href="{{url('/css/upload.css')}}">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4>@lang("physiolguard.ekg_upload_title")</h4>
			<!-- The file upload form used as target for the file upload widget -->
			<form id="fileupload" method="POST" enctype="multipart/form-data">
				<!-- Display an upload drop zone for the user -->
				<div id="dropzone" class="fade well">@lang("physiolguard.drop_to_upload")</div>
				<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				<div class="row fileupload-buttonbar">
					<div class="col-lg-7">
						<!-- The fileinput-button span is used to style the file input field as button -->
						<span class="btn btn-success fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>@lang("physiolguard.add_file")</span>
							<input type="file" name="files[]" multiple>
						</span>
						<button type="submit" class="btn btn-primary start">
							<i class="glyphicon glyphicon-upload"></i>
							<span>@lang("physiolguard.start_upload")</span>
						</button>
						<button type="reset" class="btn btn-warning cancel">
							<i class="glyphicon glyphicon-ban-circle"></i>
							<span>@lang("physiolguard.cancel_upload")</span>
						</button>
						<!-- The global file processing state -->
						<span class="fileupload-process"></span>
					</div>
					<!-- The global progress state -->
					<div class="col-lg-5 fileupload-progress fade">
						<!-- The global progress bar -->
						<div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
							 aria-valuemax="100">
							<div class="progress-bar progress-bar-success" style="width:0;"></div>
						</div>
						<!-- The extended global progress state -->
						<div class="progress-extended">&nbsp;</div>
					</div>
				</div>
				<!-- The table listing the files available for upload/download -->
				<table role="presentation" class="table table-striped">
					<tbody class="files"></tbody>
				</table>
			</form>
		</article>
	</div>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="col-md-6">
            <p class="name">{%=file.name%}</p>
            <div class="error" style="display: none"><span class="label label-danger">@lang("physiolguard.error")</span> <span class="error-msg"></span>
            <div class="error-modal">
            	<div class="modal fade" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								@lang("physiolguard.file_type_error")
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
							</div>
						</div>
					</div>
				</div>
            </div>
            </div>
        </td>
        <td class="col-md-4">
            <p class="size">@lang("physiolguard.uploading")</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0;"></div></div>
        </td>
        <td class="col-md-2">
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>@lang("physiolguard.start")</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>@lang("physiolguard.cancel")</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td class="col-md-6">
            <p class="name">
				<span>{%=file.name%}</span>
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">@lang("physiolguard.error")</span> {%=file.error%}</div>
            {% } else { %}
            	<div><span class="label label-success">@lang("physiolguard.success")</span></div>
            {% } %}
        </td>
        <td class="col-md-4">
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td class="col-md-2">
            {% if (!file.deleteUrl) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>@lang("physiolguard.cancel")</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
@endsection
@section('javascript')
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{{url('/thirdpart/jquery/jquery.ui.widget.js')}}"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="{{url('/thirdpart/blueimp/tmpl.min.js')}}"></script>
<!-- The basic File Upload plugin -->
<script src="{{url('/thirdpart/jquery-file-upload/jquery.fileupload.js')}}"></script>
<!-- The File Upload processing plugin -->
<script src="{{url('/thirdpart/jquery-file-upload/jquery.fileupload-process.js')}}"></script>
<!-- The File Upload validation plugin -->
<script src="{{url('/thirdpart/jquery-file-upload/jquery.fileupload-validate.js')}}"></script>
<!-- The File Upload user interface plugin -->
<script src="{{url('/thirdpart/jquery-file-upload/jquery.fileupload-ui.js')}}"></script>
<script>
	$(function () {
		'use strict';

		// Initialize the jQuery File Upload widget:
		$('#fileupload').fileupload({
			url: "{{ isset($type) ? url('/data/users/'.$user->id.'/ecg/upload?_token='.csrf_token()):url('/ecg/upload?_token='.csrf_token())}}",
			sequentialUploads: true,
			formData: {user_id: {{ Auth::user()->id }}},
			dropZone: $('#dropzone'),
			acceptFileTypes: /(\.|\/)(z2b|psg|xml|fat|atc)$/i
		});
	});

	$(document).bind('dragover', function (e) {
		var dropZone = $('#dropzone'),
				timeout = window.dropZoneTimeout;
		if (!timeout) {
			dropZone.addClass('in');
		} else {
			clearTimeout(timeout);
		}
		var found = false,
				node = e.target;
		do {
			if (node === dropZone[0]) {
				found = true;
				break;
			}
			node = node.parentNode;
		} while (node != null);
		if (found) {
			dropZone.addClass('hover');
		} else {
			dropZone.removeClass('hover');
		}
		window.dropZoneTimeout = setTimeout(function () {
			window.dropZoneTimeout = null;
			dropZone.removeClass('in hover');
		}, 100);
	});
</script>
@endsection
