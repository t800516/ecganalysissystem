@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/userprofile.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#account_content">@lang("physiolguard.account_title")</a></li>
				@if(false)
					<li><a data-toggle="tab" href="#profile_content">@lang("physiolguard.profile_title")</a></li>
				@endif
			</ul>
			<div class="tab-content">
				<div id="account_content" class="tab-pane fade in active">
					<form id="register_form" action="{{url('/user/account')}}" method="post" class="form-horizontal">
						<fieldset>
						    <ul class="errormsg list-group">
		 						@foreach($errors->all() as $key=>$error)
									<li class="list-group-item list-group-item-danger">{{$error}}</li>
								@endforeach
							</ul>
							<div class="form-group">
								<label class="control-label col-md-2">@lang("physiolguard.account"): </label>
								<div class="controls col-md-8">
									<div id="account" class="text-control">
										{{$email}}
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">@lang("physiolguard.password")</label>
								<div class="controls col-md-8">
									<a href="{{url('/resetpw')}}" class="text-control" >@lang("physiolguard.password_reset")</a>
								</div>
								<div class="col-md-2 email_msg"> </div>
							</div>
							{{ csrf_field() }}
						</fieldset>
					</form>
				</div>
				@if(false)
				<div id="profile_content" class="tab-pane fade">
					<form id="basic_form" action="{{url('/user/profile')}}" method="post" class="form-horizontal">
						<fieldset>
						    <ul class="errormsg list-group">
		 						@foreach($errors->all() as $key=>$error)
									<li class="list-group-item list-group-item-danger">{{$error}}</li>
								@endforeach
							</ul>
							
							<div class="form-group">
								<label for="name" class="control-label col-md-2">@lang('physiolguard.name')</label>
								<div class="controls col-md-8">
									<input type="text" name="name" id="name" class="form-control" value="{{$name}}" required>
								</div>
								<div class="col-md-2 name_msg"> </div>
							</div>
							<div class="form-group">
								<label for="sex" class="control-label col-md-2">@lang('physiolguard.gender')</label>
								<div class="controls col-md-8">
									<select id="sex" name="sex" class="form-control">
										<option value="" disabled {{($sex=="")? "selected
										":""}} hidden>@lang('physiolguard.do_select')</option>
										<option value="M" {{($sex=="M")? "selected
										":""}}>@lang('physiolguard.gender_M')</option>
										<option value="F" {{($sex=="F")? "selected
										":""}}>@lang('physiolguard.gender_F')</option>
									</select>
								</div>
								<div class="col-md-2 sex_msg"> </div>
							</div>
							<div class="form-group">
								<label for="birthday" class="control-label col-md-2">@lang('physiolguard.birthday')</label>
								<div class="col-md-4">
									<select id="birthday_year" class="form-control">
										<option value="0000" disabled {{($year=="0000")? "selected
										":""}} hidden>@lang('physiolguard.select_year')</option>
									</select>
								</div>
								<div class="col-md-2">
									<select id="birthday_month" class="form-control">
										<option value="00" disabled {{($month=="00")? "selected
										":""}} hidden>@lang('physiolguard.select_month')</option>
									</select>
								</div>
								<div class="col-md-2">
									<select id="birthday_day" class="form-control">
										<option value="00" disabled {{($day=="00")? "selected
										":""}} hidden>@lang('physiolguard.select_day')</option>
									</select>
								</div>
								<input type="hidden" name="birthday" id="birthday" value="{{$birthday}}">
								<div class="col-md-2 birthday_msg"> </div>
							</div>
							<div class="form-group">
								<label for="country" class="control-label col-md-2">@lang('physiolguard.country')</label>
								<div class="controls col-md-8">
									<select id="country" name="country" class="form-control">
										<option value="" disabled selected hidden>@lang('physiolguard.do_select')</option>
									</select>
								</div>
								<div class="col-md-2 age_msg"></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">@lang('physiolguard.address')</label>
								<div class="col-md-1">
									<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="@lang('physiolguard.zipcode')" value="{{$zipcode}}">
								</div>
								<div class="col-md-2">
									<select id="city" name="city" class="form-control">
										<option value="">@lang('physiolguard.select_country')</option>
									</select>
								</div>
								<div class="col-md-2" >
									<select id="downtown" name="downtown" class="form-control">
										<option value="">@lang('physiolguard.select_city')</option>
									</select>
								</div>
								<div class="col-md-3">
									<input type="text" id="address" name="address" class="form-control" placeholder="@lang('physiolguard.input_address')" value="{{$address}}">
								</div>
								<div class="col-md-2 address_msg"> </div>
							</div>
							<div class="form-group">
								<label for="tel" class="control-label col-md-2">@lang('physiolguard.telphone')</label>
								<div class="controls col-md-3">
									<input type="text" name="tel" id="tel" value="{{$tel}}" class="form-control">
								</div>
								<label for="cel" class="control-label col-md-1">@lang('physiolguard.celphone')</label>
								<div class="controls col-md-4">
									<input type="text" name="cel" id="cel" value="{{$cel}}" class="form-control">
								</div>
								<div class="col-md-2 tel_msg cel_msg"> </div>
							</div>
							{{ csrf_field() }}
						</fieldset>
						<div class="col-md-4 col-md-offset-4">
							<input type="submit" class="btn btn-primary form-control" value="@lang('physiolguard.save')">
						</div>
					</form>
				</div>
				@endif
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{url('/js/selectaddress.js')}}"></script>
<script src="{{url('/js/selectdate.js')}}"></script>
<script>
$(function(){
	$('#country').append(get_country_options());
	$('#country>option[value="{{$country}}"]').prop('selected',true);
	if($('#country').val()=='TW'){
		$('#city').append(get_city_options());
		$('#city>option[value="{{$city}}"]').prop('selected',true);
		select_downtown($('#city>option:selected').val(),'#downtown');
		$('#downtown>option[value="{{$downtown}}"]').prop('selected',true);

	}

	$('#country').change(function(){
		$('#city').empty();
		$('#downtown').empty();
		if($(this).val()=='TW'){
			$('#city').append(get_city_options());
			select_downtown($('#city>option:selected').val(),'#downtown');
		}else{
			$('#city').append('<option value="0">@lang("physiolguard.select_country")</option>');
			$('#downtown').append('<option value="0">@lang("physiolguard.select_city")</option>');
		}
	});
	$("#city").change(function(){
		select_downtown($(this).val(),'#downtown');
	});
	$('#birthday_year').append(get_year_options({{$year}}));
	$('#birthday_month').append(get_month_options({{$month}}));
	$('#birthday_day').append(get_day_options($('#birthday_month').val(),{{$day}}));
	$('#birthday_year,#birthday_month,#birthday_day').change(function(event){
		var year=$('#birthday_year').val();
		var month=$('#birthday_month').val();
		var day=$('#birthday_day').val();
		if(year==null)year='0000';
		if(month==null)month='00';
		if(day==null)day='00';
		$('#birthday').val(year+'-'+month+'-'+day);
	});
});
</script>
@endsection