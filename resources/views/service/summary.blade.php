@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/summary.css')}}?v={{env('CSS_VERSION','1.0.0')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="header_title">
		<button class="btn btn-success pull-right" id="export_btn">@lang('physiolguard.export')</button>
		<button class="btn btn-info pull-right" id="comment_btn" data-toggle="modal" data-target="#comment_modal">@lang('physiolguard.summary_comment')</button>
		<form id="export_form" method="POST" target="_blank" action="{{url('/services/'.$service->id.'/summary/export')}}" enctype="multipart/form-data">
			{{csrf_field()}}
			@if($poc)
				<input type="hidden" name="poc" value="{{$poc}}">
			@endif
			@if($patient)
				<input type="hidden" name="poc" value="{{$patient}}">
			@endif
			<input type="hidden" name="summary" id="summary_input">
		</form>
	</div>
	<div id="summary_page">
		<div class="section full-width">
			<div class="section-header">
				<div class="section-header-logo">
					<img src="{{url('assets/img/report_logo.png')}}"/>
				</div>
				<div class="section-header-text">
					心律安 - 居家心律量測服務報告
				</div>
				<div class="section-header-date">
					{{date('Y/m/d')}}
				</div>
			</div>
		</div>
		<div class="section">
			<div class="section-title row">
				<div class="section-title-text col-sm-2">
					@lang('physiolguard.patient_info')
				</div>
			</div>
			<div class="section-content row">
				<div class="table-row row">
					<div class="table-col col-sm-2 title">@lang('physiolguard.patient_IDNumber')</div>
					<div class="table-col col-sm-2">{{$service->patient_IDNumber}}</div>
					<div class="table-col col-sm-1 title">@lang('physiolguard.gender')</div>
					<div class="table-col col-sm-1">{{trans('physiolguard.gender_'.$service->sex)}}</div>
					<div class="table-col col-sm-1 title">@lang('physiolguard.birthday')</div>
					<div class="table-col col-sm-2">{{date('Y/m/d',strtotime($service->birthday))}}</div>
					<div class="table-col col-sm-1 title">@lang('physiolguard.patient_name')</div>
					<div class="table-col col-sm-2">{{$service->patient_name}}</div>
				</div>
			</div>
		</div>

		<div class="section">
			<div class="section-title row">
				<div class="section-title-text col-sm-2">
					@lang('physiolguard.measurement_data')
				</div>
			</div>
			<div class="section-content row">
				<div class="table-row row">
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_dates')</div>
					<div class="table-col col-sm-2">
						@if($record_num && $begin_recored_at)
							{{date('Y/m/d',strtotime($begin_recored_at))}}
						@endif
						@if($record_num && $last_recored_at)
							~ {{date('Y/m/d',strtotime($last_recored_at))}}
						@endif
					</div>
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_day')</div>
					<div class="table-col col-sm-2">{{$duration_days}}</div>
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_num')</div>
					<div class="table-col col-sm-2">{{$record_num}}</div>
				</div>
				<div class="table-row row">
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_normal_num')</div>
					<div class="table-col col-sm-2">{{$normal_num}}</div>
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_abnormal_num')</div>
					<div class="table-col col-sm-2">{{$abnormal_num}}</div>
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_unfinished_num')</div>
					<div class="table-col col-sm-2">{{$unfinished_num}}</div>
				</div>
				<div class="table-row row">
					<div class="table-col col-sm-2 title">@lang('physiolguard.abnormal_type')</div>
					<div class="table-col col-sm-10  text-left">
						@php 
							$n = 0;
						@endphp
						@foreach($abnormal_detail as $key => $detail)
							@if($n!=0),
							@endif
							{{ $detail["name"] }} 
							@if($detail["data"])
								({{$detail["data"]}})
							@endif
							@php
								$n++;
							@endphp
						@endforeach
					</div>
				</div>
			</div>
			<div class="font-bold" >@lang('physiolguard.holter_use')：<span> </span>@lang('physiolguard.holter_ID') {{ $holter_IDNumbers}}
			</div>
		</div>
		<div class="section">
			<div class="section-title row">
				<div class="section-title-text col-sm-3">
					@lang('physiolguard.heart_rhythm_abnormal_num')
				</div>
			</div>
			<div class="section-content row">
				<div class="table-row row">
					<div class="table-col col-sm-5 title">@lang('physiolguard.abnormal_type')</div>
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_total_num')</div>
					<div class="table-col col-sm-2 title">@lang('physiolguard.record_total_time')</div>
					<div class="table-col col-sm-3 title">@lang('physiolguard.record_avg')</div>
				</div>
				@php
					$abnormal_types = ['vpc', 'apc', 'afib'];
				@endphp
				@foreach($abnormal_types as $abnormal_type)
					@php
						$report_type = isset($report_type_array[$abnormal_type]) ? $report_type_array[$abnormal_type]: null;
					@endphp
						<div class="table-row row line-hight-small">
							<div class="table-col col-sm-5 text-left">
								@if($abnormal_type == 'vpc')
									Premature Ventricular Contraction
								@elseif($abnormal_type == 'apc')
									Premature Atrial Contraction
								@elseif($abnormal_type == 'afib')
									Atrial Fibrillation/Atrial Flutter
								@endif
							</div>
							<div class="table-col col-sm-2">{{$report_type ? $report_type["value"] : 0}}</div>
							<div class="table-col col-sm-2">
								@if(in_array($abnormal_type, ['vpc']))
									{{ $v_peak_num }}
								@elseif(in_array($abnormal_type, ['apc']))
									{{ $s_peak_num }}
								@else
									{{$report_type ? $report_type["value"] : 0}}
								@endif
							</div>
							<div class="table-col col-sm-3">
								@if(in_array($abnormal_type, ['vpc']))
									{{ $duration_days ? $v_peak_num / $duration_days : 0 }}
								@elseif(in_array($abnormal_type, ['apc']))
									{{ $duration_days ? $s_peak_num / $duration_days : 0 }}
								@else
									{{ $duration_days ? ( $report_type ? $report_type["value"] : 0) / $duration_days : 0 }}
								@endif
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="section">
			<div class="section-title row">
				<div class="section-title-text col-sm-3">
					@lang('physiolguard.heart_var_chart')
				</div>
			</div>
			<div class="section-content chart-content" id="heart_var_chart_container">
				<h3 class="text-center">@lang('physiolguard.average_heart_rate')</h3>
				<div id="heart_var_chart" class="chart-box" ></div>
			</div>
		</div>
		<div class="section">
			<div class="section-title row">
				<div class="section-title-text col-sm-3" >
					@lang('physiolguard.heart_abnormal_chart')
				</div>
			</div>
			<div class="section-content chart-content" id="heart_abnormal_chart_container">
				<h3 class="text-center">@lang('physiolguard.heart_abnormal_time')</h3>
				<div id="heart_abnormal_chart" class="chart-box"></div>
			</div>
		</div>

		<div class="section">
			<div class="row">
				<div class="col-sm-2 font-bold font-large">
					@lang('physiolguard.summary_comment')：
				</div>
				<div class="col-sm-10 font-bold font-large" id="comment">{!! preg_replace('/\r?\n/i','<br/>',$service->comment) !!}
				</div>
			</div>
		</div>
	</div>
</div>
<div id="comment_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="comment_modal_label">
  	<div class="modal-dialog" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="comment_modal_label">@lang('physiolguard.summary_comment')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<textarea id="comment_input" class="form-control">{!! $service->comment !!}</textarea>
	  			<div class="row mt-3">
	  				<button id="comment_cancel" class="btn btn-default col-md-3 col-md-offset-3" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="comment_save" class="btn btn-info col-md-3 col-md-offset-1">@lang('physiolguard.confirm')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{url('/thirdpart/d3/d3.min.js')}}"></script>
<script src="{{url('/thirdpart/html2canvas/html2canvas.min.js')}}"></script>
<script src="{{url('/thirdpart/jsPDF/jspdf.min.js')}}"></script>
<script>
	var summaryData = {!! json_encode($hours) !!};
	var service = {!! json_encode($service->toArray()) !!};
	$(function(){
		line_chart_init(summaryData, "#heart_var_chart_container", "#heart_var_chart");
		column_chart_init(summaryData, "#heart_abnormal_chart_container", "#heart_abnormal_chart");
		$(window).resize(function(){
			draw_line_chart(summaryData, "#heart_var_chart_container", "#heart_var_chart");
			draw_column_chart(summaryData, "#heart_abnormal_chart_container", "#heart_abnormal_chart");
		})
		$('#comment_save').click(function(event){
			event.preventDefault();
			$.post(url("/services/"+service['id']+"/summary/comment"),{"comment":$('#comment_input').val()}, function(data){
				$('#comment').html($('#comment_input').val());
				$('#comment_modal').modal('hide');
			})
		});
		$('#export_btn').click(function(event){
			event.preventDefault();
			var sections = $('#summary_page .section');
			var section_canvases = sections.map(function(key, section){
				return html2canvas(section);
			});
			Promise.all(section_canvases).then(canvases  => {
				var pdf = new jsPDF('', 'pt', 'a4');
				var a4Width = 595.28;
				var a4Height = 841.89;
				var pageHeight = 30;
				var margin = 0;
				var header_canvase;
				var header_canvase_height;
				for (var i = 0; i < canvases.length; i++) {
					let canvas = canvases[i];
					let contentWidth = canvas.width;
					let contentHeight = canvas.height;
					let imgWidth = (a4Width - margin);
					let imgHeight = a4Width/contentWidth * contentHeight;
					if( (1 * a4Height) < (pageHeight + imgHeight)){
						pageHeight = 30;
						pdf.addPage();
						pdf.addImage(header_canvase, 'JPEG', margin/2, pageHeight, imgWidth, header_canvase_height );
						pageHeight += header_canvase_height;
					}
					let pageData = canvas.toDataURL('image/jpeg', 1.0);
					pdf.addImage(pageData, 'JPEG', margin/2, pageHeight, imgWidth, imgHeight );
					pageHeight += imgHeight;
					if(i == 0){
						header_canvase = pageData;
						header_canvase_height = imgHeight;
					}
				}

				$('#summary_input').val(btoa(pdf.output()));
				$('#export_form').submit();
			});
		});
	});

	function line_chart_init(origin_data, container, selector){
		var categories = ['max_heart_rate','mean_heart_rate', 'min_heart_rate'];
		var categories_color = ['#F44336','#4CAF50','#2196F3'];
		var categories_text = ['Max Heart Rate','Mean Hart Rate', 'Min Heart Rate'];
		var data = categories.map(function(category){ return { category:category, data: origin_data.map(function(d, i){return d[category] ? [i, parseFloat(d[category]).toFixed(3)] : [i,0];}).filter(function(d){return d[1]!=0;})}});
		var margin = {top: 20, right: 20, bottom: 50, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 100;
		var maxX = origin_data.length;
		var origin_width = width;
		var maxY = d3.max(origin_data, function (d) {return d.max_heart_rate;}) + 20;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX -1]);
		var y = d3.scaleLinear().range([height,0]).domain([0, maxY]);
		
		var color = d3.scaleOrdinal().range(categories_color);
		color.domain(categories);
		
		var xAxis = d3.axisBottom().scale(x).ticks(24).tickFormat(function(d){return d+1;}).tickSize(0,0);

		var yAxis = d3.axisLeft().scale(y).ticks(8).tickSize(0,0);

		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var line = d3.line().x(function (d , i) { return x(d[0]); }).y(function (d) { return y(d[1]); });
		
		var chart = svg.append("g").attr("class","chart")
		
		var axis = svg.append("g")
					.attr("class", "axis");
		
		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);
      	svg.append("text")   
      	  	.attr("class", "axisx_text")          
      		.attr("transform", "translate(" + (width/2) + " ," +(height + margin.top + 15) + ")")
      		.style("text-anchor", "middle")
      	.text("Time (Hour)");

  		axis.append("g")
      		.attr("class", "axisy")
      		.call(yAxis);

      	svg.append("text")
      	  .attr("class", "axisy_text")  
	      .attr("transform", "rotate(-90)")
	      .attr("y", 0 - margin.left+10)
	      .attr("x",0 - (height / 2))
	      .attr("dy", "1em")
	      .style("text-anchor", "middle")
	      .text("HR (1/minute)"); 

      	var line_chart = chart
		    .selectAll(".category")
		    .data(data)
		    .enter()
		    .append("g")
		    .attr("class", "category");

		line_chart.append("path")
			.attr("class", "line")
			.attr("d",function(d) {
		      return line(d.data);
		    })
			.style("stroke", function(d) {
		      return color(d.category);
		    }).attr("stroke-width", 2).attr("fill","none");
		
		for(index in data){
			var points = line_chart.selectAll(".point_"+index)
		    .data(data[index].data)
	      	.enter().append("circle")
				.attr("class", ".point_"+index)
	         .style("stroke", function(d) {
			      return color(data[index].category);
			    })
	         .attr("fill", function(d, i) { return color(data[index].category) })
	         .attr("cx", function(d, i) { return x(d[0]) })
	         .attr("cy", function(d, i) { return y(d[1]) })
	         .attr("r", function(d, i) { return 3});
		}

		// var gridY = svg.append("g")
		// 			.attr("class","y-axis")
		// 			.attr("transform", "translate(0, 0)")
		// 			.call(yAxis.tickSize(-width).tickFormat(""));

		var legendstr = '<div class="legend">';
		for(var index in categories){
			legendstr +='<label for="legend_'+index+'">'+
					'<span class="glyphicon glyphicon-minus" style="color:'+color(categories[index])+'"></span>'+categories_text[index]+'</label>';
		}
		legendstr += '</div>';
		$(selector).append(legendstr);
	}

	function draw_line_chart(origin_data, container, selector){
		var categories = ['max_heart_rate','mean_heart_rate', 'min_heart_rate'];
		var categories_color = ['#F44336','#4CAF50','#2196F3'];
		var categories_text = ['Max Heart Rate','Mean Hart Rate', 'Min Heart Rate'];
		var data = categories.map(function(category){ return { category:category, data: origin_data.map(function(d, i){return d[category] ? [i, parseFloat(d[category]).toFixed(3)] : [i,0];}).filter(function(d){return d[1]!=0;})}});
		var margin = {top: 20, right: 20, bottom: 50, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 100;
		var maxX = origin_data.length;
		var origin_width = width;
		var maxY = d3.max(origin_data, function (d) {return d.max_heart_rate;}) + 20;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX -1]);
		var y = d3.scaleLinear().range([height,0]).domain([0, maxY]);
		
		var color = d3.scaleOrdinal().range(categories_color);
		color.domain(categories);
		
		var xAxis = d3.axisBottom().scale(x).ticks(24).tickFormat(function(d){return d+1;}).tickSize(0,0);

		var yAxis = d3.axisLeft().scale(y).ticks(8).tickSize(0,0);

		var svg = d3.select(selector)
			.select("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.select("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var line = d3.line().x(function (d , i) { return x(i[0]); }).y(function (d) { return y(d[1]); });
		
		var chart = svg.select("g.chart");
		
		var axis = svg.select("g.axis");
		
		axis.select("g.axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);

      	svg.select("text.axisx_text")             
      		.attr("transform", "translate(" + (width/2) + " ," +(height + margin.top + 15) + ")");

  		axis.select("g.axisy")
      		.call(yAxis);

      	svg.select("text.axisy_text")
	      .attr("y", 0 - margin.left+10)
	      .attr("x",0 - (height / 2)); 

      	var line_chart = chart.selectAll(".category");  

		line_chart.select("path.line")
			.attr("d",function(d) {
		      return line(d.data);
		    })
			.style("stroke", function(d) {
		      return color(d.category);
		    });

		for(index in data){
			line_chart.selectAll("circle.point_"+index)
	         .attr("cx", function(d, i) { return x(d[0]) })
		}

		// var gridY = svg.select("g.y-axis")
		// 			.attr("transform", "translate(0, 0)")
		// 			.call(yAxis.tickSize(-width).tickFormat(""));
	}

	function column_chart_init(origin_data, container,selector){
		var categories = ['abnormal_num','recored_num'];
		var categories_color = ['#F44336','#4CAF50'];
		var categories_text = ['異常次數','量測筆數'];
		var margin = {top: 20, right: 40, bottom: 50, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 100;
		var maxX = origin_data.length;
		var origin_width = width;
		var maxY = d3.max(origin_data, function (d) { return d.abnormal_num;})+5;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX-1]);
		var y = d3.scaleLinear().range([height,0]).domain([0, maxY]);
		bins = categories.map(function(category){ return { category:category, data:origin_data.map(function(d){return d[category];})}});
		var color = d3.scaleOrdinal().range(categories_color);
		color.domain(categories);
		
		var xAxis = d3.axisBottom().scale(x).ticks(24).tickFormat(function(d){return d+1;}).tickSize(0,0);

		var yAxis = d3.axisLeft().scale(y).ticks(maxY).tickSize(0,0);

		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var chart = svg.append("g").attr("class","chart")
		
		var axis = svg.append("g")
					.attr("class", "axis");
		
		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);
      	svg.append("text")
      		.attr("class", "axisx_text")
      		.attr("transform", "translate(" + (width/2) + " ," +(height + margin.top + 15) + ")")
      		.style("text-anchor", "middle")
      	.text("時間 (Hour)");

  		axis.append("g")
      		.attr("class", "axisy")
      		.call(yAxis);
      	svg.append("text")
      	  .attr("class", "axisy_text")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 0 - margin.left+10)
	      .attr("x",0 - (height / 2))
	      .attr("dy", "1em")
	      .style("text-anchor", "middle")
	      .text("紀錄數"); 

		chart.selectAll(".bar")
			.data( bins)
			.enter()
			 	.append("g") // Uses the enter().append() method
				.attr("class", "bar") // Assign a class for styling
				.attr("transform", function(d, i) {return "translate("+(i * width/48)+", 0)"; })
		        .attr("fill", function(d, i) { return color( bins[i].category) })
				.selectAll(".bar")
				.data(d => d.data)
				.enter()
				.append("rect")
		        .attr("x",function(d, i) { return x(i)+5})
				.attr("y", function(d, i) {return y(d)})
		        .attr("width", width/48 -5)
		        .attr("height", function(d) {return (height-y(d)); });

		// var gridY = svg.append("g")
		// 			.attr("class","y-axis")
		// 			.attr("transform", "translate(0, 0)")
		// 			.call(yAxis.tickSize(-width).tickFormat(""));

		svg.selectAll(".axisx").selectAll(".tick")
  			.selectAll("text")
    		.attr("x", width/96+7)
    		.attr("text-anchor", "start");

		var legendstr = '<div class="legend">';
		for(var index in categories){
			legendstr +='<label for="legend_'+index+'">'+
					'<span class="glyphicon glyphicon-minus" style="color:'+color(categories[index])+'"></span>'+categories_text[index]+'</label>';
		}
		legendstr += '</div>';
		$(selector).append(legendstr);
	}

	function draw_column_chart(origin_data, container,selector){
		var categories = ['abnormal_num','recored_num'];
		var categories_color = ['#F44336','#4CAF50'];
		var categories_text = ['異常次數','量測筆數'];
		var margin = {top: 20, right: 40, bottom: 50, left: 50};
		var width = $(container).width() - margin.left - margin.right;
		var height = $(container).height() - margin.top - margin.bottom - 100;
		var maxX = origin_data.length;
		var origin_width = width;
		var maxY = d3.max(origin_data, function (d) { return d.abnormal_num;}) + 5;
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX-1]);
		var y = d3.scaleLinear().range([height,0]).domain([0, maxY]);
		bins = categories.map(function(category){ return { category:category, data:origin_data.map(function(d){return d[category];})}});
		var color = d3.scaleOrdinal().range(categories_color);
		color.domain(categories);
		
		var xAxis = d3.axisBottom().scale(x).ticks(24).tickFormat(function(d){return d+1;}).tickSize(0,0);

		var yAxis = d3.axisLeft().scale(y).ticks(maxY).tickSize(0,0);

		var svg = d3.select(selector)
			.select("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.select("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var chart = svg.select("g.chart");
		
		var axis = svg.select("g.axis");
		
		axis.select("g.axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);

      	svg.select("text.axisx_text")
      		.attr("transform", "translate(" + (width/2) + " ," +(height + margin.top + 15) + ")");

  		axis.select("g.axisy")
      		.call(yAxis);

      	svg.select("text.axisy_text")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 0 - margin.left+10)
	      .attr("x",0 - (height / 2))
	      .attr("dy", "1em")
	      .style("text-anchor", "middle")
	      .text("紀錄數"); 

		chart.selectAll(".bar")
			.data( bins)
			.enter()
			 	.append("g") // Uses the enter().append() method
				.attr("class", "bar") // Assign a class for styling
				.attr("transform", function(d, i) {return "translate("+(i * width/48)+", 0)"; })
		        .attr("fill", function(d, i) { return color( bins[i].category) })
				.selectAll(".bar")
				.data(d => d.data)
				.enter()
				.append("rect")
		        .attr("x",function(d, i) { return x(i)+5})
				.attr("y", function(d, i) {return y(d)})
		        .attr("width", width/48 -5)
		        .attr("height", function(d) {return (height-y(d)); });

		// var gridY = svg.select("g.y-axis")
		// 			.call(yAxis.tickSize(-width).tickFormat(""));

		svg.selectAll(".axisx").selectAll(".tick")
  			.selectAll("text")
    		.attr("x", width/96+7)
    		.attr("text-anchor", "start");
	}
</script>
@endsection