@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/export.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.data_sidebar')
		<article id="main">
			<div id="tool_bar" >
				<h4 class="pull-left title">
					@lang("physiolguard.service_export_title")
				</h4>
				<div class="tools">
					<ul class="pull-right">
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="service_export_btn">
								<span class="glyphicon glyphicon-download-alt"></span>
								@lang("physiolguard.service_export_btn")
								<form action="{{url('/data/services/'.($poc ? $poc->id.'/users/':'').$patient->id.'/export')}}" id="service_export_form" method="POST" target="_blank">
									{{csrf_field()}}
									<input type="hidden" name="start" value="{{date('Y-m-d', strtotime(date('Y-m-d').' -1 month'))}}">
									<input type="hidden" name="end" value="{{date('Y-m-d')}}">
									<input type="hidden" name="search" value="">
								</form>
							</a>
						</li>
					</ul>
				</div>
				<div class="date-rang-select input-group date-input col-sm-6 col-md-5 col-sx-5">
					<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_start" name="date_start" value="{{date('Y-m-d', strtotime(date('Y-m-d').' -1 month'))}}">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-minus"></span>
					</span>
					<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_end" name="date_end" value="{{date('Y-m-d')}}">
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed table-bordered" data-toolbar="#tool_bar" data-unique-id="id" data-show-header="true" data-search="true" data-pagination="true" data-page-size="50" data-page-list="[25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="serial_number" data-sortable="true" data-halign="center" data-align="center" data-width="30%">@lang("physiolguard.serial_number")</th>
						<th data-field="start_date" data-sortable="true" data-halign="center" data-align="center" data-width="20%">@lang("physiolguard.start_date")</th>
						<th data-field="duration" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.duration")</th>
						<th data-field="site" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.site")</th>
						<th data-field="priority" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.priority")</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{asset('thirdpart/moment/moment.js')}}"></script>
<script src="{{asset('thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
$(document).ready(function() {
	$('#date_start').datepicker().on("changeDate", function(e) {
       $('#file_manager_table').bootstrapTable('refresh');
    });
	$('#date_end').datepicker().on("changeDate", function(e) {
       $('#file_manager_table').bootstrapTable('refresh');
	});
	$('#service_export_btn').click(function(event){
		event.preventDefault();
		$('#service_export_form input[name=start]').val($('#date_start').val());
		$('#service_export_form input[name=end]').val($('#date_end').val());
		$('#service_export_form input[name=search]').val($('#file_manager .search input').val());
		$('#service_export_form').submit();
	});
});
function queryParams(params) {
	params.start = $('#date_start').val();
	params.end = $('#date_end').val();
	return params;
  }
function ajaxRequest(params) {
    $.get(url("/data/services/{{($poc ? $poc->id.'/users/':'').$patient->id}}/data") + '?' + $.param(params.data)).then(function (res) {
		params.success(res);
    })
 }
</script>
@endsection