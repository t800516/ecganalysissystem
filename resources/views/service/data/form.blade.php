@extends('layouts.app')
@section('css_file')
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/datamanager.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.data_sidebar')
		<article id="main">
			@include('layouts.model_select')
			<div class="container-fluid">
				<form id="register_form" action="{{url(($patient ? '/data/'.$patient->id : '') .'/services')}}" method="post" class="form-horizontal">
					<fieldset class="row">
						<div>
						    <legend class="">{{isset($id) ? trans("physiolguard.edit"):trans("physiolguard.new")}} 
									@lang("physiolguard.service")
						    </legend>
				    	</div>
				    	<ul class="errormsg list-group">
		 					@foreach($errors->all() as $key=>$error)
								<li class="list-group-item list-group-item-danger">{{$error}}</li>
							@endforeach
						</ul>
						<div class="form-group row">
							<label for="serial_number" class="control-label col-sm-2">@lang("physiolguard.serial_number")</label>
							<div class="controls col-sm-8">
								<input type="text" name="serial_number" id="serial_number" class="form-control" value="{{ isset($service) ? $service->serial_number : old('serial_number') }}" required>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						<div class="form-group row">
							<label for="start_date" class="control-label col-sm-2">@lang("physiolguard.start_date")</label>
							<div class="controls col-sm-8">
								<input type="text" name="start_date" id="start_date" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" placeholder="YYYY-MM-DD" value="{{ isset($service)  ? $service->start_date : date('Y-m-d') }}" required>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						<div class="form-group row">
							<label for="duration" class="control-label col-sm-2">@lang("physiolguard.duration")</label>
							<div class="controls col-sm-8">
								<select name="duration" id="duration" class="form-control">
									<option value="1" {{isset($service) && $service->duration == '1'? 'selected':''}}>1</option>
									<option value="3" {{isset($service) && $service->duration == '3'? 'selected':''}}>3</option>
									<option value="5" {{isset($service) && $service->duration == '5'? 'selected':''}}>5</option>
									<option value="7" {{isset($service) && $service->duration == '7'? 'selected':''}}>7</option>
									<option value="14" {{!isset($service) || $service->duration == '14'? 'selected':''}}>14</option>
									<option value="-1" {{isset($service) && !in_array($service->duration, ['1','3','5','7','14']) ? 'selected':''}}>@lang("physiolguard.customized")</option>
								</select>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						<div class="form-group row {{!isset($service) || in_array($service->duration, ['1','3','5','7','14']) ? 'hide':''}}" id="duration_customized_box">
							<label for="customized" class="control-label col-sm-2"></label>
							<div class="controls col-sm-8">
								<input type="text" name="duration_customized" id="duration_customized" class="form-control" value="{{$service->duration or 14}}" required>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						<div class="form-group row">
							<label for="site" class="control-label col-sm-2">@lang("physiolguard.upload_at_home")</label>
							<div class="controls col-sm-8">
								@lang("physiolguard.yes")
								<input type="radio" name="site" value="home" {{isset($service) && $service->site == 'home'? 'checked':''}}/>
								@lang("physiolguard.no")
								<input type="radio" name="site" value="other" {{isset($service) && $service->site == 'other'? 'checked':''}}/>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						@if(!$patient)
						<hr/>
						<div class="form-group row">
							<label for="patient_name" class="control-label col-sm-2">@lang("physiolguard.patient_name")</label>
							<div class="controls col-sm-8">
								<input type="text" name="patient_name" id="patient_name" class="form-control" value="{{isset($service) ? $service->patient_name : ($patient ? $patient->patient_name :'') }}" required>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						<div class="form-group row">
							<label for="patient_IDNumber" class="control-label col-sm-2">@lang("physiolguard.patient_IDNumber")</label>
							<div class="controls col-sm-8">
								<input type="text" name="patient_IDNumber" id="patient_IDNumber" class="form-control" value="{{isset($service) ? $service->patient_IDNumber: ( $patient ? $patient->patient_IDNumber: '')}}" required>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						<div class="form-group row">
							<label for="sex" class="control-label col-sm-2">@lang("physiolguard.sex")</label>
							<div class="controls col-sm-8">
								<select name="sex" id="sex" class="form-control">
									<option value="M" {{ isset($service) ? ($service->sex == 'M' ? 'selected':'') : ($patient && $patient->sex == 'M' ? 'selected':'' )}}>@lang("physiolguard.gender_M")</option>
									<option value="F" {{ isset($service) ? ($service->sex == 'F' ? 'selected':'') : ($patient && $patient->sex == 'F' ? 'selected':'' )}}>@lang("physiolguard.gender_F")</option>
								</select>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						<div class="form-group row">
							<label for="birthday" class="control-label col-sm-2">@lang("physiolguard.birthday")</label>
							<div class="controls col-sm-8">
								<input type="text" name="birthday" id="birthday" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" value="{{ isset($service) ? $service->birthday : ( $patient && $patient->birthday!='0000-00-00' ? $patient->birthday : date('Y-m-d'))}}" required>
							</div>
							<div class="col-sm-2"> </div>
						</div>
						@endif
					</fieldset>
					{!! isset($id) ? '<input type="hidden" name="id" value="'.$id.'">' : '' !!}
					{!! $poc ? '<input type="hidden" name="poc" value="'.$poc->id.'">' : '' !!}

					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

					<div class="col-sm-4 col-sm-offset-4">
						<input type="submit" class="btn btn-primary form-control" value="{{isset($id) ? trans('physiolguard.save'):trans('physiolguard.create') }}">
					</div>
				</form>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
	$(function(){
		$("#register_form").submit(function(){
			
		});
		$("#duration").change(function(event){
			event.preventDefault();
			var duration = $(this).val();
			if(['1','3','5','7','14'].indexOf(duration) !== -1){
				$('#duration_customized_box').addClass('hide');
			}else{
				$('#duration_customized_box').removeClass('hide');
			}
		});
	});

</script>
@endsection