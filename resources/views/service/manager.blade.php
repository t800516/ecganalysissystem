@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/holtermanager.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">@lang("physiolguard.service_title")</h4>
				<div class="tools">
					<ul class="pull-right">

						@can('service_manager')
						<li >
							<a href="{{url('/services/create')}}" class="btn btn-default">
								<span class="glyphicon glyphicon-plus"></span>
								@lang("physiolguard.service_create")
							</a>
						</li>
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_remove">
								<span class="glyphicon glyphicon-trash"></span>
								@lang("physiolguard.delete")
							</a>
						</li>
						@endcan
					</ul>
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-click-to-select="false" data-single-select="true" data-pagination="true" data-page-size="25" data-page-list="[10,25,50,100]">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="serial_number" data-sortable="true" data-halign="center" data-align="center" data-width="30%">@lang("physiolguard.serial_number")</th>
						<th data-field="start_date" data-sortable="true" data-halign="center" data-align="center" data-width="20%">@lang("physiolguard.start_date")</th>
						<th data-field="duration" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.duration")</th>
						<th data-field="site" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.site")</th>
						<th data-field="priority" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.priority")</th>
						<th data-field="id" data-halign="center" data-align="center" data-formatter="action_formatter" data-width="20%">@lang('physiolguard.action')</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script>
$(document).ready(function() {
	var services = {!! $services !!};
	$('#file_manager_table').bootstrapTable('load',services);	
	$('#tool_bar .tools .selectedfunc').hide();
	
	$('#file_manager_table').on('check.bs.table uncheck.bs.table uncheck-all.bs.table check-all.bs.table',function(event, row, $element){
			var selections=$('#file_manager_table').bootstrapTable('getSelections');
			selected_funcs_show(selections);
	});
});
function selected_funcs_show(selections){
	if(selections.length>0){
		$('#tool_bar .tools .selectedfunc').show();
		selected_onefunc_show(selections);
		return true;
	}else{
		$('#tool_bar .tools .selectedfunc').hide();
		return false;
	}
}
function selected_onefunc_show(selections){
	if(selections.length==1){
		$('#tool_bar .tools .selectedonefunc').show();
		return true;
	}else{
		$('#tool_bar .tools .selectedonefunc').hide();
		return false;
	}
}
$("#func_remove").click(function(event){
		var selections=$('#file_manager_table').bootstrapTable('getSelections');
		if(selections.length>0){
			bootbox.confirm({
	    		message: '@lang("physiolguard.delete_accounts")',
	    		locale: lang,
	    		size: 'small',
				callback: function (result) {
					if(result){
					    var ids=selections.map(function(d){return d.id});
						$.post(url('/services/del'),{_token:window.Laravel.csrfToken,ids:ids},function(data){
							$('#file_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
						});
					}
				}
	    	});
		}
	});
	$('#file_manager_table tbody').on('click','.remove',function(event){
		var ids  = [$(this).data('id')];
		bootbox.confirm({
    		message: '@lang("physiolguard.delete_account")',
    		locale: lang,
    		size: 'small',
			callback: function (result) {
				if(result){
					$.post(url('/services/del'),{_token:window.Laravel.csrfToken, ids:ids},function(data){
							$('#file_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
						});
				}
			}
    	});
		return false;
	});
function action_formatter(value, row, index){
	var action_btns = '<div class="btn-box">';
	action_btns += '<a href="'+url("services/"+value+"/summary")+'" target="_blank" class="btn btn-default" >{{trans("physiolguard.summary")}}</a>';
	@can('service_manager')
		action_btns += '<a href="'+url("services/"+value+"/edit")+'" class="btn btn-default" >{{trans("physiolguard.edit")}}</a>';
		//action_btns += '<a href="'+url("/services/"+value+"/del")+'"class="btn btn-danger remove" data-id="'+row.id+'" >{{trans("physiolguard.delete")}}</a>';
	@endcan
	action_btns += '</div>';
	return action_btns;
}
</script>
@endsection