@extends('layouts.app')
@section('css_file')
    <link href="{{url('/css/banner.css')}}" rel="stylesheet">
    <link href="{{url('/css/home.css')}}" rel="stylesheet">
@endsection
@section('content')
<nav id="menunavbar" class="navbar navbar-default" data-spy="affix" data-offset-top="51">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menunavbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="brand">
                <a href="{{ url('/') }}">
                    PhysiolGuard
                </a>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="menunavbar-collapse">
            <ul class="nav navbar-nav">
            &nbsp;
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#banner">
                        About us
                    </a>
                </li>
                <li>
                    <a href="#team">
                        Team
                    </a>
                </li>
                <li>
                    <a href="#core_technology">
                        Core technology
                    </a>
                </li>
                <li>
                    <a href="#vision">
                        Vision
                    </a>
                </li>
                <!--
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Product <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">ECG</a></li>
                        <li><a href="#">QRS</a></li>
                        <li><a href="#">HRV</a></li>
                        <li><a href="#">SLEEP</a></li>
                    </ul>
                </li>-->
                <li>
                    <a href="#contact_box">
                        Contact
                    </a>
                </li>
                <li>
                    <a href="{{ url('/login') }}">
                        Login
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<section id="banner">
    <div class="banner">
        <div class="logo">
            <img src="{{url('/assets/img/logo.png')}}" alt="PhysiolGuard">
        </div>
        <header>
            <div class="head">Save one life, Save the world </div>
            <div class="byline">
                
            </div>
            <a href="{{url('/login')}}" class="btn btn-info">
                <div class="button">Login</div>
            </a>
        </header>
    </div>  
</section>
<section class="subtitle">Making Health-Intelligence Simple & meaningful. Having Personalized Experience.</section>
<section class="intro container">
    <div class="intro col-sm-3" >
        <div class="intro_icon">
            <div class="glyphicon glyphicon-wrench"></div>
        </div>
        <div class="intro_desc">
            Easy to use
        </div>
    </div>
    <div class="intro col-sm-3" >
        <div class="intro_icon">
            <div class="glyphicon glyphicon-time"></div>
        </div>
        <div class="intro_desc">
            Real-time record 
        </div>
    </div>
    <div class="intro col-sm-3" >
        <div class="intro_icon">
            <div class="glyphicon glyphicon-user"></div>
        </div>
        <div class="intro_desc">
            For personalized breathing traning
        </div>
    </div>
    <div class="intro col-sm-3" >
        <div class="intro_icon">
            <div class="glyphicon glyphicon-bell"></div>
        </div>
        <div class="intro_desc">
            Calming music, Notice&Alarm
        </div>
    </div>
</section>
<section id="feature_box">
    <div class="feature feature_bg1 " id="team">
        <div class="container">
            <div class="feature_pic col-sm-12">
                <img src="{{url('/assets/img/Team.png')}}">
            </div>
        </div>
    </div>
    <div class="feature feature_bg2" id="core_technology">
        <div class="container">
        <div class="feature_pic col-sm-12">
            <img src="{{url('/assets/img/Core_technology.png')}}">
        </div>
        </div>
    </div>
</section>
<section id="vision">
    <div class="vision col-sm-3">
        <ul>
            <li>Develop Intelligent Wearable Medical Device with validation of clinical trials, and comply with FDA SW and HW regulations.</li>
        </ul>
    </div>
    <div class="vision col-sm-3">
        <ul>
            <li>Cooperate with hospitals and medical centers to identify and clarify bio-parameters for medical applications and new clinical uses of clinical medicine.regulations.</li>
        </ul>
    </div>
    <div class="vision col-sm-3">
        <ul>
            <li>Innovate novel technologies with integrating knowledge from medical professionals, and develop applications for accurate, immediate and effective medical diagnosis. regulations.</li>
        </ul>
    </div>
    <div class="vision col-sm-3">
        <ul>
            <li>Establish study methods for clinical medicine, and develop medical applications/services based on cloud-based intelligent medicine (including preventive medicine, doctor-patient interaction/relationship, healthcare in remote districts, medical cloud, etc.).</li>
        </ul>
    </div>
</section>
<section id="contact_box">
    <div class="col-sm-3">
        <div class="intro_content">
            
        </div>
    </div>
    <div class="col-sm-6">
        <h2 class="title">Contact</h2>
        <form id="contact_form" action="{{url('/sendmail')}}" method="POST">
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="firstname" class="col-form-label">First Name *</label>
                    <div class="">
                        <input class="form-control" type="text" value="" name="firstname" id="firstname" placeholder="Please enter your first name." required>
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="lastname" class="col-form-label">Last Name *</label>
                    <div class="">
                        <input class="form-control" type="text" value="" name="lastname" id="lastname" placeholder="Please enter your last name." required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="email" class="col-form-label">Email *</label>
                    <div class="">
                        <input class="form-control" type="email" value="" name="email" id="email" placeholder="Please enter your email." required>
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label for="phone" class="col-form-label">Phone</label>
                    <div class="">
                        <input class="form-control" type="text" value="" name="phone" id="phone" placeholder="Please enter your phone number.">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-12">
                    <label for="msg" class="col-form-label">Message *</label>
                    <div class="">
                        <textarea class="form-control" name="msg" id="msg" placeholder="Please leave your message here." rows="6" required></textarea>
                    </div>
                </div>
            </div>
            @foreach($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">{{$error}}</li>
            @endforeach
            <h5 class="text-center">* These fields are required.</h5>
            <div class="text-center">{!! captcha_img() !!}</div>
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <input class="form-control" type="text" value="" name="captcha" id="captcha" required>
                </div>
            </div>
            {{ csrf_field() }}
            <div class="row btn_box">
                <input type="submit" class="btn btn-info" value="SEND MESSAGE">
            </div>
        </form>
    </div>
</section><!--
<section id="sitemap">
    <div class="container">
        <div class="row">
            <section class="col-sm-3">
                <div class="head">
                    About us
                </div>
                <ul class="menu">
                    <li><a href="#company">Company</a></li>
                    <li><a href="#management">Management</a></li>
                    <li><a href="#laboratory">Laboratory</a></li>
                    <li><a href="#vision">Vision</a></li>
                </ul>
            </section>
            <section class="col-sm-3">
                <div class="head">
                    Product
                </div>
                <ul class="menu">
                    <li><a href="#">ECG</a></li>
                    <li><a href="#">QRS</a></li>
                    <li><a href="#">HRV</a></li>
                    <li><a href="#">SLEEP</a></li>
                </ul>
            </section>
            <section class="col-sm-3">
                <div class="head">
                    Contact
                </div>
                <ul class="menu">
                    <li><a href="#contact_box">Message</a></li>
                </ul>
            </section>
            <section class="col-sm-3">
                <div class="head">
                    Other
                </div>
                <ul class="menu">
                    <li><a href="#">OM</a></li>
                    <li><a href="#">SageCloud-VN</a></li>
                </ul>
            </section>
        </div>
    </div>
</section>-->
<div id="gotop" data-spy="affix" data-offset-top="101">
    <a href="#">
        <span class="glyphicon glyphicon-triangle-top"></span>
    </a>
</div>
@endsection