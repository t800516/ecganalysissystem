@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/monitor_list.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">@lang('physiolguard.monitor_title')</h4>
			</div>
			<div id="holter_panel" class="holter-panel">
				@foreach($holters as $holter)
					<div id="holter_{{$holter->IDNumber}}" class="holter-item-box">
						<a href="{{url('monitor/'.$holter->id)}}" target="_blank">
							<div class="holter-item {{$holter->stream_status==1 ? 'bg-green':'bg-gray'}}">
								<div class="holter-content text-center">{{$holter->IDNumber}}</div>
								<div class="holter-content">
									<div class="text-center holter-p-name">
										{{$holter["hrsExData"]["name"]}}
									</div>
								</div>
								<div class="holter-content">
									<div class="holter-title">BED:</div>
									<div class="holter-text">{{$holter["hrsExData"]["bed_no"]}}</div>
								</div>
								<hr class="h-line"/>
								<div class="holter-content holter-data">
									<div class="holter-title">HR:</div>
									<div class="holter-text hr_data">-</div>
								</div>
								<div class="holter-content holter-data">
									<div class="holter-title">SpO2:</div>
									<div class="holter-text spo2_data">-</div>
								</div>
							</div>
						</a>
					</div>
				@endforeach
			</div>
			<div class="text-center">
				紅底：警示；綠底：正常；灰底：機器未連線上傳
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script>
var timestamp = false;
var start_timestamp = false;
var streaming = true;
$(document).ready(function() {
	var holters = {!! json_encode($holters) !!};
	getHolterData(holters.map(function(d){return d.IDNumber}));
});
	function updateHolter(holters){
		for(index in holters){
			var holter_item = $("#holter_"+index).find(".holter-item");
			if(holters[index]["status"]==2){
				holter_item.addClass('bg-red');
				holter_item.removeClass('bg-green');
				holter_item.removeClass('bg-gray');
			}else if(holters[index]["status"]==1){
				holter_item.removeClass('bg-red');
				holter_item.addClass('bg-green');
				holter_item.removeClass('bg-gray');
			}else{
				holter_item.removeClass('bg-red');
				holter_item.removeClass('bg-green');
				holter_item.addClass('bg-gray');
			}

				holter_item.find(".hr_data").html(holters[index]["data"]["heart_rate"]);
				holter_item.find(".rr_data").html(holters[index]["data"]["respiration_rate"]);
				holter_item.find(".spo2_data").html(holters[index]["data"]["spo2"]);
				
				if(holters[index]["warning"]["heart_rate"]){
					holter_item.find(".hr_data").parent().addClass('text-yellow');
				}else{
					holter_item.find(".hr_data").parent().removeClass('text-yellow');
				}

				if(holters[index]["warning"]["respiration_rate"]){
					holter_item.find(".rr_data").parent().addClass('text-yellow');
				}else{
					holter_item.find(".rr_data").parent().removeClass('text-yellow');
				}

				if(holters[index]["warning"]["spo2"]){
					holter_item.find(".spo2_data").parent().addClass('text-yellow');
				}else{
					holter_item.find(".spo2_data").parent().removeClass('text-yellow');
				}
		}
	}
	function getHolterData(holters){
		var lastResponseLength = false;
		$.ajax({
			type: 'POST',
			url: url('/monitor/holters/data'),
			data: JSON.stringify({"second":30,"holters":holters}), 
			dataType: 'json',
			contentType: "application/json",
	        processData: false,
	        xhrFields: {
	            onprogress: function(e)
	            {
	                var progressResponse;
	                var response = e.currentTarget.response;
	                if(lastResponseLength === false)
	                {
	                    progressResponse = response;
	                }
	                else
	                {
	                    progressResponse = response.substring(lastResponseLength);
	                }
	                var data = false;
	                try {
						var data = JSON.parse(progressResponse);
						lastResponseLength = response.length;
	  				} catch (error) {

 					}
 					if(data){
		                if(data.status == 'streaming'){
							if(!start_timestamp || timestamp <= data.timestamp){
				            	if(!start_timestamp){
					            	start_timestamp = data.timestamp;
				            	}else{

				            	}
								timestamp = data.timestamp;
				            }else{
								timestamp = timestamp +1 ;
				            }
				            updateHolter(data.holters);
				        }else if(data.status == 'stop'){
				            if(streaming){
								getHolterData(holters);
							}
						}
					}
	            }
	        }
		});
	}
</script>
@endsection