<!DOCTYPE html>
<html>
<head>
    <style>
        body, html {
            margin: 0;
            padding: 0;
            height: 100%;
        }
        .full-page-image {
            width: 100%;
            height: 100vh; /* 使圖片高度佔滿整頁 */
            object-fit: cover; /* 圖片保持比例並填滿頁面 */
        }
    </style>
</head>
<body>
    <img src="{{ $imagePath }}" class="full-page-image" alt="PNG Image">
</body>
</html>