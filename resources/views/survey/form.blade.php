@extends('layouts.front')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/survey.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		<article id="main" class="survey_penal">
		    	<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
				</ul>
				<form id="survey_form" action="{{url('/survey/'.$user_id.'/save')}}" method="post" class="form-horizontal">
					<div class="form-group">
						<div class="col-md-12">
						      	<button type="submit" class="btn btn-primary col-md-6 col-md-offset-3 take_btn">@lang("physiolguard.take_number")</button>
				    	</div>
			    	</div>
					<fieldset>
						<legend>@lang("physiolguard.memo")</legend>
						<div class="form-group">
							<label for="name" class="control-label col-md-2">@lang("physiolguard.name")</label>
							<div class="controls col-md-8">
								<input type="text" name="name" id="name" class="form-control">
							</div>
							<div class="col-md-2 name_msg"> </div>
						</div>
						<div class="form-group">
							<label for="email" class="control-label col-md-2">@lang("physiolguard.email")</label>
							<div class="controls col-md-8">
								<input type="email" name="email" id="email" class="form-control">
							</div>
							<div class="col-md-2 email_msg"> </div>
						</div>
					{{ csrf_field() }}
				</fieldset>
			</form>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script>
	$(function(){
		$('#name').focus();

		$("#register_form").submit(function(){
			
		});
		
	});

</script>
@endsection