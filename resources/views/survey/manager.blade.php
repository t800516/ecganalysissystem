@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/holtermanager.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">@lang("physiolguard.questionnaire_title")</h4>
				<div class="tools">
					<ul class="pull-right">
						<li >
							<a href="{{url('/survey/'.$user_id)}}" class="btn btn-default">
								<span class="glyphicon glyphicon-link"></span>
								@lang("physiolguard.questionnaire_link")
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-click-to-select="false" data-single-select="true">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="id" data-sortable="true" data-halign="center" data-align="left" data-width="20%" data-formatter="id_formatter">@lang("physiolguard.questionnaire_id")</th>
						<th data-field="name" data-sortable="true" data-halign="center" data-align="left" data-width="20%">@lang("physiolguard.name")</th>
						<th data-field="email" data-sortable="true" data-halign="center" data-align="left" data-width="20%">@lang("physiolguard.email")</th>
						<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.created_date")</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script>
$(document).ready(function() {
	var surveys = {!! $surveys !!};
	$('#file_manager_table').bootstrapTable('load',surveys);	
	$('#tool_bar .tools .selectedfunc').hide();
});
function selected_funcs_show(selections){
	if(selections.length>0){
		$('#tool_bar .tools .selectedfunc').show();
		selected_onefunc_show(selections);
		return true;
	}else{
		$('#tool_bar .tools .selectedfunc').hide();
		return false;
	}
}
function selected_onefunc_show(selections){
	if(selections.length==1){
		$('#tool_bar .tools .selectedonefunc').show();
		return true;
	}else{
		$('#tool_bar .tools .selectedonefunc').hide();
		return false;
	}
}
function id_formatter(value, row, index){
	return 'E'+pad(value,7);
}
function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
</script>
@endsection