@extends('layouts.front')
@section('css_file')
    <link href="{{url('/css/survey.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="content">
        <article id="main" class="survey_penal">
        	@lang('physiolguard.survey_finish')
        	@if($survey_id)
        	   @lang('physiolguard.survey_number') E{{str_pad($survey_id, 7, '0', STR_PAD_LEFT)}}
        	@endif
        </article>
    </div>
</div>
@endsection
@section('javascript')
<script>

</script>
@endsection