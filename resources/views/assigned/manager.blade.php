@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
	<link rel="stylesheet" href="{{asset('thirdpart/chosen/chosen.min.css')}}">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/assigned.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div class="clearfix">
				<h4 class="pull-left title">@lang("physiolguard.assigned_title")</h4>
			</div>
			<div class="col-sm-5">
				<div class="row" id="user_ecg_toolbar">
					<div class="col-sm-5">
						<select id="user-select" data-placeholder="Choose..." class="chosen-select">
							@foreach($sub_users as $sub_user)
								<option value="{{$sub_user->id}}">{{$sub_user->name}} [{{$sub_user->email}}]</option>
							@endforeach
						</select>
					</div>
					<div class="date-rang-select input-group date-input col-sm-7">
						<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_start" name="date_start" value="{{date('Y-m-d', strtotime(date('Y-m-d').' -1 month'))}}">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-minus"></span>
						</span>
						<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_end" name="date_end" value="{{date('Y-m-d')}}">
					</div>
				</div>
				<div class="ecg_table">
					<table data-toggle="table" id="file_manager_table" class="table table-condensed table-bordered" data-toolbar="#user_ecg_toolbar" data-unique-id="id" data-show-header="true" data-search="false" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams" data-group-by="true" data-group-by-field="upload_date" data-click-to-select="true" data-checkbox-header="true">
						<thead>
							<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true" data-width="3%"></th>
							<th data-field="filename" data-sortable="true" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="30%">@lang("physiolguard.filename")</th>
							<th data-field="patient_IDNumber" data-sortable="true" data-halign="center" data-align="center" data-width="15%">@lang("physiolguard.patient_IDNumber")</th>
							<th data-field="report_type" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.report_type")</th>
							<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.upload_date")</th>
							<th data-field="checked" data-sortable="true" data-halign="center" data-align="center" data-width="10%"  data-formatter="checked_formatter">@lang("physiolguard.checked")</th>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<div class="col-sm-1">
				<div class="" id="assigned_btn">
					<span class="glyphicon glyphicon-arrow-right"></span> 
				</div>
				<div class="" id="unassigned_btn">
					<span class="glyphicon glyphicon-arrow-left"></span> 
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row" id="doctor_ecg_toolbar">
					<div class="col-sm-12">
						<select id="doctor-select" data-placeholder="Choose..." class="chosen-select">
							@foreach($doctors as $doctor)
								<option value="{{$doctor->id}}">{{$doctor->name}} [{{$doctor->email}}]</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="ecg_table">
					<table data-toggle="table" id="doctor_ecg_table" class="table table-condensed table-bordered" data-toolbar="#doctor_ecg_toolbar" data-unique-id="id" data-show-header="true" data-search="false" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]" data-side-pagination="server" data-ajax="ajax2Request" data-query-params="queryParams" data-group-by="true" data-group-by-field="upload_date" data-click-to-select="true" data-checkbox-header="true">
						<thead>
							<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true" data-width="3%"></th>
							<th data-field="filename" data-sortable="true" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="30%">@lang("physiolguard.filename")</th>
							<th data-field="patient_IDNumber" data-sortable="true" data-halign="center" data-align="center" data-width="15%">@lang("physiolguard.patient_IDNumber")</th>
							<th data-field="report_type" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.report_type")</th>
							<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.upload_date")</th>
							<th data-field="checked" data-sortable="true" data-halign="center" data-align="center" data-width="10%"  data-formatter="checked_formatter">@lang("physiolguard.checked")</th>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')

<script src="{{asset('thirdpart/moment/moment.js')}}"></script>
<script src="{{asset('thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('thirdpart/chosen/chosen.jquery.min.js')}}"></script>
<script>
$(document).ready(function() {
	$('#date_start').datepicker().on("changeDate", function(e) {
        $('#file_manager_table').bootstrapTable('refresh');
    });
	$('#date_end').datepicker().on("changeDate", function(e) {
		$('#file_manager_table').bootstrapTable('refresh');
	});

	$('#user-select').chosen().chosen().change(function(e) {
		$('#file_manager_table').bootstrapTable('refresh');
	});
	$('#doctor-select').chosen().change(function(e) {
		$('#doctor_ecg_table').bootstrapTable('refresh');
	});
	
	$('#assigned_btn').click(function(e){
		var user_id = $('#doctor-select').val();
		var ids = $('#file_manager_table').bootstrapTable('getSelections').map(d=> d.id);
		var data = {ids:ids};
	    $.post(url("/data/doctors/"+user_id+"/ecgs/assigned"), data, function (res) {
			$('#file_manager_table').bootstrapTable('refresh');
			$('#doctor_ecg_table').bootstrapTable('refresh');
	    });
	});
	
	$('#unassigned_btn').click(function(e){
		var ids = $('#doctor_ecg_table').bootstrapTable('getSelections').map(d=> d.id);
		var data = {ids:ids};
	    $.post(url("/data/doctors/ecgs/unassigned"), data, function (res) {
			$('#file_manager_table').bootstrapTable('refresh');
			$('#doctor_ecg_table').bootstrapTable('refresh');
	    });
	});

});

function queryParams(params) {
	return params
}

function ajaxRequest(params) {
	var user_id = $('#user-select').val();
	if(user_id){
		$.get(url("/data/assigned_users/"+user_id+"/ecgs") + '?' + $.param(params.data)+'&start='+$('#date_start').val()+'&end='+$('#date_end').val()).then(function (res) {
			params.success(res);
	    })
	}
}

function ajax2Request(params) {
	var user_id = $('#doctor-select').val();
	if(user_id){
	    $.get(url("/data/doctors/"+user_id+"/ecgs") + '?' + $.param(params.data)).then(function (res) {
			params.success(res);
	    })
	}
 }

function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}

function filename_formatter(value, row, index){
	var icon='<span class="glyphicon glyphicon-file"></span> ';
	var desc='<span class="file_desc pull-right">'+row['description']+'</span>'
	return icon+value+desc;
}

function checked_formatter(value, row, index){
	return value ? 'Yes' : 'No';
}

function getReportType(type){
	switch(type){
		case '-1' :case -1 :return '@lang("physiolguard.report_type_other")';
		case '0' :return '@lang("physiolguard.report_type_0")';
		case '1' :return '@lang("physiolguard.report_type_1")';
		case '2' :return '@lang("physiolguard.report_type_2")';
		case '3' :return '@lang("physiolguard.report_type_3")';
		default:return '@lang("physiolguard.report_type_other")';
	}
}
</script>
@endsection