@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap4-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
	<link href="{{url('/css/guide.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/filemanager.css')}}" rel="stylesheet">
	<link href="{{url('/css/report_message.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('doctor.sidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">{{trans('physiolguard.ecg_title')}}</h4>
				<div class="tools">
					<ul class="pull-right" data-spy="affix" data-offset-top="100">
						@if($confirmed)
						<li>
							<a href="{{url('/ecg/confirmed/check')}}" class="btn btn-success" id="all_confirmed_check_btn">
								@lang('physiolguard.all_files_confirmed')
							</a>
						</li>
						@endif	
						<li class="selectedfunc" id="report_download_btn_box">
							<a href="#" class="btn btn-default" id="report_download_btn">
								<span class="glyphicon glyphicon-save"></span> 
								@lang('physiolguard.report_download')
							</a>
							<form action="{{url('/ecg/reports/download')}}" id="report_download_form" method="POST" target="_blank">
								{{csrf_field()}}
								<div>
											
								</div>
							</form>
						</li>
					</ul>
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-single-select="false" data-unique-id="id" data-show-header="true"  data-pagination="true" data-page-size="50" data-page-list="[25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams" data-group-by="true" data-group-by-field="{{$confirmed ? 'confirm_date':'upload_date'}}" data-group-order-reverse="ture" data-click-to-select="true" data-checkbox-header="true">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true" data-width="3%"></th>
						<th data-field="filename" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="30%">
							@lang('physiolguard.filename')</th>
						<th data-field="patient_IDNumber" data-halign="center" data-align="center" data-width="20%">@lang('physiolguard.patient_IDNumber')</th>
						<th data-field="report_type" data-halign="center" data-align="center" data-width="15%" data-formatter="report_type_formatter">@lang('physiolguard.report_type')</th>
						<th data-field="created_at" data-halign="center" data-align="center" data-width="10%">@lang('physiolguard.created_at')</th>
						<th data-field="confirm" data-halign="center" data-align="center" data-width="10%">@lang('physiolguard.confirm')</th>
						<th data-field="id" data-halign="center" data-align="center" data-width="10%"  data-formatter="action_formatter"></th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')

<script src="{{asset('thirdpart/moment/moment.js')}}"></script>
<script src="{{asset('thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script src="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.js')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}"></script>
<script src="{{asset('thirdpart/bootstrap4-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{url('/js/guide.js')}}"></script>
<script>
var department = {!! json_encode($department) !!};
$(document).ready(function() {
	$('#tool_bar .tools .selectedfunc').hide();
	
	$('#file_manager_table').on('check.bs.table uncheck.bs.table uncheck-all.bs.table check-all.bs.table',function(event, row, $element){
			var selections=$('#file_manager_table').bootstrapTable('getSelections');
			selected_funcs_show(selections);
	});
	selected_funcs_show([]);
	$('#func_delete').on('click',function(event){
		ecg_del($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#rename_modal').on('shown.bs.modal',function(event){
		show_rename($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#rename_save').on('click',function(event){
		ecg_rename($('#rename_input').val(),$('#rename_input').data('ecg_id'));
	});

	$('#editinfo_modal').on('shown.bs.modal',function(event){
		show_editinfo($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#analysisinfo_modal').on('shown.bs.modal',function(event){
		show_analysisinfo($('#file_manager_table').bootstrapTable('getSelections'));
	});

	$('#file_manager_table').on('click', '.prescreening_report_btn',function(event){
		event.preventDefault();
		analysis_report_handler($(this).data('ecg_id'));
	});

	$('#editinfo_save').on('click',function(event){
		var info_data={
			'description':$('#editinfo_description').val(),
			'patient_name':$('#editinfo_patient_name').val(),
			'patient_IDNumber':$('#editinfo_patient_IDNumber').val(),
			'birthday':$('#editinfo_birthday').val(),
			'sex':$('#editinfo_sex').val(),
			'comment':$('#editinfo_comment').val()
		};
		$('#file_manager_table').bootstrapTable('updateByUniqueId',{
			id: $(this).data('ecg_id'),
			row: info_data
		});
		ecginfo_update(info_data,$(this).data('ecg_id'));
	});

	$('#report_download_btn').click(function(event){
		var rows = $('#file_manager_table').bootstrapTable('getSelections');
		if(rows.length){
			var query_string = '';
			$('#report_download_form div').empty();
			for (var i = 0; i < rows.length; i++) {
				$('#report_download_form div').append('<input type="hidden" name="ids[]" value="'+rows[i].id+'" />');
			}
			$('#report_download_form').submit();
		}
	});
	$('#all_confirmed_check_btn').click(function(event){
		return confirm('@lang("physiolguard.all_files_confirmed_check")')
	});
});
function queryParams(params) {
	@if($confirmed)
		params.confirmed = true
	@endif
	return params
}

function ajaxRequest(params) {
    $.get(url('/api/v0/ecgs/data') + '?' + $.param(params.data)).then(function (res) {
      params.success(res);
    })
}
 
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}

function filename_formatter(value, row, index){
	var icon='<span class="glyphicon glyphicon-file"></span> ';
	var memo= row['analysis_status'] == 10 ? '<span class="analysis_status_memo">@lang("physiolguard.transfer_failed")</span>' : '';
	var desc='<span class="file_desc pull-right">'+row['description']+'</span>';
	return icon+'<span>'+value+'</span>'+memo+desc;
}

function report_type_formatter(value, row, index){
	var type_string = '';
	var report_type = value || '';
	var report_types = department ? department.report_types : [];
	if(report_type=='-1'){
		return '@lang("physiolguard.report_type_-1")';
	}
	for (var i = 0; i < report_types.length; i++) {
		if(report_type.indexOf(report_types[i].value) != -1){
			type_string += type_string == '' ? report_types[i].name : ', '+report_types[i].name;
		}
	}
	return type_string;
}

function analysis_status_formatter(value, row, index){
	switch(value){default:
		case 0:return '@lang("physiolguard.analysis_status_0")';
		case 9:return '@lang("physiolguard.analysis_status_9")';
		case 1:return '@lang("physiolguard.analysis_status_1")';
		case 92:return '@lang("physiolguard.analysis_status_92")';
		case 93:return '@lang("physiolguard.analysis_status_93")';
		case 82:return '@lang("physiolguard.analysis_status_82")';
		case 2:return '@lang("physiolguard.analysis_status_2")';
		case 10:return '@lang("physiolguard.analysis_status_10")';
		case 11:return '@lang("physiolguard.analysis_status_11")';
	}
}

function action_formatter(value, row, index){
	var button_html='';
	button_html += '<a href="'+url('analysis/ecg/'+value)+'" class="btn btn-info" data-ecg_id="'+value+'">Action</a>';

	return button_html;
}

function selected_funcs_show(selections){
	if(selections.length>0){
		$('#tool_bar .tools .selectedfunc').show();
		return true;
	}else{
		$('#tool_bar .tools .selectedfunc').hide();
		$('#tool_bar .tools .selectedonefunc').hide();
		return false;
	}
}

function getReportType(type){
		return report_type_formatter(type, [], 0)
}
</script>
@endsection