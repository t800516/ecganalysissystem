<aside id="sidebar" class="sidebar-nav">
	<ul class="nav nav-pills nav-stacked"> 
		<li>
			<a href="{{url('/ecg/manager')}}">
				<span class="glyphicon glyphicon-heart icon-red"></span> @lang("physiolguard.ecg_title")
			</a>
		</li>
		<li>
			<a href="{{url('/ecg/manager?confirmed=true')}}">
				<span class="glyphicon glyphicon-ok icon-green"></span> @lang("physiolguard.ecg_confirmed_title")
			</a>
		</li>
	</ul>
</aside>