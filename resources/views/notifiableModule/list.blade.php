@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/notifiable_module.css')}}" rel="stylesheet">
	<link href="{{url('/css/export.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">@lang("physiolguard.notifiable_module_title")</h4>
				<div class="tools">

				</div>
				<div class="date-rang-select input-group date-input col-sm-6 col-md-5 col-sx-5">
					<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_start" name="date_start" value="{{date('Y-m-d', strtotime(date('Y-m-d').' -3 month'))}}">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-minus"></span>
					</span>
					<input type="text" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" id="date_end" name="date_end" value="{{date('Y-m-d')}}">
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed table-bordered" data-toolbar="#tool_bar" data-search="true" data-unique-id="id" data-pagination="false" data-page-size="50" data-page-list="[25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams">
					<thead>
						<th data-field="serial_number" data-sortable="true" data-halign="center" data-align="center" data-width="30%">@lang("physiolguard.notifiable_module_serial_number")</th>
						<th data-field="start_date" data-sortable="true" data-halign="center" data-align="center" data-width="20%">@lang("physiolguard.start_date")</th>
						<th data-field="email" data-sortable="true" data-halign="center" data-align="center" data-width="30%" data-formatter="email_formatter">@lang("physiolguard.email")</th>
						<th data-field="department" data-sortable="true" data-halign="center" data-align="center" data-width="30%" data-formatter="department_formatter">@lang("physiolguard.notifiable_module_department")</th>
						<th data-field="patient_IDNumber" data-sortable="true" data-halign="center" data-align="center" data-width="20%">@lang("physiolguard.patient_IDNumber")</th>
						<th data-field="priority" data-sortable="true" data-halign="center" data-align="center"  data-formatter="priority_formatter">@lang("physiolguard.priority")</th>
						<th data-field="duration" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.duration")</th>
						<th data-field="notifiable_records" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.notifiable_records")</th>
						<th data-field="unchecked_records" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.unchecked_records")</th>
						<th data-field="id" data-halign="center" data-align="center" data-formatter="action_formatter" data-width="20%">@lang('physiolguard.action')</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
<div id="notified_modal" class="modal-dialog-box hide">
  	<div class="modal-dialog-centered">
	  	<div class="modal-content"> 
	  		<div class="container-fluid">
	  			<div class="row">
	  				<h4 class="modal-title ">@lang('physiolguard.notified_modal_progress')</h4>
	  			</div>
	  			<div class="row">
	  				<div class="notified_modal_progress col-sm-12">
	  					<div class="progress">
							<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" id="notified_modal_progress_bar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
						</div>
						<div id="progress-text" class="progress-text">
							
						</div>
						<div id="progress-result" class="result-text">
							
						</div>
	  				</div>
	  			</div>
	  			<div class="row">
	  				<button id="notified_modal_close_btn" class="btn btn-default col-sm-4 col-sm-offset-4 hide" data-dismiss="modal">@lang('physiolguard.close')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{asset('thirdpart/moment/moment.js')}}"></script>
<script src="{{asset('thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
$(document).ready(function() {
	$('#tool_bar .tools .selectedfunc').hide();
	
	$('#file_manager_table').on('check.bs.table uncheck.bs.table uncheck-all.bs.table check-all.bs.table',function(event, row, $element){
			var selections=$('#file_manager_table').bootstrapTable('getSelections');
			selected_funcs_show(selections);
		});
	$('#notified_modal_close_btn').on('click',function(event){
		$('#notified_modal').addClass('hide');
	});

	$('#file_manager_table tbody').on('click','.notified-btn',function(event){
		event.preventDefault();
		var id  = $(this).data('id');
		var enable  = $(this).data('enable');
		if(enable){
			bootbox.confirm({
	    		message: '@lang("physiolguard.notifiable_confirm")',
	    		locale: lang,
	    		size: 'small',
				callback: function (result) {
					if(result){
						var lastResponseLength = false;
						updateProgressBar(0, 0);
						$('#progress-result').html('');
						$('#notified_modal').removeClass('hide');
						$('#notified_modal_close_btn').addClass('hide');
						$.ajax({
							type: 'POST',
							url: url('/notifiable_modules/'+id+'/notified'),
							data: {}, 
							dataType: 'json',
							contentType: "application/json",
				        	processData: false,
							xhrFields: {
					            onprogress: function(e)
					            {
					                var progressResponse;
					                var response = e.currentTarget.response;
					                if(lastResponseLength === false)
					                {
					                    progressResponse = response;
					                    lastResponseLength = response.length;
					                }
					                else
					                {
					                    progressResponse = response.substring(lastResponseLength);
					                    lastResponseLength = response.length;
					                }
					                var data = JSON.parse(progressResponse);
									var rowData = $('#file_manager_table').bootstrapTable('getRowByUniqueId', data.id);
									if(rowData){
											var total_count = data.data.total_count;
											var finish_count = data.data.finish_count;
											updateProgressBar(total_count, finish_count);
											rowData.notifiable_records = parseInt(data.data.total_count, 10) - parseInt(data.data.finish_count, 10);
											$('#file_manager_table').bootstrapTable('updateByUniqueId', data.id, rowData);
									}
									if(data.data.status =='complete'){
										$('#notified_modal_close_btn').removeClass('hide');
										$('#progress-result').html('@lang("physiolguard.notified_modal_finish")');
									}
					            }
					        }
						}).done(function( data ) {
					        if(data.data.status =='error'){
								$('#progress-result').html('@lang("physiolguard.notified_modal_failed")');
					        }
							$('#notified_modal_close_btn').removeClass('hide');
						});
					}
				}
	    	});
		}
		return false;
	});

	$('#date_start').datepicker().on("changeDate", function(e) {
       $('#file_manager_table').bootstrapTable('refresh');
    });
	$('#date_end').datepicker().on("changeDate", function(e) {
       $('#file_manager_table').bootstrapTable('refresh');
	});
	$('#service_export_btn').click(function(event){
		event.preventDefault();
		$('#service_export_form input[name=start]').val($('#date_start').val());
		$('#service_export_form input[name=end]').val($('#date_end').val());
		$('#service_export_form input[name=search]').val($('#file_manager .search input').val());
		$('#service_export_form').submit();
	});
});
function queryParams(params) {
	@if(isset($related_users))
		params.related_users = {!! json_encode($related_users) !!};
	@endif
	params.start = $('#date_start').val();
	params.end = $('#date_end').val();
	return params
}

function ajaxRequest(params) {
	$.get(url('/notifiable_modules/data') + '?' + $.param(params.data)).then(function (res) {
	     params.success(res);
	})
}
function updateProgressBar(total_count, finish_count) {
	var progress = total_count ? (Math.floor(parseFloat(finish_count)*100/parseFloat(total_count))): 0 ;
	$('#notified_modal .progress-text').html('( '+finish_count+' / '+total_count+' )');
	$('#notified_modal_progress_bar').attr('aria-valuenow', progress);
	$('#notified_modal_progress_bar').css('width', progress+'%');
	$('#notified_modal_progress_bar').html(progress+'%');
}
	
function email_formatter(value, row, index){
	if(row.patient){
		return row.patient.email;
	}
	return row.user.email;
}
function department_formatter(value, row, index){
	if(row.patient){
		return row.patient.department ? row.patient.department.name : '';
	}
	return row.user.department ? row.user.department.name : '';
}
function priority_formatter(value, row, index){
	if(value==1){
		return '<span class="text-danger">real-time</span>';
	}
	return '';
}
function action_formatter(value, row, index){
	return '<a href="'+url("notifiable_modules/"+value+"/notified")+'" class="btn btn-default notified-btn" '+(row.notifiable_records==0 ? 'disabled':'')+' data-id="'+(row.id)+'" data-enable="'+(row.notifiable_records==0 ? 'false':'true')+'" >{{trans("physiolguard.notified")}}</a>';
}
</script>
@endsection