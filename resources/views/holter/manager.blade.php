@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/holtermanager.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div id="tool_bar">
				<h4 class="pull-left title">病患管理</h4>
				<div class="tools">
					<ul class="pull-right">
						<li >
							<a href="{{url('/holter/add')}}" class="btn btn-default">
								<span class="glyphicon glyphicon-plus-sign"></span>
								新增病患資料
							</a>
						</li>
						<li class="selectedfunc">
							<a href="#" class="btn btn-default">
								<span class="glyphicon glyphicon-eye-open"></span>
								查看ECG檔案
							</a>
						</li>
						<li class="selectedfunc">
							<a href="#" class="btn btn-default" id="func_delete">
								<span class="glyphicon glyphicon-trash"></span>
								刪除
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="file_manager">
				<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-click-to-select="false" data-single-select="true">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="filename" data-sortable="true" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="70%">姓名</th>
						<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center">建立時間</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>

<div id="rename_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rename_modal_label">
  	<div class="modal-dialog modal-sm" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="rename_modal_label">重新命名</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<h5>請為ECG輸入新的名稱:</h5>
	  			<input id="rename_input" type="text" class="form-control">
	  			<div class="row">
	  				<button id="rename_cancel" class="btn btn-default col-md-3 col-md-offset-4" data-dismiss="modal">取消</button>
	  				<button id="rename_save" class="btn btn-info col-md-3 col-md-offset-1">確定</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script>
$(document).ready(function() {
	$('#tool_bar .tools .selectedfunc').hide();


	$('#func_delete').on('click',function(event){
		del_ecgs($('#file_manager_table').bootstrapTable('getSelections'))
	});
	$('#rename_modal').on('shown.bs.modal',function(event){
		show_rename($('#file_manager_table').bootstrapTable('getSelections'));
	});
	$('#rename_save').on('click',function(event){
		ecg_rename($('#rename_input').val(),$('#rename_input').data('ecg_id'));
	});
});
function filename_formatter(value, row, index){
	var icon='<span class="glyphicon glyphicon-file"></span> ';
	var desc='<span class="file_desc pull-right">'+row['description']+'</span>'
	return icon+value+desc;
}
function selected_funcs_show(selections){
	if(selections.length>0){
		$('#tool_bar .tools .selectedfunc').show();
		selected_onefunc_show(selections);
		return true;
	}else{
		$('#tool_bar .tools .selectedfunc').hide();
		return false;
	}
}
function selected_onefunc_show(selections){
	if(selections.length==1){
		$('#tool_bar .tools .selectedonefunc').show();
		return true;
	}else{
		$('#tool_bar .tools .selectedonefunc').hide();
		return false;
	}
}
function ecg_rename(filename,ecg_id){
	$.post(url('/ecg/manager/rename'),{_token:window.Laravel.csrfToken,filename:filename,id:ecg_id},function(data){
		var index=$('#file_manager_table tr.selected').data('index');
		$('#file_manager_table').bootstrapTable('updateCell',{index:index,field:'filename',value:data.filename});
		$('#rename_modal').modal('hide');
	});
}
function show_rename(selections){
	if(selections.length==1){
		$('#rename_input').val(selections[0].filename).select().data('ecg_id',selections[0].id);
	}else{
	}
}
function del_ecgs(selections){
		if(selections.length==0){
			bootbox.alert({
    				message: '@lang("physiolguard.select_file")',
    				locale: lang
    			});
		}else{
			var del_msg='';
			if(selections.length==1){
				del_msg='確定刪除檔案 '+selections[0].filename+' ?';
			}else{
				del_msg='確定刪除所有已選擇的 '+selections.length+'個檔案 ?';
			}
			if(confirm(del_msg)){
				$.post(url('/ecg/manager/delete'),{_token:window.Laravel.csrfToken,ecgs:selections},function(data){
					if(data.del_success_count>0){
						var ids=$.map(selections, function (row){
							return row.id;
						});
						$('#file_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
					}
				});
			}
		}
}
</script>
@endsection