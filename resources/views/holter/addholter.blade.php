@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/addholter.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<form id="register_form" action="{{url('/')}}" method="post" class="form-horizontal">
				<fieldset>
				    <div>
				      <legend class="">病患資料</legend>
		    		</div>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<label for="IDNumber" class="control-label col-md-2">病患號碼</label>
						<div class="controls col-md-8">
							<input type="text" name="IDNumber" id="IDNumber" class="form-control" required>
						</div>
						<div class="col-md-2 IDNumber_msg"> </div>
					</div>
					<div class="form-group">
						<label for="name" class="control-label col-md-2">名稱</label>
						<div class="controls col-md-8">
							<input type="text" name="name" id="name" class="form-control" required>
						</div>
						<div class="col-md-2 name_msg"> </div>
					</div>
					<div class="form-group">
						<label for="sex" class="control-label col-md-2">性別</label>
						<div class="controls col-md-8">
							<select id="sex" name="sex" class="form-control">
								<option value="M">男</option>
								<option value="F">女</option>
							</select>
						</div>
						<div class="col-md-2 sex_msg"> </div>
					</div>
					<div class="form-group">
						<label for="birthday" class="control-label col-md-2">生日</label>
						<div class="col-md-4">
							<select id="birthday_year" class="form-control">
								<option value="0">請選擇年份</option>
							</select>
						</div>
						<div class="col-md-2">
							<select id="birthday_month" class="form-control">
								<option value="0">請選擇月份</option>
							</select>
						</div>
						<div class="col-md-2">
							<select id="birthday_day" class="form-control">
								<option value="0">請選擇日期</option>
							</select>
						</div>
						<input type="hidden" name="birthday" id="birthday">
						<div class="col-md-2 birthday_msg"> </div>
					</div>
					<div class="form-group">
						<label for="age" class="control-label col-md-2">年齡</label>
						<div class="controls col-md-8">
							<input type="number" name="age" id="age" class="form-control" required>
						</div>
						<div class="col-md-2 age_msg"> </div>
					</div>
					<div class="form-group">
						<label for="country" class="control-label col-md-2">國家</label>
						<div class="controls col-md-8">
							<select id="country" name="country" class="form-control">
							</select>
						</div>
						<div class="col-md-2 age_msg"></div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-2">聯絡地址</label>
						<div class="col-md-2">
							<input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="郵遞區號">
						</div>
						<div class="col-md-2">
							<select id="city" name="city" class="form-control">
								<option value="0">請先選取國家</option>
							</select>
						</div>
						<div class="col-md-2" >
							<select id="downtown" name="downtown" class="form-control">
								<option value="0">請先選取縣市</option>
							</select>
						</div>
						<div class="col-md-2">
							<input type="text" id="address" name="address" class="form-control" placeholder="請輸入地址">
						</div>
						<div class="col-md-2 address_msg"> </div>
					</div>
					<div class="form-group">
						<label for="tel" class="control-label col-md-2">電話</label>
						<div class="controls col-md-3">
							<input type="text" name="tel" id="tel" class="form-control">
						</div>
						<label for="cel" class="control-label col-md-1">手機</label>
						<div class="controls col-md-4">
							<input type="text" name="cel" id="cel" class="form-control">
						</div>
						<div class="col-md-2 tel_msg cel_msg"> </div>
					</div>
					<div class="form-group">
						<label for="email" class="control-label col-md-2">E-mail</label>
						<div class="controls col-md-8">
							<input type="email" name="email" id="email" class="form-control">
						</div>
						<div class="col-md-2 email_msg"> </div>
					</div>
					{{ csrf_field() }}
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-primary form-control" value="新增">
				</div>
			</form>
		</article>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{url('/js/selectaddress.js')}}"></script>
<script src="{{url('/js/selectdate.js')}}"></script>
<script>
	$(function(){
		$('#IDNumber').focus();

		$("#register_form").submit(function(){
			
		});
		$('#country').append(get_country_options());
		
		$('#country').change(function(){
			$('#city').empty();
			$('#downtown').empty();
			if($(this).val()=='TW'){
				$('#city').append(get_city_options());
				select_downtown($('#city>option:selected').val(),'#downtown');
			}else{
				$('#city').append('<option value="0">請先選取國家</option>');
				$('#downtown').append('<option value="0">請先選取城市</option>');
			}
		});
		$("#city").change(function(){
			select_downtown($(this).val(),'#downtown');
		});
		$('#country>option[value=TW]').prop('selected',true);
		$('#city').empty();
		$('#downtown').empty();
		$('#city').append(get_city_options());
		select_downtown($('#city>option:selected').val(),'#downtown');

		$('#birthday_year').append(get_year_options());
		$('#birthday_month').append(get_month_options());
		$('#birthday_day').append(get_day_options());
		$('#birthday_year,#birthday_month,#birthday_day').change(function(event){
			var year=$('#birthday_year').val();
			var month=$('#birthday_month').val();
			var day=$('#birthday_day').val();
			$('#birthday_day').html('<option value="0">請選擇日期</option>');
			$('#birthday_day').append(get_day_options(month));
			if(year==null)year='0000';
			if(month==null)month='00';
			if(day==null)day='00';
			$('#birthday').val(year+'-'+month+'-'+day);
		});
	});

</script>
@endsection