@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/loginhome.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="box col-md-3">
					<a href="{{url('/ecg/upload')}}">
					<div class="icon glyphicon glyphicon-open"></div>	
						<div class="text">
							<h4>ECG檔案上傳</h4>
							<p>上傳ECG檔案並轉換.....</p>
						</div>
					</a>
				</div>
				<div class="box col-md-3">
					<a href="{{url('/holter/add')}}">
					<div class="icon glyphicon glyphicon-plus"></div>	
						<div class="text">
							<h4>新增病患資料</h4>
							<p>輸入病患資料....</p>
						</div>
					</a>
				</div>
				<div class="col-md-3"></div>
			</div>
		</article>
	</div>
</div>
@endsection
