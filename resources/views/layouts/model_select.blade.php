@if(Auth::user()->can('ecg_manager_model') || Auth::user()->can('data_manager_model'))
	<div class="model_box row">
		@if(Auth::user()->can('ecg_manager_model'))
			<div class="pull-left">
				<a class="btn btn-default {{session('manager_model') == 'ecg' ? 'active':''}}" href="{{url('/ecg/manager')}}">Mode 1</a>
			</div>
		@endif
		@if(Auth::user()->can('data_manager_model'))
			<div class="pull-left">
				<a class="btn btn-default  {{session('manager_model') == 'data' ? 'active':''}}" href="{{url('data/users')}}">Mode 2</a>
			</div>
		@endif
	</div>
@endif