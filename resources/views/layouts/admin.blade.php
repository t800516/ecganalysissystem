<!DOCTYPE html>
<html lang="zh-Hant">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ECG Analysis System Administrator</title>

    <!-- Styles -->
    <link href="{{url('/thirdpart/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--<link href="/thirdpart/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
    <link href="{{url('/css/admin.css')}}" rel="stylesheet">
    @yield('css_file')  
    <!-- Scripts -->
    <script>
		function url(uri){
            if(!uri){
                uri='/';
            }else if(uri[0]!='/'){
                uri='/'+uri;
            }
            return '{{ url("/") }}'+uri;
        }
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="wrap">
        <header>
            <nav class="navbar navbar-inverse navbar-static-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/admin') }}">
                            ECG Analysis System Administrator
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            @if (!Auth::guest())
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->account }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="#" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                <span class="glyphicon glyphicon-off"></span> 登出
                                            </a>
                                            <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                        <div class="arrow-border"></div> 
                                        <div class="arrow"></div> 
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <section id="content">
            @yield('content')        
        </section>
    </div>
    <footer class="container-fluid">
    	
    </footer>
    <!-- Scripts -->
    <script src="{{url('/thirdpart/jquery/jquery-3.1.0.min.js')}}"></script>
    <script src="{{url('/thirdpart/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('/js/ie.js')}}"></script>
    @yield('javascript')  
</body>
</html>
