<!DOCTYPE html>
<html lang="zh-Hant">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:image" content="{{url('assets/img/shortcut.png')}}" />
    <title>PhysiolGuard</title>

    <!-- Styles -->
    <link href="{{url('/thirdpart/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--<link href="/thirdpart/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
    <link href="{{url('/css/app.css')}}" rel="stylesheet">
    @yield('css_file')  
    <!-- Scripts -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();
            a=s.createElement(o),m=s.getElementsByTagName(o)[0];
            a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-86342545-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
		function url(uri){
            if(!uri){
                uri='/';
            }else if(uri[0]!='/'){
                uri='/'+uri;
            }
            return '{{ url("/") }}'+uri;
        }
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
    </script>
</head>
<body>
    <div id="wrap">
        <section id="content">
            @yield('content')        
        </section>
    </div>
    @include('layouts.footer')
    <!-- Scripts -->
    <script src="{{url('/thirdpart/jquery/jquery-3.1.0.min.js')}}"></script>
    <script src="{{url('/thirdpart/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('/js/ie.js')}}"></script>
    <script> 
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
    </script>
    @yield('javascript')  
</body>
</html>
