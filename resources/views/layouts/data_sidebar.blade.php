<aside id="sidebar" class="sidebar-nav">
	<ul class="nav nav-pills nav-stacked"> 
		@if((isset($type) && $type!='MGR') && 
			(
				Auth::user()->can('user_list') || 
				(	
					Auth::user()->can('manager_type') && 
					Auth::user()->cannot('data_manager_model') && Auth::user()->cannot('poc_manager')
				)
			) 
			&& Auth::user()->role_id != 13 
		)
			<li><a href="{{url('/data/users/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/ecg/upload')}}"><span class="glyphicon glyphicon-open"></span> @lang("physiolguard.ekg_upload_title")</a></li>
			<li><a href="{{url('/data/users/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/ecg/export')}}"><span class="glyphicon glyphicon-export icon-lightblue"></span> @lang("physiolguard.export_title")</a></li>
			<li><a href="{{url('/data/users/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/ecgs')}}"><span class="glyphicon glyphicon-heart icon-red"></span> @lang("physiolguard.ecg_title")</a></li>
		@endif

		<li>
			<a href="{{url('/data/users')}}" >
				<span class="glyphicon glyphicon-user icon-orange"></span> @lang("physiolguard.user_data_title")
			</a>
		</li>
		@can('poc_manager')
			@php
			/*
			<li><a href="{{url('/data/pocs/manager')}}" ><span class="glyphicon glyphicon-user"></span> @lang("physiolguard.poc_manager_title")</a></li>
			*/
			@endphp
		@endcan

		@can('data_manager_model')
			@php
			/*
			<li><a href="{{url('/data/users/manager')}}" ><span class="glyphicon glyphicon-usergroup"></span> @lang("physiolguard.user_manager_title")</a></li>
			*/
			@endphp
		@endcan
		
		@can('service_manager')
			@php
			/*
			<li>
				<a href="{{url('/services')}}" >
					<span class="glyphicon glyphicon-briefcase icon-blue"></span> @lang("physiolguard.service_title")
				</a>
			</li>
			*/
			@endphp
		@endcan

		@can('service_export')
			@if((!isset($type) || ($type!='MGR' && $type!='manager')) && isset($poc))
			<li>
				<a href="{{url('/services/'.$poc->id.'/export')}}" >
					<span class="glyphicon glyphicon-paste icon-lightblue"></span> @lang("physiolguard.service_export_title")
				</a>
			</li>
			@endif
			@if(isset($type) && $type=='manager' && !isset($poc))
			<li>
				<a href="{{url('/services/'.$user->id.'/export')}}" >
					<span class="glyphicon glyphicon-paste icon-lightblue"></span> @lang("physiolguard.service_export_title")
				</a>
			</li>
			@endif
			@if(isset($type) && $type=='manager' && isset($poc))
			<li>
				<a href="{{url('/data/services/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/export')}}" >
					<span class="glyphicon glyphicon-paste icon-lightblue"></span> @lang("physiolguard.service_export_title")
				</a>
			</li>
			@endif
		@endcan

		@can('all_records')
		<li>
			<a href="{{url('/data/ecgs')}}" >
				<span class="glyphicon glyphicon-th-list icon-red"></span> @lang("physiolguard.all_records_title")
			</a>
		</li>
		@endcan
		@can('notifiable_module')
		<li>
			<a href="{{url('/notifiable_modules')}}" >
				<span class="glyphicon glyphicon-bullhorn icon-purple"></span> @lang("physiolguard.notifiable_module_title")
			</a>
		</li>
		@endcan

		@can('ecg_assigned')
		<li>
			<a href="{{url('/ecg/assigned')}}" >
				<span class="glyphicon glyphicon-transfer"></span> @lang("physiolguard.ecg_assigned")
			</a>
		</li>
		@endcan
	</ul>
</aside>