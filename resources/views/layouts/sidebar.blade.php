<aside id="sidebar" class="sidebar-nav">
	<ul class="nav nav-pills nav-stacked"> 
		@if(Auth::user()->cannot('data_type') && 
			Auth::user()->cannot('manager_type'))
			@if(Auth::user()->can('doctor_type'))
				<li><a href="{{url('/ecg/manager')}}"><span class="glyphicon glyphicon-heart icon-red"></span> @lang("physiolguard.ecg_title")</a></li>
			@elseif(Auth::user()->can('explorer_type'))
				<li><a href="{{url('/ecg/manager')}}"><span class="glyphicon glyphicon-list-alt icon-red"></span> @lang("physiolguard.report_title")</a></li>
			@elseif(Auth::user()->cannot('explorer_type'))
				@if(Auth::user()->can('upload_ekg'))
					<li><a href="{{url('/ecg/upload')}}"><span class="glyphicon glyphicon-open"></span> @lang("physiolguard.ekg_upload_title")</a></li>
				@endif
				<li><a href="{{url('/ecg/manager')}}"><span class="glyphicon glyphicon-heart icon-red"></span> @lang("physiolguard.ecg_title")</a></li>
				@if(Auth::user()->cannot('ecg_manager_model') && Auth::user()->cannot('ecg_explorer_type'))
					<li><a href="{{url('/assets/file/Introduction for Biotechnology Analysis System_Rev. 1.0.pdf')}}" target="_blank"><span class="glyphicon glyphicon-book icon-orange"></span> @lang("physiolguard.manual_title")</a></li>
					<li><a href="{{url('/monitor/manager')}}" ><span class="glyphicon glyphicon-transfer icon-red"></span> @lang("physiolguard.monitor_title")</a></li>
					<li><a href="{{url('/survey/manager')}}" ><span class="glyphicon glyphicon-list-alt icon-green"></span> @lang("physiolguard.questionnaire_title")</a></li>
					<li><a href="{{url('/ecg/export')}}"><span class="glyphicon glyphicon-export icon-lightblue"></span> @lang("physiolguard.export_title")</a></li>
				@endif
			@endif
		@else
			@if(Auth::user()->can('manager_type') && isset($type) && Auth::user()->cannot('data_manager_model'))
				<li><a href="{{url('/data/users/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/ecg/upload')}}"><span class="glyphicon glyphicon-open"></span> @lang("physiolguard.ekg_upload_title")</a></li>
				<li><a href="{{url('/data/users/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/ecg/export')}}"><span class="glyphicon glyphicon-export icon-lightblue"></span> @lang("physiolguard.export_title")</a></li>
				<li><a href="{{url('/data/users/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/ecgs')}}"><span class="glyphicon glyphicon-heart icon-red"></span> @lang("physiolguard.ecg_title")</a></li>
			@endif

			@if(session('manager_model') == 'ecg')
				<li><a href="{{url('/ecg/upload')}}"><span class="glyphicon glyphicon-open"></span> @lang("physiolguard.ekg_upload_title")3</a></li>
				<li><a href="{{url('/ecg/manager')}}"><span class="glyphicon glyphicon-heart icon-red"></span> @lang("physiolguard.ecg_title")</a></li>
			@endif

			@if(session('manager_model') == 'data' || 
				(Auth::user()->can('manager_type') && Auth::user()->cannot('ecg_manager_model') && Auth::user()->cannot('data_manager_model')))
				<li><a href="{{url('/data/users')}}" ><span class="glyphicon glyphicon-user icon-orange"></span> @lang("physiolguard.user_data_title")</a></li>
			@endif
		@endif

		@can('service_manager')
			@php
			/*
			<li>
				<a href="{{url('/services')}}" >
					<span class="glyphicon glyphicon-briefcase icon-blue"></span> @lang("physiolguard.service_title")
				</a>
			</li>
			*/
			@endphp
		@endcan

		@can('service_export')
			@if(isset($type) && $type=='manager' && !isset($poc))
			<li>
				<a href="{{url('/services/'.$user->id.'/export')}}" >
					<span class="glyphicon glyphicon-paste icon-lightblue"></span> @lang("physiolguard.service_export_title")
				</a>
			</li>
			@endif
			@if(isset($type) && $type=='manager' && isset($poc))
				<li>
					<a href="{{url('/data/services/'.(isset($poc) ? $poc->id.'/users/':'').$user->id.'/export')}}" >
						<span class="glyphicon glyphicon-paste icon-lightblue"></span> @lang("physiolguard.service_export_title")
					</a>
				</li>
			@endif
		@endcan
		
		@can('all_records')
		<li>
			<a href="{{url('/data/ecgs')}}" >
				<span class="glyphicon glyphicon-th-list icon-red"></span> @lang("physiolguard.all_records_title")
			</a>
		</li>
		@endcan

		@can('notifiable_module')
		<li>
			<a href="{{url('/notifiable_modules')}}" >
				<span class="glyphicon glyphicon-bullhorn icon-purple"></span> @lang("physiolguard.notifiable_module_title")
			</a>
		</li>
		@endcan

		@can('ecg_assigned')
		<li>
			<a href="{{url('/ecg/assigned')}}" >
				<span class="glyphicon glyphicon-transfer"></span> @lang("physiolguard.ecg_assigned")
			</a>
		</li>
		@endcan
		
		@can('doctor_type')
		<li>
			<a href="{{url('/ecg/manager?confirmed=true')}}">
				<span class="glyphicon glyphicon-ok icon-green"></span> @lang("physiolguard.ecg_confirmed_title")
			</a>
		</li>
		@endcan
	</ul>
</aside>