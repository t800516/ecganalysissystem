<footer>
        <section id="companyinfo">
            <ol class="breadcrumb">
                <li>Physiolguard Corporation Ltd.</li>
                <li>Address: 136 Beinei Street, Shulin District, New Taipei City, Taiwan, 23849</li>
                <li>TEL: 886-922426981</li>
            </ol>
            <div>
                @lang("physiolguard.footer_info")
            </div>
        </section>
        <section id="copyright">
            <div>
                V1.0.0，Ⓒ2021 Physiolguard Corporation Ltd. All rights reserved
            </div>
        </section>
</footer>