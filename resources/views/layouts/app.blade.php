<!DOCTYPE html>
<html lang="zh-Hant">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:image" content="{{url('assets/img/shortcut.png')}}" />
    <title>PhysiolGuard</title>

    <!-- Styles -->
    <link href="{{url('/thirdpart/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--<link href="/thirdpart/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
    <link href="{{url('/css/app.css')}}" rel="stylesheet">
    <link href="{{url('/css/cookie_privacy.css')}}" rel="stylesheet">
    @yield('css_file')  
    <!-- Scripts -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();
            a=s.createElement(o),m=s.getElementsByTagName(o)[0];
            a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-86342545-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
		function url(uri){
            if(!uri){
                uri='/';
            }else if(uri[0]!='/'){
                uri='/'+uri;
            }
            return '{{ url("/") }}'+uri;
        }
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
        var lang = "{{App::getLocale()=='en-US' ? 'en':'zh-TW'}}";
    </script>
</head>
<body>
    <div id="wrap">
        <header class=" {{ Route::currentRouteName()!='home'?'':'hide'}}">
            <nav class="navbar navbar-default navbar-static-top" id="topbar" data-spy="affix" data-offset-top="51">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/') }}">
                                PhysiolGuard
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            @if(App::getLocale()=='en-US')
                                <li><a href="{{ url('/lang/set/zh-TW') }}">中文</a></li>
                            @else
                                <li><a href="{{ url('/lang/set/en-US') }}">English</a></li>
                            @endif
                            @if (Auth::guest())
                                <li ><a href="{{ url('/login') }}">Login</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        @can('edit_profile')
                                        @php
                                        /*
                                        <li><a href="{{ url('/userprofile') }}"><span class="glyphicon glyphicon-user"></span> @lang('physiolguard.profile')</a></li>
                                        */
                                        @endphp
                                        @endcan
        
                                        <li>
                                            <a href="#" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                <span class="glyphicon glyphicon-off"></span> @lang('physiolguard.logout')
                                            </a>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                        <div class="arrow-border"></div> 
                                        <div class="arrow"></div> 
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <section id="content">
            @yield('content')        
        </section>
    </div>
    @include('layouts.footer')
    @include("layouts.cookie_privacy")
    <!-- Scripts -->
    <script src="{{url('/thirdpart/jquery/jquery-3.1.0.min.js')}}"></script>
    <script src="{{url('/thirdpart/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('/js/ie.js')}}"></script>
    <script src="{{url('/thirdpart/bootbox/bootbox.min.js')}}"></script>
    <script src="{{url('/thirdpart/bootbox/bootbox.locales.min.js')}}"></script>

    <script src="{{url('/thirdpart/js-cookie/js.cookie.min.js')}}"></script>
    <script src="{{url('/js/cookie_privacy.js')}}"></script>
    <script> 
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
    </script>
    @yield('javascript')  
</body>
</html>
