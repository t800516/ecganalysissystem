<aside id="sidebar" class="sidebar-nav">
	<ul class="nav nav-pills nav-stacked">
		@if(in_array(Auth::guard('admin')->user()->auth, [0]))
		<li><a href="{{url('/admin/member')}}"><span class="glyphicon glyphicon-user"></span> 會員管理</a></li>
		<li><a href="{{url('/admin/holter')}}"><span class="glyphicon glyphicon-scale"></span> Holter管理</a></li>
		<li><a href="{{url('/admin/department')}}"><span class="glyphicon glyphicon-briefcase"></span> 報告管理</a></li>
		<li><a href="{{url('/admin/priority')}}"><span class="glyphicon glyphicon-bell"></span> 優先權管理</a></li>
		<li><a href="{{url('/admin/apks')}}"><span class="glyphicon glyphicon-hdd"></span> APK管理</a></li>
		<li><a href="{{url('/admin/programs')}}"><span class="glyphicon glyphicon-floppy-disk"></span> 程式管理</a></li>
		@endif
		@if(in_array(Auth::guard('admin')->user()->auth, [0, 10]))
		<li><a href="{{url('/admin/files')}}"><span class="glyphicon glyphicon-file"></span> 檔案管理</a></li>
		@endif
		@if(in_array(Auth::guard('admin')->user()->auth, [0]))
		<li><a href="{{url('/admin/users')}}"><span class="glyphicon glyphicon-user"></span> 後台使用者管理</a></li>
		@endif
	</ul>
</aside>