<div class="alert text-center cookie-privacy hide" role="alert">
    We use cookie to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it.
    <div class="alert-btn-box">
        <button type="button" class="btn btn-primary" id="cookie-privacy-accept">Ok</button>
        <button type="button" class="btn btn-primary">No</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#privacy-modal">Privacy policy</button>
    </div>
</div>

<div class="modal fade" id="privacy-modal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="privacyModalLabel">Privacy Policy</h4>
      </div>
      <div class="modal-body">
        <li>Data collected on the basis of consent</li>
        <p>Upon your request and expression of consent, we collect the following data for the purpose of providing services to you. Your data is not used for any other purposes or shared with third parties. It is removed upon your withdrawal of consent or your request to terminate theses services.</p>
        <li>Comments</li>
        <p><b>Name, email address, content of the comment:</b> this data is collected when you leave a comment and displayed on the Website.</p>
        <p>If you leave a comment on the Website, your name and email address will also be saved in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will be saved on your computer until you delete them.</p>
        <p><b>IP and browser user agent string:</b> this data is collected when you leave a comment.<p>
        <p><b>Retention period:</b> the aforementioned data is retained indefinitely so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>
        <li>Data collected on the basis of legitimate interest</li>
        <p>Based on our <b>legitimate interests</b>, we collect the following data for the purpose of running this website. Your data is not used for any other purposes or shared with third parties. It is removed upon your request.</p>
        <li>Statistics</li>
        <p>The website uses a minimal build of Google Analytics, a service which transmits website traffic data to Google servers in the United States and allows us to notice trends to improve the user experience on our website. This minimal build processes personal data such as: the unique User ID set by Google Analytics, the date and time, the title of the page being viewed, the URL of the page being viewed, the URL of the page that was viewed prior to the current page, the screen resolution, the time in local timezone, the files that were clicked on and downloaded, the links clicked on to an outside domain, the type of device, and the country, region, and city.</p>
        <p>You may opt out of this tracking at any time by activating the “Do Not Track” setting in your browser.</p>
        <li>Embedded content from other websites</li>
        <p>Articles on the Website may include embedded content (e.g. videos, charts, etc.). Embedded content from other websites behaves in the exact same way as if the visitor had visited the other website.</p>
        <p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p>
        <li>Your rights pertaining your data</li>
        <p>If you have left comments on the Website, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we rectify or erase any personal data we hold about you. Please send your request to <a href="mailto:service@physiolguard.com">service@physiolguard.com</a></p>
        <p>• The right to withdraw consent</p>
        <p>• The right of access</p>
        <p>• The right to erasure</p>
        <p>• The right to rectification</p>
        <p>• The right to data portability</p>
        <p>• The right to object</p>
        <p>• Notification of data breaches</p>
        <p>• The right to lodge a complaint with a supervisory authority</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>