@extends('layouts.app')
@section('css_file')
    <link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
    <link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
    <link href="{{url('/css/filemanager.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="content">
        @include('layouts.sidebar')
        <article id="main">
            @lang("physiolguard.no_report_download")
        </article>
    </div>
</div>
@endsection
@section('javascript')
<script>
    window.close();
</script>
@endsection