<table style="border:1px solid #333; width:100%;text-align: center;">
	<tr>
		<th style="border:1px solid #333;">@lang("physiolguard.email")</th>
		<th style="border:1px solid #333;">@lang("physiolguard.filename")</th>
		<th style="border:1px solid #333;">@lang("physiolguard.patient_IDNumber")</th>
		<th style="border:1px solid #333;">@lang("physiolguard.patient_name")</th>
		<th style="border:1px solid #333;">@lang("physiolguard.upload_date")</th>
	</tr>
	@foreach($ecgs as $ecg)
		<tr>
			<td style="border:1px solid #333;">{{$ecg->user->email}}</td>
			<td style="border:1px solid #333;">{{$ecg->filename}}</td>
			<td style="border:1px solid #333;">{{$ecg->patient_IDNumber}}</td>
			<td style="border:1px solid #333;">{{$ecg->patient_name}}</td>
			<td style="border:1px solid #333;">{{$ecg->updated_at}}</td>
		</tr>
	@endforeach
</table>