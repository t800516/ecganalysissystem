@extends('layouts.app')
@section('css_file')
<style>
	.row{
		text-align: center;
		font-size:24px; 
	}
	.bg-info{

		padding: 5%;
		width: 50%;
		margin: 5% auto;
		border-radius: 10px;
	}
</style>
@endsection
@section('content')
<div class="container">
	<div class="content">
		<div class="row">
			<div class="bg-info">
				<h2>@lang('physiolguard.password_reset_success')</h2>
				<p>@lang('physiolguard.auto_login')</p>
				<p><a href="{{url('/')}}">@lang('physiolguard.back_home')</a></p>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection