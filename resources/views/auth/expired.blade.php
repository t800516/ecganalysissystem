@extends('layouts.app')
@section('css_file')
<style>
	.row{
		text-align: center;
		font-size:24px; 
	}
	.bg-info{

		padding: 5%;
		width: 50%;
		margin: 5% auto;
		border-radius: 10px;
	}
</style>
@endsection
@section('content')
<div class="container">
	<div class="content">
		<div class="row">
			<div class="bg-info">
				<h2>@lang('physiolguard.auth_expired')</h2>
				<p>@lang('physiolguard.auth_expired_msg')</p>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script>
</script>
@endsection