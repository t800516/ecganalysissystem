@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/login.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		<div class="row">
			<form action="{{url('/login')}}" method="post" class="form-horizontal">
				<fieldset>
				    <div>
				      <legend class="">@lang('physiolguard.login')</legend>
		    		</div>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<div class="controls col-md-12">
							<input type="email" name="email" id="email" class="form-control" placeholder="E-mail">
						</div>
					</div>
					<div class="form-group">
						<div class="controls col-md-12">
							<input type="password" name="password" id="password" class="form-control" placeholder="@lang('physiolguard.password')">
						</div>
					</div>
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-success form-control" value="@lang('physiolguard.do_login')">
					<a class="btn btn-link" href="{{url('/forgotpassword')}}">
                        @lang('physiolguard.forgot')
                    </a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('javascript')
<script>
$(function(){
	$('#email').focus();
});
</script>
@endsection