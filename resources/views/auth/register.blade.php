@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/register.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="content">
		<div class="row">
			<form id="register_form" action="{{url('/register')}}" method="post" class="form-horizontal">
				<fieldset>
				    <div>
				      <legend class="">註冊資料</legend>
		    		</div>
		    		<ul class="errormsg list-group">
 						@foreach($errors->all() as $key=>$error)
							<li class="list-group-item list-group-item-danger">{{$error}}</li>
						@endforeach
					</ul>
					<div class="form-group">
						<label for="account" class="control-label col-md-2">帳號</label>
						<div class="controls col-md-8">
							<input type="text" name="account" id="account" class="form-control" required>
						</div>
						<div class="col-md-2 account_msg"> </div>
					</div>
					<div class="form-group">
						<label for="name" class="control-label col-md-2">名稱</label>
						<div class="controls col-md-8">
							<input type="text" name="name" id="name" class="form-control" required>
						</div>
						<div class="col-md-2 name_msg"> </div>
					</div>
					<div class="form-group">
						<label for="email" class="control-label col-md-2">email</label>
						<div class="controls col-md-8">
							<input type="email" name="email" id="email" class="form-control" required>
						</div>
						<div class="col-md-2 email_msg"> </div>
					</div>
					<div class="form-group">
						<label for="password" class="control-label col-md-2">密碼</label>
						<div class="controls col-md-8">
							<input type="password" name="password" id="password" class="form-control" required>
						</div>
						<div class="col-md-2 password_msg"> </div>
					</div>
					<div class="form-group">
						<label for="password_confirmation" class="control-label col-md-2">確認密碼</label>
						<div class="controls col-md-8">
							<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" required>
						</div>
						<div class="col-md-2 password_confirmation_msg"> </div>
					</div>
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<div class="col-md-8 col-md-offset-2">
						<div class="controls col-md-1">
							<input type="checkbox" id="agree">
						</div>
						<label for="agree" class="col-md-11">我已閱讀並同意《<a href="#" id="agreeinfo_btn">資料使用條款</a>》</label>
					</div>
				</fieldset>
				<div class="col-md-4 col-md-offset-4">
					<input type="submit" class="btn btn-primary form-control" value="註冊">
				</div>
			</form>
		</div>
	</div>
</div>
<div id="agree_panel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="">
  	<div class="modal-dialog">
  	<div class="modal-content"> 
  		<div class="modal-header"> 
  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  				<span aria-hidden="true">×</span>
  			</button> 
					<h4 class="modal-title">資料使用條款</h4>
  		</div> 
  		<div class="modal-body">
  			<div id="agree_info">
				<div class="agree_content">
					<pre>
一、資料使用條款
	1.當會員完成本網站之會員註冊手續或開始使用本網站服務時，即表示已閱讀並同意接受本網站服務條款之所有內容。
	2.本網站有權於任何時間修改或變更本服務條款內容，修改後將公布本網站上，本網站不再個別通知會員，建議會員隨時注意該等修改或變更。會員於任何修改或變更後繼續使用本網站服務時，視為會員已瞭解並同意接受該等修改或變更。
	3.若不同意上述的條款修訂或更新，或不接受本服務條款的任一約定，會員應立即停止使用本網站服務。會員同意使用本網站服務所生權利義務，得以電子文件為作表示方式。</pre>
					</div>
					<div>
					<label>
					<input id="agree_check" type="checkbox">
						我已閱讀並同意資料使用條款
					</label>
					</div>
					<hr/>
					<div>
						<button id="agree_btn" class="btn btn-default">確認</button>
				</div>
			</div>
		</div>
  	</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('javascript')
<script>
	$(function(){
		$('#account').focus();
		$('#password_confirmation').blur(function(){
			if($(this).val()!=$('#password').val())
				console.log('password fail');
		});
		$("#agreeinfo_btn").click(function(){
			$("#agree_panel").modal('show');
		});
		$("#agree_btn").click(function(){
			$("#agree_panel").modal('hide');
		});
		$("#agree_check").change(function(){
			$("#agree").prop('checked',$(this).prop('checked'));
		});
		$("#agree").change(function(){
			$("#agree_check").prop('checked',$(this).prop('checked'));
		});

		$("#register_form").submit(function(){
			if(!$("#agree").prop('checked')){
				alert('請同意資料使用條款');
				return false;
			}
		});
	});
</script>
@endsection