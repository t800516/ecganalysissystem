@if($department)
	@if(Auth::user()->can('report_type_by_comment'))
		<div class="comment mb-3">
			<h4 class="section_title">@lang("physiolguard.comment")</h4>
			<div id="comment" class="comment">
				<textarea id="report_type_comment_input" class="form-control" style="min-height: 240px;resize: vertical">{{$comment}}</textarea>
			</div>
		</div>
	@else
		@foreach($department->report_types as $key => $report_type)
			<div class="row">
				<label class="col-md-5">
					<input type="{{ $department->select_type==0 ? 'radio':'checkbox' }}" value="{{$report_type->value}}" name="report_tpye" class="report_tpye"  data-name="{{$report_type->name}}">{{$report_type->name}} 
				</label>
				@if($report_type->duration==1)
					<div class="col-md-7">
						<div class="col-md-6 text-right">
							@lang('physiolguard.duration_time')[{{$report_type->duration_low.'-'.$report_type->duration_up}}]:
						</div>
						<label class="col-md-6">
							<input type="number" min="{{$report_type->duration_low}}" value="{{$report_type->duration_low}}" max="{{$report_type->duration_up}}" name="report_tpye_duration" class="report_tpye_duration form-control" data-name="{{$report_type->name}}">
						</label>
					</div>
				@endif
			</div>
		@endforeach
	@endif
@endif