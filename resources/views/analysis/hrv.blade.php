@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/hrvanalysis.css')}}" rel="stylesheet">

@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4 class="pull-left title">@lang("physiolguard.hrv_analysis")＞{{$filename}}</h4>
			<div id="toolbar">
				<ul class="btn-toolbar">
					<li>
						<div class="export_box">
							<form id="export_form" action="{{url('/analysis/hrv/export')}}" method="POST">
								 {{ csrf_field() }}
								<input type="hidden" name="id" value="{{$ecg_id}}">
								<input type="hidden" name="interval" value="5">
								<input type="hidden" name="type" value="xlsx">
								<input type="submit" id="export_btn" class="btn btn-success" value="@lang('physiolguard.export')">
							</form>
						</div>
					</li>
					<li>
						<div class="interval_select_box">
							<div class="col-sm-5">
								@lang("physiolguard.analysis_range"):
							</div>
							<div class="col-sm-3">
							@can('hrv_interval_input')
								<select id="interval_select" class="form-control">
									<option value="5"> 5 @lang("physiolguard.minute")</option>
									<option value="10">10 @lang("physiolguard.minute")</option>
									<option value="15">15 @lang("physiolguard.minute")</option>
									<option value="30">30 @lang("physiolguard.minute")</option>
									<option value="60">60 @lang("physiolguard.minute")</option>
								</select>
							@else
								<div class="form-control">
									5 @lang("physiolguard.minute")
									<input type="hidden" id="interval_select" value="5">
								</div>
							@endcan
							</div>
							<div class="col-sm-4">
								<button id="run_btn" class="btn btn-default">@lang("physiolguard.analysis_start")</button>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<hr/>
			<div id="hrv_loading" class="ajax_loading">
				<img src="/assets/img/loading.gif">
			</div>
			<div id="analysis_result">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#table_content">@lang("physiolguard.table")</a></li>
					@can('hrv_chart')
						<li><a data-toggle="tab" href="#chart_content">@lang("physiolguard.trend_chart")</a></li>
					@endcan
				</ul>
				<div class="tab-content" >
					<div id="table_content" class="tab-pane fade in active">
						<table id="analysis_result_table" class="table table-condensed">

						</table>
					</div>
					@can('hrv_chart')
						<div id="chart_content" class="tab-pane fade">
							<div id="chart_container"></div>
							<div id="legend_container">
								
							</div>
						</div>
					@endcan
				</div>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-zh-TW.min.js')}}"></script>
<script src="{{url('/thirdpart/d3/d3.min.js')}}"></script>
<script>
$(function(){
	var hrvData=[];
	init_chart('.tab-content','#chart_container');

	$("#analysis_result").hide();
	$("#hrv_loading").hide();
	$("#interval_select").change(function(event){
		$("#export_form input[name=interval]").val($(this).val());
	});
	
	$('#run_btn').click(function(event){
		$("#analysis_result").hide();
		$("#hrv_loading").show();
		$('#analysis_result_table').bootstrapTable('destroy');
		var interval=$("#interval_select").val();
		$.get(url("/api/v0/analysis/hrv/{{$ecg_id}}/"+interval),function(result){
			var data=hrvData=result.hrv_data;
			columns=[];
			for(var index in data[0]){
				columns.push({field:data[0][index],title:data[0][index]});
			}
			tabledata=[];
			for(var i=1;i<data.length;i++){
				rowdata={};
				for(var j=0;j<data[i].length;j++){
					rowdata[data[0][j]]=data[i][j];
				}
				tabledata.push(rowdata);
			}
			$('#analysis_result_table').bootstrapTable({
				columns:columns,
				data:tabledata
			});
			
			$("#analysis_result").show();
			$("#hrv_loading").hide();
			
			draw_chart(data,'#chart_content','#chart_container');
		});
	});

	$('#legend_container').on('change','input',function(event){
		draw_chart(hrvData,'#chart_content','#chart_container');
	});

	$('#legend_container').on('mouseover','label',function(event){
		var legend=$(this).text();
		var chart = d3.select('#chart_container').select('svg .chart').selectAll("path").each(function(d,i){
			if(d.key==legend){
				d3.select(this).style("stroke-width", 5);
			}
		});
	});
	$('#legend_container').on('mouseout','label',function(event){
		var chart = d3.select('#chart_container').select('svg .chart').selectAll("path").each(function(d,i){
			d3.select(this).style("stroke-width", 2);
		});
	});
	$(window).on('resize',resize);

	function init_chart(container,selector){
		var margin = {top: 20, right: 50, bottom: 20, left: 50};
		var origin_width = 1550;
		var width = $(container).width() - margin.left - margin.right;
		var height = $(selector).height() - margin.top - margin.bottom;

		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var x = d3.scaleLinear().range([0,  origin_width]);
		var y = d3.scaleLinear().range([height,0]);
		
		var xAxis = d3.axisBottom().scale(x).ticks($("#interval_select").val()).tickFormat(function(d){
				var sec=d%60;
				var min=Math.floor(d/60);
				return min+':'+sec;
			});
		var yAxis = d3.axisLeft().scale(y);

		svg.append("g").attr("class","chart")
		
		var axis=svg.append("g")
					.attr("class", "axis");
		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(0," + height + ")");
      		//.call(xAxis);
  		axis.append("g")
      		.attr("class", "axisy");
      		//.call(yAxis);

	}
	function draw_chart(data,container,selector){
		var margin = {top: 20, right: 50, bottom: 20, left: 50};
		var origin_width = 1550;
		var width = $(container).width() - margin.left - margin.right;
		var height = $(selector).height() - margin.top - margin.bottom;
		var filter_data = filterData(data);
		var tranData = filter_data[0].map(function(d,i){
			return {
				key : d,
				values : filter_data.slice(1).map(function(p){
					return {
						time : p[0],
						value : p[i]
					};
				})
			};
		}).slice(1);
		var maxX = d3.max(filter_data.slice(1), function (d) { return timeToSecond(d[0]); });
		var maxY = d3.max(filter_data.slice(1), function (d) { return d3.max(d.slice(1),function(p){
				return parseFloat(p);
			}); 
		});
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
		var y = d3.scaleLinear().range([height,0]).domain([0, maxY]);
		var z = d3.scaleOrdinal(d3.schemeCategory20c).domain(data[0].slice(1).map(function(c) { return c; }));
		var xAxis = d3.axisBottom().scale(x).ticks($("#interval_select").val()).tickFormat(function(d){
				var sec=d%60;
				var min=Math.floor(d/60);
				return min+':'+sec;
			});

		var yAxis = d3.axisLeft().scale(y);

		var line = d3.line().x(function (d) { return x(timeToSecond(d.time)); }).y(function (d) { return y(d.value); });
		
		d3.select(selector)
			.select('svg .axisx')
			.call(xAxis);

		d3.select(selector)
			.select('svg .axisy')
			.call(yAxis);
		/*
		var svg=d3.select(selector).select('svg')
		var axis=svg.append("g")
					.attr("class", "axis");
		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(0," + height + ")")
      		.call(xAxis);
  		axis.append("g")
      		.attr("class", "axisy")
      		.call(yAxis);
      		*/
		var chart = d3.select(selector).select('svg .chart')
			.selectAll('.feature')
			.data([]);
		chart.exit().remove();

		var chart = d3.select(selector).select('svg .chart')
			.selectAll('.feature')
			.data(tranData);
			
   		chart.enter().append("g")
   			.attr("class", "feature")
			.append("path")
			.attr("class", "line")
			.attr("d", function(d){ return line(d.values)})
			.style("stroke", function(d) { return z(d.key); })
			.on("mouseover", function(d){
				var path=d3.select(this);
				path.style("stroke-width", 5);
			}).on("mouseout", function() {
				var path=d3.select(this);
				path.style("stroke-width", 2);
  			});
		chart.exit().remove();
		

		if($('#legend_container>div').length==0){
			var category=z.domain();
			for(var index in category){
					$('#legend_container').append(
						'<div>'+
						'<input type="checkbox" id="legend_'+index+'" value="'+category[index]+'" checked>'+
						'<label for="legend_'+index+'">'+
						'<div class="legend_rect" style="background-color:'+z(category[index])+'"></div>'+category[index]+'</label>'+
						'</div>');
			}
		}
	}
	function filterData(data){
		var fitler_data=[];
		var filter = $('#legend_container input:not(:checked)').map(function(i,d){return $(d).val();});
		var filterIndex=[];
		for (var i=0;i<data[0].length;i++){
			if($.inArray(data[0][i],filter)!=-1){
				filterIndex.push(i);
			}
		}
		
		for(var i=0;i<data.length;i++){
			var rowdata=[];
			for(var j=0;j<data[i].length;j++){
				if(filterIndex.indexOf(j)!=-1){
					continue;
				}
				rowdata.push((i!=0&&j!=0&&isNaN(data[i][j]))?"0.0":data[i][j]);
			}
			fitler_data.push(rowdata);
		}
		return fitler_data;
	}

	function resize(event){
		var margin = {top: 20, right: 50, bottom: 20, left: 50};
		var origin_width = 1550;
		var width = $('#chart_content').width() - margin.left - margin.right;
		d3.select('#chart_container')
			.select('svg')
			.select('g')
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");
	}

	function timeToSecond($time){
		var time_arr=$time.split(':');
		return parseInt(time_arr[0],10)*60 + parseInt(time_arr[1],10);
	}	
});
</script>
@endsection