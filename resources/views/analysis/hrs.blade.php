@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/ecganalysis.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<link href="{{url('/css/hrs_chart.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<meta http-equiv="pragma" content="no-cache">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4 class="pull-left title">Monitor ＞{{$holter->IDNumber}}＞數值播回</h4>
			<hr/>
			<div id="loading" class="ajax_loading">
				<img src="{{url('/assets/img/loading.gif')}}">
			</div>
			<div class="action_bar text-right">
				<div class="holter-info">
					<span class="holter-info-item">ID : {{$ID}}</span>
					<span class="holter-info-item">Name : {{$name}}</span>
					<span class="holter-info-item">Bed No : {{$bed_no}}</span>
				</div>
				<div id='range_btns' class="btn-group range_btns" data-toggle="buttons">
					<label class="btn btn-default active">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="0" checked> All 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="30"> 30 sec 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="60"> 1 mins 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="300"> 5 mins 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="1800"> 30 mins 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="3600"> 1 hr 
					</label>
				</div>
			</div>
			<div class="chart" id="HRView"  data-type='hr' data-sleep-index="0" data-sample-rate="1" data-y-scale-ratio="1" data-y-scale-low-bound="45" data-y-scale-up-bound="120" data-line-color="orange">
				<div class="title orange"><span class="cht">@lang("physiolguard.hrv_view")</span><span class="eng"> (HR)</span></div>
				
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="RespRView" data-type='respr' data-sleep-index="1" data-sample-rate="1" data-y-scale-ratio="1" data-y-scale-low-bound="0" data-y-scale-up-bound="45" data-line-color="yellow">
				<div class="title yellow"><span class="cht">@lang("physiolguard.respr_view")</span><span class="eng"> (Respiratory rate)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="SPO2View"  data-type='spo2' data-sleep-index="2" data-sample-rate="1" data-y-scale-ratio="1" data-y-scale-low-bound="45" data-y-scale-up-bound="105" data-line-color="green">
				<div class="title green"><span class="cht">@lang("physiolguard.spo2_view")</span><span class="eng"> (SpO2)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/d3/d3.v4.min.js')}}"></script>
<script src="{{url('/thirdpart/moment/moment.js')}}"></script>
<script src="{{url('/thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/js/hrs_chart.js')}}?v={{env('STATIC_FILE_VERSION','1.0.2')}}"></script>
<script>
	var hotler_IDNumber = "{{$holter->IDNumber}}";
	var sampleRate = 256;
	var hrsData=[];
	var hrsExData = [];
	var tagPositionData = [];
	var maxDataLenIndex = 0;
	var dataTotalTime = 0;
	var dataNum = $('.chart').length;
	var mainCahrt = $('#HRView');
	var svgWidth = parseFloat(mainCahrt.width());
	var windowWidth = $(window).width();
	$(function(){
		$("#sidebar").addClass("sidebarhide").append('<div id="sidebarmenu"><span class="glyphicon glyphicon-menu-hamburger"></span></div>');
		
		$('#message').html('');
		loadHrsData(function(){
			maxDataLenIndex = findMaxHzIndex();
			dataTotalTime = hrsData[maxDataLenIndex].length / parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate')));
			svgWidth = parseFloat(mainCahrt.find('.chartbox').width()) - scrollbarWidth;
			
			$('#range_btns input[type=radio]').each(function(index,d){
				if(parseInt($(d).val(),10) > dataTotalTime){
					$(d).addClass('inactive')
					$(d).parent().addClass('disabled')
					$(d).prop('disabled',true)
				}else{
					$(d).removeClass('inactive')
					$(d).parent().removeClass('disabled')
					$(d).prop('disabled',false)
				}
			});

			updateAllView(0, false);
		});
		$('#range_btns input.range_radio').on('click',function(event){
			event.preventDefault();
			return !$(this).hasClass('inactive');
		});
		$('#range_btns').on('change','input.range_radio',function(event){
			if(!$(this).hasClass('inactive')){
				updateChart();
			}
		});

		$('.action').on('click','.yZoomIn',function(event){
			var view = $(this).parent().parent();
			chartZoom(view, 1);
		});
		$('.action').on('click','.yZoomOut',function(event){

			var view = $(this).parent().parent();
			chartZoom(view, -1);
		});
		$(".chartbox").on('mousewheel',function(event) {
            this.scrollLeft -= event.originalEvent.wheelDelta;
            event.preventDefault();
        });
		$('.chartbox').scroll(function(event){
			var preventScrollEvent = $(this).data('prevent-scroll-event');
			if(preventScrollEvent){
				$(this).data('prevent-scroll-event',false);
				return false;
			}
			var current_left_scroll = $(this).scrollLeft();
			var lastLeftScroll = $(this).data('last-scroll-left');

			if(lastLeftScroll != current_left_scroll){
				$(this).data('last-scroll-left',current_left_scroll);
				var range_select = $('#range_btns input[type=radio]:checked').val();
				if(range_select!=0){
					$(this).data('dont-set-scroll-left',true);
					updateAllView(current_left_scroll, true);
					$(this).data('dont-set-scroll-left', false);
				}

			}
			var current_top_scroll = $(this).scrollTop();

			var lastTopScroll = $(this).data('last-scroll-top');

			if(lastTopScroll!=current_top_scroll){
				$(this).data('last-scroll-top',current_top_scroll);
				var chart = $(this).parent();
				var container_id = chart.attr('id');
				updateAxisYPosition("#"+container_id, current_top_scroll);
			}
		});

		$(window).resize(function(event){
			var currentWindowWidth = $(window).width();
			if(currentWindowWidth != windowWidth){
				resize();
				windowWidth = currentWindowWidth;
			}
		});
	});
	var ajaxCallback = function(index, callback){
		return function(data, textStatus, jqXHR){
			if(data.errorcode==null){
				hrsData[index] = data.data.sort(function(a, b) {
					return a[0]< b[0];
				});
				var meanY = d3.mean(hrsData[index], function (d) { return d[1]; });
				var minY = d3.min(hrsData[index], function (d) { return d[1]; });
				var maxY = d3.max(hrsData[index], function (d) { return d[1]; });
				var minX = d3.min(hrsData[index], function (d) { return d[0]; });
				var maxX = d3.max(hrsData[index], function (d) { return d[0]; });
				hrsExData[index] = {
					meanY : meanY,
					minY : minY,
					maxY : maxY,
					minX : minX,
					maxX : maxX,
				};
				dataNum--;
				if(dataNum==0){
					callback();
				}
			}
		};
	}

	function loadHrsData(callback){
		$('#loading').show();
		$('.chart').each(function(index, d){
			$.get(url('monitor/'+hotler_IDNumber+'/hrs/data')+'?type='+$(d).data('type'), ajaxCallback(index, function(){
				callback();
				$('#loading').hide();
			}));
		});
	}

	function drawChart(hrsDataIndex, positionX){
		var viewData = getViewByIndex(hrsDataIndex);
		var chartData = hrsData[hrsDataIndex];
		var range_select = $('#range_btns input[type=radio]:checked').val();
		var start = getStart(positionX, viewData.sampleRate);
		var end_temp = start+parseInt(range_select * viewData.sampleRate , 10);
		var end = range_select == 0 ? chartData.length - 1 : (end_temp >= chartData.length ? chartData.length -1 : end_temp - 1 );
		var range = {
			start: start, 
			end: end
		};
		positionX = range_select == 0 ? 0 : positionX;
		var exData = {
			positionX : positionX,
			rangeType:range_select,
			maxDataLen : hrsData[maxDataLenIndex].length,
			maxDataLenSampleRate : parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))),
			maxDataSecond : hrsData[maxDataLenIndex].length / parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))),
			dataLen: hrsData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) / viewData.sampleRate),
			meanY : hrsExData[hrsDataIndex].meanY,
			minX : hrsExData[hrsDataIndex].minX,
			maxX : hrsExData[hrsDataIndex].maxX,
			minY : hrsExData[hrsDataIndex].minY,
			maxY : hrsExData[hrsDataIndex].maxY
		};
		draw_chart(viewData.viewId, chartData, range, exData);
	}

	function getStart(positionX, sampleRate){
		var range_select = $('#range_btns input[type=radio]:checked').val();
		range_select = range_select == 0 ? 1 : range_select;
		var dataIndex = Math.ceil( ((positionX  * (range_select-1)) * sampleRate) / svgWidth);
		return dataIndex;
	}

	function getDataIndex(sleepIndex, positionX, sampleRate){
		var range_select = $('#range_btns input[type=radio]:checked').val();
		range_select = range_select == 0 ? hrsData[sleepIndex].length : parseInt(range_select,10);
		var dataIndex = Math.floor((range_select-1) * sampleRate * (positionX / svgWidth) );
		return dataIndex;
	}

	function getViewByIndex(hrsDataIndex){
		var viewData= {
			viewId : '#'+$('.chart').eq(hrsDataIndex).attr('id'),
			sampleRate : parseFloat(eval($('.chart').eq(hrsDataIndex).data('sample-rate')))
		};
		return viewData;
	}

	function updateAllView(positionX){
		for (var i = 0; i < $('.chart').length; i++) {
			drawChart(i, positionX);
		}
	}
	function updateChart(){
		var range_select = $('#range_btns input[type=radio]:checked').val();
		var x = mainCahrt.find('.chartbox').scrollLeft();
		var dataLen = hrsData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) * 30);
		var range = range_select == 0 ? 0 : (range_select/30 * svgWidth/dataLen);

		x = ((x + range) > svgWidth ) ? (svgWidth - range) : x;
		updateAllView(x, false);
	}

	function findMaxHzIndex(){
		var max = 0;
		var index = 0;
		for (var i = 0; i < hrsData.length; i++) {
			var len = hrsData[i].length;
			if(len>max){
				max = len;
				index = i;
			}
		}
		return index;
	}
	function resize(){
		svgWidth = parseFloat(mainCahrt.find('.chartbox').width()) - scrollbarWidth;
		var range_select = $('#range_btns input[type=radio]:checked').val();
		var range = range_select/30 * svgWidth/(hrsData[maxDataLenIndex].length);
		var svgOffset = mainCahrt.find('.chartbox').offset();
		var x = 0;
		updateAllView(x, false);
	}
	function chartZoom(view, value){
		var chartbox = view.find('.chartbox');
		chartbox.data('last-scroll-top',0);
		var yScaleRatio = parseFloat(view.attr('data-y-scale-ratio')) + value;
		if(yScaleRatio<1){
			yScaleRatio=1;
		}
		view.attr('data-y-scale-ratio',yScaleRatio);
		var x = mainCahrt.find('.chartbox').scrollLeft();
		drawChart(view.data('sleep-index'), x, false);
	}
</script>
@endsection