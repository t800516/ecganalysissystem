@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/ecganalysis.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<link href="{{url('/css/monitor.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<meta http-equiv="pragma" content="no-cache">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4 class="pull-left title">Monitor ＞{{$holter ? $holter->IDNumber : ''}}</h4>
			<hr/>
			<div id="loading" class="ajax_loading">
				<img src="{{url('/assets/img/loading.gif')}}">
			</div>
			<div class="holter-info">
				<span class="holter-info-item">ID : {{$ID}}</span>
				<span class="holter-info-item">Name : {{$name}}</span>
				<span class="holter-info-item">Bed No : {{$bed_no}}</span>
			</div>
			<div class="monitor">
				<div class="data_view">
					<div class="value_box orange" id="heart_rate_value">
						<div class="title">
							Heart Rate
							<span class="glyphicon glyphicon-info-sign red blinking hide warning-icon"></span>
						</div>
						<div class="value blinking">－</div>
					</div>
					<div class="value_box yellow" id="respiration_rate_value">
						<div class="title">
							Respiration Rate
							<span class="glyphicon glyphicon-info-sign red blinking hide warning-icon"></span>
						</div>
						<div class="value blinking">－</div>
					</div>
					<div class="value_box green" id="spo2_value">
						<div class="title">
							Spo2
							<span class="glyphicon glyphicon-info-sign red blinking hide warning-icon"></span>
						</div>
						<div class="value blinking">－</div>
					</div>
					<div class="action">
						<div class="btn btn-info yZoomIn"><span class="glyphicon glyphicon-plus"></span></div>
						<div class="btn btn-info yZoomOut"><span class="glyphicon glyphicon-minus"></span></div>
					</div>
					<div class="btn_box" id="data_back">
						<a href="{{$holter ? url('monitor/'.$holter->id.'/backplay'):'#'}}">
							<div class="btn-border">
								<div  class="btn btn-default">
									數值播回
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="chart_view">
					<div class="chart" id="ecg_l1" data-sleep-index="0" data-sample-rate="512" data-y-scale-ratio="1" data-svg-height="" data-y-scale-low-bound="-25000" data-y-scale-up-bound="25000">
						<div class="title">ECG L1</div>
						<div class="chartbox">
							<svg class="chart_svg">
								<g class="grid_box"></g>
								<svg class="line_0"><g class="pathbox"></g></svg>
								<svg class="line_1"><g class="pathbox"></g></svg>
								<g class="updatebar"><rect class="current"></rect><rect class="circle"></rect></g>
							</svg>
						</div>
					</div>
					<div class="chart" id="ecg_l2" data-sleep-index="1" data-sample-rate="512" data-y-scale-ratio="1" data-svg-height="" data-y-scale-low-bound="-25000" data-y-scale-up-bound="25000">
						<div class="title">ECG L2</div>
						<div class="chartbox">
							<svg class="chart_svg">
								<g class="grid_box"></g>
								<svg class="line_0"><g class="pathbox"></g></svg>
								<svg class="line_1"><g class="pathbox"></g></svg>
								<g class="updatebar"><rect class="current"></rect><rect class="circle"></rect></g>
							</svg>
						</div>
					</div>
					<div class="chart" id="ecg_l3" data-sleep-index="2" data-sample-rate="512" data-y-scale-ratio="1" data-svg-height="" data-y-scale-low-bound="-25000" data-y-scale-up-bound="25000">
						<div class="title">ECG L3</div>
						<div class="chartbox">
							<svg class="chart_svg">
								<g class="grid_box"></g>
								<svg class="line_0"><g class="pathbox"></g></svg>
								<svg class="line_1"><g class="pathbox"></g></svg>
								<g class="updatebar"><rect class="current"></rect><rect class="circle"></rect></g>
							</svg>
						</div>
					</div>
					<div class="chart" id="ppg_r" data-sleep-index="3" data-sample-rate="512" data-y-scale-ratio="1.3" data-svg-height="140" data-y-scale-low-bound="-25000" data-y-scale-up-bound="25000">
						<div class="title">PPG R</div>
						<div class="chartbox">
							<svg class="chart_svg">
								<g class="grid_box"></g>
								<svg class="line_0"><g class="pathbox"></g></svg>
								<svg class="line_1"><g class="pathbox"></g></svg>
								<g class="updatebar"><rect class="current"></rect><rect class="circle"></rect></g>
							</svg>
						</div>
					</div>
					<div class="chart" id="ppg_ir" data-sleep-index="4" data-sample-rate="512" data-y-scale-ratio="1.3" data-svg-height="140" data-y-scale-low-bound="-25000" data-y-scale-up-bound="25000">
						<div class="title">PPG IR</div>
						<div class="chartbox">
							<svg class="chart_svg">
								<g class="grid_box"></g>
								<svg class="line_0"><g class="pathbox"></g></svg>
								<svg class="line_1"><g class="pathbox"></g></svg>
								<g class="updatebar"><rect class="current"></rect><rect class="circle"></rect></g>
							</svg>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/d3/d3.v4.min.js')}}"></script>
<script src="{{url('/thirdpart/moment/moment.js')}}"></script>
<script src="{{url('/thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/js/monitor.js')}}?v={{env('STATIC_FILE_VERSION','1.0.2')}}"></script>
<script>
	var hotler_IDNumber = "{{$holter ? $holter->IDNumber : ''}}";
	var windowWidth = $(window).width();
	var timestamp = false;
	var start_timestamp = false;
	var streaming = true;
	var sampleRate = 128;
	var offlineData = getOfflineData();
	var offlineRowData = getOfflineRowData();
	var waveData = JSON.parse(JSON.stringify(offlineData));
	var chartIndex = -1;
	var lastChartIndex = 9;
	var currentUpdateRow = 0;
	var isStart = true;
	$(function(){
		$("#sidebar").addClass("sidebarhide").append('<div id="sidebarmenu"><span class="glyphicon glyphicon-menu-hamburger"></span></div>');
		initChart();
		if(streaming){
			getStream();
		}
		$('.action').on('click','.yZoomIn',function(event){
			chartZoom($("#ppg_r"), 0.1);
			chartZoom($("#ppg_ir"), 0.1);
		});
		$('.action').on('click','.yZoomOut',function(event){
			chartZoom($("#ppg_r"), -0.1);
			chartZoom($("#ppg_ir"), -0.1);
		});
	});
	function getOfflineRowData(){
		var linedata = [];
			for (var j = 0; j < sampleRate; j++) {
				linedata[j] = 0 ;
			}
		return linedata
	}
	function getOfflineData(){
		var chardata = [];
		for (var i = 0; i < 5; i++) {
			var linedata = [];
			for (var j = 0; j < sampleRate * 10; j++) {
				linedata[j] = 0 ;
			}
			chardata[i] = linedata;
		}
		return [chardata, chardata];
	}
	function getStream(){
		var lastResponseLength = false;
		$.ajax({
			type: 'GET',
			url: url('/monitor/'+hotler_IDNumber+'/streams/data')+'?second=30',
			//data: JSON.stringify({"second":30}), 
			dataType: 'json',
			contentType: "application/json",
	        processData: false,
	        xhrFields: {
	            onprogress: function(e)
	            {
	                var progressResponse;
	                var response = e.currentTarget.response;
	                if(lastResponseLength === false)
	                {
	                    progressResponse = response;
	                }
	                else
	                {
	                    progressResponse = response.substring(lastResponseLength);
	                }
	                var data = false;
	                try {
						var data = JSON.parse(progressResponse);
						lastResponseLength = response.length;
	  				} catch (error) {

 					}
 					if(data){
		                if(data.status == 'streaming'){
							$('#heart_rate_value .value').removeClass('blinking');
							$('#respiration_rate_value .value').removeClass('blinking');
							$('#spo2_value .value').removeClass('blinking');
				            if(!start_timestamp || timestamp <= data.timestamp){
				            	if(!start_timestamp){
					            	start_timestamp = data.timestamp;
									chartIndex = 0;
				            	}else{
									chartIndex = (data.timestamp - start_timestamp) % 10;
				            	}
								timestamp = data.timestamp;
								updateWaveData(0, JSON.parse(data.ecg_l1));
								updateWaveData(1, JSON.parse(data.ecg_l2));
								updateWaveData(2, JSON.parse(data.ecg_l3));
								updateWaveData(3, JSON.parse(data.ppg_r));
								updateWaveData(4, JSON.parse(data.ppg_ir));

								$('#heart_rate_value .value').html(data.heart_rate);
								$('#respiration_rate_value .value').html(data.respiration_rate);
								$('#spo2_value .value').html(data.spo2);

								if(data.warning.heart_rate){
									$('#heart_rate_value .warning-icon').removeClass('hide');
								}else{
									$('#heart_rate_value .warning-icon').addClass('hide');
								}
								if(data.warning.respiration_rate){
									$('#respiration_rate_value .warning-icon').removeClass('hide');
								}else{
									$('#respiration_rate_value .warning-icon').addClass('hide');
								}
								if(data.warning.spo2){
									$('#spo2_value .warning-icon').removeClass('hide');
								}else{
									$('#spo2_value .warning-icon').addClass('hide');
								}
								drawChart(chartIndex, lastChartIndex, currentUpdateRow);
				            }else{
				            	chartIndex = (chartIndex + 1) % 10;
								timestamp = timestamp +1 ;
								updateWaveData(0, JSON.parse(JSON.stringify(offlineRowData)));
								updateWaveData(1, JSON.parse(JSON.stringify(offlineRowData)));
								updateWaveData(2, JSON.parse(JSON.stringify(offlineRowData)));
								updateWaveData(3, JSON.parse(JSON.stringify(offlineRowData)));
								updateWaveData(4, JSON.parse(JSON.stringify(offlineRowData)));

				                $('#heart_rate_value .value').html('-');
				                $('#respiration_rate_value .value').html('-');
					            $('#spo2_value .value').html('-');

								$('#heart_rate_value .warning-icon').addClass('hide');
								$('#respiration_rate_value .warning-icon').addClass('hide');
								$('#spo2_value .warning-icon').addClass('hide');

					            drawChart(chartIndex, lastChartIndex, currentUpdateRow);
				            }
				            if(chartIndex == 9){
								currentUpdateRow ++;
								currentUpdateRow = currentUpdateRow % 2;
							}
							if(chartIndex == 0){
								updateWaveData(0, JSON.parse(data.ecg_l1));
								updateWaveData(1, JSON.parse(data.ecg_l2));
								updateWaveData(2, JSON.parse(data.ecg_l3));
								updateWaveData(3, JSON.parse(data.ppg_r));
								updateWaveData(4, JSON.parse(data.ppg_ir));
							}
							lastChartIndex = chartIndex;
				        }else if(data.status == 'offline'){
				            $('#heart_rate_value .value').addClass('blinking').html('－');
				            $('#respiration_rate_value .value').addClass('blinking').html('－');
							$('#spo2_value .value').addClass('blinking').html('－');
							$('#heart_rate_value .warning-icon').addClass('hide');
							$('#respiration_rate_value .warning-icon').addClass('hide');
							$('#spo2_value .warning-icon').addClass('hide');
							timestamp = timestamp +1 ;
							chartIndex = (chartIndex + 1) % 10;
							
							updateWaveData(0, JSON.parse(JSON.stringify(offlineRowData)));
							updateWaveData(1, JSON.parse(JSON.stringify(offlineRowData)));
							updateWaveData(2, JSON.parse(JSON.stringify(offlineRowData)));
							updateWaveData(3, JSON.parse(JSON.stringify(offlineRowData)));
							updateWaveData(4, JSON.parse(JSON.stringify(offlineRowData)));

							drawChart(chartIndex, lastChartIndex, currentUpdateRow)
							if(chartIndex == 9){
								currentUpdateRow ++;
								currentUpdateRow = currentUpdateRow % 2;
							}
							lastChartIndex = chartIndex
						}else if(data.status == 'stop'){
				            if(streaming){
								getStream();
							}
						}
					}
	            }
	        }
		});
		}
		function updateWaveData(waveDataIndex, data){
			waveData[currentUpdateRow][waveDataIndex].splice(chartIndex * sampleRate, sampleRate, ...data);
			waveData[currentUpdateRow][waveDataIndex].fill(data[data.length-1],  (chartIndex+1) * sampleRate)
		}
		function initChart(){
			for (var waveDataIndex = 0; waveDataIndex < waveData[currentUpdateRow].length-2; waveDataIndex++) {
				var viewData = getViewByIndex(waveDataIndex);
				drawGrid(viewData.viewId);
				var chartData = offlineData[0];
				var exData = {
					maxDataLen : sampleRate,
					maxDataLenSampleRate : sampleRate,
					maxDataSecond : 1,
					dataLen:sampleRate,
				};
				init_chart(viewData.viewId, chartData, exData);
			}
		}
		function drawChart(chartIndex, lastChartIndex, updateIndex, animation = true){
			for (var waveDataIndex = 0; waveDataIndex < waveData[currentUpdateRow].length; waveDataIndex++) {
				var viewData = getViewByIndex(waveDataIndex);
				var chartData = waveData[updateIndex][waveDataIndex];
				var exData = {
					maxDataLen : sampleRate,
					maxDataLenSampleRate : sampleRate,
					maxDataSecond : 1,
					dataLen:sampleRate,
					chartIndex:chartIndex,
					lastChartIndex:lastChartIndex,
					animation:animation,
					waveDataIndex:waveDataIndex,
					updateIndex:updateIndex,
				};
				if(!chartData.length){
					chartData = offlineData[0];
				}
				draw_chart(viewData.viewId, chartData, exData);
			}
		}
		function getViewByIndex(waveDataIndex){
			var viewData= {
				viewId : '#'+$('.chart').eq(waveDataIndex).attr('id')
			};
			return viewData;
		}
		function chartZoom(view, value){
			var yScaleRatio = parseFloat(view.attr('data-y-scale-ratio')) + value;
			if(yScaleRatio<0.1){
				yScaleRatio=0.1;
			}
			view.attr('data-y-scale-ratio',yScaleRatio);
		}
</script>
@endsection