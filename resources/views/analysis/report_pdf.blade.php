<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title></title>
    <link href="{{url('/css/report_pdf.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
    <link href="{{url('/css/report_2_pdf.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
</head>
<body>
    <header class="">
        <div class="title">
            Report
        </div>
    </header>
    <table class="report_inforamtion">
        <tr class="info-column sub">
            <td class="info-label">@lang("physiolguard.patient"):</td>
            <td class="info-value">{{$ecgInfo->patient_name}}</td>
            <td class="info-label">@lang("physiolguard.diagnostic"):</td>
            <td class="info-value">{{Auth::user()->name}}</td>
        </tr>
        <tr class="info-column">
            <td class="info-label">@lang("physiolguard.patient_ID"): </td>
            <td class="info-value"># {{$ecgInfo->id}}</td>
            <td class="info-label">@lang("physiolguard.filename"):</td>
            <td class="info-value">{{$ecgInfo->filename}}</td>
        </tr>
        <tr class="info-column">
            <td class="info-label">@lang("physiolguard.age"):  </td>
            <td class="info-value"> {{$ecgInfo->age}}</td>
            <td class="info-label">@lang("physiolguard.record_date"): </td>
            <td class="info-value">{{ date("Y-m-d",strtotime($summary['start_time'])) }}</td>
        </tr>
        <tr class="info-column">
            <td class="info-label">@lang("physiolguard.gender"): </td>
            <td class="info-value">@lang("physiolguard.gender_".$ecgInfo->sex)</td>
            <td class="info-label">@lang("physiolguard.analysis_date"): </td>
            <td class="info-value">{{ date("Y-m-d",strtotime($ecgInfo->created_at)) }}</td>
        </tr>
    </table>

    <table class="report_inforamtion second">
        <!-- GENERAL SUMMARY and HEART RATE SUMMARY Titles -->
        <thead>
            <tr>
                <th colspan="2" class="info-label">GENERAL SUMMARY</th>
                <th colspan="2" class="info-label">HEART RATE SUMMARY</th>
            </tr>
        </thead>
        
        <!-- Start Time and Min HR -->
        <tbody>
            <tr>
                <td class="info-label">@lang("physiolguard.start_time"):</td>
                <td class="info-value">{{ date("H:i:s", strtotime($summary['start_time'])) }}</td>
                <td class="info-label">@lang("physiolguard.min_hr"):</td>
                <td class="info-value">{{ round($summary['min_hr']) }} @lang("physiolguard.bpm")</td>
            </tr>
            <!-- Total Time and Max HR -->
            <tr>
                <td class="info-label">@lang("physiolguard.total_time"):</td>
                <td class="info-value">{{ round($summary['total_time'], 2) }} @lang("physiolguard.second")</td>
                <td class="info-label">@lang("physiolguard.max_hr"):</td>
                <td class="info-value">{{ round($summary['max_hr']) }} @lang("physiolguard.bpm")</td>
            </tr>
            <!-- Total Beats and Mean HR -->
            <tr>
                <td class="info-label">@lang("physiolguard.total_beats"):</td>
                <td class="info-value">{{ $summary['total_beats'] }} @lang("physiolguard.times")</td>
                <td class="info-label">@lang("physiolguard.mean_hr"):</td>
                <td class="info-value">{{ round($summary['mean_hr']) }} @lang("physiolguard.bpm")</td>
            </tr>
        </tbody>
        
        <!-- VENTRICULAR SUMMARY and SUPRAVENTRICULAR SUMMARY Titles -->
        <thead>
            <tr>
                <th colspan="2" class="info-label">VENTRICULAR SUMMARY</th>
                <th colspan="2" class="info-label">SUPRAVENTRICULAR SUMMARY</th>
            </tr>
        </thead>
        
        <!-- Total V and Total S -->
        <tbody>
            <tr>
                <td class="info-label">@lang("physiolguard.total"):</td>
                <td class="info-value">{{ $summary['V_total'] }}</td>
                <td class="info-label">@lang("physiolguard.total"):</td>
                <td class="info-value">{{ $summary['S_total'] }}</td>
            </tr>
            <!-- V Pairs and S Pairs -->
            <tr>
                <td class="info-label">@lang("physiolguard.pairs"):</td>
                <td class="info-value">{{ $summary['V_pairs'] }}</td>
                <td class="info-label">@lang("physiolguard.pairs"):</td>
                <td class="info-value">{{ $summary['S_pairs'] }}</td>
            </tr>
        </tbody>
        
        <!-- Comment Section -->
        <thead>
            <tr>
                <th colspan="4" class="info-label">@lang("physiolguard.comment")</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="4" class="info-value">{{ $ecgInfo->comment }}</td>
            </tr>
        </tbody>
    </table>
    <div class="page-break"></div>
    <table class="report_inforamtion general_info">
            <thead>
                <tr>
                    <th>Interval</th>
                    <th colspan="3">Heart Rate</th>
                    <th>Total</th>
                    <th colspan="3">VPB</th>
                    <th colspan="3">SVPB</th>
                </tr>
                <tr>
                    <th>Starting</th>
                    <th>Lo</th>
                    <th>Mean</th>
                    <th>Hi</th>
                    <th>Beats</th>
                    <th>Total</th>
                    <th>Pairs</th>
                    <th>Single</th>
                    <th>Total</th>
                    <th>Pairs</th>
                    <th>Single</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($generalProfile as $general)
                    <tr>
                        <td>{{date("H:i:s",strtotime($summary['start_time'])+$general['start_time'])}}</td>
                        <td>{{round($general['min_hr'])}}</td>
                        <td>{{round($general['mean_hr'])}}</td>
                        <td>{{round($general['max_hr'])}}</td>
                        <td>{{$general['total_beats']}}</td>
                        <td>{{$general['V_total']}}</td>
                        <td>{{$general['V_pairs']}}</td>
                        <td>{{$general['V_total']-2*$general['V_pairs']}}</td>
                        <td>{{$general['S_total']}}</td>
                        <td>{{$general['S_pairs']}}</td>
                        <td>{{$general['S_total']-2*$general['S_pairs']}}</td>
                    </tr>
                @empty
                    <tr>@lang("physiolguard.data")資料</tr>
                @endforelse
                <tr class="summary">
                        <td>Summary: </td>
                        <td>{{ round($summary['min_hr']) }}</td>
                        <td>{{ round($summary['mean_hr']) }}</td>
                        <td>{{ round($summary['max_hr']) }}</td>
                        <td>{{ $summary['total_beats'] }}</td>
                        <td>{{ $summary['V_total'] }}</td>
                        <td>{{ $summary['V_pairs'] }}</td>
                        <td>{{ $summary['V_total']-2*$summary['V_pairs'] }}</td>
                        <td>{{ $summary['S_total'] }}</td>
                        <td>{{ $summary['S_pairs'] }}</td>
                        <td>{{ $summary['S_total']-2*$summary['S_pairs'] }}</td>
                        </tr>
                </tbody>
            </table>
            <div class="page-break"></div>
            <div class="page-head"></div>
            <div class="page">
                    <div class="trend_info section">
                        <div class="hrbmp chart">
                            <img src="{{$hrbmp}}"/>
                        </div>
                        <div class="vpbbmp chart">
                            <img src="{{$vpbbmp}}" />
                        </div>
                        <div class="svpbbmp chart">
                            <img src="{{$svpbbmp}}" />
                        </div>
                    </div>
                </div>
        @foreach($svgs as $key => $svg)
            @if(($key+1)%3==1)
                <div class="page-break"></div>
                <div class="page-head"></div>
                <div class="page">
                    <div class="section ">
            @endif
                        <div class="ecg_rr_chart_info">
                                <div>{{$svg_info[$key]['peak_time']}}</div>
                                <div>HR={{$svg_info[$key]['hr']}}</div>
                            </div>
                        <div class="svg chart">
                            <img src="{{$svg}}"/>
                        </div>
            @if(($key+1)%3==0)
                </div>
                </div>
            @endif
        @endforeach

        @if(count($svgs) && count($svgs)%3 !== 0)
                </div>
            </div>
        @endif
    </body>
</html>
