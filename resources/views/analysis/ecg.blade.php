@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/ecganalysis.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<link href="{{url('/css/chart.css')}}" rel="stylesheet">
	<link href="{{url('/css/report_message.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4 class="pull-left title">
				@lang("physiolguard.analysis_title")＞{{$filename}} 
				@can('ecg_analysis_switch')
					@if($ecg->filename_ext === 'z2b')
						<span style="font-weight: bold; color:blue;padding-left: 15px;">
							@lang('physiolguard.'.$mode.'_mode')
						</span>
					@endif
				@endcan
			</h4>
			<div id="toolbar">
				<ul class="btn-toolbar pull-right">
					@cannot('explorer_type')
						@if(Auth::user()->cannot('ecg_explorer_type'))
							<li class="dropdown">
								<a href="#" class="btn btn-default" id="check_btn" data-checked="{{$ecg->checked}}">
									<span class="glyphicon glyphicon-check"></span>
									<span class="btn-text">
										@if($ecg->checked)
											@lang("physiolguard.unchecked")
										@else
											@lang("physiolguard.checked")
										@endif
									</span>
								</a>
							</li>
						<li class="dropdown" id="analysis_btn">
						<a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" role="button">
							<span class="glyphicon glyphicon-cog"></span>@lang("physiolguard.options")
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li class="dropdown-item">
								<a href="#"  id="QRS_btn">
									<span class="glyphicon glyphicon-stats"></span>@lang("physiolguard.qrs_analysis")
								</a>
							</li>
							<li class="dropdown-item">
								<a href="#"  id="RRAlign_btn">
									<span class="glyphicon glyphicon-object-align-bottom"></span>
									@lang("physiolguard.rr_align")
								</a>
							</li>
							@cannot('doctor_type')
								@can('hrv_analysis')
									<li class="dropdown-item">
										<a href="{{url('/analysis/hrv/'.$ecg_id)}}" target="_blank">
											<span class="glyphicon glyphicon-stats"></span>
											@lang("physiolguard.hrv_analysis")
										</a>
									</li>
								@endcan
								@can('peak_overview')
									<li class="dropdown-item">
										<a href="{{url('/analysis/ecg/batchpeak/'.$ecg_id)}}">
											<span class="glyphicon glyphicon-check"></span>
											@lang("physiolguard.batchpeak")
										</a>
									</li>
								@endcan
							@endcannot
							@can('rr_download')
								<li class="dropdown-item">
									<a href="{{url('/analysis/ecg/exportRR/'.$ecg_id)}}">
										<span class="glyphicon glyphicon-download-alt"></span>
										@lang("physiolguard.rr_download")
									</a>
								</li>
							@endcan
							@if($ecg->filename_ext=='psg')
								<li class="dropdown-item">
									<a href="{{url('/analysis/ecg/report/'.$ecg_id)}}" target="_blank">
										<span class="glyphicon glyphicon-list-alt"></span>
										@lang("physiolguard.analysis_report")
									</a>
								</li>
							@else
								@cannot('doctor_type')
									<li class="dropdown-item">
										<a href="#" target="_blank" id="analysis_report_btn">
											<span class="glyphicon glyphicon-list-alt"></span>
											@lang("physiolguard.analysis_report")
										</a>
									</li>
								@endcannot
								@can('QTC_report_download')
									<li class="selectedonefunc status_option">
										<a href="#" id="func_QTC_report">
											<span class="glyphicon glyphicon-download-alt"></span>
											@lang('physiolguard.QTC_report')
										</a>
									</li>
								@endcan
							@endif

							@can('ecg_analysis_switch')

								@if($ecg->filename_ext === 'z2b')
								<li class="dropdown-item">
									<a href="{{url('/analysis/ecg/mode?mode='.($mode == 'one_lead' ? 'holter':'one_lead'))}}" id="func_mode_switch">
										<span class="glyphicon glyphicon-retweet"></span>
										@if($mode == 'one_lead')
											@lang('physiolguard.to_holter_mode')
										@else
											@lang('physiolguard.to_one_lead_mode')
										@endif
									</a>
								</li>
								@endif
							@endcan
						</ul>
					</li>
					@endif
					@endcannot
					@if($mode == 'holter')
					<li>
						<div class="input-group range_select">
							<input type="text" class="form-control" id="range_start">
							  <span class="input-group-addon">-</span>
							<input type="text" class="form-control" id="range_end">
							<span class="input-group-btn"><button id="range_delete" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span></button></span>
						</div>
					</li>
					@endif
					<li>
						<div class="view_options">
							@if($mode == 'holter')
								<label class="control-label show_option"><input id="showL1" type="checkbox" value="line1" checked>L1</label>
								<label class="control-label show_option"><input id="showL2" type="checkbox" value="line2" checked>L2</label>
								<label class="control-label show_option"><input id="showL3" type="checkbox" value="line3" checked>L3</label>
							@endif
							<label class="control-label show_option"><input id="showRR" type="checkbox" value="peakMarker" checked>RR</label>
							<label class="control-label show_option"><input id="showGrid" type="checkbox" value="ecgGrid" checked>Grid</label>
							<label class="control-label"><input id="invertECg" type="checkbox" value="invertECg">Invert</label>
							@if($mode != 'holter')
							<label class="control-label"><input id="filterECg" type="checkbox" value="filterECg">Filter</label>
							@endif
						</div>
					</li>
					<li>
						<div class="btn-group">
							<button class="btn btn-default" id="prev_peak_btn">Previous Beat</button>
							<button class="btn btn-default" id="next_peak_btn">Next Beat</button>
						</div>
					</li>
					<li>
						<div id="peak_mode_btns" class="btn-group" data-toggle="buttons">
							<label class="btn btn-default active" data-toggle="tooltip" data-placement="bottom" title="Delete">
								<input type="radio" id="peak_D" name="peak_mode" autocomplete="off" value="D" checked> D
							</label>
							<label class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Normal">
								<input type="radio" id="peak_N" name="peak_mode" value="N" autocomplete="off"> N
							</label>
							<label class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="VPC">
								<input type="radio" id="peak_V" name="peak_mode" value="V" autocomplete="off"> V
							</label>
							<label class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Supraventricular">
								<input type="radio" id="peak_S" name="peak_mode" value="S" autocomplete="off"> S
							</label>
							<label class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Q">
								<input type="radio" id="peak_Q" name="peak_mode" value="Q" autocomplete="off"> Q
							</label>
						</div>
					</li>
				</ul>
			</div>
			<hr/>
			<div id="ecg_view_loading" class="ajax_loading">
				<img src="/assets/img/loading.gif">
			</div>
			@if($mode == 'holter')
				<div id="ecgView">
					<svg>
						<g id="ecgLineGroup" class="lineGroup">
							<path id="line1" class="line1" stroke="#B71C1C" fill="none" d=""></path>
							<path id="line2" class="line2" stroke="#1B5E20" fill="none" d=""></path>
							<path id="line3" class="line3" stroke="#01579B" fill="none" d=""></path>
						</g>
						<g id="ecgGrid" transform="translate(0 0) scale(1)"></g>
						<rect id="mouseLine" width="0" height="0"
							  stroke="#000" stroke-width="0.5" fill="none"></rect>
						<g id="current_time_box">
							<rect x="0" y="0" width="50" height="20" stroke="#000" stroke-width="0.5" fill="white">
							</rect>
							<text x="5" y="15" class="current_time">0</text>
						</g>
						<g id="peakEventMarker"></g>
						<g id="peakMarker1"></g>
						<text id="current_start_time"></text>
						<text id="current_end_time"></text>
						<rect id="selectionWindow" width="0" height="0" stroke="#B71C1C" stroke-width="5" fill="none" stroke-dasharray="15, 10"></rect>
					</svg>
				</div>
			@else
				<div class="one_lead_ecgView">
					<div id="ecgView" class="lock_svg">
						<svg>
							<g id="ecgLineGroup" class="lineGroup">
								<path class="line2" stroke="#1B5E20" fill="none" d=""></path>
							</g>
							<g id="ecgLineGroup2" class="lineGroup">
								<path class="line2" stroke="#1B5E20" fill="none" d=""></path>
							</g>
							<g id="ecgLineGroup3" class="lineGroup">
								<path class="line2" stroke="#1B5E20" fill="none" d=""></path>
							</g>
							<g id="ecgGrid" transform="translate(0 0) scale(1)"></g>
							<rect id="mouseLine" width="0" height="0"
								  stroke="#000" stroke-width="0.5" fill="none"></rect>
							<g id="current_time_box">
								<rect x="0" y="0" width="50" height="20" stroke="#000" stroke-width="0.5" fill="white">
								</rect>
								<text x="5" y="15" class="current_time">0</text>
							</g>
							<g id="peakEventMarker1"></g>
							<g id="peakEventMarker2"></g>
							<g id="peakEventMarker3"></g>
							<g id="peakMarker1"></g>
							<g id="peakMarker2"></g>
							<g id="peakMarker3"></g>
							<rect id="selectionWindow" width="0" height="0" stroke="#B71C1C" stroke-width="5" fill="none" stroke-dasharray="15, 10"></rect>
						</svg>
					</div>
					@if($filename_ext=='z2b' || $filename_ext=='xml')
						<div class="rr_beat_chart w-quarter-3-half w-quarter-fix-h">
							<h4 class="section_title">Tachogram</h4>
							<div id="rr_beat_chart"></div>
							<div class="axisx_title">Beat#</div>
							<div class="axisy_title">RR[sec]</div>
						</div>
						<div class="rr_histogram w-quarter-3-half w-quarter-fix-h">
							<h4 class="section_title">Histogram</h4>
							<div id="rr_histogram"></div>
							<div class="axisx_title">RR[sec]</div>
							<div class="axisy_title">Count</div>
						</div>
						<div class="rr_avgBeatChart w-quarter w-quarter-fix-h">
							<h4 class="section_title">Average beat</h4>
							<div id="rr_avgBeatChart"></div>
						</div>
					@endif
				</div>
				<div class="one_lead_box">
					<div class="report_type_box">
						<h4 class="section_title">@lang("physiolguard.report_type")</h4>
						<div id="report_type_list" class="report_type_list">
							<div class="col-sm-6">
								@foreach($types as $key => $type)
							  		<div class="row">
							  			<label class="col-md-12 label-control">
							  				<input type="checkbox" value="{{$type['value']}}" name="report_tpye" class="report_tpye" data-name="{{$type['name']}}" {{in_array($type['value'], $ecg_report_types) ? 'checked':''}}> {{$type['name']}}
							  			</label>
							  		</div>
						  		@endforeach
					  		</div>
							<div class="col-sm-6 report_type_list--ab">
								@foreach($types_2 as $key => $type)
							  		<div class="row">
							  			<label class="col-md-12 label-control">
							  				<input type="checkbox" value="{{$type['value']}}" name="report_tpye" class="report_tpye" data-name="{{$type['name']}}" {{in_array($type['value'], $ecg_report_types) ? 'checked':''}}> {{$type['name']}}
							  			</label>
							  		</div>
						  		@endforeach
					  		</div>
						</div>
					</div>
					<div class="comment">
						<h4 class="section_title">@lang("physiolguard.comment")</h4>
						<div id="comment" class="comment">
							<textarea id="comment_input" class="form-control" style="min-height: 240px;resize: vertical">{!! $ecg->comment !!}</textarea>
							<div class="btn_action"><button id="comment_confirmed" class="btn btn-info">@lang('physiolguard.confirm')</button></div>
						</div>
					</div>
				</div>
			@endif
			@if($mode == 'holter')
				<h4>RR Interval</h4>
				<div id="rr_loading" class="ajax_loading">
					<img src="{{url('/assets/img/loading.gif')}}">
				</div>
				<div id="rr_line_chart">
					<svg>
						<g class="rr_row"></g>
						<g>
							<rect id="rr_slidewindow" class="rr_slidewindow"></rect>
							<rect id="selectionRRWindow" width="0" height="75"
							  stroke="#B71C1C" stroke-width="2" fill="#DDD" opacity="0.3" stroke-dasharray="15, 10"></rect>
						</g>
						<g class="event_row"></g>
					</svg>
				</div>
				<div id="rr_time_row">
					<div class="start_time"></div>
					<div class="end_time"></div>
				</div>
				<div class="ecg_overview_chart {{$filename_ext=='z2b' || $filename_ext=='xml' ? 'w-half':''}}">
					<h4>@lang("physiolguard.ecg_overview")</h4>
					<div id="ecg_overview_loading" class="ajax_loading">
						<img src="{{url('/assets/img/loading.gif')}}">
					</div>
					<div id="ecg_overview_chart"></div>
				</div>
				@if($filename_ext=='z2b' || $filename_ext=='xml')
					<div class="rr_beat_chart w-quarter w-quarter-fix-h">
						<h4 class="section_title">Tachogram</h4>
						<div id="rr_beat_chart"></div>
						<div class="axisx_title">Beat#</div>
						<div class="axisy_title">RR[sec]</div>
					</div>
					<div class="rr_histogram w-quarter w-quarter-fix-h">
						<h4 class="section_title">Histogram</h4>
						<div id="rr_histogram"></div>
						<div class="axisx_title">RR[sec]</div>
						<div class="axisy_title">Count</div>
					</div>
				@endif
			@endif
			<div class="clearfix"></div>
		</article>
	</div>
</div>

<div id="report_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="report_modal_label">
  	<div class="modal-dialog modal-md" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="report_modal_label">@lang("physiolguard.analysis_report")</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<h5>@lang("physiolguard.do_select"):</h5>
	  			<div class="report_type_list">
	  				@if($department)
	  					@if(Auth::user()->can('report_type_by_comment'))
	  						<div class="comment">
								<h4 class="section_title">@lang("physiolguard.comment")</h4>
								<div id="comment" class="comment">
									<textarea id="report_type_comment_input" class="form-control" style="min-height: 240px;resize: vertical">{!! $ecg->comment !!}</textarea>
								</div>
							</div>
	  					@else
			  				@foreach($department->report_types as $key => $type)
					  			<div class="row">
					  				<label class="col-md-5">
					  					<input type="{{ $department->select_type==0 ? 'radio':'checkbox' }}" value="{{$type->value}}" name="report_tpye" class="report_tpye" data-name="{{$type->name}}"> {{$type->name}}
					  				</label>
					  				@if($type->duration==1)
							  				<div class="col-md-7">
								  				<div class="col-md-6 text-right">
								  					@lang("physiolguard.duration_time")[{{$type->duration_low.'-'.$type->duration_up}}]:
								  				</div>
								  				<label class="col-md-6">
								  					<input type="number" min="{{$type->duration_low}}" value="{{$type->duration_low}}" max="{{$type->duration_up}}" name="report_tpye_duration" class="report_tpye_duration form-control" data-name="{{$type->name}}">
								  				</label>
							  				</div>
						  				@endif
					  			</div>
				  			@endforeach
	  					@endif
		  			@endif
	  			</div>
	  			<div class="row">
	  				<button id="report_cancel" class="btn btn-default col-md-3 col-md-offset-3" data-dismiss="modal">@lang("physiolguard.cancel")</button>
	  				<button id="report_create" class="btn btn-info col-md-4 col-md-offset-1">@lang("physiolguard.create_report")</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
<div id="report_download_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="report_modal_download_label">
  	<div class="modal-dialog modal-md" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="report_modal_download_label">@lang("physiolguard.create_report")</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<div class="row">
	  				<div class="col-md-12" style="margin-bottom: 2em">
	  				@lang("physiolguard.create_report_msg_1")<span class="report_type"></span> @lang("physiolguard.create_report_msg_2")
	  				</div>
	  			</div>
	  			@can('send_report')
	  			<div class="row">
	  				<div class="col-md-12" style="margin-bottom: 2em">
	  					<form id="report_email_form" action="{{url('/analysis/ecg/report/email')}}" method="POST">
	  					{{csrf_field()}}
	  					<input type="hidden" name="report_ecg_id" id="report_ecg_id">
	  					<div class="col-sm-3 title">
	  						@lang("physiolguard.send_report_to")
	  					</div>
	  					<div class="col-sm-6">
	  						<input type="email" class="form-control" name="report_email" id="report_email"  placeholder="E-mail" required>
	  					</div>
	  					<button id="report_email_send" class="btn btn-success col-sm-2" type="submit">@lang("physiolguard.send_report")</button>
	  					</form>
	  				</div>
	  				<div class="col-sm-12" id="send_message">
						<div id="send_message_content" class="title text-center">

						</div>
	  					<div id="send_message_loading" class="ajax_loading text-center">
							<img src="/assets/img/loading.gif">
						</div>
	  				</div>
	  			</div>
	  			@endcan
	  			<div class="row">
	  				<button id="report_download_cancel" class="btn btn-default col-md-2 col-md-offset-1" data-dismiss="modal">@lang("physiolguard.cancel")</button>
	  				<button id="report_download" class="btn btn-info col-md-2 col-md-offset-1">@lang("physiolguard.download")</button>
	  				<button id="report_create_show" class="btn btn-info col-md-4 col-md-offset-1">@lang("physiolguard.recreate_report")</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
<div id="report_creating_msg" class="hide">
	<div class="text-center msg_box">
		<div class="icon">
			<span class="glyphicon glyphicon-repeat"></span>
		</div>
		<span class="text">@lang('physiolguard.report_generating')</span>
	</div>
</div>
@endsection

@section('javascript')

<script src="{{url('/thirdpart/d3/d3.min.js')}}"></script>
<script>
	var ecgType = '{{$filename_ext}}';
	var ecgSampleRate = {{$sample_rate}};
</script>

<script src="{{url('/thirdpart/moment/moment.js')}}"></script>
<script src="{{url('/thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/js/draw_ecg_chart.js')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}"></script>
<script>
	$("#sidebar").addClass("sidebarhide").append('<div id="sidebarmenu"><span class="glyphicon glyphicon-menu-hamburger"></span></div>');
	var ecgID = "{{$ecg_id}}";
	var ecg = {!! $ecg->toJson() !!};
	var mode = '{!! $mode !!}';
	var originEcgData;
	var ecgData;
	var ecgData256;
	var rrData;
	var eventData;
	var last_left_scroll=0;
	var ecg_size=0;
	var overview_sample_rate = $('#ecgView').width() * 5;
	var ecg_sample_rate = ecgSampleRate;
	var current_peak_index=0;
	var scroll_bar={width:getBrowserScrollbarWidth()};
	var container_width=($("#main").width())-scroll_bar.width;
	var overview_container_width = $('.ecg_overview_chart').width()-scroll_bar.width;
	var ecgview_svg_width=$('#ecgView svg').width();
	var ecgview_width=$('#ecgView').width()-scroll_bar.width;
	var department = "{{$department}}";
	peakPermisson = {!! !Auth::user()->can('explorer_type') ? 'true':'false'!!};
	hrMode = {!! Auth::user()->role_id == 13 ? 'true':'false'!!};
	
	var selectRangeStart = false;
	var selectRangeEnd = false;
	var filteredECG = false;
	load_ecg_content();

	$('#rr_line_chart').hide();
	
	$('#toolbar .view_options').on('change', '.show_option input', function(event){
		var option_value = $(this).val();
		if($(this).prop('checked')){
			if(option_value == 'peakMarker'){
				$("#"+option_value+'1').show();
				$("#"+option_value+'2').show();
				$("#"+option_value+'3').show();
			}else{
				$("#"+option_value).show();
			}
		}else{
			if(option_value == 'peakMarker'){
				$("#"+option_value+'1').hide();
				$("#"+option_value+'2').hide();
				$("#"+option_value+'3').hide();
			}else{
				$("#"+option_value).hide();
			}
		}
	});

	$('#func_mode_switch').click(function(event){

	});

	$('#invertECg').change(function(event){
		invertedECG = $(this).prop('checked');
		resize();
		@if($mode == 'holter')
			update_ecg_overview(ecgData.L2, overview_sample_rate, '.ecg_overview_chart', '#ecg_overview_chart');
		@endif
	});

	$('#filterECg').change(function(event){
		filteredECG = $(this).prop('checked');
		if( filteredECG ){
			ecgData = filterECG(ecgData256);
		}else{
			ecgData = originEcgData;
		}

		resize();
		@if($mode == 'holter')
			update_ecg_overview(ecgData.L2, overview_sample_rate, '.ecg_overview_chart', '#ecg_overview_chart');
		@endif
	});

	$('#range_delete').click(function(event){
		event.preventDefault();
		var startTime = $('#range_start').val() || 0;
		var endTime = $('#range_end').val() || 0;
		bootbox.confirm({
			message: '@lang("physiolguard.delete_range_peaks")',
			locale: lang,
			size: 'small',
			callback: function (result) {
				if(result){
					deletePeaks(startTime, endTime);
				}
			}
		});
	});
	$('#range_start').on('focusin', function(){
		selectRangeStart = true;
	});
	$('#range_start').on('focusout', function(){
		selectRangeStart = false;
	});

	$('#range_end').on('focusin', function(){
		selectRangeEnd = true;
	});
	$('#range_end').on('focusout', function(){
		selectRangeEnd = false;
	});
	$('#QRS_btn').click(function(event){
		bootbox.confirm({
			message: '@lang("physiolguard.to_qrs_analysis")',
			locale: lang,
			size: 'small',
			callback: function (result) {
				if(result){
					$.get(url('/analysis/qrs/'+ecgID), function (result) {
						if(result.errorcode==null){
							if(ecg_size>1){
								loadRR(function(){
									loadEvent();
@if($mode == 'holter')
									label_peek_ecg_overview(rrData, overview_sample_rate, '.ecg_overview_chart', '#ecg_overview_chart');
@endif
								}, true);
							}
						}
					});
				}
			}
		});
	});

	$('#check_btn').click(function(event){
		event.preventDefault();
		var checked = $(this).data('checked');
			bootbox.confirm({
				message: checked == '0'? '@lang("physiolguard.to_checked")' : '@lang("physiolguard.to_unchecked")',
				locale: lang,
				size: 'small',
				callback: function (result) {
					if(result){
						if( checked =='0'){
							$.get(url('analysis/ecgs/'+ecgID+'/checked'), function (result) {
								if(result.status=='success'){
									$('#check_btn').data('checked','1');
									$('#check_btn .btn-text').html('@lang("physiolguard.unchecked")');
									window.close();
								}
							});
						}else{
							$.get(url('analysis/ecgs/'+ecgID+'/unchecked'), function (result) {
								if(result.status=='success'){
									$('#check_btn').data('checked','0');
									$('#check_btn .btn-text').html('@lang("physiolguard.checked")');
								}
							});
						}
					}
				}
			});
	});
	$('#RRAlign_btn').click(function(event){
		bootbox.confirm({
			message: '@lang("physiolguard.to_rr_align")',
			locale: lang,
			size: 'small',
			callback: function (result) {
				if(result){
					$.get(url('/analysis/rr_align/'+ecgID), function (result) {
						if(result.errorcode==null){
							if(ecg_size>1){
								loadRR(function(){
									loadEvent();
									label_peek_ecg_overview(rrData, overview_sample_rate, '.ecg_overview_chart', '#ecg_overview_chart');
								},true);
							}
						}
					});
				}
			}
		});
	});
	$('#comment_confirmed').click(function(event){
		event.preventDefault();
		bootbox.confirm({
			message: '@lang("physiolguard.to_ecg_confirm")',
			locale: lang,
			size: 'small',
			callback: function (result) {
				if(result){
					var report_type_inputs = $('#report_type_list input:checked');
					var report_types = report_type_inputs.map(function(i,d){return $(d).val();}).toArray();
					var comment = $('#comment_input').val();
					$.post(url('/api/v0/analysis/reports/'+ecgID+'/confirmed'), {report_types:report_types, comment:comment}, function(result) {
						if(result.status == 'success'){
							if(ecg.confirm == 1){
								window.location = url('/ecg/manager?confirmed=true');
							}else{
								window.location = url('/ecg/manager');
							}
						}
					});
				}
			}
		});
		
	});
	function loadRR(callback, isUpadte=false) {
		$('#rr_loading').show();
		$.get(url('/api/v0/analysis/rr/'+ecgID), function(result) {
			$('#rr_line_chart .rr_row').empty();
			rrData = result.rrdata;
			if(rrData.length>1){
				// Prepare the position of the interval on the graph
				if(rrData.length>0){
					rrData[0].intervalPos = 0;
				}
				for (var i = 1; i < rrData.length; i++) {
					var prevPeakTime = rrData[i - 1].time;
					var thisPeakTime = rrData[i].time;
					rrData[i].intervalPos = prevPeakTime + (thisPeakTime - prevPeakTime) / 2;
					rrData[i].prevTime = prevPeakTime;
				}
@if($mode == 'holter')
				$('#rr_line_chart').show();
				draw_rr_interval(rrData,ecgData.recorded_at, '#main', '#rr_line_chart');
				rr_slide_window('#main', '#rr_line_chart');
				label_peek_ecg_overview(rrData, overview_sample_rate, '.ecg_overview_chart', '#ecg_overview_chart');
@endif
				markPeak(result.rrdata, $("#ecgView").scrollLeft());
				var ecgView = $("#ecgView");
				var peakMarker = $("#peakMarker");
				// var newScrollTop = ecgView.scrollTop() + peakMarker.height();
				// ecgView.scrollTop(newScrollTop);
				// peakMarker.attr("transform", "translate(0 " + newScrollTop + ")");

				ecg_current_peak_window('#ecgView');
				if(ecgType=='z2b' || ecgType=='xml'){
					if(isUpadte){
						tachogram_update(rrData, '.rr_beat_chart','#rr_beat_chart');
						histogram_update(rrData, '.rr_histogram','#rr_histogram');
						avgBeatChart_update(ecgData256, rrData, '.rr_avgBeatChart','#rr_avgBeatChart');
					}else{
						tachogram_init(rrData, '.rr_beat_chart','#rr_beat_chart');
						histogram_init(rrData, '.rr_histogram','#rr_histogram');
						avgBeatChart_init(ecgData256, rrData, '.rr_avgBeatChart','#rr_avgBeatChart');
					}
				}
				callback();
			}
			$('#rr_loading').hide();
		});
	}
	function loadEvent() {
		$.get(url('/api/v0/analysis/event/'+ecgID), function(result) {
			eventData = result.eventdata;
			// Prepare the position of the interval on the graph
			eventData[0].intervalPos = 0;
			eventData[0].interval = 8;
			for (var i = 1; i < eventData.length; i++) {
				var prevPeakTime = eventData[i - 1].time;
				var thisPeakTime = eventData[i].time;
				eventData[i].intervalPos = 0;
				eventData[i].interval = (thisPeakTime - prevPeakTime);
				eventData[i].prevTime = prevPeakTime;
			}
@if($mode == 'holter')
			label_event_peek_ecg_overview(eventData, overview_sample_rate, '.ecg_overview_chart', '#ecg_overview_chart');
@endif
			markEventPeak(eventData, $("#ecgView").scrollLeft());
@if($mode == 'holter')
			label_event_peek_rr_interval(rrData,eventData,'#main', '#rr_line_chart');
@endif
		});
	}

	$(window).on('resize',resize);

	$("#ecgView").on('click','svg',function(event) {
		var peak_mode = $('#peak_mode_btns .active input').val();
		var line_group = getLineGroup(event.offsetY);
@if($mode == 'holter')
		line_group = 1;
@endif
		var peak_time = Math.floor(((line_group - 1) * viewableWidth+event.offsetX)/scale);
		if (peak_mode != "D") {
			if(peakPermisson){
				addPeak(peak_time, peak_mode, line_group);
@if($mode == 'holter')
				add_peak_overview(peak_time, peak_mode);
@endif
			}
		}
    });
	
	$("#ecgView").on('mousewheel',function(event) {
@if($mode == 'holter')
        this.scrollLeft -= event.originalEvent.wheelDelta;
@endif
        event.preventDefault();
    });

	$('#ecgView').scroll(function(event){
		var current_left_scroll=$(this).scrollLeft();
		if(current_left_scroll!=last_left_scroll){
			var rr_x=(current_left_scroll*container_width/ecgview_svg_width);
			
			move_rr_window(rr_x, $('#rr_line_chart').scrollTop());
			var chart_num=parseFloat(ecg_size*scale/overview_sample_rate);
			
@if($mode == 'holter')
			var window_size=chart_num*overview_container_width*ecgview_width/ecgview_svg_width;
			var overview_x=(current_left_scroll*chart_num*overview_container_width/ecgview_svg_width)%(overview_container_width);
			var overview_y=parseInt((current_left_scroll*chart_num*overview_container_width/ecgview_svg_width)/overview_container_width);
			move_overview_window(overview_x,overview_y,window_size);
@endif

			last_left_scroll=current_left_scroll;
		}
	});

	$('#rr_line_chart').scroll(function(event){
		var current_left_scroll=$('#ecgView').scrollLeft();
		var current_top_scroll=$(this).scrollTop();
		var rr_x=(current_left_scroll * container_width / ecgview_svg_width);
		move_rr_window(rr_x, $('#rr_line_chart').scrollTop());
		d3.select('#rr_line_chart').select('.event_row').attr("transform", "translate(" + 0 + "," +$('#rr_line_chart').scrollTop()+ ") scale(" + (container_width / origin_width) + ",1)");
	});


	$('#rr_line_chart').on('mousedown','svg',function(event){
		var x=event.pageX-15;
		var rr_size = d3.select('#rr_slidewindow').attr('width')*overview_container_width*2/origin_width;
		move_ecgview_window((x-rr_size/2)*ecgview_svg_width/ecgview_width);
	});

	$('#ecg_overview_chart').on('mousedown','svg',function(event){
		var chart_num=parseFloat(ecg_size*scale/overview_sample_rate);
		var window_size=chart_num*origin_width*ecgview_width/ecgview_svg_width;
		var x=event.pageX-15;
		var y=parseInt((event.pageY-$(this).offset().top)/50);
		move_ecgview_window(overview_sample_rate*(y+(x-window_size*(overview_container_width/origin_width)/2)/overview_container_width));
		
	});

	$('#prev_peak_btn').click(function(event){
		if(rrData!=null){
			var peak_mode = $('#peak_mode_btns .active input').val();
			if(current_peak_index>=0){
				var tmp_index=current_peak_index-1;
				while(tmp_index>=0 && rrData[tmp_index].peak!=peak_mode && peak_mode!='D'){
					tmp_index-=1;
				}
				if(tmp_index != -1){
					current_peak_index=tmp_index
				}
			}
			move_current_peak_window(rrData[current_peak_index].time);
		}
	});
	$('#next_peak_btn').click(function(event){
		if(rrData!=null){
			var peak_mode = $('#peak_mode_btns .active input').val();
			if(current_peak_index < rrData.length){
				var tmp_index=current_peak_index+1;
				while(tmp_index < rrData.length && rrData[tmp_index].peak!=peak_mode && peak_mode!='D'){
					tmp_index+=1;
				}
				if(tmp_index != rrData.length){
					current_peak_index=tmp_index;
				}
			}
			move_current_peak_window(rrData[current_peak_index].time);
		}
	});

	$('#analysis_report_btn').click(function(event){
		event.preventDefault();
		$('#send_message_content').html('');
		if(ecg.filename_ext=='z2b' || ecg.filename_ext=='xml'){
			if(ecg.report_status != '0'){
				$('#report_download_modal').modal('show');
				$('#report_download_modal .report_type').html('');
				$('#report_download').data('ecg_id', ecg.id);
				$('#report_ecg_id').val(ecg.id);
				$('#report_create_show').data('ecg_id', ecg.id).data('filename_ext',ecg.filename_ext).data('report_type',ecg.report_type);
			}else{
				show_report_modal(ecg.id);
			}
		}
	});

	$('#func_QTC_report').click(function(event){
		event.preventDefault();
		if(	ecg.filename_ext=='psg' || ecg.filename_ext=='z2b' || ecg.filename_ext=='xml'){
			
			var ecg_id = ecg.id;
			window.open(url('analysis/z2b/'+ecg_id+'/report/qtc'));
		}
	});

	$('#report_create').click(function(event){
		if(!check_report_duration()){
			return false;
		}
		var report_department = $(this).data('department');
		var report_type=$('#report_modal input[name="report_tpye"]:checked').map(function(index, item){
			var report_tpye_duration = $(item).parent().parent().find('.report_tpye_duration');
			report_tpye_duration = report_tpye_duration.length > 0 ? report_tpye_duration.val() : '';
			return $(item).val()+report_tpye_duration;}).toArray().join(',');
		var ecg_id = $(this).data('ecg_id');

		var report_comment_input = $('#report_type_comment_input');
		var report_comment = report_comment_input.length ? report_comment_input.val() : '';
		$('#report_creating_msg').removeClass('hide');
		if( report_department == "QTC"){
			window.open(url('analysis/z2b/'+ecg_id+'/report?type='+report_type+(report_department ? ('&department='+report_department):'')+(report_comment_input.length ? ('&comment='+report_comment):'')));
			$('#report_modal').modal('hide');
			$('#report_creating_msg').addClass('hide');
		}else{
			$.get(url('analysis/z2b/'+ecg_id+'/report?type='+report_type+(report_department ? ('&department='+report_department):'')+(report_comment_input.length ? ('&comment='+report_comment):'')),function(data){
				$('#report_modal').modal('hide');
				$('#report_creating_msg').addClass('hide');
				//$('#report_download_modal').modal('show');
			})
		}
	});

	$('#report_download').click(function(event){
		if(!check_report_duration()){
			return false;
		}
		var report_department = $(this).data('department');
		var report_type=$('#report_modal input[name="report_tpye"]:checked').map(function(index, item){
			var report_tpye_duration = $(item).parent().parent().find('.report_tpye_duration');
			report_tpye_duration = report_tpye_duration.length > 0 ? report_tpye_duration.val() : '';
			return $(item).val()+report_tpye_duration;}).toArray().join(',');
		window.open(url('analysis/z2b/'+($(this).data('ecg_id'))+'/report/download?type='+report_type+(report_department ? ('&department='+report_department):'')));
		$('#report_download_modal').modal('hide');
	});
	$('#report_create_show').click(function(event){
		event.preventDefault();
		if($(this).data('filename_ext')=='z2b' || $(this).data('filename_ext')=='xml'){
			show_report_modal($(this).data('ecg_id'));
		}
	});
	$('#report_email_form').submit(function(event){
		var ecg_id = $('#report_ecg_id').val();
		var report_email = $('#report_email').val();
		$('#send_message_content').html('@lang("physiolguard.sending")');
		$('#send_message_loading').show();
		$.post(url('/analysis/ecg/report/email'),{ecg_id:ecg_id,report_email:report_email},function(data){
			$('#send_message_loading').hide();
			if(data.send==1){
				$('#send_message_content').html('<span class="text-success">寄送成功</span>');
			}else{
				$('#send_message_content').html('<span class="text-danger">寄信失敗</span>');
			}
		});
		return false;
	});

	function check_report_duration(){
		var report_type_duration = $('#report_modal input[name="report_tpye_duration"]');
		for (var i = 0; i <report_type_duration.length; i++) {
			var duration = report_type_duration.eq(i).val();
			var min = parseInt(report_type_duration.eq(i).attr('min'), 10);
			var max = parseInt(report_type_duration.eq(i).attr('max'), 10);
			if(duration < min || duration > max){
				bootbox.alert({
    				message: '@lang("physiolguard.out_of_range")',
    				locale: lang
    			});
				return false;
			}
		}
		return true;
	}
	function show_report_modal(ecg_id)
	{
		$.get(url('/analysis/ecg/'+ecg_id+'/peak/check'),function(data){
			var result = data.data;
			if(result){
				$('#report_download_modal').modal('hide');
				$('#report_modal').modal('show');
				$('#report_modal input[name="report_tpye"]').each(function(i,d){
					if(result.peak_AN==0){
						if($(d).data('name')=='Normal sinus rhythm'){
							$(d).prop('checked',true);
						}else{
							$(d).prop('checked',false);
						}
					}else{
						if($(d).data('name')=='Abnormal rhythm or beat'){
							$(d).prop('checked',true);
						}else{
							$(d).prop('checked',false);
						}
					}
				});
				$('#report_create').data('ecg_id', ecg_id);
			}
		});
	}


	function load_ecg_content(){
		$('#ecg_view_loading').show();
		$('#ecg_overview_loading').show();
		$.get(url('/api/v0/analysis/ecg/'+ecgID),function(result){
			originEcgData = {...result.ecgdata};
			ecgData = result.ecgdata;
			ecgData256 = result.ecgdata256;
			ecg_size = ecgData.L2.length;
			if(ecg_size>1){

@if($mode == 'holter')
				draw_ecg_overview(ecgData.L2,overview_sample_rate,'.ecg_overview_chart','#ecg_overview_chart');
				$('#ecg_overview_loading').hide();
@endif
				initECGGraph(ecgData, "#ecgView");

				$('#ecg_view_loading').hide();
@if($mode == 'holter')
				ecg_slide_window(ecgData.L2,'.ecg_overview_chart','#ecg_overview_chart');
@endif
				ecgview_svg_width=$('#ecgView svg').width();
				
				loadRR(function(){
					loadEvent();
				});
			}
		});
	}

	function resize(event){
		if(ecgData){
			container_width=$('#main').width()-scroll_bar.width;
			overview_container_width = $('.ecg_overview_chart').width()-scroll_bar.width;
			ecgview_svg_width=$('#ecgView svg').width();
			ecgview_width=$('#ecgView').width()-scroll_bar.width;
			initECGGraph(ecgData, "#ecgView");
			markPeak(rrData, 0);
			var chart_num=parseFloat(ecg_size*scale/overview_sample_rate);
			var ecg_view_size=(chart_num)*origin_width*(ecgview_width)/(ecgview_svg_width);
			var rr_size=origin_width*(ecgview_width)/(ecgview_svg_width);
			
			d3.selectAll('#ecg_overview_chart svg>g')
				.selectAll('g')
				.attr("transform",function(d, i){
					return "translate(0 " + i * 50 +") scale(" + (overview_container_width/origin_width) + " 1)";
				});
			d3.selectAll('#ecg_overview_chart svg')
				.selectAll('.rr_peak_row')
				.attr("transform",function(d, i){
					return "translate(0 " + i * 50 +") scale(" + (overview_container_width/origin_width) + " 1)";
				});
			d3.selectAll('#ecg_overview_chart svg')
				.selectAll('.event_peak_row')
				.attr("transform",function(d, i){
					return "translate(0 " + i * 50 +") scale(" + (overview_container_width/origin_width) + " 1)";
				});
			d3.select('#rr_line_chart svg>g')
				.selectAll('g')
				.attr("transform",function(d,i){
					return "scale(" + (container_width/origin_width) + " 1)";
				});
				
			d3.select('#rr_slidewindow')
				.attr("width",rr_size)
				.attr("transform", "translate(0 0) scale(" + (container_width/origin_width) + " 1)");
			d3.select('#ecg_slidewindow')
				.attr("width",ecg_view_size)
				.attr("transform", "translate(0 0) scale(" + (overview_container_width/origin_width) + " 1)");
			d3.select('#ecg_slidewindow_wrap')
				.attr("width",ecg_view_size)
				.attr("transform", "translate(0 0) scale(" + (overview_container_width/origin_width) + " 1)");
			move_ecgview_window($("#ecgView").scrollLeft());

			if((ecgType=='z2b' || ecgType=='xml') && ecg_size > 1){
				tachogram_update(rrData, '.rr_beat_chart','#rr_beat_chart');
				histogram_update(rrData, '.rr_histogram','#rr_histogram');
				avgBeatChart_update(ecgData256, rrData, '.rr_avgBeatChart','#rr_avgBeatChart');
			}
		}
	}

	function slide_window_resize(scale){

		ecgview_svg_width=$('#ecgView svg').width();
		var chart_num=parseFloat(ecg_size*scale/overview_sample_rate);
		var ecg_view_size=(chart_num)*origin_width*(ecgview_width)/(ecgview_svg_width);
		var rr_size=origin_width*(ecgview_width)/(ecgview_svg_width);

		d3.select('#rr_slidewindow')
			.attr("width",rr_size)
			.attr("transform", "translate(0 0) scale(" + (container_width/origin_width) + " 1)");
		d3.select('#ecg_slidewindow')
			.attr("width",ecg_view_size)
			.attr("transform", "translate(0 0) scale(" + (overview_container_width/origin_width) + " 1)");
		d3.select('#ecg_slidewindow_wrap')
			.attr("width",ecg_view_size)
			.attr("transform", "translate(0 0) scale(" + (overview_container_width/origin_width) + " 1)");
	}

	function move_ecgview_window(x){
		$("#ecgView").scrollLeft(x);
	}
	function move_overview_window(x,y,window_size){
		$('#ecg_overview_chart').scrollTop((y-(y%4))*50);
		d3.select('#ecg_slidewindow')
			.attr("transform","translate(" + x + " "+ y * 50 + ") scale(" + (overview_container_width / origin_width) + " 1)");
		if(x+window_size>overview_container_width){
			
			$('#ecg_slidewindow_wrap').show();
			d3.select('#ecg_slidewindow_wrap')
				.attr("transform","translate("+(x-overview_container_width)+" "+(y+1)*50+") scale("+(overview_container_width/origin_width)+" 1)");
		}else{
			$('#ecg_slidewindow_wrap').hide();
			
		}
	}

	function move_rr_window(x , y) {
		d3.select("#rr_slidewindow")
				.attr("transform", "translate(" + x + " "+ y +") scale(" + (container_width / origin_width) + " 1)");
	}

	function move_current_peak_window(x_pos) {
@if($mode == 'holter')
		var start = 0
		var line_group = 1
@else
		var start = $('#ecgView').scrollLeft() / scale;
		var line_group = getLineGroupByData(start, x_pos);
@endif
		var x_offset = (line_group - 1) * viewableWidth;
		var x = (x_pos - x_offset) * scale;
		d3.select('#current_peak_window')
				.attr("transform", "translate(" + (parseInt(x) - 15) + " " + $("#ecgView").scrollTop() + ")");

		$('#current_peak_window').stop(true, true).show().fadeOut(1500);
@if($mode == 'holter')
		move_ecgview_window(x - ecgview_width / 2);
@else
		move_ecgview_window(x_pos * scale - (ecgview_width * 3) / 2);
@endif
	}

	function getLineGroupByData(start, time){
		start = parseInt(start);
		var end = start + viewableWidth;
		if(time < end){
			return 1;
		}
		var g_start = end;
		end = g_start + viewableWidth;
		if(time < end){
			return 2;
		}
		g_start = end;
		end = g_start + viewableWidth;
		if(time < end){
			return 3;
		}
	}
	function getLineGroup(offsetY){
		var y_pos = Math.floor(offsetY/scale);
		if(y_pos < singleLineYOffsetUV * yScaleRatio * scale){
			return 1;
		}else if(y_pos < singleLineYOffsetUV * 2 * yScaleRatio * scale){
			return 2;
		}else if(y_pos < singleLineYOffsetUV * 3 * yScaleRatio * scale){
			return 3;
		}
		return 3;
	}


// ECG graph scaling
var initZoomMousePosition;
var finalZoomMousePosition;
var isZooming = false;
var body = $("body");
var scale = 1;
body.on("mousedown", function(e) {
@if($mode == 'holter')
	// If the user right click on the ECG graph, zooming starts
	if (e.which == 3 && (e.target.id == "ecgView" || $(e.target).parents("#ecgView").length)) {
		isZooming = true;

		var ecgView = $("#ecgView");
		initZoomMousePosition = {
			x: e.pageX - ecgView.offset().left + ecgView.scrollLeft(),
			y: e.pageY - ecgView.offset().top + ecgView.scrollTop()
		};
		finalZoomMousePosition = {
			x: e.pageX - ecgView.offset().left + ecgView.scrollLeft(),
			y: e.pageY - ecgView.offset().top + ecgView.scrollTop()
		};
	}
	if ( false && e.which == 3 && (e.target.id == "rr_line_chart" || $(e.target).parents("#rr_line_chart").length)) {
		isZooming = true;

		var rrView = $("#rr_line_chart");
		initZoomMousePosition = {
			x: e.pageX - rrView.offset().left + rrView.scrollLeft(),
			y: e.pageY - rrView.offset().top + rrView.scrollTop()
		};
		finalZoomMousePosition = {
			x: e.pageX - rrView.offset().left + rrView.scrollLeft(),
			y: e.pageY - rrView.offset().top + rrView.scrollTop()
		};
	}
	if(selectRangeStart){
		$('#range_start').val(Math.floor(e.offsetX/scale));
	}
	if(selectRangeEnd){
		$('#range_end').val(Math.floor(e.offsetX/scale));
	}
@endif
});

body.on("mousemove", function(e) {
	// Update the position of the mouse
	var ecgView = $("#ecgView");
	finalZoomMousePosition = {
		x: e.pageX - ecgView.offset().left + ecgView.scrollLeft(),
		y: e.pageY - ecgView.offset().top + ecgView.scrollTop()
	};
	if (isZooming) {
		// Hide the selection window when mouse pointer is not on the ECG graph
		if (!$(e.target).parents("#ecgView").length && !$(e.target).parents("#rr_line_chart").length) {
			$("#selectionWindow").attr("height", 0).attr("width", 0);
			return;
		}

		// Update the position and the size of the selection indicator window
		var x = Math.min(initZoomMousePosition.x, finalZoomMousePosition.x);
		var y = Math.min(initZoomMousePosition.y, finalZoomMousePosition.y);
		var width = Math.abs(finalZoomMousePosition.x - initZoomMousePosition.x);
		var height = Math.abs(finalZoomMousePosition.y - initZoomMousePosition.y);
		if ($(e.target).parents("#ecgView").length ) {
			$("#selectionWindow").attr("transform", "translate(" + x + " " + y + ")").attr("height", height).attr("width", width);
		}
		if ($(e.target).parents("#rr_line_chart").length) {
			$("#selectionRRWindow").attr("transform", "translate(" + x + " "+$("#rr_line_chart").scrollTop()+")").attr("width", width);
		}
	}

	if ($(e.target).parents("#ecgView").length ) {
		
		$("#mouseLine").attr("transform", "translate(" + finalZoomMousePosition.x + " " + ecgView.scrollTop() + ")").attr("height", ecgView.height()).attr("width", 1);
		$("#current_time_box").attr("transform", "translate(" + finalZoomMousePosition.x + " " + ecgView.height()/2 + ")");
		var line_group = getLineGroup(event.offsetY);
		@if($mode == 'holter')
			line_group = 1;
		@endif
		$("#current_time_box .current_time").text(Math.floor(((line_group - 1) * viewableWidth+e.offsetX)/scale));
	}
});

body.on("contextmenu", function(e){
	// Handle the right mouse release here
	if (isZooming) {
		var ecgView = $("#ecgView");
		var rrView = $("#rr_line_chart");

		// Use the moving direction of the mouse to decide zoom in or zoom out
		var newScale = 1;
		var dx = finalZoomMousePosition.x - initZoomMousePosition.x;
		var dy = finalZoomMousePosition.y - initZoomMousePosition.y;
		var delta = dx + dy;

		if (delta > 0) {
			// Zoom in
			newScale = Math.min(ecgView.width() / dx, ecgView.height() / dy);
		} else if (delta < 0) {
			// Zoom out
			newScale = 1;
		}
		// Do nothing when delta = 0 (Right click and release immediately)

		// Resize the ECG graph
		var svg = ecgView.find("svg");
		newScale = Math.min(newScale,5.0);
		var newWidth = svg.width() * newScale / scale;
		var newHeight = svg.height() * newScale / scale;
		svg.width(newWidth).height(newHeight);

		// The center point for zooming
		var centerPoint = {
			x: (initZoomMousePosition.x + dx / 2) * newScale / scale,
			y: (initZoomMousePosition.y + dy / 2) * newScale / scale
		};

		// Apply the new scale and redraw
		scale = newScale;
		ecgView.scrollLeft(centerPoint.x - ecgView.width() / 2).scrollTop(centerPoint.y - ecgView.height() / 2);
		drawECGGrid("#ecgView");

		// Resize the RR interval slide window size and ECG overview slide window size
		slide_window_resize(scale);

		// Unset isZooming
		isZooming = false;

		// Hide the selection window
		$("#selectionWindow").attr("height", 0).attr("width", 0);


		// Prevent the context menu shows up after releasing right mouse button
		e.preventDefault();
	}
});

// key event
body.on("keypress",function(event){
	switch(event.charCode){default:
		break;case 100:
			$('#peak_D').parent().addClass('active').siblings().removeClass('active');
		break;case 110:
			$('#peak_N').parent().addClass('active').siblings().removeClass('active');
		break;case 118:
			$('#peak_V').parent().addClass('active').siblings().removeClass('active');
		break;case 97:
			$('#peak_A').parent().addClass('active').siblings().removeClass('active');
		break;case 115:
			$('#peak_S').parent().addClass('active').siblings().removeClass('active');
		break;case 101:
			$('#peak_E').parent().addClass('active').siblings().removeClass('active');
		break;case 113:
			$('#peak_Q').parent().addClass('active').siblings().removeClass('active');
		break;
	}
});
</script>
@endsection