@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/ecganalysis.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<link href="{{url('/css/report.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4 class="pull-left title">@lang("physiolguard.ecg_analysis")＞<a href="{{url('analysis/ecg').'/'.$ecg_id}}">{{$fileName}}</a>＞@lang("physiolguard.report")</h4>
			<div id="toolbar">
				<ul class="btn-toolbar">
					<li >
						<div class="select_peaks">
							<label class="control-label">Select export peak on ecg:  </label>
							
							<label class="control-label"><input id="peak_V" type="checkbox" value="V">V</label>							
							<label class="control-label"><input id="peak_S" type="checkbox" value="S">S</label>
						</div>
					</li>
					<li class="pull-right">
						<div class="col-md-3">
							<button id="export_btn" class="btn btn-success">
								@lang("physiolguard.export")
							</button>
						</div>
					</li>

				</ul>
			</div>
			<hr/>
			<div id="ecg_loading" class="ajax_loading">
				<img src="{{url('/assets/img/loading.gif')}}">
			</div>
			<div id="ecg_exporting">
				輸出處理中...
			</div>
			<div id="temp_analysis_report" class="analysis_report">
				
			</div>
			<form id="report_summary_form" class="form-horizontal" action="{{url('/analysis/ecg/report/'.$ecg_id)}}" method="POST">
			{{ csrf_field() }}
			<div id="analysis_report" class="analysis_report">
				<div class="page">
					<h1 style="text-align:center;">Report</h1>
					<div class="patient_info section">
						<div class="row head">
							<div class="col-sm-2 title">
								@lang("physiolguard.patient"): </div>
							<div class="col-sm-4 data"> 
								<input type="text" id="patient_name" name="patient_name" class="form-control" value="{{$ecgInfo->patient_name}}">
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.diagnostic"): </div>
							<div class="col-sm-4 data">{{Auth::user()->name}}</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.patient_ID"): </div>
							<div class="col-sm-4 data"> 
								# {{$ecgInfo->id}}
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.filename"): </div>
							<div class="col-sm-4 data">{{$fileName}}</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.age"): </div>
							<div class="col-sm-4 data"> 
								<input type="number" id="age" name="age" class="form-control" value="{{$ecgInfo->age}}">
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.record_date"): </div>
							<div class="col-sm-4 data">{{ date("Y-m-d",strtotime($summary['start_time'])) }}</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.gender"): </div>
							<div class="col-sm-4 data"> 
								<select id="sex" name="sex" class="form-control" >
									<option value="M" {{ $ecgInfo->sex=="M"? "selected":'' }} >@lang("physiolguard.gender_M")</option>
									<option value="F" {{ $ecgInfo->sex=="F"? "selected":'' }} >@lang("physiolguard.gender_F")</option>
								</select>
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.analysis_date"): </div>
							<div class="col-sm-4 data">{{ date("Y-m-d",strtotime($ecgInfo->created_at)) }}</div>
						</div>
					</div>
					<div class="analysis_info section">
						<div class="row title_head">
							<div class="col-sm-6 title">GENERAL SUMMARY</div>
							<div class="col-sm-6 title">HEART RATE SUMMARY</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.start_time"): </div>
							<div class="col-sm-4 data"> 
								{{date("H:i:s",strtotime($summary['start_time']))}}
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.min_hr"): </div>
							<div class="col-sm-4 data">{{ round($summary['min_hr']) }} @lang("physiolguard.bpm")</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.total_time"): </div>
							<div class="col-sm-4 data"> 
								{{ round($summary['total_time'],2) }} @lang("physiolguard.second")
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.max_hr"): </div>
							<div class="col-sm-4 data">{{ round($summary['max_hr']) }} @lang("physiolguard.bpm")</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.total_beats"): </div>
							<div class="col-sm-4 data"> 
								{{$summary['total_beats']}} @lang("physiolguard.times")
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.mean_hr"): </div>
							<div class="col-sm-4 data">{{ round($summary['mean_hr']) }} @lang("physiolguard.bpm")</div>
						</div>
						<div class="row title_head">
							<div class="col-sm-6 title">VENTRICULAR SUMMARY</div>
							<div class="col-sm-6 title">SUPRAVENTRICULAR SUMMARY</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.total"): </div>
							<div class="col-sm-4 data"> 
								{{ $summary['V_total'] }}
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.total"): </div>
							<div class="col-sm-4 data">
								{{ $summary['S_total'] }}
							</div>
						</div>
						<div class="row">
							<div class="col-sm-2 title">@lang("physiolguard.pairs"): </div>
							<div class="col-sm-4 data"> 
								{{$summary['V_pairs']}}
							</div>
							<div class="col-sm-2 title">@lang("physiolguard.pairs"): </div>
							<div class="col-sm-4 data"> 
								{{$summary['S_pairs']}}
							</div>
						</div>
						<div class="row title_head">
							<div class="col-sm-12 title">@lang("physiolguard.comment"): </div>
						</div>
						<div class="row">
							<div class="col-sm-12 data">
								<textarea rows="6" class="form-control" name="comment" id="comment">{{ $ecgInfo->comment }}</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="page">
					<div class="general_info section">
						<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th>Interval</th>
									<th colspan="3">Heart Rate</th>
									<th>Total</th>
									<th colspan="3">VPB</th>
									<th colspan="3">SVPB</th>
								</tr>
								<tr>
									<th>Starting</th>
									<th>Lo</th>
									<th>Mean</th>
									<th>Hi</th>
									<th>Beats</th>
									<th>Total</th>
									<th>Pairs</th>
									<th>Single</th>
									<th>Total</th>
									<th>Pairs</th>
									<th>Single</th>
								</tr>
							</thead>
							<tbody>
								@forelse ($generalProfile as $general)
								    <tr>
								    	<td>{{date("H:i:s",strtotime($summary['start_time'])+$general['start_time'])}}</td>
										<td>{{round($general['min_hr'])}}</td>
										<td>{{round($general['mean_hr'])}}</td>
										<td>{{round($general['max_hr'])}}</td>
										<td>{{$general['total_beats']}}</td>
										<td>{{$general['V_total']}}</td>
										<td>{{$general['V_pairs']}}</td>
										<td>{{$general['V_single']}}</td>
										<!-- <td>{{$general['V_total']-2*$general['V_pairs']}}</td> -->
										<td>{{$general['S_total']}}</td>
										<td>{{$general['S_pairs']}}</td>
										<td>{{$general['S_single']}}</td>
										<!-- <td>{{$general['S_total']-2*$general['S_pairs']}}</td> -->
								    </tr>
								@empty
								    <tr>@lang("physiolguard.data")資料</tr>
								@endforelse
								<tr class="summary">
								    	<td>Summary: </td>
										<td>{{ round($summary['min_hr']) }}</td>
										<td>{{ round($summary['mean_hr']) }}</td>
										<td>{{ round($summary['max_hr']) }}</td>
										<td>{{ $summary['total_beats'] }}</td>
										<td>{{ $summary['V_total'] }}</td>
										<td>{{ $summary['V_pairs'] }}</td>
										<td>{{ $summary['V_total']-2*$summary['V_pairs'] }}</td>
										<td>{{ $summary['S_total'] }}</td>
										<td>{{ $summary['S_pairs'] }}</td>
										<td>{{ $summary['S_total']-2*$summary['S_pairs'] }}</td>
								    </tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="page">
					<div class="trend_info section">
						<div class="hrbmp chart">
						</div>
						<div class="vpbbmp chart">
						</div>
						<div class="svpbbmp chart">
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="hrbmp" />
			<input type="hidden" name="vpbbmp" />
			<input type="hidden" name="svpbbmp" />
			<div id="tmp_ecgs" class="hide">
				
			</div>
			<div id="svg_inputs">

			</div>

				<button class="btn btn-success pull-right hidden" type="submit" id="report_summary_form_submit">
					@lang('physiolguard.export')
				</button>
			</form>
		</article>
	</div>
</div>
@endsection

@section('javascript')

<script src="{{url('/thirdpart/d3/d3.min.js')}}"></script>
<script src="{{url('/thirdpart/jsPDF/jspdf.debug.js')}}"></script>
<script>
$(function(){
	$("#sidebar").addClass("sidebarhide").append('<div id="sidebarmenu"><span class="glyphicon glyphicon-menu-hamburger"></span></div>');
	var ecgID = {{$ecg_id}};
	var ecgData ;
	var rrData ;
	var filename="{{ $fileName }}";
	var startTime = {{ strtotime($summary['start_time']) }};
	var sampleRate = {{ ($ecgInfo->filename_ext=='EKG' || $ecgInfo->filename_ext=='ekg')? 250:256 }};
	var peakData=[];
	var rrN=0;
	var chartN=0;
	var reportNum={rr_n:0,chart_num:0};

	$('#export_btn').click(async function(event){
		$('#ecg_exporting').show();
		await convertSvgToPngBase64($(".hrbmp"), function(base64){
			$('#report_summary_form input[name=hrbmp]').val(base64);
		}, 1920)
		await convertSvgToPngBase64($(".vpbbmp"), function(base64){
			$('#report_summary_form input[name=vpbbmp]').val(base64);
		}, 1920)
		await convertSvgToPngBase64($(".svpbbmp"), function(base64){
			$('#report_summary_form input[name=svpbbmp]').val(base64);
		}, 1920)
		var report_num={rr_n:0,chart_num:0};

		$('#tmp_ecgs').empty();
		$('#svg_inputs').empty();

		setTimeout(async function(){
			var select_peaks=$('#toolbar .select_peaks input:checked').map(function(i,d){return $(d).val()}).toArray();
			let acc = 0;
			while(report_num.rr_n<rrData.length){
				select_ecg_data(select_peaks, '#tmp_ecgs', report_num, 960);
				await convertSvgToPngBase64($("#tmp_ecgs .charts"), function(base64, key, elm){
					$('#svg_inputs').append('<input type="hidden" name="svgs['+$(elm).data('id')+']" class="svgs" value="'+base64+'">');
					$('#svg_inputs').append('<input type="hidden" name="svg_info['+$(elm).data('id')+'][peak_time]" class="svg_info" value="'+$(elm).data('peak-time')+'">');
					$('#svg_inputs').append('<input type="hidden" name="svg_info['+$(elm).data('id')+'][hr]" class="svg_info" value="'+$(elm).data('hr')+'">');
					$('#tmp_ecgs').empty();
				});
			}
			$('#report_summary_form').submit();
			$('#ecg_exporting').hide();
		},100);
	});

	$('#report_summary_form').on('keydown', 'input', function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

	$('#toolbar').hide();
	$('#ecg_exporting').hide();
	loadECG();

	$('#toolbar .select_peaks input').change(function(event){
		$('#ecg_loading').show();
		$('#analysis_report .page.charts').remove();
		reportNum = {rr_n:0,chart_num:0};
		if(ecgData==null || rrData==null){
			$('#ecg_loading').hide();
			return false;
		}
		setTimeout(function(){
			var select_peaks=$('#toolbar .select_peaks input:checked').map(function(i,d){return $(d).val()}).toArray();
			if(select_peaks.length>0){
				select_ecg_data(select_peaks, '#analysis_report',reportNum);
			}
			$('#ecg_loading').hide();
		});
	});
	loadChart();

	function loadECG(){
		$('#ecg_loading').show();
		$.get(url('/api/v0/analysis/ecg/{{$ecg_id}}?sample_256=false'),function(result){
			ecgData=result.ecgdata;
			$('#ecg_loading').hide();
			loadRR()
		});
	}
	function loadRR(){
		$('#rr_loading').show();
		$.get(url('/api/v0/analysis/rr/{{$ecg_id}}'), function (result) {
			rrData = result.rrdata;
			rrData.splice(0,1);
			for(var i=0;i<rrData.length;i++){
				var prevPeakTime = (i==0)?0:rrData[i - 1].time;
				var thisPeakTime = rrData[i].time;
				rrData[i].intervalPos = prevPeakTime + (thisPeakTime - prevPeakTime) / 2;
				rrData[i].prevTime = prevPeakTime;
			}
			draw_hr_chart(rrData,".hrbmp","Heart Rate BPM");

			var mintueRRData=get_minute_rr(rrData);
			var maxX = rrData[rrData.length-1].time/(sampleRate/2)
			draw_peaks_chart(mintueRRData,".vpbbmp","VPB per Min","V_total", maxX);
			draw_peaks_chart(mintueRRData,".svpbbmp","SVPB per Min","S_total", maxX);
			$('#toolbar').show();
			$('#ecg_loading').hide();
		});
	}

	function loadChart(){
		$(window).scroll(function(){
			if(rrData && ecgData){
				if(($(window).scrollTop()+$(window).height())>=($(document).height()-150)){
						$('#ecg_loading').show();
						setTimeout(function(){
							var select_peaks=$('#toolbar .select_peaks input:checked').map(function(i,d){return $(d).val()}).toArray();
							select_ecg_data(select_peaks, '#analysis_report',reportNum);
							// convert_svg_to_img($('#analysis_report'));
							$('#ecg_loading').hide();
						});
				}
			}
		});
	}

	function select_ecg_data(peaks, container, reportNum, configWidth){
		var slice_len = sampleRate*4;
		var slice_num = ecgData.L2.length/slice_len;
		var page_str='';
		//var chart_num=0;
		var peakData={};
		while(reportNum.rr_n<rrData.length){
			if(peaks.indexOf(rrData[reportNum.rr_n].peak) != -1){
				reportNum.chart_num++;
				var datetime=new Date((startTime+(rrData[reportNum.rr_n].time/(sampleRate/2)))*1000);
				var hr=Math.round(60/(rrData[reportNum.rr_n].interval/(sampleRate*4)));
				var peak_time=datetime.getHours()+':'+datetime.getMinutes()+':'+datetime.getSeconds();
				var info='-';
				peakData[reportNum.chart_num] = rrData[reportNum.rr_n];
				if(reportNum.chart_num%3==1){
					page_str+='<div class="page charts"><div class="ecg_info section">';
				}
				page_str+='<div class="ecg_part_chart_info"><div>'+peak_time+'</div><div>HR='+hr+'</div></div>';
				page_str+='<div class="ecg_part_'+reportNum.chart_num+' chart" data-id="'+(reportNum.chart_num-1)+'" data-peak-time="'+peak_time+'" data-hr="'+hr+'"></div>';
				if(reportNum.chart_num % 3 == 0){
					page_str+='</div></div>';
					$(container).append(page_str);
					reportNum.rr_n++;
					break;
				}
				
			}
			reportNum.rr_n++;
		}
		if(page_str != '' && reportNum.rr_n==rrData.length){
			page_str += '</div></div>';
			$(container).append(page_str);
		}
		for(var index in peakData){
			draw_ecg_chart(container, '.ecg_part_'+index, peakData[index],slice_len, configWidth);
		}
	}

	function get_minute_rr(data){
		var time_interval=0;
		var minute_rr=[];
		var interval_result={
			'time_interval':0,
			'V_total':0,
			'S_total':0
		};
		for(var i=0;i<data.length;i++){
			var peak_time=data[i].time/(sampleRate/2);
			if(parseInt(peak_time/60)*60 != time_interval){
				interval_result['time_interval']=time_interval;
				minute_rr.push(interval_result);
				time_interval = parseInt(peak_time/60) * 60;
				interval_result={
					'time_interval':0,
					'V_total':0,
					'S_total':0
				};
			}
			switch(data[i].peak){
				case 'V':case 'v':
					interval_result['V_total']++;
				break;
				case 'S':case 's':
					interval_result['S_total']++;
				break;
				default:
				break;
			}
		}
		interval_result['time_interval']=time_interval;
		minute_rr.push(interval_result);
		return minute_rr;
	}
	function draw_hr_chart(data, selector,axisYLabel){
		var margin = {top: 30, right: 20, bottom: 20, left: 50};
		var width = $(selector).parent().width() - margin.left - margin.right;
		var height = $(selector).height() - margin.top - margin.bottom;
		var origin_width = width;

		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom);
		var chart=svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		var maxX = data[data.length-1].time/(sampleRate/2);
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
		var maxY = d3.max(data, function (d) { return 60/(d.interval/(sampleRate*4));});
		var y = d3.scaleLinear().range([height,0]).domain([0, maxY+10]);
		
		var xAxis = d3.axisTop().scale(x).ticks(12).tickFormat(function(d){
				var datetime=new Date((startTime+d)*1000);
				return datetime.getHours()+':'+datetime.getMinutes()+':'+datetime.getSeconds();
			});
		var xAxis2 = d3.axisTop().scale(x).ticks(12).tickFormat(function(d){return "";});
		var yAxis = d3.axisLeft().scale(y);

		var line = d3.line().x(function (d) { return x(d.time/(sampleRate/2)); }).y(function (d) { return y(60/(d.interval/(sampleRate*4))); });

		var gridY = chart.append("g").selectAll('.gridY').data(d3.range(0,maxY+10,10));
		gridY.enter()
			.append("line")
			.attr('class','gridY')
			.attr("x1", 0)
			.attr("x2", origin_width)
			.attr("y1", function(d) { return y(d); })
			.attr("y2", function(d) { return y(d); })
			.attr("stroke", "black")
			.attr("stroke-width", 1)
			.attr("stroke-opacity", 0.5)
			.attr("stroke-dasharray", function(d){return (d%50==0)?"0,0":"5,5"});
			//.attr("shape-rendering", "crispEdges");
		gridY.exit().remove();

		chart.append("g")
			.append("path")
			.attr("fill","none")
			.attr("stroke","black")
			.attr("stroke-width","1.5px")
			.attr("d", line(data));

		var axis=svg.append("g").attr("class", "axis");

		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(" + (margin.left) + "," + margin.top + ") scale(" + (width / origin_width) + " 1)")
      		.call(xAxis);
      	axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(" + (margin.left) + "," + (height+margin.top) + ") scale(" + (width / origin_width) + " 1)")
      		.call(xAxis2);
  		axis.append("g")
      		.attr("class", "axisy")
      		.attr("transform", "translate(" + (margin.left-10) + "," + margin.top + ") scale(" + (width / origin_width) + " 1)")
      		.call(yAxis);
      	svg.append("text")
		    .attr("class", "y label")
		    .attr("text-anchor", "end")
		    .attr("y", 2)
		    .attr("x", -100)
		    .attr("dy", ".75em")
		    .attr("transform", "rotate(-90)")
		    .text(axisYLabel);
	}
	function draw_peaks_chart(data,selector,axisYLabel,featureY, maxX){
		var margin = {top: 30, right: 20, bottom: 20, left: 50};
		var width = $(selector).parent().width() - margin.left - margin.right;
		var height = $(selector).height() - margin.top - margin.bottom;
		var origin_width = width;

		var svg = d3.select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom);

		var chart=svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		// var maxX = data[data.length-1].time_interval;
		var maxY = d3.max(data, function (d) { return d[featureY];});
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, maxX]);
		var y = d3.scaleLinear().range([0,height]).domain([maxY+10,0]);
		
		var xAxis = d3.axisTop().scale(x).ticks(12).tickFormat(function(d){
				var datetime=new Date((startTime+d)*1000);
				return datetime.getHours()+':'+datetime.getMinutes()+':'+datetime.getSeconds();
			});
		var xAxis2 = d3.axisTop().scale(x).ticks(12).tickFormat(function(d){return "";});
		var yAxis = d3.axisLeft().scale(y);

		var bar_chart=chart.append("g").selectAll(".bar").data(data);
		bar_chart.enter()
			.append("rect")
			.attr('class','bar')
			.attr('x',function(d){return x(d["time_interval"])-1})
			.attr('y',function(d){return y(d[featureY])})
			.attr("width","3px")
			.attr("height",function(d){return height-y(d[featureY])});
		bar_chart.exit().remove();
		var grid_size = 1;
		var grid_size_weight = 5;
		if(maxY>100){
			grid_size=50;
			grid_size_weight = 10;
		}
		var gridY = chart.append("g").selectAll('.gridY').data(d3.range(0,maxY+10,grid_size));
		gridY.enter()
			.append("line")
			.attr('class','gridY')
			.attr("x1", 0)
			.attr("x2", origin_width)
			.attr("y1", function(d) { return y(d); })
			.attr("y2", function(d) { return y(d); })
			.attr("stroke", "black")
			.attr("stroke-width", 1)
			.attr("stroke-opacity", 0.5)
			.attr("stroke-dasharray", function(d){return (d%grid_size_weight==0)?"0,0":"5,5"});
			//.attr("shape-rendering", "crispEdges");
		gridY.exit().remove();

		var axis=svg.append("g").attr("class", "axis");

		axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(" + (margin.left) + "," + margin.top + ") scale(" + (width / origin_width) + " 1)")
      		.call(xAxis);
      	axis.append("g")
      		.attr("class", "axisx")
      		.attr("transform", "translate(" + (margin.left) + "," + (height+margin.top) + ") scale(" + (width / origin_width) + " 1)")
      		.call(xAxis2);
  		axis.append("g")
      		.attr("class", "axisy")
      		.attr("transform", "translate(" + (margin.left-10) + "," + margin.top + ") scale(" + (width / origin_width) + " 1)")
      		.call(yAxis);
      	svg.append("text")
		    .attr("class", "y label")
		    .attr("text-anchor", "end")
		    .attr("y", 2)
		    .attr("x", -100)
		    .attr("dy", ".75em")
		    .attr("transform", "rotate(-90)")
		    .text(axisYLabel);
	}

	function draw_ecg_chart(container, selector, peakdata, slice_len, configWidth){
		var time = peakdata.time;
		if(time<slice_len/2){
			var start_pos=0;
		}else if(time>ecgData.L2.length-slice_len){
			var start_pos=ecgData.L2.length-slice_len;
		}else{
			var start_pos=time-slice_len/2;
		}
		//var start_pos=(time<slice_len/2)?0:time-slice_len/2;
		//var end_pos=(time>ecgData.L2.length-((slice_len/2)+1))?ecgData.L2.length:time+((slice_len/2)+1);
		var end_pos=start_pos+slice_len;
		var current_pos=(time-start_pos<0)?0:time-start_pos;

		var margin = {top: 10, right: 0, bottom: 10, left: 0};
		var width = (configWidth ?? $(container).find(selector).parent().width()) - margin.left - margin.right;
		var height = $(container).find(selector).height() - margin.top - margin.bottom;
		
		var origin_width = width;

		var svg = d3.select(container).select(selector)
			.append("svg")
			.attr("width", '100%')
			.attr("height", height+ margin.top + margin.bottom);

		var chart=svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ") scale(" + (width / origin_width) + " 1)");

		chart.append('rect')
			.attr('x',0)
			.attr('y',0)
			.attr('width',origin_width)
			.attr('height',height)
			.attr('stroke','black')
			.attr('fill','none')
			.attr('stroke-width',1);

		var maxY = d3.max(ecgData.L2, function (d) { return d;});
		var minY = d3.min(ecgData.L2, function (d) { return d;});
		var meanY = d3.mean(ecgData.L2, function (d) { return d;});
		var x = d3.scaleLinear().range([0,  origin_width]).domain([0, slice_len]);
		var y = d3.scaleLinear().range([0, height]).domain([meanY-2000,meanY+2000]);
		var line = d3.line().x(function(d, i) { return x(i) }).y(function(d) { return y(d); });

		chart.append('text')
			.attr("transform", "translate(0 40) scale(1 1)")
			.style("font-size", "24px")
			.attr('x', x(current_pos)-5)
			.text(peakdata.peak);

		var rrtext=chart.append('g').attr("transform", "translate(0 60) scale(1 1)");
		for(var i=0;i<rrData.length;i++){
			if(rrData[i].time>=start_pos && rrData[i].time<=end_pos){
				var rr_pos=(rrData[i].intervalPos-start_pos<0)?0:rrData[i].intervalPos-start_pos;
				rrtext.append('text')
					.attr('x',x(rr_pos))
					.text(rrData[i].interval);
			}
		}

		chart.append('g')
			.append("path")
			.attr("transform", "translate(0 0) scale(1 0.5)")
			.attr("fill","none")
			.attr("stroke","red")
			.attr("stroke-width","1px")
			.attr('d',line(ecgData.L1.slice(start_pos,end_pos)));
		chart.append('g')
			.append("path")
			.attr("transform", "translate(0 "+height/3+") scale(1 0.5)")
			.attr("fill","none")
			.attr("stroke","green")
			.attr("stroke-width","1px")
			.attr('d',line(ecgData.L2.slice(start_pos,end_pos)));
		chart.append('g')
			.append("path")
			.attr("transform", "translate(0 "+height/2+") scale(1 0.5)")
			.attr("fill","none")
			.attr("stroke","blue")
			.attr("stroke-width","1px")
			.attr('d',line(ecgData.L3.slice(start_pos,end_pos)));

		
		var gridX = chart.append("g").selectAll('.gridX').data(d3.range(0,origin_width,origin_width/40));
		gridX.enter()
			.append("line")
			.attr('class','gridX')
			.attr("x1", function(d) { return d; })
			.attr("x2", function(d) { return d; })
			.attr("y1", 0)
			.attr("y2", height)
			.attr("stroke", "black")
			.attr("stroke-width", 1)
			.attr("stroke-opacity", 0.25);
		gridX.exit().remove();

		var gridY = chart.append("g").selectAll('.gridY').data(d3.range(0,height,15));
		gridY.enter()
			.append("line")
			.attr('class','gridY')
			.attr("x1", 0)
			.attr("x2", origin_width)
			.attr("y1", function(d) { return d; })
			.attr("y2", function(d) { return d; })
			.attr("stroke", "black")
			.attr("stroke-width", 1)
			.attr("stroke-opacity", 0.25);
		gridY.exit().remove();

	}

	function svgToBase64(container, callback){
		container.find('svg').each(function(i, d){
			var svg_str = new XMLSerializer().serializeToString(d);
			var svg64 = btoa(svg_str);
			var b64Start = 'data:image/svg+xml;base64,';
			var image64 = b64Start + svg64;
			callback(image64, i, $(d).parent())
		});
	}

	function convert_svg_to_img(container,callback){
		container.find('svg').each(function(i,d){
			svgToImgUrl(i,d, callback);
		});
	}

	async function convertSvgToPngBase64(container, callback, width) {
		const promises = []
		container.find('svg').each(function(i,d){
			const promise = new Promise((resolve) => {
			    const svgData = new XMLSerializer().serializeToString(d);
			    const svgBase64 = 'data:image/svg+xml;base64,' + btoa(svgData);
			    const img = new Image();
			    img.onload = () => {
			      const canvas = document.createElement('canvas');
			      canvas.width = width ?? 960;
			      canvas.height = 300;
			      const ctx = canvas.getContext('2d');
			      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
			      const pngBase64 = canvas.toDataURL('image/png');
			      resolve({base64:pngBase64, index:i, elm:d});
			    };
			    img.src = svgBase64;
			 });
			promises.push(promise)
		})

		await Promise.all(promises).then( pngs  => {
			for (var i = 0; i < pngs.length; i++) {
				callback(pngs[i].base64, pngs[i].index, $(pngs[i].elm).parent())
			}
		})
	}



	function svgToImgUrl(index, eml, callback){
		var svg_str = new XMLSerializer().serializeToString(eml);
		var canvas = document.createElement("canvas");
		$(canvas).attr('width',$(eml).parent().parent().width()).attr('height',$(eml).parent().height());
		var ctx = canvas.getContext("2d");
		var DOMURL = self.URL || self.webkitURL || self;
		var img = new Image();
		var svg = new Blob([svg_str], {type: "image/svg+xml;charset=utf-8"});
		var url = DOMURL.createObjectURL(svg);
		img.onload = function() {
		    ctx.drawImage(img, 0, 0);
		    var png = canvas.toDataURL("image/png");
		    var eml_p=$(eml).parent();
		    eml_p.html('<img src="'+png+'" style="width:100%"/>');
		    DOMURL.revokeObjectURL(png);
		    if(callback!=null){
		    	img=null;
		    	svg=null;
		    	ctx=null;
		    	canvas=null;
		    	svg_str=null;
		    	callback(png, index, eml_p);
		    }
		};
		img.src = url;
		/*var svg_str = new XMLSerializer().serializeToString(eml);
		var svg64 = btoa(xml);
		var b64Start = 'data:image/svg+xml;base64,';
		var image64 = b64Start + svg64;*/
	}
});
</script>
@endsection