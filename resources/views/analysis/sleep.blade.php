@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/ecganalysis.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<link href="{{url('/css/sleep_chart.css')}}" rel="stylesheet">
	<meta http-equiv="pragma" content="no-cache">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4 class="pull-left title">@lang("physiolguard.sleep_analysis") ＞ {{$filename}}</h4>
			<div id="toolbar">
				<ul class="btn-toolbar pull-right">
					<li >
						<a href="#" class="btn btn-default" id="sleep_btn">
							<span class="glyphicon glyphicon-stats"></span>
							@lang("physiolguard.sleep_analysis")
						</a>
					</li>
					<li >
						<a href="#" class="btn btn-default" id="sleep_report_btn" target="_blank">
							<span class="glyphicon glyphicon-list-alt"></span>
							<form action="{{url('/analysis/sleep/'.$ecg_id.'/report')}}" method="POST" id="sleep_report_form" target="_blank" class="hide">
								{{csrf_field()}}
							</form>
							@lang("physiolguard.create_report")
						</a>
					</li>
				</ul>
			</div>
			<hr/>
			<div id="loading" class="ajax_loading">
				<img src="{{url('/assets/img/loading.gif')}}">
			</div>
			<div id="message" class="text-center text-danger">

			</div>
			<div class="action_bar text-right">
				<div> 
					<bottun class="btn btn-default" id="full_screen_btn"><span class="glyphicon glyphicon-fullscreen"> </span></bottun>
				</div>
				<div id='range_btns' class="btn-group range_btns" data-toggle="buttons">
					<label class="btn btn-default active">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="0" checked> All 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="30"> 30 sec 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="60"> 1 mins 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="300"> 5 mins 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="1800"> 30 mins 
					</label>
					<label class="btn btn-default">
						<input type="radio" class="range_radio" name="range_SPO2" autocomplete="off" value="3600"> 1 hr 
					</label>
				</div>
				<!--
				<div id="data_filter">
					<span class="text">資料大於</span>
					<input type="number" aria-describedby="data-filter" id="data_filter_condition" value="230000" min="27499">
					<span class="text">過濾</span>
					<input type="text" aria-describedby="data-filter" id="data_filter_value" value="128" min="1">
				</div>
				-->
			</div>
			<div class="chart" id="StageView" data-sleep-index="0" data-sample-rate="1/30" data-y-scale-ratio="1" data-y-scale-low-bound="-1.2" data-y-scale-up-bound="2.2" data-line-interpolate="step" >
				<div class="title"><span class="cht">
							@lang("physiolguard.stage_view")</span><span class="eng">(Staging)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
						<rect class="slidewindow"></rect>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="ArousalView" data-sleep-index="1" data-sample-rate="1" data-y-scale-ratio="1" data-y-scale-low-bound="-0.2" data-y-scale-up-bound="1.2" data-y-offset="0" data-line-interpolate="step">
				<div class="title"><span class="cht">
							@lang("physiolguard.arousal_view")</span><span class="eng">(Arousal event)</span></div>
				
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
						<g class="peakMarker"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="SPO2View" data-sleep-index="2" data-sample-rate="1" data-y-scale-ratio="1" data-y-scale-low-bound="60" data-y-scale-up-bound="105">
				<div class="title"><span class="cht">@lang("physiolguard.spo2_view")</span><span class="eng">(SpO2)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
						<g class="tagMarker"></g>
						<g class="tempTagMarker">
							<rect></rect>
						</g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="PPG1View" data-sleep-index="3" data-sample-rate="128" data-y-scale-ratio="1">
				<div class="title"><span class="cht">@lang("physiolguard.ppg1_view")</span><span class="eng">(PPG waveform)</span></div>
				
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="ECG2View" data-sleep-index="4" data-sample-rate="128" data-y-scale-ratio="1">
				<div class="title"><span class="cht">@lang("physiolguard.ecg2_view")</span><span class="eng">(ECG waveform)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="HRView" data-sleep-index="5" data-sample-rate="1" data-y-scale-ratio="1" data-y-scale-low-bound="45" data-y-scale-up-bound="120">
				<div class="title"><span class="cht">@lang("physiolguard.hrv_view")</span><span class="eng">(HR)</span></div>
				
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="PTTView" data-sleep-index="6" data-sample-rate="1" data-y-scale-ratio="1">
				<div class="title"><span class="cht">@lang("physiolguard.ptt_view")</span><span class="eng">(PTT/BP)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="RespRView" data-sleep-index="7" data-sample-rate="1" data-y-scale-ratio="1">
				<div class="title"><span class="cht">@lang("physiolguard.respr_view")</span><span class="eng">(Respiratory rate)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="positionView" data-sleep-index="8" data-sample-rate="1/10" data-y-scale-ratio="1"  data-y-scale-low-bound="-1" data-y-scale-up-bound="3">
				<div class="title"><span class="cht">@lang("physiolguard.position_view")</span><span class="eng">(Position)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
			<div class="chart" id="AirFlowView" data-sleep-index="9" data-sample-rate="32" data-y-scale-ratio="1"  data-y-scale-low-bound="0" data-y-scale-up-bound="300">
				<div class="title"><span class="cht">@lang("physiolguard.air_flow_view")</span><span class="eng">(Air Flow)</span></div>
				<div class="axisbox">
					<svg>
						<g class="axisY"></g>
					</svg>
				</div>
				<div class="action">
					<div class="btn btn-default yZoomIn"><span class="glyphicon glyphicon-zoom-in"></span></div>
					<div class="btn btn-default yZoomOut"><span class="glyphicon glyphicon-zoom-out"></span></div>
				</div>
				<div class="chartbox">
					<svg>
						<g class="y-grid"></g>
						<g class="x-grid"></g>
						<g class="pathbox"></g>
					</svg>
				</div>
				<div class="timebar">
					<div class="start_at"></div>
					<div class="end_at"></div>
					<div class="timeaxisbox">
						<svg>
							<g class="axisX"></g>
						</svg>
					</div>
				</div>
			</div>
		</article>
	</div>
</div>
<div id="confirm_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="del_peak_modal_label">
  	<div class="modal-dialog modal-sm" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">x</span>
	  			</button>
	  		</div> 
	  		<div class="modal-body">
	  			<div class="msg_content">
	  				@lang("physiolguard.to_delete_peak")?
	  			</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary confirm" >@lang("physiolguard.confirm")</button>
	  			<button class="btn btn-default" data-dismiss="modal">@lang("physiolguard.close")</button>
			</div>
	  	</div>
	</div>
</div>
<div id="report_download_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="report_modal_download_label">
  	<div class="modal-dialog modal-md" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="report_modal_download_label">@lang("physiolguard.analysis_report")</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<div class="row">
	  				<div class="col-md-12" style="margin-bottom: 2em">
	  				@lang("physiolguard.create_report_msg_1")<span class="report_type"></span> @lang("physiolguard.create_report_msg_2")
	  				</div>
	  			</div>
	  			<div class="row">
	  				<button id="report_download_cancel" class="btn btn-default col-md-2 col-md-offset-1" data-dismiss="modal">@lang("physiolguard.cancel")</button>
	  				<a id="report_download" class="btn btn-info col-md-2 col-md-offset-1" href="{{url('/analysis/sleep/'.$ecg_id.'/report')}}" target="_blank">@lang("physiolguard.download")</a>
	  				<button id="report_create_show" class="btn btn-info col-md-4 col-md-offset-1">@lang("physiolguard.recreate_report")</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
@endsection

@section('javascript')

<script src="{{url('/thirdpart/d3/d3.v4.min.js')}}"></script>
<script src="{{url('/thirdpart/moment/moment.js')}}"></script>
<script src="{{url('/thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/js/sleep_chart.js')}}"></script>
<script>
	var ecgType = '{{$filename_ext}}';
	var ecgID = '{{$ecg_id}}';
	var ecgRecordedAt = '{{$recorded_at}}';
	var analysis_status = {{$analysis_status ? "true":"false"}};
	var report_status = {{$report_status ? "true":"false"}};
	var sleepData=[];
	var sleepExData = [];
	var tagPositionData = [];
	var maxDataLenIndex = 0;
	var dataTatalTime = 0;
	var dataNum = $('.chart').length;
	var svgWidth = parseFloat($('#StageView').width());
	var windowWidth = $(window).width();
	var stageViewSlideWindowDrag=false;

	$(function(){
		$("#sidebar").addClass("sidebarhide").append('<div id="sidebarmenu"><span class="glyphicon glyphicon-menu-hamburger"></span></div>');

		$('#sleep_btn').click(function(event){
			sleepData=[];
			dataNum=$('.chart').length;
			$.get(url('analysis/sleep/'+ecgID+'/run'), function(response){
				if(response.response=='ok'){
					location.reload();
				}
			});
		});
		
		if(analysis_status){
			$('#message').html('');
			$('.action_bar').show();
			$('.chart').show();
			$('#sleep_report_btn').show();
			loadSleepData(function(){
				maxDataLenIndex = findMaxHzIndex();
				dataTatalTime = sleepData[maxDataLenIndex].length / parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate')));
				svgWidth = parseFloat($('#StageView').find('.chartbox').width())-scrollbarWidth;
				updateAllView(0, false);
				$('#StageView').data('no-update',true);
				var range_select = $('#range_btns input[type=radio]:checked').val();
				var slideWindowWidth = range_select == 0 ? 0 : svgWidth;
				updateSlideWindow('#StageView', slideWindowWidth, {x:0} , function(position){

					});
				});
		}else{
			$('#message').html('尚未進行分析');
			$('.action_bar').hide();
			$('.chart').hide();
			$('#sleep_report_btn').hide();
		}
		$('#sleep_report_btn').click(function(event){
			event.preventDefault();
			if(!report_status){
				$('#sleep_report_form').submit();
				return true;
			}else{
				$('#report_download_modal').modal('show');
			}
		});
		$('#report_create_show').click(function(event){
			event.preventDefault();
			$('#sleep_report_form').submit();
		});
		$('#full_screen_btn').click(function(event){
			toggleFullScreen();
		});
		$('#range_btns').on('click','.btn',function(event){
			event.preventDefault();
			return parseInt($(this).find('input.range_radio').val(), 10) <= dataTatalTime
		});
		$('#range_btns').on('change','input.range_radio',function(event){
			if(parseInt($(this).val(), 10) <= dataTatalTime){
				updateChart();
			}
		});
		$('#data_filter_condition').on('change',function(event){
			updateChart();
		});
		$('#data_filter_value').on('change',function(event){
			updateChart();
		});
		$("#StageView").on('mousedown','svg',function(event){
			stageViewSlideWindowDrag = true;
			var range_select = $('#range_btns input[type=radio]:checked').val();
			if(range_select!=0){

				var svgOffset = $(this).offset();
				var dataLen = sleepData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) * 30);
				var range = range_select == 0 ? 0 : (range_select/30 * svgWidth/dataLen);
				
				var x = event.pageX - svgOffset.left;
				if((x-range/2)<=0){
					x = 0;
				}else if( ( x + range/2 ) >= ( svgWidth + 1 )){
					x = svgWidth + 1 - range;
				}else{
					x = x-range/2;
				}

				updateSlideWindow('#StageView', range, {x:x},function(){
					updateAllView(x, false);
				});
			}
		});
		$("#StageView").on('mousemove','svg',function(event){
			if(stageViewSlideWindowDrag){
				var range_select = $('#range_btns input[type=radio]:checked').val();
				if(range_select!=0){

					var svgOffset = $(this).offset();
					var dataLen = sleepData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) * 30);
					var range = range_select == 0 ? 0 : (range_select/30 * svgWidth/dataLen);
					var x = event.pageX - svgOffset.left;
					if((x-range/2)<=0){
						x = 0;
					}else if((x+range/2)>=(svgWidth + 1)){
						x = svgWidth + 1 - range;
					}else{
						x = x-range/2;
					}
					updateSlideWindow('#StageView', range, {x:x}, function(){
						updateAllView(x, false);
					});
				}
			}
		});
		$("#StageView").on('mouseup','svg',function(event){
			stageViewSlideWindowDrag = false;
		});

		$("#ArousalView .chartbox svg").on('click',function(event) {
			event.stopPropagation();
			var container = $("#ArousalView").find('.chartbox');
			var sleepIndex = $("#ArousalView").data('sleep-index');
			var svgOffset = $(this).offset();
			var positionX = event.pageX - svgOffset.left;
			var notFromStage = true;
			var sampleRate = parseFloat(eval($("#ArousalView").data('sample-rate')));

			var peakTime = getDataIndex(sleepIndex, positionX, sampleRate, notFromStage);
			if(sleepData[sleepIndex][peakTime] == 1){
				return false;
			}
			var peakIndex = -1;
			for(var index = 0 ; index < sleepExData[sleepIndex].peak.length; index++){
				var peak = sleepExData[sleepIndex].peak[index];
				if(peak.start > peakTime){
					peakIndex = (index == sleepExData[sleepIndex].peak.length-1) ? -1 : index;
					break;
				}
			}
			var peakData = {time: peakTime, data: 1, start: peakTime, end:peakTime, sleepIndex: sleepIndex};
			if(peakTime != 0 && sleepData[sleepIndex][peakTime-1]==1){
				peakIndex = sleepExData[sleepIndex].peak.findIndex(function(d){
					return d.start <= peakTime-1 && d.end >= peakTime-1;
				});
				sleepExData[sleepIndex].peak[peakIndex].end = peakTime;
				sleepExData[sleepIndex].peak[peakIndex].time = (sleepExData[sleepIndex].peak[peakIndex].start+peakTime)/2;
			}else{
				if(peakIndex == -1){
					sleepExData[sleepIndex].peak.push(peakData);
					peakIndex = sleepExData[sleepIndex].peak.length - 1;
				}else{
					sleepExData[sleepIndex].peak.splice(peakIndex, 0, peakData);
				}
			}

			if(peakTime != (sleepData[sleepIndex].length - 1) && sleepData[sleepIndex][peakTime+1]==1){
				var nextIndex = sleepExData[sleepIndex].peak.findIndex(function(d){
					return d.start <= peakTime+1 && d.end >= peakTime+1;
				});
				sleepExData[sleepIndex].peak[peakIndex].end = sleepExData[sleepIndex].peak[nextIndex].end;
				sleepExData[sleepIndex].peak[peakIndex].time = (sleepExData[sleepIndex].peak[peakIndex].start + sleepExData[sleepIndex].peak[nextIndex].end)/2;
				sleepExData[sleepIndex].peak.splice(nextIndex, 1);
			}

			sleepData[sleepIndex][peakTime] = 1;
			drawChart(sleepIndex, container.scrollLeft(), notFromStage);
			addPeak(ecgID, peakTime);
	    });
		var tagMousePress = false;
		var tagInsertData = {time: -1, data: 1, start: -1, end:-1, sleepIndex: 2, pos:-1};
		$("#SPO2View .chartbox svg").on('mousedown',function(event) {
			event.stopPropagation();
			if(!tagMousePress){
				var container_id = 'SPO2View';
				var container = $("#"+container_id).find('.chartbox');
				var sleepIndex = $("#"+container_id).data('sleep-index');
				var svgOffset = $(this).offset();
				var positionX = event.pageX - svgOffset.left;
				var notFromStage = true;
				var sampleRate = parseFloat(eval($("#"+container_id).data('sample-rate')));

				var tagTime = getDataIndex(sleepIndex, positionX, sampleRate, notFromStage);
				tagInsertData.time = tagTime;
				tagInsertData.start = tagTime;
				tagInsertData.end = tagTime;
				tagInsertData.pos = positionX;
				tagMousePress=true;
			}
	    });
	    $("#SPO2View .chartbox svg").on('mousemove',function(event) {
			event.stopPropagation();
			if(tagMousePress){
				var container_id = 'SPO2View';
				var container = $("#"+container_id).find('.chartbox');
				var sleepIndex = $("#"+container_id).data('sleep-index');
				var current_scroll = container.scrollTop();
				var svgOffset = $(this).offset();
				var positionX = event.pageX - svgOffset.left;
				var svg = d3.select(container[0]).select('svg');
				var g = svg.select("g.tempTagMarker").attr("transform", "translate( 0 "+current_scroll+")");
				var width =  positionX - tagInsertData.pos;
				var rect = g.select('rect')
					.attr("width", Math.abs(width))
					.attr("height", "20")
					.attr("fill", "green")
					.style("stroke", "white")
					.style("stroke-width", "1")
					.attr("transform", "translate("+tagInsertData.pos+" , 0) scale( "+(width>=0 ? '1':'-1')+",1)");
			}
	    });
	    $("#SPO2View .chartbox svg").on('mouseup',function(event) {
			event.stopPropagation();
			if(tagMousePress){
				var container_id = 'SPO2View';
				var container = $("#"+container_id).find('.chartbox');
				var sleepIndex = $("#"+container_id).data('sleep-index');
				var svgOffset = $(this).offset();
				var positionX = event.pageX - svgOffset.left;
				var notFromStage = true;
				var sampleRate = parseFloat(eval($("#"+container_id).data('sample-rate')));

				var tagTime = getDataIndex(sleepIndex, positionX, sampleRate, notFromStage);

				if(tagInsertData.start > tagTime){
					tagInsertData.end = tagInsertData.start;
					tagInsertData.start = tagTime;
				}else{
					tagInsertData.end = tagTime;
				}
				if(tagInsertData.end==tagInsertData.start){
					tagInsertData.end++;
				}
				tagInsertData.time = (tagInsertData.end + tagInsertData.start) / 2 ;

				if(tagInsertData.start!=0 && tagPositionData[sleepIndex][tagInsertData.start-1][0]==1){
					var mergeTagIndex = sleepExData[sleepIndex].tag.findIndex(function(d){
						return d.end >= tagInsertData.start && d.start<=tagInsertData.start;
					});
					if(mergeTagIndex!=-1){
						tagInsertData.start = sleepExData[sleepIndex].tag[mergeTagIndex].start;
						tagInsertData.time = (tagInsertData.end + tagInsertData.start) / 2 ;
						sleepExData[sleepIndex].tag.splice(mergeTagIndex, 1);
					}
				}
				if(tagInsertData.end != tagPositionData[sleepIndex].length - 1 && tagPositionData[sleepIndex][tagInsertData.end+1][0]==1){
					var mergeTagIndex = sleepExData[sleepIndex].tag.findIndex(function(d){
						return d.start <= tagInsertData.end && d.end >= tagInsertData.end;
					});
					if(mergeTagIndex!=-1){
						tagInsertData.end = sleepExData[sleepIndex].tag[mergeTagIndex].end;
						tagInsertData.time = (tagInsertData.end + tagInsertData.start) / 2 ;
						sleepExData[sleepIndex].tag.splice(mergeTagIndex, 1);
					}
				}
				$.each( sleepExData[sleepIndex].tag, function(i,d){
					if(d.start >= tagInsertData.start && d.end <= tagInsertData.end){
						delete sleepExData[sleepIndex].tag[i];
					}
				});
				sleepExData[sleepIndex].tag = sleepExData[sleepIndex].tag.filter(function(d){
						return d;
				});
				sleepExData[sleepIndex].tag.push(JSON.parse(JSON.stringify(tagInsertData)));
				
				for (var i = tagInsertData.start; i <= tagInsertData.end; i++) {
					tagPositionData[sleepIndex][i][0] = 1;
				}
				
				drawChart(sleepIndex, container.scrollLeft(), notFromStage);
				addTag(ecgID, tagInsertData.start, tagInsertData.end);
				tagMousePress=false;
				var svg = d3.select(container[0]).select('svg');
				var g = svg.select("g.tempTagMarker").attr("transform", "translate( 0 0)");
				var width =  positionX - tagInsertData.pos;
				var rect = g.select('rect')
					.attr("width", 0)
					.attr("height", "20")
					.attr("fill", "green")
					.style("stroke", "white")
					.style("stroke-width", "1")
					.attr("transform", "translate("+tagInsertData.pos+" , 0) scale( "+(width>=0 ? '1':'-1')+",1)");
			}
	    });
	    $('#confirm_modal').on('click','.confirm',function(event){
	    	$('#confirm_modal').modal('hide');
	    	var container_id = $(this).data('container-id');
	    	var container = $(container_id).find('.chartbox');
	    	var sleepIndex = $(container_id).data('sleep-index');
	    	var peakTime = $(this).data('pos');
	    	var notFromStage = true;
	    	var peakIndex = -1;
	    	if(sleepIndex!=2){
		    	for(var index = 0 ; index < sleepExData[sleepIndex].peak.length; index++){
					if(sleepExData[sleepIndex].peak[index].time == peakTime){
						peakIndex = index;
						break;
					}
				}
				if(peakIndex != -1){
					var delPeak = sleepExData[sleepIndex].peak.splice(peakIndex, 1)[0];
					for (var i = delPeak.start; i <= delPeak.end; i++) {
			    		sleepData[sleepIndex][i] = 0;
					}
					drawChart(sleepIndex, container.scrollLeft(), notFromStage);
					deletePeak(ecgID, delPeak.start, delPeak.end);
				}
			}else{
				for(var index = 0 ; index < sleepExData[sleepIndex].tag.length; index++){
					if(sleepExData[sleepIndex].tag[index].time == peakTime){
						tagIndex = index;
						break;
					}
				}
				if(tagIndex != -1){
					var delTag = sleepExData[sleepIndex].tag.splice(tagIndex, 1)[0];
					for (var i = delTag.start; i <= delTag.end; i++) {
			    		tagPositionData[sleepIndex][i][0] = 0;
					}
					drawChart(sleepIndex, container.scrollLeft(), notFromStage);
					deleteTag(ecgID, delTag.start, delTag.end);
				}
			}
			$('#confirm_modal .confirm').data('container-id',"");
	    	$('#confirm_modal .confirm').data('pos',-1);
	    });

	    $("#ArousalView .chartbox svg").on('click','g.peakMarker path',function(event) {
	    	event.stopPropagation();
	    	$('#confirm_modal .confirm').data('container-id',"#ArousalView");
	    	$('#confirm_modal .confirm').data('pos',$(this).attr('pos'));
	    	$('#confirm_modal').modal('show');
	    });
	    $("#SPO2View .chartbox svg").on('click','g.tagMarker rect',function(event) {
	    	event.stopPropagation();
	    	$('#confirm_modal .confirm').data('container-id',"#SPO2View");
	    	$('#confirm_modal .confirm').data('pos',$(this).attr('pos'));
	    	$('#confirm_modal').modal('show');
	    });
	    $("#SPO2View .chartbox svg").on('mouseup, mousedown','g.tagMarker rect',function(event) {
	    	event.stopPropagation();
	    	
	    });

		$('.action').on('click','.yZoomIn',function(event){

			var view = $(this).parent().parent();
			chartZoom(view, 1);
		});
		$('.action').on('click','.yZoomOut',function(event){

			var view = $(this).parent().parent();
			chartZoom(view, -1);
		});
		$(".chartbox").on('mousewheel',function(event) {
            this.scrollLeft -= event.originalEvent.wheelDelta;
            event.preventDefault();
        });
		$('.chartbox').scroll(function(event){
			var preventScrollEvent = $(this).data('prevent-scroll-event');
			if(preventScrollEvent){
				$(this).data('prevent-scroll-event',false);
				return false;
			}
			var current_left_scroll = $(this).scrollLeft();
			var lastLeftScroll = $(this).data('last-scroll-left');

			if(lastLeftScroll!=current_left_scroll){
				$(this).data('last-scroll-left',current_left_scroll);
				var range_select = $('#range_btns input[type=radio]:checked').val();
				if(range_select!=0){
					var dataLen = sleepData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) * 30);
					var range = range_select/30 * svgWidth/( dataLen);
					var x = ( current_left_scroll * range_select / (30 * svgWidth)) * svgWidth / dataLen;
					/*
					var range = range_select/30 * svgWidth/(sleepData[0].length);
					var x = ( current_left_scroll * range_select / (30 * svgWidth)) * svgWidth /sleepData[0].length;
					*/
					$(this).data('dont-set-scroll-left',true);

					updateSlideWindow('#StageView', range, {x:x},function(){
						updateAllView(current_left_scroll, true);
					});

					$(this).data('dont-set-scroll-left', false);
				}

			}
			var current_top_scroll = $(this).scrollTop();
			var lastTopScroll = $(this).data('last-scroll-top');
			if(lastTopScroll!=current_top_scroll){
				$(this).data('last-scroll-top',current_top_scroll);
				var chart = $(this).parent();
				var container_id = chart.attr('id');
				updateAxisYPosition("#"+container_id, current_top_scroll);
				updatePeakPositionY("#"+container_id, current_top_scroll);
				updateTagPositionY("#"+container_id, current_top_scroll);
				if(container_id == 'StageView'){
					var range_select = $('#range_btns input[type=radio]:checked').val();
					var position = $(this).find('svg').find('.slidewindow').offset();
					var svgOffset = $(this).offset();
					var dataLen = sleepData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) * 30);

					var range = range_select/30 * svgWidth/(dataLen);
					var x = (position.left - svgOffset.left);
					updateSlideWindow('#StageView', range, {x:x},function(){});
				}
			}
		});
		$(window).resize(function(event){
			var currentWindowWidth = $(window).width();
			if(currentWindowWidth != windowWidth){
				resize();
				windowWidth = currentWindowWidth;
			}
		});
		$(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e){
			if (screen.width == window.innerWidth && screen.height == window.innerHeight) {
				$('.navbar-static-top').addClass('topbar-relative');
			} else {
				$('.navbar-static-top').removeClass('topbar-relative');
				
			}
		});
	});

	var sleepCallback = function(index, callback){
		return function(data, textStatus, jqXHR){
			if(data.errorcode==null){
				sleepData[index] = JSON.parse(data.data);
				var meanY = d3.mean(sleepData[index], function (d) { return d; });
				var minY = d3.min(sleepData[index], function (d) { return d; });
				var maxY = d3.max(sleepData[index], function (d) { return d; });
				var peakPositionData = [];
				if(index == 0){
					sleepData[index] = sleepData[index].map(function(d,i){
											if(d==3) return 2;
											if(d==5) return -1;
											return d;
										});
				}
				if(index == 1){
					var hasPeak = false;
					var peakData = {};
					for (var i = 0; i < sleepData[index].length; i++) {
						if(sleepData[index][i] == 1){
							if(!hasPeak){
								peakData = {time:i, data:1, start:i, sleepIndex:index};
								hasPeak = true;
							} 
						}else{
							if(hasPeak){
								peakData.end = i-1;
								peakData.time =  (peakData.start + peakData.end )/2;
								peakPositionData.push(peakData);
								peakData = {};
								hasPeak = false;
							}
						}
					}
				}

				sleepExData[index] = {
					meanY : meanY,
					minY : minY,
					maxY : maxY,
					peak : peakPositionData,
					tag : [],
				};
				dataNum--;
				if(dataNum==0){
					callback();
				}
			}
		};
	}

	function loadSleepData(callback){
		$('#loading').show();
		$('.chart').each(function(index,d){
			$.get(url('analysis/sleep/'+ecgID+'/result/'+index), sleepCallback(index, function(){
				$('#loading').hide();
				
				loadTagData(function(tagsData){
					tagPositionData[2] = tagsData;
					var tagPosData = [];
					var hasTag = false;
					var tagData = {};
					for (var i = 0; i < tagsData.length; i++) {
						if(tagsData[i][0] == 1){
							if(!hasTag){
								var text = tagsData[i][1] ? tagsData[i][1]:'';
								tagData = {time:i, data:1, text:text , start:i, sleepIndex:2};
								hasTag = true;
							} 
						}else{
							if(hasTag){
								tagData.end = i;
								tagData.time =  (tagData.start + tagData.end )/2;
								tagPosData.push(tagData);
								tagData = {};
								hasTag = false;
							}
						}
					}
					sleepExData[2].tag = tagPosData;
					callback();
				});
			}));
		});
	}
	function loadTagData(callback){
		$.get(url('analysis/sleep/'+ecgID+'/tag'), function(response){
				callback(JSON.parse(response.data));
			});
	}
	function drawChart(sleepDataIndex, positionX, notFromStage){
		//var filterCondition = parseInt($('#data_filter_condition').val(),10);
		//var filterValue =  parseInt($('#data_filter_value').val(),10);

		var viewData = getViewByIndex(sleepDataIndex);
		var chartData = sleepData[sleepDataIndex];
		var range_select = sleepDataIndex != 0 ? $('#range_btns input[type=radio]:checked').val() : 0;
		var start = sleepDataIndex != 0 ? getStart(positionX, viewData.sampleRate, notFromStage) : 0;
		var end_temp = start+parseInt(range_select * viewData.sampleRate , 10);
		var end = range_select == 0 ? chartData.length -1 : (end_temp >= chartData.length ? chartData.length -1 : end_temp - 1 );
		var range = {
			start: start, 
			end: end
		};
		positionX = range_select == 0 ? 0 : positionX;
		var exData = {
			recorded_at : ecgRecordedAt,
			notFromStage : notFromStage,
			positionX : positionX,
			maxDataLen : sleepData[maxDataLenIndex].length,
			maxDataLenSampleRate : parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))),
			maxDataSecond : sleepData[maxDataLenIndex].length / parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))),
			dataLen: sleepData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) / viewData.sampleRate),
			meanY : sleepExData[sleepDataIndex].meanY,
			minY : sleepExData[sleepDataIndex].minY,
			maxY : sleepExData[sleepDataIndex].maxY,
			peak : sleepExData[sleepDataIndex].peak,
			tag : sleepExData[sleepDataIndex].tag,
			//filterCondition : filterCondition,
			//filterValue : filterValue
		};
		draw_chart(viewData.viewId, chartData, range, exData);
	}
	function getStart(positionX, sampleRate, notFromStage){
		if(notFromStage){
			var range_select = $('#range_btns input[type=radio]:checked').val();
			range_select = range_select == 0 ? 1 : range_select;
			var dataIndex = Math.ceil( ((positionX  * (range_select-1)) * sampleRate) / svgWidth);
			return dataIndex;
		}
		return Math.floor( positionX * sampleRate * (sleepData[maxDataLenIndex].length / parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate')))) / svgWidth);
	}
	function getDataIndex(sleepIndex, positionX, sampleRate, notFromStage){
		var range_select = $('#range_btns input[type=radio]:checked').val();
		range_select = range_select == 0 ? sleepData[sleepIndex].length : parseInt(range_select,10);
		var dataIndex = Math.floor((range_select-1) * sampleRate * (positionX / svgWidth) );
		return dataIndex;
	}
	function getViewByIndex(sleepDataIndex){
		var viewData= {
			viewId : '#'+$('.chart').eq(sleepDataIndex).attr('id'),
			sampleRate : parseFloat(eval($('.chart').eq(sleepDataIndex).data('sample-rate')))
		};
		return viewData;
	}
	function updateAllView(positionX, notFromStage){
		for (var i = 0; i < $('.chart').length; i++) {
			var doUpdate = $('.chart').eq(i).data('no-update');
			if(!doUpdate){
				drawChart(i, positionX, notFromStage);
			}
		}
	}
	function updateChart(){
		var range_select = $('#range_btns input[type=radio]:checked').val();
		var x = range_select == 0 ? 0 : $('#StageView .slidewindow').data('x');
		var dataLen = sleepData[maxDataLenIndex].length / (parseFloat(eval($('.chart').eq(maxDataLenIndex).data('sample-rate'))) * 30);
		var range = range_select == 0 ? 0 : (range_select/30 * svgWidth/dataLen);
		x = ((x + range) > svgWidth ) ? (svgWidth - range) : x;
		updateSlideWindow('#StageView', range, {x:x},function(x){
			updateAllView(x, false);
		});
	}
	function findMaxHzIndex(){
		var max = 0;
		var index = 0;
		for (var i = 0; i < sleepData.length; i++) {
			var len = sleepData[i].length;
			if(len>max){
				max = len;
				index = i;
			}
		}
		return index;
	}
	function resize(){
		svgWidth = parseFloat($('#StageView').find('.chartbox').width()) - scrollbarWidth;
		var range_select = $('#range_btns input[type=radio]:checked').val();
		var range = range_select/30 * svgWidth/(sleepData[0].length);
		var svgOffset = $('#StageView').find('.chartbox').offset();
		var slidewindowOffset = $('#StageView').find('.slidewindow').offset();
		var x = slidewindowOffset.left - svgOffset.left;
		$('#StageView').data('no-update',false);
		updateAllView(x, false);
		$('#StageView').data('no-update',true);
		updateSlideWindow('#StageView', range, {x:x},function(){});
	}
	function chartZoom(view, value){
		var chartbox = view.find('.chartbox');
		chartbox.data('last-scroll-top',0);
		var yScaleRatio = parseFloat(view.attr('data-y-scale-ratio')) + value;
		if(yScaleRatio<1){
			yScaleRatio=1;
		}
		view.attr('data-y-scale-ratio',yScaleRatio);
		var position = $('#StageView').find('svg').find('.slidewindow').offset();
		var svgOffset = $('#StageView').find('.chartbox').offset();
		var x = view.data('sleep-index')!='0'? (position.left - svgOffset.left) : 0;
		drawChart(view.data('sleep-index'), x, false);
		
		if(view.data('sleep-index')=='0'){
			var range= $('#StageView').find('.chartbox').find('svg').find('.slidewindow').width();
			updateSlideWindow('#StageView', range, { x: (position.left - svgOffset.left) },function(){})
		};
	}
	function toggleFullScreen() {
		if (!document.fullscreenElement &&
			!document.mozFullScreenElement && 
			!document.webkitFullscreenElement && 
			!document.msFullscreenElement
			){
			if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} else {
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			}
		}
	}
</script>
@endsection