@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/ecganalysis.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<link href="{{url('/css/batchpeak.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.sidebar')
		<article id="main">
			<h4 class="title">@lang("physiolguard.analysis_title")＞<a href="{{url('analysis/ecg').'/'.$ecg_id}}">{{$filename}}</a>＞@lang("physiolguard.batchpeak")</h4>
			<div id="toolbar" data-spy="affix" data-offset-top="51">
				<ul class="btn-toolbar">
					<li>
						<div class="select_box peak_select_box">
							<div class="col-md-4">
								@lang("physiolguard.peak"):
							</div>
							<div class="col-md-5">
								<select id="peak_select" class="form-control">
									<option value="N">N</option>
									<option value="Q">Q</option>
									<option value="V">V</option>
									<option value="S">S</option>
								</select>
							</div>
							<div class="col-md-3">
								<button id="search_btn" class="btn btn-default">
									@lang("physiolguard.search")
								</button>
							</div>
						</div>
					</li>
					<li>
						<div class="checked-box">
							<label class="control-label"><input id="select_all" type="checkbox" value="all">Select all</label>
						</div>
					</li>
					<li>
						<div class="select_box action_select_box">
							<div class="col-md-4">
								@lang("physiolguard.action"):
							</div>
							<div class="col-md-5">
								<select id="action_select" class="form-control">
									<option value="D">@lang("physiolguard.delete")</option>
									<option value="N">N</option>
									<option value="Q">Q</option>
									<option value="V">V</option>
									<option value="S">S</option>
								</select>
							</div>
							<div class="col-md-3">
								<button id="run_btn" class="btn btn-default">
									@lang("physiolguard.excute")
								</button>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<hr/>
			<div id="ecg_view_loading" class="ajax_loading">
				<img src="{{url('/assets/img/loading.gif')}}">
			</div>
			<div id="selectEcg">
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')

<script src="{{url('/thirdpart/d3/d3.min.js')}}"></script>
<script>
	var ecgID = {{$ecg_id}};
	var recorded_at={{$recorded_at}};
	var ecgData;
	var rrData;
	var sampleRate={{ ($filename_ext=='EKG' || $filename_ext=='ekg')? 250:256 }};
	var peakData=[];
	$("#sidebar").addClass("sidebarhide").append('<div id="sidebarmenu"><span class="glyphicon glyphicon-menu-hamburger"></span></div>');
	loadECG();

	$('#search_btn').click(function(event){
		var peak = $('#peak_select').val();
		$('#selectEcg').empty();
		peakData=[];
		if(ecgData){
			loadRR(peak);
		}else{
			bootbox.alert({
    				message: '@lang("physiolguard.data_loading")',
    				locale: lang
    			});
		}
		$('#select_all').prop('checked', false).change();
	});

	$('#select_all').change(function(event){
		var peaks = $('.peak_time')
		for (var i = 0; i < peaks.length; i++) {
			if($(this).prop('checked')){
				peaks.eq(i).prev().show();
			}else{
				peaks.eq(i).prev().hide()
			}
		}
		peaks.prop('checked', $(this).prop('checked'))
	});

	$('#run_btn').click(function(event){
		var peak = $('#action_select').val();
		var selectPeaks=$('#selectEcg input:checked').map(function(){return $(this).val();}).toArray();
		if(selectPeaks.length>0){
			$.post(url('/analysis/ecg/batchpeak/setpeaks/{{$ecg_id}}'),{_token:window.Laravel.csrfToken,peakstime:selectPeaks,peak:peak}, function (result){
				if(result['error']){
					
				}else{
					if(peak!='D'){
						$('#selectEcg input:checked').parent().parent().parent().find('text').html(peak);
					}else{
						$('#selectEcg input:checked').parent().parent().parent().find('text').html('');
					}
					$('#selectEcg input[type=checkbox]').prop('checked',false);
					$('#selectEcg .check_box_border span').hide();
				}
			});
		}
	});

	$('#selectEcg').on('change','input[type=checkbox]',function(event){
		if($(this).prop('checked')){
			$(this).prev().show();
		}else{
			$(this).prev().hide();
		}

		if($('#selectEcg input:checked').length != $('#selectEcg input').length){
			$('#select_all').prop('checked', false);
		}

	});
	function loadRR(peak) {
		$.get(url('/api/v0/analysis/rr/{{$ecg_id}}'), function (result){
			rrData = result.rrdata;
			for(var i = 0; i < rrData.length; i++){
				if(rrData[i].peak==peak){
					peakData.push(rrData[i]);
					var peakDateTime=new Date((recorded_at+(rrData[i].time/(sampleRate/2)))*1000);
					var peakTime=peakDateTime.getHours()+':'+peakDateTime.getMinutes()+':'+peakDateTime.getSeconds()+'.'+peakDateTime.getMilliseconds();
					$('#selectEcg').append('<div class="select_rr_ecg">'+
						'<label>'+
							'<div class="check_box">'+
								'<div class="check_box_border">'+
									'<span class="glyphicon glyphicon-ok"></span>'+
									'<input type="checkbox" class="peak_time" value="'+rrData[i].time+'">'+
								'</div>'+
							'</div>'+
							'<div class="chart_box">'+
								'<div class="peak_row"></div>'+
								'<div class="ecg_chart"></div>'+
								'<div class="time_row">'+peakTime+'</div>'+
							'</div>'+
						'</label>'+
					'</div>');
				}
			}
			if(peakData.length==0){
				$('#selectEcg').append('<h4 style="text-align:center"> no peaks</h4>');
			}
			draw_chart();
		});
	}
	function loadECG(){
		$("#ecg_view_loading").show();
		$.get(url('/api/v0/analysis/ecg/{{$ecg_id}}'),function(result){
			ecgData=result.ecgdata;
			$("#ecg_view_loading").hide();
		});
	}

	function draw_chart(){
		$('#selectEcg').find('.ecg_chart').each(function(i,d){
			var time = peakData[i].time;
			var start_pos=(time<sampleRate/2)?0:time-sampleRate/2;
			var end_pos=(time>ecgData.L2.length-((sampleRate/2)+1))?ecgData.L2.length:time+((sampleRate/2)+1);
			var current_pos=(time-start_pos<0)?0:time-start_pos;
			var svg = d3.select(d)
						.append('svg')
							.attr("width", '95%')
							.attr("height", '300')
						.append('g');

			var sliceData = ecgData.L1.slice(start_pos,end_pos);
			var x = d3.scaleLinear().range([0, 400]).domain([0, sampleRate]);
			var y = d3.scaleLinear().range([100, 0]).domain(d3.extent(sliceData, function (d) { return d; }));
			var line = d3.line().x(function (d, i) { return x(i); }).y(function (d) { return y(d); });

			svg.append('text').attr("transform", "translate(0 15)")
				.attr('x', x(current_pos)-5)
				.text(peakData[i].peak);
;
			svg.append("g").attr("transform", "translate(0 0)")
			.append("path")
			.datum(sliceData)
			.attr("class", "line1")
			.attr("d", line);

			sliceData = ecgData.L2.slice(start_pos,end_pos);
			x = d3.scaleLinear().range([0, 400]).domain([0, sampleRate]);
			y = d3.scaleLinear().range([100, 0]).domain(d3.extent(sliceData, function (d) { return d; }));
			line = d3.line().x(function (d, i) { return x(i); }).y(function (d) { return y(d); });

			svg.append("g").attr("transform", "translate(0 101)")
			.append("path")
			.datum(sliceData)
			.attr("class", "line2")
			.attr("d", line)

			sliceData = ecgData.L3.slice(start_pos,end_pos);
			x = d3.scaleLinear().range([0, 400]).domain([0, sampleRate]);
			y = d3.scaleLinear().range([100, 0]).domain(d3.extent(sliceData, function (d) { return d; }));
			line = d3.line().x(function (d, i) { return x(i); }).y(function (d) { return y(d); });

			svg.append("g").attr("transform", "translate(0 201)")
			.append("path")
			.datum(sliceData)
			.attr("class", "line3")
			.attr("d", line);
		});
	} 

</script>
@endsection