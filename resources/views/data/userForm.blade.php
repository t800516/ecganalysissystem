@extends('layouts.app')
@section('css_file')
    <link rel="stylesheet" href="{{asset('thirdpart/multi-select/css/multi-select.css')}}">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
    <link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/datamanager.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.data_sidebar')
		<article id="main">
			@include('layouts.model_select')
			<div class="container-fluid">
				<form id="register_form" action="{{ 
					isset($poc) && $poc ?
					url('/data/pocs/'.$poc->id.'/users/create') :
				 	( isset($id) ? 
					 	url('/data/'.(isset($type) && $type=='MGR' ? 'pocs' : 'users').'/manager/edit'): 
					 	url('/data/'.(isset($type) && $type=='MGR' ? 'pocs' : 'users').'/manager/create')
				 	)}}" method="post" class="form-horizontal">
					<fieldset class="row">
						<div>
						    <legend class="">
						    	@if(isset($poc) && $poc)
						    		<a href="{{url('data/pocs/'.$poc->id.'/users')}}">{{$poc->name}}</a> / 
						    	@endif
						    	@if( isset($type) && $type=='MGR')
						    		{{isset($id) ? trans("physiolguard.edit"):trans("physiolguard.new")}} 
									@lang("physiolguard.poc")
								@else
									{{isset($id) ? trans("physiolguard.edit"):trans("physiolguard.new")}} 
									@lang("physiolguard.patient")
								@endif
						    </legend>
				    	</div>
				    	<ul class="errormsg list-group">
		 					@foreach($errors->all() as $key=>$error)
								<li class="list-group-item list-group-item-danger">{{$error}}</li>
							@endforeach
						</ul>
						<div class="form-group row">
							<label for="email" class="control-label col-sm-2">E-mail</label>
							<div class="controls col-sm-8">
								<input type="email" name="email" id="email" class="form-control" value="{{ $email or '' }}" required {{!(isset($type) && $type=='MGR') && isset($id) ? 'readonly':''}}>
							</div>
							<div class="col-sm-2 email_msg"> </div>
						</div>
						<div class="form-group row">
							<label for="name" class="control-label col-sm-2">@lang("physiolguard.user_name")</label>
							<div class="controls col-sm-8">
								<input type="text" name="name" id="name" class="form-control" value="{{ $name or '' }}" required>
							</div>
							<div class="col-sm-2 name_msg"> </div>
						</div>
						@if(!isset($id))
							<div class="form-group row">
								<label for="password" class="control-label col-sm-2">@lang("physiolguard.user_password")</label>
								<div class="controls col-sm-8">
									<input type="password" name="password" id="password" value="{{ $password or '' }}" class="form-control" required>
								</div>
								<div class="col-sm-2 password_msg"> </div>
							</div>
							<div class="form-group row">
								<label for="password_confirmation" class="control-label col-sm-2">@lang("physiolguard.user_password_confirmation")</label>
								<div class="controls col-sm-8">
									<input type="password" name="password_confirmation" value="{{ $password or '' }}" id="password_confirmation" class="form-control" required>
								</div>
								<div class="col-sm-2 password_confirmation_msg"></div>
							</div>
						@endif
					</fieldset>
					<fieldset class="row">
						<div class="form-group row">
							<label for="checked" class="control-label col-sm-2">@lang("physiolguard.user_status")</label>
							<div class="controls col-sm-8">
								<select id="checked" name="checked" class="form-control">
									<option value="0" {{( isset($checked) && $checked==0 )? 'selected' : ''}}>@lang("physiolguard.disable")</option>
									<option value="1" {{( isset($checked) && $checked==1 )? 'selected' : ''}}>@lang("physiolguard.enable")</option>
								</select>
							</div>
						</div>
					</fieldset>
					@if(!isset($type) || $type!='MGR')
						@can('otc_user_info')
						<hr/>
							<!--div class="form-group row">
								<label for="patient_name" class="control-label col-sm-2">@lang("physiolguard.patient_name")</label>
								<div class="controls col-sm-8">
									<input type="text" name="patient_name" id="patient_name" class="form-control" value="{{ $patient_name or '' }}">
								</div>
								<div class="col-sm-2"> </div>
							</div-->
							<div class="form-group row">
								<label for="patient_IDNumber" class="control-label col-sm-2">@lang("physiolguard.patient_IDNumber")</label>
								<div class="controls col-sm-8">
									<input type="text" name="patient_IDNumber" id="patient_IDNumber" class="form-control" value="{{ $patient_IDNumber or '' }}" {{ isset($type) && $type=='MGR' ? '':'required'}}>
								</div>
								<div class="col-sm-2"> </div>
							</div>
							<div class="form-group row">
								<label for="sex" class="control-label col-sm-2">@lang("physiolguard.sex")</label>
								<div class="controls col-sm-8">
									<select name="sex" id="sex" class="form-control">
										<option value="" hidden>@lang("physiolguard.do_select")</option>
										<option value="M" {{isset($sex) && $sex == 'M'? 'selected':''}}>@lang("physiolguard.gender_M")</option>
										<option value="F" {{isset($sex) && $sex == 'F'? 'selected':''}}>@lang("physiolguard.gender_F")</option>
									</select>
								</div>
								<div class="col-sm-2"> </div>
							</div>
							<div class="form-group row">
								<label for="birthday" class="control-label col-sm-2">@lang("physiolguard.birthday")</label>
								<div class="controls col-sm-8">
									<input type="text" name="birthday" id="birthday" class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-today-highlight="true" data-date-language="zh-TW" data-date-end-date="0d" placeholder="YYYY-MM-DD" value="{{isset($birthday) &&  $birthday && $birthday !='0000-00-00' ? $birthday:''}}"  {{ isset($type) && $type=='MGR' ? '':'required'}}>
								</div>
								<div class="col-sm-2"> </div>
							</div>
						@endcan
					@endif

					{!! isset($id) ? '<input type="hidden" name="id" value="'.$id.'">' : '' !!}
					
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<div class="col-sm-4 col-sm-offset-4">
						<input type="submit" class="btn btn-primary form-control" value="{{isset($id) ? trans('physiolguard.save'):trans('physiolguard.create') }}">
					</div>
				</form>
			</div>
		</article>
	</div>
</div>
@endsection
@section('javascript')

<script src="{{asset('thirdpart/jquery.quicksearch/jquery.quicksearch.js')}}"></script>
<script src="{{asset('thirdpart/multi-select/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
	$(function(){
		$('#account').focus();
		$('#password_confirmation').blur(function(){
			if($(this).val()!=$('#password').val()){
				$('.password_confirmation_msg').html('password confirmation not match.');
			}else{
				$('.password_confirmation_msg').html('');
			}
		});

		$("#register_form").submit(function(){
			@if(!isset($id))
				if($('#password_confirmation').val()!=$('#password').val()){
					$('.password_confirmation_msg').html('password confirmation not match.');
					return false;
				}
			@endif
		});
	});
</script>
@endsection