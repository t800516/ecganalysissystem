<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-single-select="false" data-checkbox-header="true" data-unique-id="id" data-show-header="true" data-pagination="true" data-page-size="50" data-page-list="[25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams" data-group-by="true" data-group-by-field="upload_date" data-group-order-reverse="true" {!!/*data-group-order-reverse="true" data-sort-name="created_at" data-sort-order="desc"*/'';!!} data-search-time-out="1000">
					<thead>
						@php
							$fix_width = 29 ;
							$fix_width += (Auth::user()->can('ecg_table_conversion')) ? 0 : 10;
							$fix_width += (Auth::user()->can('ecg_table_priority')) ? 	0 : 5;
							$fix_width -= (Auth::user()->can('ecg_table_checked')) ? 5 : 0;
							$fix_width -= (Auth::user()->can('ecg_table_confirm')) ? 5 : 0;
							$fix_width -= (Auth::user()->role_id == 5) ? 5 : 0;
							$fix_width -= (Auth::user()->role_id == 13) ? 10 : 0;
							$fix_width = $fix_width < 0 ? 5 : $fix_width;
						@endphp
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true" data-width="2%"></th>
						<th data-field="filename" data-sortable="true" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="{{$fix_width}}%">@lang('physiolguard.filename')</th>
						<th data-field="patient_IDNumber" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang('physiolguard.patient_IDNumber')</th>
						@if(Auth::user()->role_id == 13)
							<th data-field="patient_name" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang('physiolguard.patient_name')</th>
						@endif
						<th data-field="report_status" data-sortable="true" data-halign="center" data-align="center" data-formatter="report_status_formatter" data-width="10%">@lang('physiolguard.report_status')</th>
						@if(Auth::user()->role_id == 13)
							<th data-field="comment" data-sortable="true" data-halign="center" data-align="center" data-width="15%">@lang('physiolguard.comment')</th>
						@else
							<th data-field="report_type" data-sortable="true" data-halign="center" data-align="center" data-width="15%" data-formatter="report_type_formatter">@lang('physiolguard.report_type')</th>
						@endif
						<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center" data-width="6%">@lang('physiolguard.upload_date')</th>
						@can('ecg_table_conversion')
							<th data-field="analysis_status" data-sortable="true" data-halign="center" data-align="center" data-formatter="analysis_status_formatter" data-width="10%">@lang('physiolguard.analysis_status')</th>
						@endcan
						@can('ecg_table_priority')
							<th data-field="priority" data-halign="center" data-align="center" data-formatter="priority_formatter" data-width="5%">@lang('physiolguard.priority')</th>
						@endcan
						@can('ecg_table_checked')
							<th data-field="checked" data-halign="center" data-align="center" data-formatter="checked_formatter" data-width="5%">@lang('physiolguard.checked')</th>
						@endcan
						@can('ecg_table_confirm')
							<th data-field="confirm" data-halign="center" data-align="center" data-formatter="confirm_formatter" data-width="5%">@lang('physiolguard.confirm')</th>
						@endcan
						@if(Auth::user()->role_id == 5)
							<th data-field="sqi" data-sortable="true" data-halign="center" data-align="center" data-width="5%">@lang('physiolguard.sqi')</th>
						@endif
						<th data-field="id" data-halign="center" data-align="center" data-formatter="action_formatter" data-width="10%">@lang('physiolguard.action')</th>
					</thead>
					<tbody>

					</tbody>
				</table>