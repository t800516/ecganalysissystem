@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/datamanager.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.data_sidebar')
		<article id="main">
			@include('layouts.model_select')
			<div id="tool_bar">
				<h4 class="pull-left title">
					@if( isset($type) && $type=='MGR')
						@lang("physiolguard.user_data_title")
					@else
						@if( isset($poc) && $poc)
							<a href="{{url('/data/pocs/manager')}}">{{$poc->name}}</a> /
						@endif
						@lang("physiolguard.patient_manager_title")
					@endif
				</h4>
				<div class="tools">
					<ul class="pull-right">
						@can('poc_edit')
							<li class="">
								@if( isset($type) && $type=='MGR')

									@cannot('otc_manager')
										<a href="{{isset($poc) && $poc ? 
											url('/data/pocs/'.$poc->id.'/users/create'):
											url('/data/pocs/manager/create')}}" class="btn btn-default" id="new_user">
											<span class="glyphicon glyphicon-plus"></span>
											@lang("physiolguard.new_poc_label")
										</a>
									@else
										<a href="{{url('/data/users/manager/create')}}" class="btn btn-default" id="new_user">
											<span class="glyphicon glyphicon-plus"></span>
											@lang("physiolguard.new_poc_label")
										</a>
									@endcannot
								@else
									<a href="{{isset($poc) && $poc ? 
										url('/data/pocs/'.$poc->id.'/users/create'):
										url('/data/users/manager/create')}}" class="btn btn-default" id="new_user">
										<span class="glyphicon glyphicon-plus"></span>
										@lang("physiolguard.new_user_label")
									</a>
								@endif
							</li>
							@if( isset($type) && $type=='MGR')
								@cannot('otc_manager')
									<li class="selectedfunc">
										<a href="#" class="btn btn-default" id="func_remove">
											<span class="glyphicon glyphicon-trash"></span>
											@lang("physiolguard.delete")
										</a>
									</li>
								@endcannot
							@endif
						@endcan
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<div id="member_manager">
				<table data-toggle="table" id="member_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-search="true" data-click-to-select="true" data-single-select="false" data-checkbox-header="true" data-pagination="true" data-page-size="10" data-page-list="[10,25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true"></th>
						<th data-field="email" data-sortable="true" data-halign="center" data-align="center">E-mail</th>
						<th data-field="name" data-sortable="true" data-halign="center" data-align="center" data-width="15%">
								@lang("physiolguard.user_name")</th>
						@if(Auth::user()->role_id != 13 )
							<th data-field="department" data-sortable="true" data-halign="center" data-align="center" data-formatter="department_formatter">@lang("physiolguard.department")</th>
						@endif
						<th data-field="non_checked_count" data-sortable="true" data-halign="center" data-align="center">@lang("physiolguard.ecg_non_checked_count")</th>
						<th data-field="id" data-halign="center" data-align="center" data-formatter="function_formatter">@lang("physiolguard.action")</th>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</article>
	</div>
</div>
@endsection

@section('javascript')
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script>
$(function(){
	@can('poc_edit')
	$("#func_remove").click(function(event){
		var selections=$('#member_manager_table').bootstrapTable('getSelections');
		if(selections.length>0){
			bootbox.confirm({
	    		message: '@lang("physiolguard.delete_accounts")',
	    		locale: lang,
	    		size: 'small',
				callback: function (result) {
					if(result){
					    var ids=selections.map(function(d){return d.id});
						$.post(url('/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users/del' : (isset($type) && $type=='MGR' ? 'pocs/manager/del' : 'users/manager/del')}}'),{_token:window.Laravel.csrfToken,ids:ids},function(data){
							$('#member_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
						});
					}
				}
	    	});
		}
	});
	$('#member_manager tbody').on('click','.remove',function(event){
		var ids  = [$(this).data('id')];
		bootbox.confirm({
    		message: '@lang("physiolguard.delete_account")',
    		locale: lang,
    		size: 'small',
			callback: function (result) {
				if(result){
					$.post(url('/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users/del' : (isset($type) && $type=='MGR' ? 'pocs/manager/del' : 'users/manager/del')}}'),{_token:window.Laravel.csrfToken, ids:ids},function(data){
							$('#member_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
						});
				}
			}
    	});
		return false;
	});
	@endcan
});
function department_formatter(value, row, index){
	return value ? value.name:'';
}

function function_formatter(value, row, index){
	var btns = '';
	@if(isset($type) && $type=='MGR')
		btns +='<a target="_blank" href="'+url("/data/users/"+value+"/ecgs")+'" class="btn btn-default" >{{trans("physiolguard.check")}}</a>';
		@cannot('otc_manager')
			@can('poc_edit')
				btns +='<a target="_blank"  href="'+url("/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users' : 'pocs'}}/"+value+"/users")+'" class="btn btn-default" >{{trans("physiolguard.users")}}</a>';
			@else
				if(row.users_count){
					btns +='<a target="_blank"  href="'+url("/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users' : 'pocs'}}/"+value+"/users")+'" class="btn btn-default" >{{trans("physiolguard.users")}}</a>';
				}else{
					btns +='<button class="btn btn-default" disabled>{{trans("physiolguard.users")}}</button>';
				}
			@endcan
		@endcannot
	@else
		btns +='<a target="_blank" href="'+url("/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users' : 'users'}}/"+value+"/ecgs")+'" class="btn btn-default" >{{trans("physiolguard.check")}}</a>';
	@endif

	@can('service_read')
		if(row.role_id == 7 ){
			btns += '<a href="'+url("/data/{{isset($poc)  && $poc ? 'pocs/'.$poc->id.'/users/' : (isset($type) && $type=='MGR' ? 'pocs/manager/' : 'users/manager/')}}"+value+"/services")+'" class="btn btn-default" >{{trans("physiolguard.service")}}</a>';
		}else{
			btns += '<button class="btn btn-default" disabled>{{trans("physiolguard.service")}}</button>';
		}
	@endcan

	@can('poc_edit')
		@cannot('otc_manager')
			btns += '<a href="'+url("/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users/' : 'pocs/manager/'}}edit/"+value)+'" class="btn btn-default" >{{trans("physiolguard.edit")}}</a>';
		@else
			btns += '<a href="'+url("/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users/' : 'users/manager/'}}edit/"+value)+'" class="btn btn-default" >{{trans("physiolguard.edit")}}</a>';
		@endcannot

		@cannot('otc_manager')
			btns += '<a href="'+url("/data/{{isset($poc) && $poc ? 'pocs/'.$poc->id.'/users/' : (isset($type) && $type=='MGR' ? 'pocs/manager/' : 'users/manager/')}}del/"+value)+'"class="btn btn-danger remove" data-id="'+row.id+'" >{{trans("physiolguard.delete")}}</a>';
		@endcannot
	@endcan
	return btns;
}

function ajaxRequest(params) {
    $.get(url("{{ !isset($user_manager_type) || $user_manager_type ? '/data/'.(isset($poc) && $poc ? 'pocs/'.$poc->id.'/users/list' : (isset($type) && $type=='MGR' ? 'pocs/manager/list' : 'users/manager/list')) : '/data/users/data'}}") + '?' + $.param(params.data)).then(function (res) {
      params.success(res);
    })
}

function queryParams(params){
	return params;
}
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}
</script>
@endsection