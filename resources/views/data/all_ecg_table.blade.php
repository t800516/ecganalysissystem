<table data-toggle="table" id="file_manager_table" class="table table-condensed" data-toolbar="#tool_bar" data-unique-id="id" data-show-header="true" data-search="true" data-click-to-select="true" data-pagination="true" data-page-size="50" data-page-list="[25,50,100]" data-side-pagination="server" data-ajax="ajaxRequest" data-query-params="queryParams" data-custom-search="customSearch">
					<thead>
						<th data-field="selected" data-halign="center" data-align="center" data-checkbox="true" data-width="2%"></th>
						<th data-field="filename" data-sortable="true" data-halign="center" data-align="left" data-formatter="filename_formatter" data-width="25%">@lang("physiolguard.filename")</th>
						<th data-field="patient_IDNumber" data-sortable="true" data-halign="center" data-align="center" data-width="15%">@lang("physiolguard.patient_IDNumber")</th>
						<th data-field="user" data-sortable="true" data-halign="center" data-align="center" data-width="15%" data-formatter="user_formatter">@lang("physiolguard.account")</th>
						<th data-field="report_type" data-sortable="true" data-halign="center" data-align="center" data-width="10%">@lang("physiolguard.report_type")</th>
						<th data-field="created_at" data-sortable="true" data-halign="center" data-align="center" data-width="11%">@lang("physiolguard.upload_date")</th>
						<th data-field="checked" data-sortable="true" data-halign="center" data-align="center" data-width="5%"  data-formatter="checked_formatter">@lang("physiolguard.checked")</th>
						<th data-field="priority" data-sortable="true" data-halign="center" data-align="center" data-width="5%">@lang("physiolguard.priority")</th>
						<th data-field="analysis_status" data-sortable="true" data-halign="center" data-align="center" data-formatter="analysis_status_formatter" data-width="10%">@lang('physiolguard.analysis_status')</th>
					</thead>
					<tbody>

					</tbody>
				</table>