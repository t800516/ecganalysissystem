@extends('layouts.app')
@section('css_file')
	<link href="{{url('/thirdpart/bootstraptable/bootstrap-table.min.css')}}" rel="stylesheet">
	<link href="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap4-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
	<link rel="stylesheet" href="{{asset('thirdpart/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
	<link href="{{url('/css/guide.css')}}" rel="stylesheet">
	<link href="{{url('/css/sidebar.css')}}" rel="stylesheet">
	<link href="{{url('/css/filemanager.css')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}" rel="stylesheet">
	<link href="{{url('/css/report_message.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
	<div class="content">
		@include('layouts.data_sidebar')
		<article id="main">
			@include('layouts.model_select')
			<div id="tool_bar">
				<h4 class="pull-left title">@lang("physiolguard.user_data_title") / 
					<a href="{{isset($poc) ? url('/data/pocs/'.$poc->id.'/users/') : url('/data/users')}}">
					{{$user->name}}
					</a> / @lang("physiolguard.ecg_list")</h4>
				<div class="tools">
					<ul class="pull-right gap flex" data-spy="affix" data-offset-top="100">
						@can('ecg_table_type_filter')
						<li>
							<div class="filter_options">
								<label class="control-label filter_option"><input id="filter_checked" type="checkbox" value="unconfirmed"> Unconfirmed Record</label>
								<label class="control-label filter_option"><input id="filter_checked" type="checkbox" value="checked"> Checked Record</label>
								<label class="control-label filter_option"><input id="filter_abnormal" type="checkbox" value="abnormal"> Display Abnormal files</label>
							</div>
						</li>
						@endcan
						@can('manager_type')
							@can('ecg_transfer')
								<li class="selectedfunc">
									<a href="#" class="btn btn-default" id="ecg_trans_btn">
										<span class="glyphicon glyphicon-retweet"></span>
										@lang("physiolguard.ecg_translate")
									</a>
								</li>
							@endcan

							@can('ecg_analysis_page')
							<li class="selectedfunc analysis_item">
								<a href="#" class="btn btn-default" id="ecg_analysis_btn" target="_blank">
									<span class="glyphicon glyphicon-stats"></span>
									@lang('physiolguard.ecg_analysis')
								</a>
							</li>
							@endcan
							@can('report_summary')
								<li class="selectedfunc">
									<a href="#" class="btn btn-default" id="report_summary_btn">
										<span class="glyphicon glyphicon-object-align-bottom"></span>
										@lang("physiolguard.report_summary")
									</a>
									<form action="{{url('data/users/'.$user->id.'/reports/summary')}}" id="report_summary_form" method="GET">
										{{csrf_field()}}
										<div>
											
										</div>
									</form>
								</li>
							@endcan
						@endcan

						@can('ecg_list_option')
						<li class="dropdown selectedfunc" id="funcs_btn">
							<a href="#" class="dropdown-toggle btn btn-default" data-toggle="dropdown" role="button" aria-expanded="false">
								<span class="glyphicon glyphicon-cog"></span>@lang('physiolguard.options')
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								@can('manager_type')
									<li class="selectedonefunc">
										<a href="#" id="func_editinfo"  data-toggle="modal" data-target="#editinfo_modal">
											<span class="glyphicon glyphicon-edit"></span> 
											@lang('physiolguard.edit_info')
										</a>
									</li>
									@can('rr_download')
										<li class="selectedfunc status_option">
											<a href="#" id="func_exportrr">
												<span class="glyphicon glyphicon-download-alt"></span>
												@lang('physiolguard.rr_download')
											</a>
											<form action="{{url('/analysis/ecg/exportRR/')}}" id="export_rr_form" method="POST" target="_blank">
												{{csrf_field()}}
												<div>
													
												</div>
											</form>
										</li>
									@endcan
								
								@endcan

								<li class="selectedonefunc status_option">
									<a href="#" id="analysis_report_btn" target="_blank">
										<span class="glyphicon glyphicon-list-alt"></span>
										@lang('physiolguard.analysis_report')
									</a>
								</li>

								@can('QTC_report_download')
									<li class="selectedonefunc status_option">
										<a href="#" id="func_QTC_report">
											<span class="glyphicon glyphicon-download-alt"></span>
											@lang('physiolguard.QTC_report')
										</a>
									</li>
								@endcan

								@if(isset($mode) && $mode=='normal' && Auth::user()->role_id != 5 && Auth::user()->role_id != 13 )
									<li class="selectedfunc">
										<a href="#" id="func_delete">
											<span class="glyphicon glyphicon-trash"></span>
											@lang('physiolguard.delete')
										</a>
									</li>
								@endif

								<li class="selectedfunc" id="report_download_btn_box">
									<a href="#" id="report_download_btn">
										<span class="glyphicon glyphicon-save"></span> 
										@lang('physiolguard.report_download')
									</a>
									<form action="{{url('data/users/'.$user->id.'/reports/download')}}" id="report_download_form" method="POST" target="_blank">
										{{csrf_field()}}
										<div>
											
										</div>
									</form>
								</li>
							</ul>
						</li>
						@endcan
					</ul>
				</div>
				@if(isset($mode) && $mode=='all')
					@include('layouts.date_range')
				@endcan
			</div>
			<div id="file_manager">
				@if(isset($mode) && $mode=='normal')
					@include('data.ecg_table')
				@elseif(isset($mode) && $mode=='all')
					@include('data.all_ecg_table')
				@endif
			</div>
		</article>
	</div>
</div>
<div id="editinfo_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editinfo_modal_label">
  	<div class="modal-dialog" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="editinfo_modal_label">@lang('physiolguard.edit_info')</h4>
	  		</div> 
	  		<div class="modal-body">

	  			@cannot('data_manager_model')
		  			<h5>@lang('physiolguard.filename_ext'):</h5>
		  			<div id="editinfo_filename_ext" class="form-control">
		  			</div>
	  			@endcan
	  			<h5>@lang('physiolguard.patient_name'):</h5>
	  			<input id="editinfo_patient_name" type="text" class="form-control">
	  			<h5>@lang('physiolguard.description'):</h5>
	  			<textarea id="editinfo_description"  class="form-control">
	  			</textarea>
	  			<h5>@lang('physiolguard.birthday'):</h5>
	  			<input id="editinfo_birthday" type="text" class="form-control">
	  			<h5>@lang('physiolguard.gender'):</h5>
	  			<select id="editinfo_sex" class="form-control">
	  				<option value="M">@lang('physiolguard.gender_M')</option>
	  				<option value="F">@lang('physiolguard.gender_F')</option>
	  			</select>
	  			<h5>@lang('physiolguard.comment'):</h5>
	  			<textarea id="editinfo_comment" class="form-control">
	  			</textarea>
	  			<h5>@lang('physiolguard.patient_IDNumber'):</h5>
	  			<input id="editinfo_patient_IDNumber" type="text" class="form-control">
	  			<h5>@lang('physiolguard.holter_IDNumber'):</h5>
	  			<div id="editinfo_holter_IDNumber" class="form-control">
	  			</div>
	  			@cannot('data_manager_model')
		  			<h5>@lang('physiolguard.report_status'):</h5>
		  			<div id="editinfo_report_status" class="form-control">
		  			</div>
	  			@endcan
	  			<h5>@lang('physiolguard.report_type'):</h5>
	  			<div id="editinfo_report_type" class="form-control">
	  			</div>
	  			<div class="row mt-3">
	  				<button id="editinfo_cancel" class="btn btn-default col-md-3 col-md-offset-4" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="editinfo_save" class="btn btn-info col-md-3 col-md-offset-1">@lang('physiolguard.confirm')</button>
	  			</div>
			</div>
	  	</div>
	</div>
</div>
<div id="report_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="report_modal_label">
  	<div class="modal-dialog modal-md" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="report_modal_label">@lang('physiolguard.analysis_report')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			@can('manager_type')
	  			<div class="report_type_list" id="report_type_list">
	  				@if($department)
	  					@if(Auth::user()->can('report_type_by_comment'))
	  						<div class="comment">
								<h4 class="section_title">@lang("physiolguard.comment")</h4>
								<div id="comment" class="comment">
									<textarea id="report_type_comment_input" class="form-control" style="min-height: 240px;resize: vertical"></textarea>
								</div>
							</div>
		  				@else
			  				@foreach($department->report_types as $key => $report_type)
					  			<div class="row">
					  				<label class="col-md-5">
					  					<input type="{{ $department->select_type==0 ? 'radio':'checkbox' }}" value="{{$report_type->value}}" name="report_tpye" class="report_tpye" data-name="{{$report_type->name}}">{{$report_type->name}}
					  				</label>
					  				@if($report_type->duration==1)
							  				<div class="col-md-7">
								  				<div class="col-md-6 text-right">
								  					@lang('physiolguard.duration_time')[{{$report_type->duration_low.'-'.$report_type->duration_up}}]:
								  				</div>
								  				<label class="col-md-6">
								  					<input type="number" min="{{$report_type->duration_low}}" value="{{$report_type->duration_low}}" max="{{$report_type->duration_up}}" name="report_tpye_duration" class="report_tpye_duration form-control" data-name="{{$report_type->name}}">
								  				</label>
							  				</div>
						  				@endif
					  			</div>
				  			@endforeach
			  			@endif
		  			@endif
	  			</div>
	  			<div class="row">
	  				<button id="report_cancel" class="btn btn-default col-md-3 col-md-offset-3" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="report_create" class="btn btn-info col-md-4 col-md-offset-1">@lang('physiolguard.create_report')</button>
	  			</div>
	  			@elsecannot('manager_type')
	  				<h5>@lang('physiolguard.report_status_0')</h5>
	  				<div class="row">
	  					<button class="btn btn-default col-md-3 col-md-offset-3" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				</div>
	  			@endcan
			</div>
	  	</div>
	</div>
</div>
<div id="report_download_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="report_modal_download_label">
  	<div class="modal-dialog modal-md" role="document">
	  	<div class="modal-content"> 
	  		<div class="modal-header"> 
	  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  				<span aria-hidden="true">×</span>
	  			</button> 
				<h4 class="modal-title" id="report_modal_download_label">@lang('physiolguard.analysis_report')</h4>
	  		</div> 
	  		<div class="modal-body">
	  			<div class="row">
	  				@can('manager_type')
	  					<div class="col-md-12" style="margin-bottom: 2em">
	  						@lang('physiolguard.create_report_msg_1')<span class="report_type"></span>@lang('physiolguard.create_report_msg_2')?
	  					</div>
	  				@elsecannot('manager_type')
	  					<div class="col-md-12" style="margin-bottom: 2em">
	  						@lang('physiolguard.download_report')<span class="report_type"></span>
	  					</div>
	  				@endcan
	  			</div>
	  			<div class="row">
	  				<button id="report_download_cancel" class="btn btn-default col-md-2 col-md-offset-1" data-dismiss="modal">@lang('physiolguard.cancel')</button>
	  				<button id="report_download" class="btn btn-info col-md-2 col-md-offset-1">@lang('physiolguard.download')</button>
	  				@can('manager_type')
	  					<button id="report_create_show" class="btn btn-info col-md-4 col-md-offset-1">@lang('physiolguard.recreate_report')</button>
	  				@endcan
	  			</div>
			</div>
	  	</div>
	</div>
</div>

<div id="report_creating_msg" class="hide">
	<div class="text-center msg_box">
		<div class="icon">
			<span class="glyphicon glyphicon-repeat"></span>
		</div>
		<span class="text">@lang('physiolguard.report_generating')</span>
	</div>
</div>
@endsection
@section('javascript')

<script src="{{asset('thirdpart/moment/moment.js')}}"></script>
<script src="{{asset('thirdpart/moment/locales/zh-tw.js')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/bootstrap-table.min.js')}}"></script>
<script src="{{url('thirdpart/bootstraptable/extensions/group-by-v2/bootstrap-table-group-by.js')}}?v={{env('STATIC_FILE_VERSION','1.0.1')}}"></script>
<script src="{{url('/thirdpart/bootstraptable/locale/bootstrap-table-'.App::getLocale().'.min.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap4-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('thirdpart/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('/js/guide.js')}}"></script>
<script>
var department = {!! json_encode($department) !!};
var evtSource = null;
$(document).ready(function() {
	$('#tool_bar .tools .selectedfunc').hide();
	$('#date_start').datepicker().on("changeDate", function(e) {
        $('#file_manager_table').bootstrapTable('refresh');
    });
	$('#date_end').datepicker().on("changeDate", function(e) {
		$('#file_manager_table').bootstrapTable('refresh');
	});
	$('#file_manager_table').on('check.bs.table uncheck.bs.table uncheck-all.bs.table check-all.bs.table',function(event, row, $element){
			var selections=$('#file_manager_table').bootstrapTable('getSelections');
			set_analysis_url(selections);
			selected_funcs_show(selections);
			selected_show_report_download(selections);
	});
	$('#file_manager_table').on('load-success.bs.table', function(data){
		updateECGStatus();
	});
	selected_funcs_show([]);
	selected_show_report_download([]);
	$('#func_delete').on('click',function(event){
		ecg_del($('#file_manager_table').bootstrapTable('getSelections'));
	});
	$('#editinfo_modal').on('shown.bs.modal',function(event){
		show_editinfo($('#file_manager_table').bootstrapTable('getSelections'));
	});
	$('#analysisinfo_modal').on('shown.bs.modal',function(event){
		show_analysisinfo($('#file_manager_table').bootstrapTable('getSelections'));
	});
	$('#editinfo_save').on('click',function(event){
		var info_data={
			'description':$('#editinfo_description').val(),
			'patient_name':$('#editinfo_patient_name').val(),
			'patient_IDNumber':$('#editinfo_patient_IDNumber').val(),
			'birthday':$('#editinfo_birthday').val(),
			'sex':$('#editinfo_sex').val(),
			'comment':$('#editinfo_comment').val()
		};
		$('#file_manager_table').bootstrapTable('updateByUniqueId',{
			id: $(this).data('ecg_id'),
			row: info_data
		});
		ecginfo_update(info_data,$(this).data('ecg_id'));
	});
	$('#func_exportrr').on('click',function(event){
		event.preventDefault();
		var ecg_ids = $(this).data('ecg_ids');
		export_rr(ecg_ids);
	});
	$('.analysis_item').on('click', 'a', function(event){
		event.preventDefault();
		var href = $(this).attr('href');
		$('#file_manager_table').bootstrapTable('uncheckAll');
		$('#file_manager_table input[name=btSelectGroup]').prop('checked', false);
		window.open(href)
		return false
	});
	$('#ecg_trans_btn').click(function(event){
		event.preventDefault();
		var ecg_ids = $(this).data('ecg_ids');
		var lastResponseLength = false;
		$.ajax({
			type: 'POST',
			url: url('ecg/transfer'),
			data: JSON.stringify({"ecg_ids":ecg_ids}), 
			dataType: 'json',
			contentType: "application/json",
        	processData: false,
			xhrFields: {
	            onprogress: function(e)
	            {
	                var progressResponse;
	                var response = e.currentTarget.response;
	                if(lastResponseLength === false)
	                {
	                    progressResponse = response;
	                    lastResponseLength = response.length;
	                }
	                else
	                {
	                    progressResponse = response.substring(lastResponseLength);
	                    lastResponseLength = response.length;
	                }
	                var data = JSON.parse(progressResponse);
	                var rowData = $('#file_manager_table').bootstrapTable('getRowByUniqueId',data.id);
					if(rowData && 'analysis_status' in rowData){
						rowData.analysis_status = data.status;
						$('#file_manager_table').bootstrapTable('updateByUniqueId', data.id, rowData);
						if(rowData.analysis_status==2){
							selected_funcs_show([rowData]);
						}
					}
	            }
	        }
		}).done(function( data ) {
		   	console.log( data );
		});
	});
	$('#analysis_report_btn').click(function(event){
		if($(this).data('filename_ext')=='z2b' || $(this).data('filename_ext')=='xml'){
		event.preventDefault();
			var row = $('#file_manager_table').bootstrapTable('getRowByUniqueId', $(this).data('ecg_id'));
			$.get(url('/analysis/z2b/'+row.id+'/report/exists'),function(response){
				if($('#report_type_list').data('QTC')){
					$('#report_type_list').html(response.report_type_inputs);
					$('#report_type_list').data('QTC',false);
				}else{
					if(!$('#report_type_list > .row').length){
						$('#report_type_list').html(response.report_type_inputs);
						$('#report_type_list').data('QTC',false);
					}
				}
				if(response.exists==1){
					$('#report_download_modal').modal('show');
					$('#report_download_modal .report_type').html('');
					$('#report_download').data('ecg_id', row.id);
					$('#report_download').data('department', false);
					$('#report_ecg_id').val(row.id);
					$('#report_create_show').data('ecg_id', row.id).data('filename_ext', row.filename_ext).data('report_type',row.report_type);
				}else{
					show_report_modal(row.id);
				}
			});
		}
	});

	$('#func_QTC_report').click(function(event){
		event.preventDefault();
		if($(this).data('filename_ext')=='psg' || $(this).data('filename_ext')=='z2b' || $(this).data('filename_ext')=='xml'){
			var row = $('#file_manager_table').bootstrapTable('getRowByUniqueId', $(this).data('ecg_id'));
			
			var ecg_id = row.id;
			window.open(url('analysis/z2b/'+ecg_id+'/report/qtc'));
		}
	});
	$('#report_create').click(function(event){
		if(!check_report_duration()){
			return false;
		}
		var report_department = $(this).data('department');
		var report_type=$('#report_modal input[name="report_tpye"]:checked').map(function(index, item){
			var report_tpye_duration = $(item).parent().parent().find('.report_tpye_duration');
			report_tpye_duration = report_tpye_duration.length > 0 ? report_tpye_duration.val() : '';
			return $(item).val()+report_tpye_duration;}).toArray().join(',');

		var ecg_id = $(this).data('ecg_id');
		var report_comment_input = $('#report_type_comment_input');
		var report_comment = report_comment_input.length ? report_comment_input.val() : '';
		$('#report_creating_msg').removeClass('hide');
		if( report_department == "QTC"){
			window.open(url('analysis/z2b/'+ecg_id+'/report?type='+report_type+(report_department ? ('&department='+report_department):'')+(report_comment_input.length ? ('&comment='+report_comment):'')));
			$('#report_modal').modal('hide');
			$('#report_creating_msg').addClass('hide');
		}else{
			$.get(url('analysis/z2b/'+ecg_id+'/report?type='+report_type+(report_department ? ('&department='+report_department):'')+(report_comment_input.length ? ('&comment='+report_comment):'')),function(data){
				var rowData = $('#file_manager_table').bootstrapTable('getRowByUniqueId',ecg_id);
				rowData.report_status = data.report_status;
				$('#file_manager_table').bootstrapTable('updateByUniqueId', data.id, rowData);
				$('#report_modal').modal('hide');
				$('#report_creating_msg').addClass('hide');
			})
		}
	});
	$('#report_download').click(function(event){
		if(!check_report_duration()){
			return false;
		}
		var report_department = $(this).data('department');
		var report_type=$('#report_modal input[name="report_tpye"]:checked').map(function(index, item){
			var report_tpye_duration = $(item).parent().parent().find('.report_tpye_duration');
			report_tpye_duration = report_tpye_duration.length > 0 ? report_tpye_duration.val() : '';
			return $(item).val()+report_tpye_duration;}).toArray().join(',');
		window.open(url('data/analysis/z2b/'+($(this).data('ecg_id'))+'/report/download?type='+report_type+(report_department ? ('&department='+report_department):'')));
		$('#report_download_modal').modal('hide');
	});

	$('#report_download_btn').click(function(event){
		var rows = $('#file_manager_table').bootstrapTable('getSelections');
		if(rows.length){
			$('#report_download_form div').empty();
			for (var i = 0; i < rows.length; i++) {
				$('#report_download_form div').append('<input type="hidden" name="ids[]" value="'+rows[i].id+'" />');
			}
			$('#report_download_form').submit();
		}
	});



	$('#report_summary_btn').click(function(event){
		var rows = $('#file_manager_table').bootstrapTable('getSelections');
		if(rows.length){
			$('#report_summary_form div').empty();
			for (var i = 0; i < rows.length; i++) {
				$('#report_summary_form div').append('<input type="hidden" name="ids[]" value="'+rows[i].id+'" />');
			}
			$('#report_summary_form').submit();
		}
	});

	$('#report_create_show').click(function(event){
		event.preventDefault();
		if($(this).data('filename_ext')=='z2b'){
			$('#report_download_modal').modal('hide');
			$('#report_modal').modal('show');
			$('#report_create').data('ecg_id', $(this).data('ecg_id'));
		}
	});
	$('#editinfo_birthday').datetimepicker({
        format:'YYYY-MM-DD',
        showTodayButton:true

    });    
    $('#file_manager_table').on('click','.report_btn',function(event){
    	if($(this).attr('disabled')){
    		return false;
    	}
    	return true;
    });
    @can('ecg_table_type_filter')
    	$('#tool_bar').on('change','.filter_option',function(event){
	    	$('#file_manager_table').bootstrapTable('refresh');
	    });
    @endcan

    function show_report_modal(ecg_id, report_department)
	{
		$.get(url('/analysis/ecg/'+ecg_id+'/peak/check'),function(data){
			var result = data.data;
			$('#report_download_modal').modal('hide');
			$('#report_modal').modal('show');
			$('#report_modal input[name="report_tpye"]').each(function(i,d){
				if(result.peak_AN==0){
					if($(d).data('name')=='Normal sinus rhythm'){
						$(d).prop('checked',true);
					}else{
						$(d).prop('checked',false);
					}
				}else{
					if($(d).data('name')=='Abnormal rhythm or beat'){
						$(d).prop('checked',true);
					}else{
						$(d).prop('checked',false);
					}
				}
			});
			$('#report_create').data('ecg_id', ecg_id);
			if(report_department){
				$('#report_create').data('department', report_department);
			}
		});
	}
});

function updateECGStatusHandler(event){
	if(evtSource){
		try {
			var ecgStatus = JSON.parse(event.data)
		} catch (e) {
			evtSource.removeEventListener("message", updateECGStatusHandler)
			evtSource.close();
			evtSource = null
			return false
		}
		if(ecgStatus.length === 0){
			evtSource.removeEventListener("message", updateECGStatusHandler)
			evtSource.close();
			evtSource = null
			return false
		}else{
			for(const item of ecgStatus){
				var rowData = $('#file_manager_table').bootstrapTable('getRowByUniqueId',item.id);
				if(rowData){
					for(const key in item ){
						rowData[key] = item[key]
					}
					$('#file_manager_table').bootstrapTable('updateByUniqueId', item.id, rowData);
				}
			}
		}
	}
}
function updateECGStatus(){
	if(evtSource){
		evtSource.removeEventListener("message", updateECGStatusHandler)
		evtSource.close();
	}
	var rows = $('#file_manager_table').bootstrapTable('getData');
	evtSource = new EventSource(url('/api/v0/ecgs/status?user_id={{$user->id}}&ecg_ids='+rows.map(d=>d.id).join(',')), {
		withCredentials: true,
	});
	evtSource.addEventListener("message", updateECGStatusHandler)
}

@if(isset($mode) && $mode=='normal')
	function queryParams(params) {
		var options = $('.filter_option input:checked');
		for (var i = 0; i < options.length; i++) {
			params[options.eq(i).val()] = options.eq(i).val();
		}
		return params
	}
	function ajaxRequest(params) {
	    $.get(url('/data/{{isset($poc) ? "pocs/".$poc->id."/" : ""}}users/'+'{{$user->id}}'+'/ecgs/data') + '?' + $.param(params.data)).then(function (res) {
	      params.success(res);
	    })
	 }
@elseif(isset($mode) && $mode=='all')
	
	function queryParams(params) {
		@if(isset($related_users))
			params.related_users = {!! json_encode($related_users) !!};
		@endif
		params.start = $('#date_start').val();
		params.end = $('#date_end').val();
		return params
	 }
		function ajaxRequest(params) {
	    	$.get(url('/data/ecgs/data') + '?' + $.param(params.data)).then(function (res) {
			params.success(res);
	    })
	 }
@endif
 function ecg_del(selections){
		if(selections.length==0){
			bootbox.alert({
    			message: '@lang("physiolguard.select_file")',
    			locale: lang
    		});
		}else{
			var del_msg='';
			if(selections.length==1){
				del_msg='@lang("physiolguard.to_delete") '+selections[0].filename+' ?';
			}else{
				del_msg='@lang("physiolguard.to_delete_all") '+selections.length+' ?';
			}
			bootbox.confirm({
    			message: del_msg,
    			locale: lang,
    			size: 'small',
				callback: function (result) {
				    if(result){
				    	$.post(url('/data/{{isset($poc) ? "pocs/".$poc->id."/" : ""}}users/'+'{{$user->id}}'+'/ecgs/delete'),{_token:window.Laravel.csrfToken,ecgs:selections},function(result){
							if(result.del_success_count>0){
								var ids=$.map(selections, function (row){
									return row.id;
								});
								$('#file_manager_table').bootstrapTable('remove', {field: 'id', values: ids});
								$('#tool_bar .tools .selectedfunc').hide();
							}
						});
				    }
				}
    		});
		}
}
function check_report_duration(){
		var report_type_duration = $('#report_modal input[name="report_tpye_duration"]');
		for (var i = 0; i <report_type_duration.length; i++) {
			var duration = report_type_duration.eq(i).val();
			var min = parseInt(report_type_duration.eq(i).attr('min'), 10);
			var max = parseInt(report_type_duration.eq(i).attr('max'), 10);
			if(duration < min || duration > max){
				bootbox.alert({
    				message: '@lang("physiolguard.out_of_range")',
    				locale: lang
    			});
				return false;
			}
		}
		return true;
	}
function convertUTCTimetoLocal(time) {
	var regexDate = time.match(/(\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
	var date = new Date(Date.UTC(regexDate[1], parseInt(regexDate[2]) - 1, regexDate[3], regexDate[4], regexDate[5],
			regexDate[6]));

	var year = date.getFullYear();
	var month = ("0" + (date.getMonth() + 1)).slice(-2);
	var day = ("0" + date.getDate()).slice(-2);
	var hour = ("0" + date.getHours()).slice(-2);
	var minute = ("0" + date.getMinutes()).slice(-2);
	var second = ("0" + date.getSeconds()).slice(-2);

	return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second;
}

function set_analysis_url(selections){
	if(selections.length>0){
		$('#ecg_trans_btn').data('ecg_ids',selections.map(function(d){return d.id;}));
		$('#func_exportrr').data('ecg_ids',selections.map(function(d){return d.id;}));
		$('#ecg_analysis_btn').attr('href',url('analysis/ecg/'+selections[0].id));
		$('#hrv_analysis_btn').attr('href',url('analysis/hrv/'+selections[0].id));
		$('#sleep_analysis_btn').attr('href',url('analysis/sleep/'+selections[0].id));
		if(selections[0].filename_ext=='psg'){
			$('#analysis_report_btn').attr('href',url('analysis/ecg/report/'+selections[0].id)).data('filename_ext','psg');
			$('#func_QTC_report').attr('href',url('analysis/ecg/report/'+selections[0].id)).data('filename_ext','psg').data('department','QTC');
		}else{
			$('#analysis_report_btn').attr('href','#').data('filename_ext',selections[0].filename_ext).data('ecg_id',selections[0].id).data('report_type',selections[0].report_type);
			$('#func_QTC_report').attr('href','#').data('filename_ext','z2b').data('ecg_id',selections[0].id).data('department','QTC');
		}
	}else{
		$('#ecg_trans_btn').attr('href','#');
		$('#ecg_analysis_btn').attr('href','#');
		$('#hrv_analysis_btn').attr('href','#');
		$('#sleep_analysis_btn').attr('href','#');
        $('#analysis_report_btn').attr('href','#').data('filename_ext',false);
        $('#func_QTC_report').attr('href','#').data('filename_ext',false);
	}
}
function filename_formatter(value, row, index){
	var icon='<span class="glyphicon glyphicon-file"></span> ';
	var memo= row['analysis_status'] == 10 ? '<span class="analysis_status_memo">@lang("physiolguard.transfer_failed")</span>' : '';
	var desc='<span class="file_desc pull-right">'+row['description']+'</span>';
	return icon+'<span>'+value+'</span>'+memo+desc;
}

function report_status_formatter(value, row, index){
	return getReportStatus(value)
}
function report_type_formatter(value, row, index){
	var type_string = '';
	var report_type = value || '';
	var report_types = department ? department.report_types : [];
	if(report_type=='-1'){
		return '@lang("physiolguard.report_type_-1")';
	}
	for (var i = 0; i < report_types.length; i++) {
		if(report_type.indexOf(report_types[i].value) != -1){
			type_string += type_string == '' ? report_types[i].name : ', '+report_types[i].name;
		}
	}
	return type_string;
}
function analysis_status_formatter(value, row, index){
	switch(value){default:
		case 0:return '@lang("physiolguard.analysis_status_0")';
		case 9:return '@lang("physiolguard.analysis_status_9")';
		case 1:return '@lang("physiolguard.analysis_status_1")';
		case 92:return '@lang("physiolguard.analysis_status_92")';
		case 93:return '@lang("physiolguard.analysis_status_93")';
		case 82:return '@lang("physiolguard.analysis_status_82")';
		case 2:return '@lang("physiolguard.analysis_status_2")';
		case 10:return '@lang("physiolguard.analysis_status_10")';
		case 11:return '@lang("physiolguard.analysis_status_11")';
	}
}
function QT_formatter(value, row, index){
	return value
}

function QTc_formatter(value, row, index){
	return value
}
function priority_formatter(value, row, index){
	return value == '1' ? '<p class="text-danger">real-time</p>':'';
}

function checked_formatter(value, row, index){
	return value ? 'Yes' : 'No';
}

function confirm_formatter(value, row, index){
	return value ? 'Yes' : 'No';
}

function user_formatter(value, row, index){
	return value.email;
}
function action_formatter(value, row, index){
	var button_html='';
	var report_type=row.report_type;

	button_html += '<a class="btn btn-info report_btn" '+(row.report_status==2?'':'disabled')+' target="_blank" href="'+url('analysis/z2b/'+(value))+'/report/download?report_type='+report_type+'">@lang("physiolguard.report")</a>';

	return button_html;
}

function selected_funcs_show(selections){
	if(selections.length>0){
		$('#tool_bar .tools .selectedfunc').show();
		status_option(selections);
		selected_onefunc_show(selections);
		return true;
	}else{
		$('#tool_bar .tools .selectedfunc').hide();
		$('#tool_bar .tools .selectedonefunc').hide();
		return false;
	}
}

function selected_show_report_download(selections){
	if(selections.length){
		var pass = true;
		for (var i = 0; i < selections.length; i++) {
			if(selections[i].report_status != 2){
				pass= false;
			}
		}
		if(pass){
			$('#report_download_btn_box').show();
			$('#report_summary_btn_box').show();
			return true;
		}else{
			$('#report_download_btn_box').hide();
			$('#report_summary_btn_box').hide();
			return false;
		}
	}else{
		$('#report_download_btn_box').hide();
		$('#report_summary_btn_box').hide();
		return false;
	}
}

function selected_onefunc_show(selections){
	if(selections.length==1){
		$('#tool_bar .tools .selectedonefunc').show();
		status_option(selections);
		//$('#funcs_btn').show();
		return true;
	}else{
		$('#tool_bar .tools .selectedonefunc').hide();
		//$('#funcs_btn').hide();
		return false;
	}
}
function status_option(selections){
	if(selections[0].analysis_status==2){
		$('#tool_bar .tools .status_option').show();
	}else{
		$('#tool_bar .tools .status_option').hide();
	}
}

function show_editinfo(selections){
	if(selections.length==1){
		$('#editinfo_filename_ext').html(selections[0].filename_ext);
		$('#editinfo_patient_name').val(selections[0].patient_name);
		$('#editinfo_description').val(selections[0].description);
		$('#editinfo_birthday').val(selections[0].birthday);
		$('#editinfo_sex').val(selections[0].sex);
		$('#editinfo_comment').val(selections[0].comment);
		$('#editinfo_patient_IDNumber').val(selections[0].patient_IDNumber);
		$('#editinfo_holter_IDNumber').html(selections[0].holter_IDNumber);
		$('#editinfo_report_status').html(getReportStatus(selections[0].report_status));
		$('#editinfo_report_type').html(getReportType(selections[0].report_type));
		$('#editinfo_save').data('ecg_id',selections[0].id);
	}else{

	}
}

function export_rr(selections){
	if(selections.length){
		$('#export_rr_form div').empty();
		for (var i = 0; i < selections.length; i++) {
			$('#export_rr_form div').append('<input type="hidden" name="ids[]" value="'+selections[i]+'" />');
		}
		$('#export_rr_form').submit();
	}
}
function ecginfo_update(update_data,ecg_id){
	update_data['_token']=window.Laravel.csrfToken;
	update_data['id']=ecg_id;
	$.post(url('/ecg/manager/update_info'),update_data,function(result){
		$('#editinfo_modal').modal('hide');
	});
}
function getReportType(type){
	return report_type_formatter(type, [], 0)
	switch(type){
		case '-1' :case -1 :return '@lang("physiolguard.report_type_other")';
		case '0' :return '@lang("physiolguard.report_type_0")';
		case '1' :return '@lang("physiolguard.report_type_1")';
		case '2' :return '@lang("physiolguard.report_type_2")';
		case '3' :return '@lang("physiolguard.report_type_3")';
		default:return type;
	}
}
function getReportStatus(status){
	switch(status){
		case 0 :return '@lang("physiolguard.report_status_0")';
		case 1 :return '@lang("physiolguard.report_status_1")';
		case 2 :return '@lang("physiolguard.report_status_2")';
		case 3 :return '@lang("physiolguard.report_status_3")';
		default:return status;
	}
}
</script>
@endsection