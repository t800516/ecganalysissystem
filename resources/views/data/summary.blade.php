@extends('layouts.app')
@section('css_file')
	<link href="{{url('/css/summary.css')}}?v={{env('CSS_VERSION','1.0.0')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container">
	<div class="header_title">
		<button class="btn btn-success pull-right" id="export_btn">@lang('physiolguard.export')</button>
	</div>
	<div id="summary_page">
		<div class="section full-width">
			<div class="section-header">
				<div class="section-header-logo">
					<img src="{{url('assets/img/report_logo.png')}}"/>
				</div>
				<div class="section-header-text">
					手腕式心電圖記錄器報告單
				</div>
				<div class="section-header-date">
					{{date('Y/m/d')}}
				</div>
			</div>
		</div>
		@if($errors->any())
			<div class="section full-width text-center text-danger">
				{{$errors->first()}}
			</div>
		@endif
		<form id="report_summary_form" class="form-horizontal" action="{{url('/data/users/'.$id.'/reports/summary/export')}}" method="POST">
			{{ csrf_field() }}
			<input name="ids" type="hidden" value="{{$report_ids}}">
			<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.hospital'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="hospital" type="text" class="form-control" value="{{old('hospital', $name)}}" required>
		  		</div>
		  		<div class="controls col-sm-4">
		  			<input name="hospital_en" type="text" class="form-control" placeholder="option title" value="{{old('hospital_en')}}">
		  		</div>
		  	</div>
			<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.physician'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="physician" type="text" class="form-control" value="{{old('physician')}}" required>
		  		</div>
		  	</div>
			<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.indications'):</div>
		  		<div class="controls col-sm-4">
			  		<input name="indications" type="text" class="form-control" value="{{old('indications')}}" required>
			  	</div>
		  	</div>
		  	<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.report_number'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="report_number" type="text" class="form-control"  value="{{old('report_number')}}" required>
		  		</div>
		  	</div>
			<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.test_date'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="test_date" type="text" class="form-control" value="{{old('test_date', $test_date)}}" required>
		  		</div>
		  	</div>
		  	<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.record_normal'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="record_normal" type="text" class="form-control" value="{{old('record_normal',$record_normal)}}" required>
		  		</div>
		  	</div>
		  	<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.record_abnormal'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="record_abnormal" type="text" class="form-control" value="{{old('record_abnormal',$record_abnormal)}}" required>
		  		</div>
		  	</div>
		  	<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.record_noisy'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="record_noisy" type="text" class="form-control"  value="{{old('record_noisy',$record_noisy)}}" required>
		  		</div>
		  	</div>
		  	<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.min_hr'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="min_hr" type="text" class="form-control" value="{{old('min_hr',$min_hr)}}">
		  		</div>
		  		<div class="control-label col-sm-2 text-left">
		  			BPM
		  		</div>
		  	</div>
		  	<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.max_hr'):</div>
		  		<div class="controls col-sm-4">
		  			<input name="max_hr" type="text" class="form-control"  value="{{old('max_hr',$max_hr)}}">
		  		</div>
		  		<div class="control-label col-sm-2 text-left">
		  			BPM
		  		</div>
		  	</div>
			<div class="form-group row">
		  		<div class="control-label col-sm-2">@lang('physiolguard.finding'):</div>
		  		<div class="controls col-sm-8">
			  		<textarea id="finding" name="finding" rows="6" class="form-control">{{old('finding',$finding)}}</textarea>
			  	</div>
		  	</div>
			<div class="form-group row">
		  		<button class="btn btn-success pull-right hidden" type="submit" id="report_summary_form_submit">@lang('physiolguard.export')</button>
		  	</div>
		</form>
		<div>
			<h5>@lang('physiolguard.reports')</h5>
			<div class="">
				@foreach($reports as $key => $report)
					<div class="row">
						<div class="col-sm-2 text-right">{{$key+1}}</div>
						<div class="col-sm-8"><a href="{{$report['url']}}" target="_blank">{{ $report['report_name'] }}</a></div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@endsection
@section('javascript')
<script>
	$(function(){
		$('#export_btn').click(function(event){
			$('#report_summary_form_submit').click();
		})
		$('#report_summary_form').on('keydown', 'input', function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		})
	})
</script>
@endsection