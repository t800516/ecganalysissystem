<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link href="{{url('/css/report_pdf.css')}}" rel="stylesheet">
</head>
<body>
    <header class="">
        <div class="title">
          {{$hospital}}
        </div>
        <div class="title-sub">
          {{$hospital_en}}
        </div>
        <div class="title">
            手腕式心電圖記錄器報告單
        </div>
    </header>
    <table class="info">
        <tr class="info-column">
            <td class="info-label">病歷號:</td>
            <td class="info-value">{{$patient_IDNumber}}</td>
            <td class="info-label">姓名:</td>
            <td class="info-value">{{$patient_name}}</td>
            <td class="info-label">病號:</td>
            <td class="info-value"></td>
        </tr>
    </table>
    <div class="patient_inforamtion">
        <div class="block-title">Patient Information</div>
        <table class="block-table">
            <tr class="block-table-row">
                <td class="block-table-col">
                    <table>
                        <tr>
                            <td width="60">DoB:</td>
                            <td width="180">{{$birthday}}</td>
                        </tr>
                        <tr>
                            <td>Indications: </td>
                            <td>{{$indications}}</td>
                        </tr>
                    </table>
                </td>
                <td class="block-table-col">
                    <table>
                        <tr>
                            <td>Age:</td>
                            <td width="20">{{$age}}</td>
                            <td>Sex:</td>
                            <td width="20">{{$sex}}</td>
                        </tr>
                        <tr>
                            <td>Height: </td>
                            <td>{{$height}}</td>
                            <td>Weight: </td>
                            <td>{{$weight}}</td>
                        </tr>
                        <tr>
                            <td>Physician:</td>
                            <td colspan="3">{{$physician}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="report_inforamtion">
        <div class="block-title">Report</div>
        <table class="block-table">
            <tr class="block-table-row">
                <td class="block-table-col">
                    <table>
                        <tr>
                            <td width="100">Report Number:</td>
                            <td width="150">{{$report_number}}</td>
                        </tr>
                        <tr>
                            <td>Report Date:</td>
                            <td>{{date('Y/m/d')}}</td>
                        </tr>
                        <tr>
                            <td>Test Date:</td>
                            <td>
                                {{$test_date}}
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="block-table-col">
                    <table>
                        <tr>
                            <td class="text-left">Total records:</td>
                            <td width="60" class="text-center">{{$record_num}}</td>
                            <td width="10"></td>
                        </tr>
                        <tr>
                            <td class="text-left">Normal:</td>
                            <td class="text-center">{{$record_normal}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-left">Abnormal:</td>
                            <td class="text-center">{{$record_abnormal}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-left">Noisy:</td>
                            <td class="text-center">{{$record_noisy}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-left">Min. Vent. Rate:</td>
                            <td class="text-center">{{$min_hr}}</td>
                            <td>bpm</td>
                        </tr>
                        <tr>
                            <td class="text-left">Max. Vent. Rate:</td>
                            <td class="text-center">{{$max_hr}}</td>
                            <td>bpm</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <div class="finding_inforamtion">
        <div class="block-title">Finding</div>
        <div class="block-content">{!! nl2br($finding) !!}</div>
        <div class="block-footer">
            <table>
                <tr>
                    <td width="180"></td>
                    <td>Signed: </td>
                    <td width="80" class="underline"></td>
                    <td>Date:</td>
                    <td width="80" class="underline"></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
