# ECG Analysis System
1. 系統設定
	將本專案從版本控制Git下載到伺服器後，將資料夾/storage、/bootstrap/app以及其子目錄權限調整為777。將 .env.example 複製為 .env並更改資料庫設定。
	
	使用composer的命令安裝相關套件:

	`composer install`

	完成後使用artisan產生應用程式金鑰:

	`php artisan key:generate`

	確認 .env 檔案中資料庫連線設定正確，並在資料庫中新增所對應的資料庫，執行migrate指令，將資料表遷移至指定的資料庫:

	`php artisan migrate`

	使用laravel passport的API認證，此方式為oauth2 server的實作。使用passprot指令建立可認證的用戶端:

	`php artisan passport:install`

	系統規劃Uanalyze API的使用者透過Password Credentials Grant 的流程來取得Access Token。

	安裝中文字型供PDF正常輸出。

	`sudo apt-get install fonts-wqy-zenhei xfonts-wqy`

	2. API使用流程
	客戶端可以發送Register API向伺服器註冊使用者，同時伺服器將回傳包含Access Token、Refresh Token、token_type、expires_in以及註冊資訊給客戶端。客戶端取得Access Token和Refresh Token後可以使用API向伺服器請求各項資源。Refresh Token用於Access Token過期後可發送 Refresh API 向伺服器請求新的Access Token。如有Access Token 過期且Refresh Token遺失的情形可發送Auth Token API 附帶帳號與密碼向伺服器重新認證核發新的Access Token。

	取得Access Token後，使用API向伺服器請求資源時，必須將Access Token附加到HTTP的header中以啟發送到伺服器:
~~~~
	headers:{
		Accept : 'application/json',
		Authorization : 'Bearer '+accessToken,
	}
~~~~
3. 資料庫

  `php artisan migrate`

  `php artisan db:seed`

4.排程設定
	linux: 
	~~~~
		crontab -e
	~~~~
	新增一列
	~~~~
		* * * * * cd /{path-to-your-project} && php artisan schedule:run >> /dev/null 2>&1
	~~~~
5.Mail Server
	編輯.env將mailserver設定改為以下

	~~~~
		MAIL_DRIVER=smtp
		MAIL_HOST=smtp.gmail.com
		MAIL_PORT=587
		MAIL_USERNAME=ncu.ibsalab@gmail.com
		MAIL_PASSWORD=N210212214n
		MAIL_ENCRYPTION=tls
	~~~~

6. 安裝matlab環境與相依套件
	下載 MATLAB Runtime 9.1 (R2016B)
	`wget https://ssd.mathworks.com/supportfiles/downloads/R2016b/deployment_files/R2016b/installers/glnxa64/MCR_R2016b_glnxa64_installer.zip`

	`unzip MCR_R2016b_glnxa64_installer.zip`

	`sudo ./install -mode silent -agreeToLicense yes`
	
	`sudo apt install libxt6 libxext6 libxrandr2`
	
	`sudo apt install ghostscript`
	
7. 啟動queue
	`php artisan queue:work --sleep=2 --tries=3 --timeout=900 --queue=link_service,manual_transfer,transfer,priority_report,manual_report,report,default`

8. tensorflow
	`sudo apt install python3.9`
	`mkdir /var/www/.local`
	`sudo chmod 777 -R /var/www`
	`cd {project-home}/app/Http/Controllers/File`
	`sudo pip install tensorflow`
	`sudo pip install scipy`
	for www-data 
	`sudo -H -u www-data pip install tensorflow`
	`sudo -H -u www-data pip install scipy`
9. atc_decode
	for www-data 
	`sudo -H -u www-data pip install numpy`


