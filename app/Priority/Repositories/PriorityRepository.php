<?php
namespace App\Priority\Repositories;
use App\Repositories\Repository;
use App\Priority;

class PriorityRepository extends Repository
{
    public function __construct(Priority $priority)
    {
        $this->model = $priority;
    }
}