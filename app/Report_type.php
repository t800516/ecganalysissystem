<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report_type extends Model
{
    protected $fillable = [
        'name','value','duration','duration_low','duration_up'
    ];
}
