<?php
namespace App\Holter\Repositories;

use App\Holter;

class HolterRepository
{
    protected $holter;

    public function __construct(Holter $holter)
    {
        $this->holter = $holter;
    }
   
   	public function insertHolter($data){
   		$this->holter->create($data);
   	}

   	public function updateHolter($id,$data){
   		$this->holter->where('id',$id)->update($data);
   	}

   	public function getHolters($used){
        if($used!=0){
            return $this->holter->has('users','==',0)->orderBy('IDNumber','asc')->get();
        }else{
            $holters = $this->holter->with([ 'users' => function($query){ $query->select('email');}])->orderBy('IDNumber','asc')->get();
            
            return $holters;
        }
   	}

   	public function getHolter($id){
   		return $this->holter->where('id',$id)->first();
   	}
   	public function delholter($id){
   		return $this->holter->where('id',$id)->delete();
   	}
}