<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'name','email'
    ];
    public function users(){
    	return $this->belongsTo('App\User');
    }
}
