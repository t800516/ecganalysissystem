<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ECG\Repositories\ECGRepository;

class LinkService implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $ecg_id;
    public function __construct($ecg_id)
    {
        $this->ecg_id = $ecg_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ECGRepository $ecgRepository)
    {
        $ecg = $ecgRepository->get($this->ecg_id);
        if($ecg){
            $user = $ecg->user;
            $service = $user->services()->where('serial_number', $ecg->service_SN)->orderBy('created_at', 'desc')->first();
            $serive_data = [
                    'serial_number'=>$ecg->service_SN,
                    'start_date'=>$ecg->start_date ? $ecg->start_date:null,
                    'duration'=>$ecg->duration ? $ecg->duration : 0,
                    'site'=>$ecg->site,
                    'priority'=>$ecg->site == 'home' ? 1 : $ecg->priority,
                    'patient_name'=>$ecg->patient_name,
                    'patient_IDNumber'=>$ecg->patient_IDNumber,
                    'sex'=>$ecg->sex,
                    'birthday'=>$ecg->birthday && $ecg->birthday!='0000-00-00' && $ecg->birthday!='1900-00-00' ? $ecg->birthday : null
                ];
            if($service){
                $user->services()->where('id',$service->id)->update($serive_data);
                $ecgRepository->update($this->ecg_id, ['service_id'=> $service->id]);
            }else{
                $service = $user->services()->create($serive_data);
                if($service){
                    $ecgRepository->update($this->ecg_id, ['service_id'=> $service->id]);
                }
            }
        }
    }
}
