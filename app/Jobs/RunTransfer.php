<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ECG\Repositories\ECGRepository;
use App\User;
use App\Permission;
use App\Jobs\RunReport;
use App\Analysis\ReportHandler;
use App\Http\Controllers\File\TransferHandler;
use Storage;
class RunTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $ecg_id;
    protected $ecgRepository;
    public function __construct($ecg_id)
    {
        $this->ecg_id = $ecg_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ECGRepository $ecgRepository, Permission $permission)
    {

        $this->ecgRepository = $ecgRepository;
        $result = [];
        $ecg_id = $this->ecg_id;
        $ecg = $ecgRepository->get($ecg_id);
        if($ecg){
            $user = $ecg->user;
            if($ecg->filename_ext == 'atc'){
                $this->atcFlow($user,$ecg,$permission);
            }else{
                $this->defaultFlow($user,$ecg,$permission);
            }
        }
    }
    function reportHandler($user, $ecg)
    {   
        $reportHandler = new ReportHandler($ecg);
        if($user->department && in_array($user->department->name, ['HRV', 'HRVMED'])){
            RunReport::dispatch($reportHandler)->onQueue('priority_report');
        }else{
            RunReport::dispatch($reportHandler)->onQueue('report');
        }
    }
    function atcFlow($user, $ecg, $permission){
        $ecg_id = $ecg->id;
        $user_id = $user->id;
        $holters = $user->holters;
        $transfer_handler = new TransferHandler($user_id, $ecg->filename.'.'.$ecg->filename_ext);
        $file_path = public_path("uploads/{$user_id}/ekg/".$ecg->filename.'.'.$ecg->filename_ext);
        
        $this->ecgRepository->setInfo($ecg_id, [
                "analysis_status" => 9,
        ]);
        
        $ecganalysis_time = $transfer_handler->run_atc_decode($file_path);

        if($transfer_handler->info_file_exists()){
            $holter_id_pass_permission = $permission->where('name','holter_id_pass')->first();
            $check_result = $transfer_handler->checkHolterID($holters, $user->hasPermission($holter_id_pass_permission));
            if($check_result){
                 $this->ecgRepository->setInfo($ecg_id, [
                        "ecganalysis_time" => $ecganalysis_time,
                        "recorded_at" => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', $check_result['recorded_at']).' -8 hours')),
                        "sample_rate" => $check_result['sample_rate'],
                        "holter_IDNumber" => $check_result['holter_id'],
                        "analysis_status" => 92
                ]);

                $predict_result = $transfer_handler->run_predict($file_path, $check_result['sample_rate']);
                
                 $this->ecgRepository->setInfo($ecg_id, [
                    "hf" => $predict_result,
                    "analysis_status" => 2
                ]);
                    
                $report_type = $transfer_handler->get_report_type();
                $ecg->hf = $predict_result;
                $ecg->report_type = $report_type;
                $ecg->report_type_alg = $report_type;
                $ecg->report_status = 0;
                $ecg->save();
                if($user->auto_report==1){
                    $this->reportHandler($user, $ecg);
                }
            }else{
                 $this->ecgRepository->setInfo($ecg_id, [
                    "ecganalysis_time" => $ecganalysis_time,
                    "analysis_status" => 11
                ]);
            }
        }else{
             $this->ecgRepository->setInfo($ecg_id, [
                    "analysis_status" => 10
            ]);
        }
    }

    function defaultFlow($user,$ecg,$permission){
        $ecg_id = $ecg->id;
        $user = $ecg->user;
        $user_id = $user->id;
        $holters = $user->holters;
        $transfer_handler = new TransferHandler($user_id, $ecg->filename.'.'.$ecg->filename_ext);
        $file_path = public_path("uploads/{$user_id}/ekg/".$ecg->filename.'.'.$ecg->filename_ext);
        
         $this->ecgRepository->setInfo($ecg_id, [
                "analysis_status" => 9,
            ]);
        $ecganalysis_time = $transfer_handler->run_to_txt($file_path);

        if($transfer_handler->info_file_exists()){
            $holter_id_pass_permission = $permission->where('name','holter_id_pass')->first();
            $check_result = $transfer_handler->checkHolterID($holters, $user->hasPermission($holter_id_pass_permission));
            if($check_result){
                 $this->ecgRepository->setInfo($ecg_id, [
                        "ecganalysis_time" => $ecganalysis_time,
                        "recorded_at" => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', $check_result['recorded_at']).' -8 hours')),
                        "sample_rate" => $check_result['sample_rate'],
                        "holter_IDNumber" => $check_result['holter_id'],
                        "analysis_status" =>  92
                ]);
                    
                $rranalysis_time = $transfer_handler->run_to_rr(public_path());
                
                 $this->ecgRepository->setInfo($ecg_id, [
                        "rranalysis_time" => $rranalysis_time,
                        "analysis_status" => 82
                ]);
                    
                $rerranalysis_time = $transfer_handler->run_to_rr_second(public_path());
                
                 $this->ecgRepository->setInfo($ecg_id, [
                    "rranalysis_time" => $rerranalysis_time,
                    "analysis_status" => 2
                ]);

                $predict_result = $transfer_handler->run_predict($file_path, $check_result['sample_rate']);
                
                 $this->ecgRepository->setInfo($ecg_id, [
                    "hf" => $predict_result
                ]);
                    
                $report_type = $transfer_handler->get_report_type();
                $ecg->hf = $predict_result;
                $ecg->report_type = $report_type;
                $ecg->report_type_alg = $report_type;
                $ecg->report_status = 0;
                $ecg->save();
                if($user->auto_report==1){
                     $this->reportHandler($user, $ecg);
                }
            }else{
                 $this->ecgRepository->setInfo($ecg_id, [
                    "ecganalysis_time" => $ecganalysis_time,
                    "analysis_status" => 11
                ]);
            }
        }else{
             $this->ecgRepository->setInfo($ecg_id, ["analysis_status" => 10]);
        }
    }
}
