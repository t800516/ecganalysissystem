<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\ECG\Repositories\ECGRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\QTCReport;

class RunReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $reportHandler;
    protected $department;
    public function __construct($reportHandler, $department=false)
    {
		$this->reportHandler = $reportHandler;
        $this->department = $department;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ECGRepository $ecgRepository)
    {
		$ecg = $this->reportHandler->get_ecg();
        $ecgRepository->update($ecg->id, ['report_status'=>1]);
        if($this->reportHandler->run_report(false, $this->department)){
            $result = $this->reportHandler->getResult();
            $ecgRepository->update($ecg->id, [
                'report_status'=>2, 
                'QT'=>$result['QT'], 
                'QTc'=>$result['QTc'], 
                'HR'=>$result['HR'], 
                'min_HR'=>$result['min_HR'], 
                'max_HR'=>$result['max_HR']
            ]);
            $permission = \App\Permission::with('roles')->where('name','QTC_column')->first();
            if($permission && $ecg->user->hasPermission($permission)){
                if($QTResult['QTc'] > 500){
                    foreach ($ecg->user->master_users as $key => $master_user) {
                        Mail::to($master_user->email)->send(new QTCReport($ecg));
                    }
                }
            }
        }else{
            $ecgRepository->update($ecg->id, ['report_status'=>0]);
        }
	}
}
