<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\User;
use App\Traits\ECGDelete;

class DeleteData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ECGDelete;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = date('Y-m-d 00:00:00', strtotime('-14 days'));
        $emails = ['eventmaster@holterholder.com'];
        $users = User::where('role_id', 13)->whereIn('email', $emails)->get();
        foreach ($users as $user) {
            echo $user->name." (".$user->email.")".PHP_EOL;
            $patients = $user->users;
            foreach ($patients as $patient) {
                echo '  '.$patient->name." (".$patient->email.")";
                $ecgs = $patient->ecgs()->where('created_at', '<=', $now)->get();
                echo "：".$ecgs->count().' ecgs'.PHP_EOL;
                foreach ($ecgs as $key => $ecg) {
                    echo $ecg->filename.PHP_EOL;
                    if($this->del_file($ecg->user_id, $ecg->filename,  $ecg->filename_ext)){
                        $ecg->delete();
                    }
                }
            }
        }
    }
}
