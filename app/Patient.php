<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    protected $fillable = [
        'user_id','IDNumber','name','comment','age','sex','birthday','height','weight'
    ];
    public function users(){
    	return $this->belongsToMany('App\User');
    }
    public function ecgs()
    {
        return $this->hasMany('App\ECG');
    }
}
