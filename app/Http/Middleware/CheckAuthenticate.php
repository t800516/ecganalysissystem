<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user->checked == 0) {
            if ($request->wantsJson()){
                $uri = $request->path();
                $actionMethod = $request->method();
                return response()->json(['status'=>'error','error' => ['message'=>['Your account is under review.']], 'uri'=> $uri,'method'=>$actionMethod], 401);
            }
            return redirect('/authchecking');
        }
        return $next($request);
    }
}
