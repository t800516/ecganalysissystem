<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckExpiration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if (($user->expiration_date && $user->expiration_date < date('Y-m-d'))) {
            if ($request->wantsJson()){
                $uri = $request->path();
                $actionMethod = $request->method();
                return response()->json(['status'=>'error','error' => ['message'=>['Your account is expired.']], 'uri'=> $uri,'method'=>$actionMethod], 401);
            }
            return redirect('/authexpired');
        }
        return $next($request);
    }
}
