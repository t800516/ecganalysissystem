<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class DataAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$role)
    {
        if (Auth::user()->checked == 0) {
            return redirect('/authchecking');
        }
        if(!$role){
            if(!in_array(Auth::user()->role_id, [1,2,3,6,7,8,9,11,12])){
                return redirect('/');
            }
        }else{
            if (!in_array(Auth::user()->role_id, $role)) {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
