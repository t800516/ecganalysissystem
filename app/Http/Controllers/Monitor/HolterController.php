<?php

namespace App\Http\Controllers\Monitor;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Analysis\StreamHandler;

class HolterController extends Controller
{

	public function __construct()
	{
		
	}

	public function data(Request $request)
	{
		ob_start();
		ob_end_flush();
		$holter_ids = $request->input('holters', []);
		$hold_time = $request->input('second', 10);
		$timestamp = $request->input('timestamp', 0);
		$holterHandlers = [];
		$holters = [];
		foreach ($holter_ids as $key => $holter_id) {
			$holterHandlers[$holter_id] = new StreamHandler($holter_id);
			$holters[$holter_id] = [];
		}
		$start_time = $time = time();
		while($start_time + $hold_time > $time){
			foreach ($holterHandlers as $key => $holterHandler) {
				if($holterHandlers[$key]->islive()){
					$is_warning = false;
					if($holterHandlers[$key]->hasHrs()){
						$analysis_data = $holterHandlers[$key]->getHrsData();
						$hrsBoundary_data = $holterHandlers[$key]->getHrsBoundaryData();
						$heart_rate_warning = $analysis_data['heart_rate'] <= $hrsBoundary_data["hr_down"] || $analysis_data['heart_rate'] >= $hrsBoundary_data["hr_up"];
						$respiration_rate_warning = $analysis_data['respiration_rate'] <= $hrsBoundary_data["respr_down"] || $analysis_data['respiration_rate'] >= $hrsBoundary_data["respr_up"];
						$spo2_warning = $analysis_data['spo2'] <= $hrsBoundary_data["spo2_down"];
						
						$is_warning = ($heart_rate_warning || $respiration_rate_warning|| $spo2_warning);
						$warning=[
							"heart_rate" => $heart_rate_warning ,
							"respiration_rate"=> $respiration_rate_warning,
							"spo2"=>$spo2_warning
						];
					}else{
						$analysis_data = $holterHandlers[$key]->getResultData();
						$warning=[
							"heart_rate"=>false,
							"respiration_rate"=>false,
							"spo2"=>false
						];
					}

					$holters[$key] = [
						"status"=>$is_warning ? 2 :1, 
						"data"=>[
							"heart_rate"=>$analysis_data['heart_rate'],
							"respiration_rate"=>$analysis_data['respiration_rate'],
							"spo2"=>$analysis_data['spo2']
						],
						"warning"=>$warning
					];
				}else{
					$holters[$key] = [
						"status"=>0,
						"data"=>[
							"heart_rate"=>"-",
							"respiration_rate"=>"-",
							"spo2"=>"-"
						],
						"warning"=>[
							"heart_rate"=>false,
							"respiration_rate"=>false,
							"spo2"=>false]
					];
				}
			}
			$data = [
				'status'=>'streaming',
				'timestamp'=>$time,
				'holters'=>$holters
			];
			$this->output_msg(json_encode($data));
			sleep(1);
			$time = time();
		}

		foreach ($holters as $key => $holters) {
			$holters[$key] = ["status"=>0];
		}

		$this->output_msg(json_encode([
			'status'=>'stop',
			'timestamp'=>$time,
			'holters'=>$holters
		]));
	}
	function output_msg($msg){
        echo $msg;
        ob_flush(); 
        flush();
    }
}
