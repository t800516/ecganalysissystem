<?php

namespace App\Http\Controllers\Monitor;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Analysis\StreamHandler;

class HrsController extends Controller
{

	public function __construct()
	{
		
	}

	public function showWeb(Request $request, $id)
	{
		$holter = Auth::user()->holters()->where('id',$id)->first();
		$streamHandler = new StreamHandler($holter->IDNumber);
		$hrsExData = $streamHandler->getHrsExData();
		return view('/analysis/hrs', ['holter' => $holter, 'ID'=>$hrsExData['ID'], 'name'=>$hrsExData['name'], 'bed_no'=>$hrsExData['bed_no']]);
	}
	public function data(Request $request, $IDNumber){
		$type = $request->input('type');
		$streamHandler = new StreamHandler($IDNumber);
		$data = $streamHandler->getHistoryHrsData(strtotime("-8 day"), $type);
		return response()->json(["response" => "ok",'data' => $data]);
	}
}
