<?php

namespace App\Http\Controllers\Monitor;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Analysis\StreamHandler;

class ManagerController extends Controller
{

	public function __construct()
	{
		
	}

	public function showWeb(Request $request)
	{
		$user_id = Auth::user()->id;
		$holters = Auth::user()->holters->sortByDesc('IDNumber')->sortByDesc('stream_status')->values();
		foreach ($holters as $key => $holter) {
			$holterHandler = new StreamHandler($holter->IDNumber);
			$hrsExData = $holterHandler->getHrsExData();
			$holters[$key]["hrsExData"]=$hrsExData;
		}
		return view('/monitor/manager', ['holters'=>$holters]);
	}
}
