<?php

namespace App\Http\Controllers\Monitor;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Analysis\StreamHandler;

class StreamController extends Controller
{

	public function __construct()
	{
		
	}

	public function data(Request $request, $IDNumber)
	{
		ob_start();
		ob_end_flush();
		$streamHandler = new StreamHandler($IDNumber);
		$hold_time = $request->input('second', 10);
		$timestamp = $request->input('timestamp', 0);
		$start_time = $time = time();
		while($start_time + $hold_time > $time){
			if($streamHandler->islive()){
				if($streamHandler->hasHrs()){
					$analysis_data = $streamHandler->getHrsData();
					$hrsBoundary_data = $streamHandler->getHrsBoundaryData();
					$heart_rate_warning = $analysis_data['heart_rate'] <= $hrsBoundary_data["hr_down"] || $analysis_data['heart_rate'] >= $hrsBoundary_data["hr_up"];
					$respiration_rate_warning = $analysis_data['respiration_rate'] <= $hrsBoundary_data["respr_down"] || $analysis_data['respiration_rate'] >= $hrsBoundary_data["respr_up"];
					$spo2_warning = $analysis_data['spo2'] <= $hrsBoundary_data["spo2_down"];
						
					$warning=[
						"heart_rate" => $heart_rate_warning ,
						"respiration_rate"=> $respiration_rate_warning,
						"spo2"=>$spo2_warning
					];
				}else{
					$analysis_data = $streamHandler->getResultData();
					$warning=[
						"heart_rate" => false ,
						"respiration_rate"=> false,
						"spo2"=>false
					];
				}
				$wave_data = $streamHandler->getRealtimeWaveData();
				//$timestamp = $streamHandler->getTimestamp();
				$data = [
					'status'=>'streaming',
					'timestamp'=>$time,//$timestamp,
					'heart_rate'=>$analysis_data['heart_rate'],
					'spo2'=>$analysis_data['spo2'],
					'respiration_rate'=>$analysis_data['respiration_rate'],
					'ecg_l1'=>$wave_data['ecg_l1'],
					'ecg_l2'=>$wave_data['ecg_l2'],
					'ecg_l3'=>$wave_data['ecg_l3'],
					'ppg_r'=>$wave_data['ppg_r'],
					'ppg_ir'=>$wave_data['ppg_ir'],
					'warning'=>$warning
				];
			}else{
				$data = [
					'status'=>'offline'
				];
			}

			$output = json_encode($data);
			
			if(strlen($output) > 16384){
				$data = [
					'status'=>'error',
					'size'=>strlen($output)
				];
				$output = json_encode($data);
			}
			$this->output_msg($output);
			sleep(1);
			$time = time();
		}
		$data = [
			'status'=>'stop'
		];
		$output = json_encode($data);
		$this->output_msg($output);
	}
	function output_msg($msg){
        echo $msg;
        ob_flush(); 
       	flush();
    }
}
