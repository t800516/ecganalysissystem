<?php

namespace App\Http\Controllers\User;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Repositories\UserRepository;
class GuideController extends Controller
{
    protected $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function getGuide()
    {
        $guide='';
        if(Auth::user()->guide==1){
            $guide = view('user.guide')->render();
        }
        return response()->json(['response'=>'ok','guide'=>$guide]);
    }
    public function setGuide(Request $request)
    {
        $guide=$request->input('guide');
        $this->userRepository->setUserGuide(Auth::user()->id,$guide);
        return response()->json(['response'=>'ok']);
    }
    
}
