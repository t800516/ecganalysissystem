<?php

namespace App\Http\Controllers\User;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct()
    {
        
    }
    public function showWeb()
    {
        $birthday=explode("-", Auth::user()->birthday);
    	$year=$birthday[0];
    	$month=$birthday[1];
    	$day=$birthday[2];
    	$user_profile=[
    		"account"=>Auth::user()->account,
    		"name"=>Auth::user()->name,
    		"email"=>Auth::user()->email,
    		"sex"=>Auth::user()->sex,
    		"birthday"=>Auth::user()->birthday,
    		"year"=>$year,
    		"month"=>$month,
    		"day"=>$day,
    		"zipcode"=>Auth::user()->zipcode,
    		"country"=>Auth::user()->country,
    		"city"=>Auth::user()->city,
    		"downtown"=>Auth::user()->downtown,
    		"address"=>Auth::user()->address,
    		"tel"=>Auth::user()->tel,
    		"cel"=>Auth::user()->cel
    	];
        return view('user.userprofile',$user_profile);
    }
    function updateaccount(Request $request){
    	if(Auth::user()->email!=$request->email){
	    	$this->validate($request, [
	            'email' =>'required|email|max:255|unique:users'
	        ]);
    		$user=User::find(Auth::user()->id);
	        $user->forceFill([
	            'email' => $request->input('email'),
	        ])->save();
	        $this->guard()->login($user);
	        return redirect('/userprofile');
    	}return redirect('/userprofile');
    }
    function updateprofile(Request $request){
    	$this->validate($request, [
			'name' =>'required|max:255',
			'sex' => 'min:1|max:8'
		]);
    	$user=User::find(Auth::user()->id);
    	$user->forceFill([
    		"name"=>$request->input('name'),
    		"sex"=>($request->input('sex')!==null)?$request->input('sex'):'',
    		"birthday"=>$request->input('birthday'),
    		"zipcode"=>$request->input('zipcode'),
    		"country"=>($request->input('country')!==null)?$request->input('country'):'',
    		"city"=>$request->input('city'),
    		"downtown"=>$request->input('downtown'),
    		"address"=>$request->input('address'),
    		"tel"=>$request->input('tel'),
    		"cel"=>$request->input('cel')
    		])->save();
	    $this->guard()->login($user);
	    return redirect('/userprofile');
    }
    
    protected function guard()
    {
        return Auth::guard();
    }
}
