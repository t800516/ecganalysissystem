<?php
namespace App\Http\Controllers\ECG;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;
Use App\User\Repositories\UserRepository;
use App\User;
use DB;
class AssignedController extends Controller
{
    protected $ecgRepository;
    protected $userRepository;
    
    public function __construct(ECGRepository $ecgRepository, UserRepository $userRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->userRepository = $userRepository;
    }
    public function index(Request $request)
    {
        $user = $request->user();
        $data=[
            'user'=>$user,
            'sub_users'=>$user->users,
            'doctors'=>$this->userRepository->getsWith([],['role_id'=>11])
        ];
        return view('assigned.manager', $data);
    }
    function ecgData(Request $request, $user_id){
        $user = $request->user();
        $subuser = $user->users()->find($user_id);
        $where=[
            'user_id'=>$subuser? $subuser->id : 0,
            'confirm'=>0,
            'assignee'=>0
        ];

        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->ecgRepository;
        $search_fields = ['filename', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            if($order_column == 'user'){
                $orderBy['user_id'] = $order;
            }else{
                $orderBy[$order_column] = $order;
            }
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        
        $data_total = $repositoryBy->whereBy($where)->toCount();

        $tableData = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $data_total,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }
    function assigned(Request $request, $doctor_id)
    {
        $ids = $request->input('ids', []);
        $doctor = $this->userRepository->getBy(['id'=>$doctor_id, 'role_id'=>11]);

        if($doctor){
            $this->ecgRepository->whereBy(['id.in'=>$ids, 'confirm'=>0])->toUpdate(['assignee'=>$doctor->id]);
        }

        return response()->json(['ids'=>$ids]);
    }
    function unassigned(Request $request)
    {
        $ids = $request->input('ids', []);
        $this->ecgRepository->whereBy(['id.in'=>$ids, 'confirm'=>0])->toUpdate(['assignee'=>0]);

        return response()->json(['ids'=>$ids]);
    }

    function doctorEcgData(Request $request, $user_id=0){
        $user = $request->user();
        if(!$user_id || $user_id=='null'){
            return response()->json([
                "total" => 0,
                "totalNotFiltered" => 0,
                "rows" => []
            ]);
        }
        $where=[
            'assignee'=>$user_id,
            'confirm'=>0];

        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->ecgRepository;
        $search_fields = ['filename', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            if($order_column == 'user'){
                $orderBy['user_id'] = $order;
            }else{
                $orderBy[$order_column] = $order;
            }
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        
        $data_total = $repositoryBy->whereBy($where)->toCount();

        $tableData = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $data_total,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }
}
