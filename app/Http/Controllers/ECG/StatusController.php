<?php

namespace App\Http\Controllers\ECG;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;
Use App\ECG;
class StatusController extends Controller
{
    protected $ecgRepository;
    
    public function __construct(ECGRepository $ecgRepository)
    {
        $this->ecgRepository=$ecgRepository;
    }
    public function sse(Request $request)
    {
        $user = $request->user();
        $user_id = $request->input('user_id', 0);
        $ecg_ids = explode(',',$request->input('ecg_ids', ''));
        $event_id = $request->input('event_id',1);
        $timestamp = $request->input('timestamp',time());
        $today = date('Y-m-d 00:00:00', strtotime('-1 day'));
        return response()->stream(function () use ($user, $event_id, $user_id, $today, $ecg_ids) {
                $start = time();
                $this->outputMsg($event_id, 'start', $start);
                $process_ecgs = [];
                while(true) {
                    if (connection_aborted() || $start + 60 < time()) {
                        $this->outputMsg($event_id, 'update', 'end');
                        break;
                    }

                    // $ecgs = $this->ecgRepository->model()->whereIn('id', $ecg_ids)->where('user_id',$user_id ? $user_id:$user->id)->where(function($query){
                    //         $query->whereNotIn('analysis_status',[0,1,2,10,11])
                    //         ->orWhere(function($query2){
                    //             $query2->whereNotIn('report_status',[0,2]);
                    //         });
                    // })->get();
                    $ecgs = ECG::whereIn('id', $ecg_ids)->select('id','analysis_status', 'report_status', 'comment', 'report_type', 'checked', 'notified', 'confirm', 'created_at', 'confirm_time')->get();

                    $ecg_status = [];
                    foreach ($ecgs as $ecg) {
                        if(!isset($process_ecgs[$ecg->id])){
                            $process_ecgs[$ecg->id] = $ecg->toArray();
                            $ecg_status[$ecg->id] = $ecg->toArray();
                        }else{
                            $change = false;
                            foreach ($ecg->toArray() as $key2 => $value) {
                                if($process_ecgs[$ecg->id][$key2] != $value){
                                    $change = true;
                                    break;
                               }
                            }
                            if($change){
                                $ecg_status[$ecg->id] = $ecg->toArray();
                                $process_ecgs[$ecg->id] = $ecg->toArray();
                            }
                        }
                    }
                    if(count($ecg_status)){
                        $this->outputMsg($event_id, 'message', json_encode(array_values($ecg_status)));
                    }
                    sleep(2);
                }
            }, 200, [
                'Cache-Control' => 'no-cache',
                'Content-Type' => 'text/event-stream',
                'Connection'=>'keep-alive'
            ]);
    }

    function outputMsg(&$event_id, $event_type, $data){
        echo "id: {$event_id}\n","event: {$event_type}\n", "data: {$data}", "\n\n";
        ob_flush();
        flush();
        ++$event_id;
    }
}
