<?php

namespace App\Http\Controllers\Service;

use Validator;
use Excel;
use Illuminate\Http\Request;
use App\Service\Repositories\ServiceRepository;
use App\User\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Traits\ServiceHandler;

class UserServiceController extends Controller
{
    use ServiceHandler;
    protected $serviceRepository;
    protected $userRepository;

    public function __construct(ServiceRepository $serviceRepository, UserRepository $userRepository)
    {
        $this->serviceRepository = $serviceRepository;
        $this->userRepository = $userRepository;
    }
    public function index(Request $request, $poc_id, $id=false)
    {
        $user = $request->user();
        if(!$id){
            $id = $poc_id;
            $poc = false;
            $patient = $this->userRepository->get($id);
        }else{
            $patient = $this->userRepository->get($id);
            $poc = $this->userRepository->get($poc_id);
        }

        $services = $this->serviceRepository->getsByUser($user->id, $patient->id, $patient->id);

        $data = [
            'poc'=>$poc,
            'patient'=>$patient,
            'services'=>$services
        ];
        return view('service.data.manager', $data);
    }

    public function create(Request $request)
    {
        $user = $request->user();
        $poc = $request->input('poc', false);
        $patient = $request->input('patient', false);
        if($poc){
            $poc = $this->userRepository->get($poc);
        }
        if($patient){
            $patient = $this->userRepository->get($patient);
        }


        $data = [
            'poc'=>$poc,
            'patient'=>$patient
        ];

        return view('service.data.form', $data);
    }

    public function edit(Request $request, $id)
    {
        $user = $request->user();
        $poc = $request->input('poc', false);
        $patient = $request->input('patient', false);
        if($poc){
            $poc = $this->userRepository->get($poc);
        }
        if($patient){
            $patient = $this->userRepository->get($patient);
        }

        $service = $this->serviceRepository->get($id);

        $data = [
            'id'=>$id,
            'poc'=>$poc,
            'patient'=>$patient,
            'service'=>$service
        ];

        return view('service.data.form', $data);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'serial_number' => 'required|max:255',
            'start_date' => 'required|date',
            'duration' => 'required|numeric',
            'duration_customized' => 'nullable|numeric',
            'site' => 'required|in:home,other',
            'patient_name'=> 'max:255|nullable',
            'sex'=> 'in:M,F|nullable',
            'patient_IDNumber'=>'max:255|nullable', 
            'birthday'=>'date|nullable'
        ]);
    }

    protected function save(Request $request, $patient_user_id) 
    {
        $user = $request->user();

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();
        }
        $poc_id = $request->input('poc',false);
        
        $id = $request->input('id',false);

        $patient = $this->userRepository->get($patient_user_id);

        $input_data = $request->only(['serial_number', 'start_date', 'duration', 'site', 'patient_name', 'sex', 'patient_IDNumber', 'birthday', 'duration_customized']);

        $data=[
            'patient_user_id'=>$patient_user_id,
            'serial_number'=>$input_data['serial_number'], 
            'start_date'=>$input_data['start_date'],  
            'duration'=>$input_data['duration'] != -1 ? $input_data['duration'] : $input_data['duration_customized'],  
            'site'=>$input_data['site'], 
            'priority'=>$input_data['site'] == 'home' ? 1 : 0,
            'patient_name'=>$patient ? $patient->patient_name : '', 
            'sex'=>$patient ? $patient->sex:'',  
            'patient_IDNumber'=>$patient ? $patient->patient_IDNumber:'',  
            'birthday'=>$patient ? ($patient->birthday!=='0000-00-00' && $patient->birthday ? $patient->birthday : null) : null, 
        ];
        if($id){
            $service = $this->serviceRepository->get($id);
            if($service){
                $service->update($data);
            }else{
                $service = $this->serviceRepository->update($id, $data);
            }
        }else{
            $data['user_id'] = $patient_user_id;
            $data['created_by'] = $user->id;
            $service = $this->serviceRepository->create($data);
        }

        if($poc_id){
            return redirect('/data/pocs/'.$poc_id.'/users/'.$patient_user_id.'/services');
        }
        return redirect('/data/users/manager/'.$patient_user_id.'/services');
    }

    public function delete(Request $request, $id=0){
        $user = $request->user();
        $poc_id = $request->input('poc',false);
        $patient_id = $request->input('patient',false);
        $del_ids= $request->input('ids');
        $del_success_count=0;
        if(!$id){
            foreach ($del_ids as $key => $del_id) {
                $service = $user->services()->where('id',$del_id)->first();

                if($service){
                    $service = $this->serviceRepository->delete($service->id);
                }else{
                    $service = $this->serviceRepository->delete($del_id);
                }
                $del_success_count++;
            }

            return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
        }

        $service = $user->services()->where('id',$id)->first();
        if($service){
            $service = $this->serviceRepository->delete($service->id);
        }else{
            $service = $this->serviceRepository->delete($id);
        }

        if($poc_id){
            return redirect('/data/pocs/'.$poc_id.'/users/'.$patient_id.'/services');
        }
        return redirect('/data/users/manager/'.$patient_id.'/services');
    }

    public function summary(Request $request, $id)
    {
        $user = $request->user();
        $service = $user->services()->find($id);
        $service = $service ? $service : $this->serviceRepository->get($id);
        $data = $this->summaryData($service);
        $data['poc'] = $request->input('poc', false);
        $data['patient'] = $request->input('patient', false);
        return view('service.summary', $data);
    }

    public function exportList(Request $request, $poc_id=false, $id=false)
    {
        $user = $request->user();
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $patient = $user->users()->find($id);
            }else{
                $patient = $user;
            }
        }else{
            $user = $user->users()->find($poc_id);
            $patient = $user->users()->find($id);
        }

        $services = $this->serviceRepository->getsByUser($user->id, $patient->id, $patient->id);

        $data = [
            'user'=>$patient,
            'poc'=>$user,
            'patient'=>$patient,
            'services'=>$services,
            'type'=>'manager'
        ];

        return view('service.data.export', $data);
    }
    public function exportData(Request $request, $poc_id, $id=false)
    {
        $user = $request->user();
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $patient = $user->users()->find($id);
            }else{
                $patient = $user;
            }
        }else{
            $user = $user->users()->find($poc_id);
            $patient = $user->users()->find($id);
        }
        $where=[];
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->serviceRepository;
        $search_fields = ['serial_number', 'patient_name', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            $orderBy[$order_column] = $order;
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $data_total = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->whereBy($where)->toCount();
        $data_total_filtered = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->toCount();
        $tableData = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $data_total_filtered,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }


    public function export(Request $request, $poc_id, $id=false)
    {
        $user = $request->user();
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $patient = $user->users()->find($id);
            }else{
                $patient = $user;
            }
        }else{
            $user = $user->users()->find($poc_id);
            $patient = $user->users()->find($id);
        }
        $where=[];
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->serviceRepository;
        $search_fields = ['serial_number', 'patient_name', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            $orderBy[$order_column] = $order;
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $sevices = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();

        $filename = date('Ymd').'_services';
        $csv_data = [];
        $fields = [];
        foreach ($sevices as $key => $sevice) {
            if($key == 0){
                $sevice->makeHidden(['id', 'sex', 'updated_at', 'user_id','patient_user_id', 'created_by']);
                $fields = array_keys($sevice->toArray());
                array_push($csv_data, $fields);
            }
            $row = [];
            foreach ($fields as $key2 => $field) {
                array_push($row, $sevice->{$field});
            }
            array_push($csv_data, $row);
        }
        Excel::create($filename, function($excel) use ($csv_data) {
                $excel->sheet('SheetService', function($sheet) use($csv_data) {
                    $sheet->rows($csv_data);
                });
            })->export('csv');
    }
}