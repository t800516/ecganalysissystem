<?php

namespace App\Http\Controllers\Service;
use Storage;
use Validator;
use Excel;
use iio\libmergepdf\Merger;
use Illuminate\Http\Request;
use App\Service\Repositories\ServiceRepository;
use App\User\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Traits\ServiceHandler;

class ServiceController extends Controller
{
    use ServiceHandler;
    protected $serviceRepository;
    protected $userRepository;

    public function __construct(ServiceRepository $serviceRepository, UserRepository $userRepository)
    {
        $this->serviceRepository = $serviceRepository;
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $user = $request->user();
        $services = $user->services()->orderBy('start_date','desc')->get();
        $data = [
            'services'=>$services
        ];
        return view('service.manager', $data);
    }

    public function create(Request $request)
    {
        $user = $request->user();

        $data = [
            
        ];

        return view('service.form', $data);
    }

    public function edit(Request $request, $id)
    {
        $user = $request->user();
        $service =$user->services()->find($id);

        return view('service.form', $service);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'serial_number' => 'required|max:255',
            'start_date' => 'required|date',
            'duration' => 'required|numeric',
            'duration_customized' => 'nullable|numeric',
            'site' => 'required|in:home,other',
            'patient_name'=> 'required|max:255',
            'sex'=> 'required|in:M,F',
            'patient_IDNumber'=>'required', 
            'birthday'=>'required|date'
        ]);
    }

    protected function save(Request $request) 
    {
        $user = $request->user();
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();
        }
        $id = $request->input('id',false);
        $input_data = $request->only(['serial_number', 'start_date', 'duration', 'site', 'patient_name', 'sex', 'patient_IDNumber', 'birthday', 'duration_customized']);
        $data=[
            'serial_number'=>$input_data['serial_number'], 
            'start_date'=>$input_data['start_date'],  
            'duration'=>$input_data['duration'] != -1 ? $input_data['duration'] : $input_data['duration_customized'],  
            'site'=>$input_data['site'], 
            'priority'=>$input_data['site'] == 'home' ? 1 : 0,
            'patient_name'=>$input_data['patient_name'], 
            'sex'=>$input_data['sex'],  
            'patient_IDNumber'=>$input_data['patient_IDNumber'],  
            'birthday'=>$input_data['birthday'], 
        ];
        if($id){
            $user->services()->where('id', $id)->update($data);
        }else{
            $service = $user->services()->create($data);
        }
        return redirect('/services');
    }
    public function delete(Request $request, $id=0){
        $user = $request->user();
        $del_ids= $request->input('ids');
        $del_success_count=0;
        if(!$id){
            foreach ($del_ids as $key => $id) {
                $user->services()->where('id',$id)->delete();
                $del_success_count++;
            }
            return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
        }
        $user->services()->where('id',$id)->delete();
        return redirect('/services');
    }

    public function exportList(Request $request, $poc_id, $id=false)
    {
        $user = $request->user();
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $patient = $user->users()->find($id);
            }else{
                $patient = $user;
            }
        }else{
            $user = $user->users()->find($poc_id);
            $patient = $user->users()->find($id);
        }

        $services = $this->serviceRepository->getsByUser($user->id, $patient->id, $patient->id);
        
        $data = [
            'poc'=>$poc,
            'patient'=>$patient,
            'services'=>$services
        ];

        return view('service.export', $data);
    }

    public function exportData(Request $request, $poc_id, $id=false)
    {
        $user = $request->user();
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $patient = $user->users()->find($id);
            }else{
                $patient = $user;
            }
        }else{
            $user = $user->users()->find($poc_id);
            $patient = $user->users()->find($id);
        }
        $where=[];
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->serviceRepository;
        $search_fields = ['serial_number', 'patient_name', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            $orderBy[$order_column] = $order;
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $data_total = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->whereBy($where)->toCount();
        $data_total_filtered = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->toCount();
        $tableData = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $data_total_filtered,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }


    public function export(Request $request, $poc_id, $id=false)
    {
        $user = $request->user();
        if(!$id){
            $id = $poc_id;
            $poc = false;
            $patient = $this->userRepository->get($id);
        }else{
            $patient = $this->userRepository->get($id);
            $poc = $this->userRepository->get($poc_id);
        }
        $where=[];
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->serviceRepository;
        $search_fields = ['serial_number', 'patient_name', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            $orderBy[$order_column] = $order;
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $sevices = $repositoryBy->userBy($user->id, $patient->id, $patient->id)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();

        $filename = date('Ymd').'_services';
        $csv_data = [];
        $fields = [];
        foreach ($sevices as $key => $sevice) {
            if($key == 0){
                $sevice->makeHidden(['id', 'sex', 'updated_at', 'user_id','patient_user_id', 'created_by']);
                $fields = array_keys($sevice->toArray());
                array_push($csv_data, $fields);
            }
            $row = [];
            foreach ($fields as $key2 => $field) {
                array_push($row, $sevice->{$field});
            }
            array_push($csv_data, $row);
        }
        Excel::create($filename, function($excel) use ($csv_data) {
                $excel->sheet('SheetService', function($sheet) use($csv_data) {
                    $sheet->rows($csv_data);
                });
            })->export('csv');
    }

    public function summary(Request $request, $id)
    {
        $user = $request->user();
        $service = $user->services()->find($id);
        $data = $this->summaryData($service);
        return view('service.summary', $data);
    }
    public function summaryExport(Request $request, $id)
    {
        $merger = new Merger;
        $user = $request->user();
        $user_role = $user->role_id;
        $department = $user->department;
        $time = time();
        $date = date('Ymd');
        $service = $this->serviceRepository->get($id);
        $poc_id = $request->input('poc', false);
        $dataUrl = $request->input('summary');
        $export_path = 'summarys/'.$user->id;

        if($poc_id){
            $poc = $this->userRepository->get($poc_id);
            $department = $poc->department;
            $user_role = $poc->role_id;
        }

        if(!Storage::disk('public')->has($export_path)){
            Storage::disk('public')->makeDirectory($export_path);
        }

        $temp_file = $export_path.'/'.$time.'_tmp.pdf';

        Storage::disk('public')->put($temp_file,  base64_decode($dataUrl));

        $file_path = Storage::disk('public')->path($temp_file);

        $is_merge = false;
        $ecgs = $service->ecgs()->orderBy('recorded_at')->get();
        $merge_files = [];
        $ecg_filename_suffix = "";
        if(in_array($department->name, ["TWDHA", "TFDA", "TFDAHF"])){
            $data_mode = $user_role == 7;
            $ecg_filename_suffix = "_TPOC";
            if($data_mode){
                $ecg_filename_suffix = "_TOTC";
            }else{
                $ecg_filename_suffix = "_TPOC";
            }
        }
        foreach ($ecgs as $key => $ecg) {
            if($ecg->report_status == 2 && $this->isAbnormalRecord($ecg->report_type)){
                $report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.$ecg_filename_suffix);
                if(file_exists($report_file_path.'.pdf')){
                    array_push($merge_files, $report_file_path.'.pdf');
                }
            }
        }
        foreach ($ecgs as $key => $ecg) {
            if($ecg->report_status == 2 && $this->isNormalRecord($ecg->report_type)){
                $report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.$ecg_filename_suffix);
                if(file_exists($report_file_path.'.pdf')){
                    array_push($merge_files, $report_file_path.'.pdf');
                }
            }
        }
        foreach ($ecgs as $key => $ecg) {
            if($ecg->report_status == 2 && $this->isPoorSignalRecord($ecg->report_type)){
                $report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.$ecg_filename_suffix);
                if(file_exists($report_file_path.'.pdf')){
                    array_push($merge_files, $report_file_path.'.pdf');
                }
            }
        }

        foreach ($merge_files as $key => $merge_file) {
            if(!$is_merge){
                $merger->addfile($file_path);
                $is_merge = true;
            }
            $merger->addfile($merge_file);
        }

        if($is_merge){
            $createdPdf = $merger->merge();
            Storage::disk('public')->put($temp_file, $createdPdf);
        }

        $filename = $service->serial_number.'_Summary_'.$date.'.pdf';

        return response()->file($file_path, [
                        'Content-Type' => 'application/pdf',
                        'Content-Transfer-Encoding'=>'binary',
                        'Cache-Control'=>'no-store,no-cache',
                        'Expires'=>'0',
                        'Content-Disposition'=>'attachment; filename="' . $filename . '"',
                    ])->deleteFileAfterSend(true);
    }

    protected function updateComment(Request $request, $id) 
    {
        $input_data = $request->only(['comment']);
        $data=[
            'comment'=>$input_data['comment']
        ];
        if($id){
            $this->serviceRepository->update($id, $data);
        }
        return response()->json($input_data);
    }
}