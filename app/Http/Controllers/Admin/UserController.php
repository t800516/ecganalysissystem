<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Admin\Repositories\AdminRepository;
class UserController extends Controller
{
    protected $adminRepository;
    public function __construct(AdminRepository $adminRepository)
    {
    	$this->adminRepository = $adminRepository;
    }

    public function index(Request $request)
    {
        $users = $this->adminRepository->gets();
        $data = [
            'users'=>$users
        ];
        return view('admin.user.list',$data);
    }

    public function create(Request $request)
    {
        $data = [
            'user'=>null
        ];
        return view('admin.user.form', $data);
    }

    public function edit(Request $request, $id)
    {
        $user = $this->adminRepository->get($id);
        $data = [
            'user'=>$user
        ];
        return view('admin.user.form', $data);
    }


    public function save(Request $request)
    {   
        $id = $request->input("id", null);
        $this->validate($request, [
            'account' => ['required','max:255', $id ? Rule::unique('admins')->ignore($id):'unique:admins'],
            'password' => ($id ? "nullable|":"required|").'confirmed|min:6|max:255',
            "auth"=>'in:0,10,11'
        ]);
        $data = [
            "account" => $request->input("account"),
            "auth"=>  $request->input("auth",1)
        ];
        $password = $request->input("password", null);
        if($password){
            $data["password"] = Hash::make($password);
        }

        if($id){
            $this->adminRepository->update($id, $data);
        }else{
            $this->adminRepository->create($data);
        }
        return redirect('/admin/users');
    }

    public function delete(Request $request,$id = 0)
    {
        $ids = $id ? [$id] : $request->input('ids',[]);
        $del_success_count = 0;
        foreach ($ids as $key => $id) {
            if($id!=1){
                $this->adminRepository->delete($id);
                $del_success_count++;
            }
        }
        return redirect('/admin/users');
    }
}
