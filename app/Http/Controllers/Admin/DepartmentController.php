<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department\Repositories\DepartmentRepository;
use App\NotifiableModule\Repositories\NotifiableModuleRepository;

class DepartmentController extends Controller
{
	protected $departmentRepository;
    protected $notifiableModuleRepository;

    public function __construct(DepartmentRepository $departmentRepository, NotifiableModuleRepository $notifiableModuleRepository)
    {
    	$this->departmentRepository = $departmentRepository;
        $this->notifiableModuleRepository = $notifiableModuleRepository;
    }
    
    public function index()
    {   
        $departments = $this->departmentRepository->gets();
        $data = [
            'departments'=>$departments
        ];
        return view('admin.department.list',$data);
    }

    public function showForm($id = 0)
    {
        $department = $this->departmentRepository->get($id);
        $modules =  $this->notifiableModuleRepository->gets();
        $data = [
            'department'=>$department,
            'modules'=>$modules
        ];
        return view('admin.department.form', $data);
    }
    public function save(Request $request)
    {
        $id = $request->input('id',false);
        $this->validate($request, [
            'name' =>'required|max:255',
            'email' =>'required|email',
        ]);
        $data = $request->only(['name','email','phone','select_type', 'module_id']);
        if($id){
            $department = $this->departmentRepository->update($id, $data);
        }else{
            $department = $this->departmentRepository->create($data);
        }
        $report_types = $request->input('report_types',[]);
        $report_type_ids = [];
        foreach ($report_types as $key => $report_type) {
            $data = [
                    'name'=>$report_type['name'],
                    'value'=>$report_type['value'],
                    'duration'=>isset($report_type['duration']) ? 1 : 0,
                    'duration_low'=>isset($report_type['duration_low']) ? $report_type['duration_low'] : 0,
                    'duration_up'=>isset($report_type['duration_up']) ? $report_type['duration_up'] : 0
                ];
            if($report_type['id']==0){
                $report_type_data = $department->report_types()->create($data);
                array_push($report_type_ids, $report_type_data->id);
            }else{
                $report_type_data = $department->report_types()->where('id',$report_type['id'])->update($data);
                array_push($report_type_ids, $report_type['id']);
            }
        }
        $department->report_types()->whereNotIn('id', $report_type_ids)->delete();
        $data = [
            'department'=>$department
        ];
        return redirect('/admin/department');
    }
    public function getList()
    {
        $data = $this->departmentRepository->gets();
        return response()->json(['response'=>'ok','data'=>$data]);
    }
    public function delete(Request $request)
    {
        $del_ids= $request->input('ids');
        $del_success_count = 0;
        foreach ($del_ids as $key => $id) {
           $this->departmentRepository->delete($id);
           $del_success_count++;
        }
        return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
    }
}
