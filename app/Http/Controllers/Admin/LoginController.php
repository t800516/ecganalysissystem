<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Lang;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Repositories\AdminRepository;
class LoginController extends Controller
{
    protected $admin;
    protected $redirectTo='/admin';

    public function __construct(AdminRepository $admin)
    {
        $this->admin = $admin;
    }
    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $credentials = $this->credentials($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        return $this->sendFailedLoginResponse($request);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'account' => 'required',
            'password' => 'required'
        ]);
    }
    protected function credentials(Request $request)
    {
        return $request->only('account', 'password');
    }
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
    protected function sendLoginResponse(Request $request)
    {
        //$request->session()->regenerate();

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());

        return redirect()->intended('/admin');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
       return redirect()->back()
            ->withInput($request->only('account'))
            ->withErrors([
                'account' => Lang::get('auth.failed'),
            ]);
    }
    protected function authenticated(Request $request, $user)
    {
        //
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        return redirect('/admin/login');
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
    }
}
