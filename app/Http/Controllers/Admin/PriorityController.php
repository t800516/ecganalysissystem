<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Priority\Repositories\PriorityRepository;
use App\User\Repositories\UserRepository;

class PriorityController extends Controller
{
	protected $priorityRepository;
    protected $userRepository;

    public function __construct(PriorityRepository $priorityRepository, UserRepository $userRepository)
    {
    	$this->priorityRepository = $priorityRepository;
        $this->userRepository = $userRepository;
    }
    
    public function index()
    {   
        $users = $this->userRepository->getsWith([]);
        $priorities = $this->priorityRepository->getsWith(['emails','exception_users'],[],['value'=>'ASC','updated_at'=>'DESC']);
        $data = [
            'priorities'=>$priorities,
            'users'=>$users
        ];
        return view('admin.priority.list',$data);
    }

    public function showForm($id = 0)
    {
        $priority = $this->priorityRepository->get($id);
        $data = [
            'priority'=>$priority
        ];
        return view('admin.priority.form', $data);
    }
    public function save(Request $request)
    {
        $id = $request->input('id',false);
        $this->validate($request, [
            'value' =>'required|numeric',
            'start_hr'=>'numeric',
            'start_min'=>'numeric',
            'end_hr'=>'numeric',
            'end_min'=>'numeric',
            'period_hr'=>'numeric',
            'period_min'=>'numeric',
            'emails.*' =>'nullable|email'
        ]);
        $data = $request->only(['value','start_hr','start_min','end_hr','end_min','period_hr','period_min']);
        if($id!='0'){
            $priority = $this->priorityRepository->update($id, $data);
        }else{
            $priority = $this->priorityRepository->create($data);
        }
        $emails = $request->input('emails',[]);
        foreach ($emails as $key => $email) {
            if($email==''){
                continue;
            }
            $data = [
                    'email'=>$email
                ];
               $email_data = $priority->emails()->where('email',$email)->first();
            if(!$email_data){
                $report_type_data = $priority->emails()->create($data);
            }
        }

        $priority->emails()->whereNotIn('email', $emails)->delete();

        $priority->exception_users()->sync($request->input('priority_exceptions'));

        return redirect('/admin/priority');
    }
    public function getList()
    {
        $data = $this->priorityRepository->gets();
        return response()->json(['response'=>'ok','data'=>$data]);
    }
    public function delete(Request $request)
    {
        $del_ids= $request->input('ids');
        $del_success_count = 0;
        foreach ($del_ids as $key => $id) {
           $this->priorityRepository->delete($id);
           $del_success_count++;
        }
        return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
    }
}
