<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apk\Repositories\ApkRepository;
use App\Department\Repositories\DepartmentRepository;

class ApkController extends Controller
{
	protected $apkRepository;
    protected $departmentRepository;
    public function __construct(ApkRepository $apkRepository, DepartmentRepository $departmentRepository)
    {
    	$this->apkRepository = $apkRepository;
        $this->departmentRepository = $departmentRepository;
    }
    
    public function index()
    {   
        $apks = $this->apkRepository->getsWith(['department']);
        $data = [
            'apks'=>$apks
        ];
        return view('admin.apk.list',$data);
    }

    public function showForm($id = 0)
    {
        $apk = $this->apkRepository->get($id);
        $departments = $this->departmentRepository->gets();
        $data = [
            'apk'=>$apk,
            'departments'=>$departments
        ];
        return view('admin.apk.form', $data);
    }
    public function save(Request $request)
    {
        $id = $request->input('id',false);
        $this->validate($request, [
            'name' =>'required|max:255',
        ]);
        $data = $request->only(['name','department_id']);

        if($request->has('apk_file')){
            $target_dir = public_path("uploads/apks/");
            if(!file_exists($target_dir)){
                mkdir($target_dir, 0777);
            }
            $target_dir .= "{$data['department_id']}/";
            if(!file_exists($target_dir)){
                mkdir($target_dir, 0777);
            }
            $target_file = $target_dir . basename($_FILES["apk_file"]["name"]);
            $data['path'] =  "uploads/apks/{$data['department_id']}/" . basename($_FILES["apk_file"]["name"]);
            move_uploaded_file($_FILES["apk_file"]["tmp_name"], $target_file);
        }
        if($id){
            $apk = $this->apkRepository->update($id, $data);
        }else{
            $apk = $this->apkRepository->create($data);
        }

        $data = [
            'apk'=>$apk
        ];
        return redirect('/admin/apks');
    }
    public function getList()
    {
        $data = $this->apkRepository->gets();
        return response()->json(['response'=>'ok','data'=>$data]);
    }
    public function delete(Request $request)
    {
        $del_ids= $request->input('ids');
        $del_success_count = 0;
        foreach ($del_ids as $key => $id) {
           $apk = $this->apkRepository->get($id);
           unlink(public_path($apk->path));
           $this->apkRepository->delete($id);
           $del_success_count++;
        }
        return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
    }
}
