<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Repositories\UserRepository;
use ZipArchive;

class FileController extends Controller
{
	protected $userRepository;
    public function __construct(UserRepository $userRepository)
    {
		$this->userRepository = $userRepository;
    }
    
    public function index(Request $request)
    {
    	$admin_user = $request->user();
    	$layer = $request->input('layer', 'uploads');
    	$layers = explode('/', $layer);
    	$uploads_path = public_path($layer);
    	$files = [];
    	$user_name = '';
    	if($layer == 'uploads'){
    		$directories = array_diff(scandir($uploads_path), ['.', '..']);
    		$users = $this->userRepository->gets();
    		foreach ($directories as $key => $directory) {
    			$has_activity_dir = is_dir($uploads_path.'/'.$directory.'/activities');
    			if(in_array($admin_user->auth, [11]) && !$has_activity_dir){
    				continue;
    			}
	    		$user = $users->where('id', $directory)->first();
	    		if($user){
	    			$is_dir = is_dir($uploads_path.'/'.$directory);
	    			$file = [
	    				'id'=>$user->id,
	    				'name'=>$user->email, 
	    				'dir'=>$is_dir,
	    				'filepath'=> $layer.'/'.$directory,
	    				'link'=>url('admin/files?layer='.$layer.'/'.$user->id),
	    				'created_at'=>date ("Y-m-d H:i:s", filemtime($uploads_path.'/'.$directory))
	    			];
	    			array_push($files, $file);
	    		}
	    	}
	    	if(in_array($admin_user->auth, [0,10])){
	    		$file = [
		    		'id'=>'holter',
		    		'name'=>'holter', 
		    		'dir'=>true,
		    		'filepath'=> $layer.'/holter',
		    		'link'=>url('admin/files?layer='.$layer.'/holter'),
		    		'created_at'=>date ("Y-m-d H:i:s", filemtime($uploads_path.'/holter'))
		    	];
		    	array_push($files, $file);
    		}
    	}else{
    		$directories = array_diff(scandir($uploads_path), ['.', '..']);
    		if(count($layers)>1){
    			$user = $this->userRepository->get($layers[1]);
    			$user_name = $user? $user->email : $layers[1];
    		}

    		if(count($layers)==2){
    			if(in_array($admin_user->auth, [0,10])){
    				$directories = array_diff($directories, ['del', 'failure' ,'hrv' ,'sleep']);
    			}else if(in_array($admin_user->auth, [11])) {
    				$directories = ["activities"];
    			}else{
    				$directories = [];
    			}
    		}

	    	foreach ($directories as $key => $directory) {
	    		$is_dir = is_dir($uploads_path.'/'.$directory);
	    		$file = [
	    			'id'=>$key, 
	    			'name'=>$directory, 
	    			'dir'=>$is_dir,
	    			'filepath'=> $layer.'/'.$directory,
	    			'link'=>$is_dir ? url('admin/files?layer='.$layer.'/'.$directory) : url('admin/files/downloads?path='.$layer.'/'.$directory),
	    			'created_at'=>date ("Y-m-d H:i:s", filemtime($uploads_path.'/'.$directory))
	    		];
				array_push($files, $file);
	    	}
    	}
    	$files = array_reverse(array_values(array_sort($files, function ($value) {
			return $value['created_at'];
		})));
    	$data = [
    		'path'=>$uploads_path,
    		'layers'=>$layers,
    		'user_name'=>$user_name,
    		'files'=>$files
    	];
        return view('admin.file.list', $data);
    }
    public function downloads(Request $request){
    	$path = $request->input('path');
    	$paths = is_array($path) ? $path : [$path];
    	$headers = ["Cache-Control: no-store,no-cache"];
    	if(count($paths)==1 && !is_dir(public_path($paths[0]))){
    		$file_path = public_path($paths[0]);
	    	return response()->download($file_path, basename($file_path), $headers);
	    }else{
			$zip_path = $this->zipFiles($paths);

			if($zip_path){
				return response()->download($zip_path, basename($zip_path), $headers);
			}

			return redirect()->back();
	    }
    	return redirect()->back();
    }
    private function zipFiles($paths){
		$zip = new ZipArchive();

	    $dir = public_path('uploads/zip');

	    if (!file_exists( $dir ) && !is_dir( $dir )){
	        mkdir($dir, 0755, true);
	    }
	    
	    $zip_path = public_path('uploads/zip/sage_file_'.date('YmdHis').'.zip');
	    	
	    $zip_handle = $zip->open($zip_path, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
	        
	    if ($zip_handle!==TRUE) {
			return false;
		}

	    foreach ($paths as $key => $filepath) {
	    	if(is_dir(public_path($filepath))){
	    		$directories = array_diff(scandir(public_path($filepath)), ['.', '..']);
	    		foreach ($directories as $key => $file) {
	    			$zip->addFile(public_path($filepath.'/'.$file), basename($file));
	    		}
	    	}else{
	    		$zip->addFile(public_path($filepath), basename($filepath));
	    	}
	    }

		$zip->close();

		return $zip_path;
    }
}
