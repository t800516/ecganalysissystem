<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\UserHolter;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Department\Repositories\DepartmentRepository;
use App\User\Repositories\UserRepository;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/member';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $userRepository;
    protected $departmentRepository;

    public function __construct(UserRepository $userRepository, DepartmentRepository $departmentRepository)
    {
        $this->userRepository = $userRepository;
        $this->departmentRepository = $departmentRepository;
    }

    public function showWeb()
    {
        $roles = Role::get();
        $data = [
            'departments'=>$this->departmentRepository->gets(),
            'roles'=>$roles,
            'allUsers'=>$this->userRepository->gets()
        ];
        return view('admin.userregister', $data );
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        //$this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $insert_data=[
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'storage' => $data['storage'],
            'remark' => $data['remark'],
            'checked' => $data['checked'],
            'auto_transfer'=> isset($data['auto_transfer']) ? $data['auto_transfer'] : 0,
            'role_id' => $data['role_id'],
            'department_id' => $data['department_id']
        ];
        $holters=isset($data['holter'])?$data['holter']:[];
        $user = $this->userRepository->createUser($insert_data,$holters);
        $uers_ids = isset($data['user_ids'])? $data['user_ids'] : [];
        $user->users()->sync($uers_ids);
        
        return $user;
    }
}