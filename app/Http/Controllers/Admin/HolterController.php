<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Holter\Repositories\HolterRepository;

class HolterController extends Controller
{
	protected $holterRepository;

    public function __construct(HolterRepository $holterRepository)
    {
    	$this->holterRepository = $holterRepository;
    }
    
    public function showWeb()
    {
        return view('admin.holter');
    }

    public function showCreateWeb()
    {
        return view('admin.holtercreate');
    }

    public function showEditWeb($hid)
    {
        $holter_data=$this->holterRepository->getHolter($hid);

        return view('admin.holtercreate',$holter_data);
    }

    public function addHolter(Request $request)
    {
        $this->validate($request, [
            'IDNumber' =>'required|max:255'
        ]);
        $insert_data=[
            'IDNumber'=>$request->input('IDNumber')
        ];
        if($request->has('id')){
            $this->holterRepository->updateHolter($request->input('id'),$insert_data);
        }else{
            $this->holterRepository->insertHolter($insert_data);
        }
        return redirect('/admin/holter');
    }

    public function getHolterList($mode=0)
    {
        $holter_list = $this->holterRepository->getHolters($mode);
        return response()->json(['response'=>'ok','holterList'=>$holter_list]);
    }

    public function delHolters(Request $request)
    {
        $del_ids= $request->input('ids');
        $del_success_count=0;
        foreach ($del_ids as $key => $id) {
           $this->holterRepository->delholter($id);
           $del_success_count++;
        }
        return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
    }
}
