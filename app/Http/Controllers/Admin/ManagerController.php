<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{
    public function __construct()
    {

    }
    
    public function showWeb()
    {
        return view('admin.manager');
    }

}
