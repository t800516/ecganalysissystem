<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department\Repositories\DepartmentRepository;
use App\User\Repositories\UserRepository;
use App\Role;
//use App\UserHolter;
class MemberController extends Controller
{
	protected $userRepository;
    protected $departmentRepository;
    public function __construct(UserRepository $userRepository, DepartmentRepository $departmentRepository)
    {
    	$this->userRepository = $userRepository;
        $this->departmentRepository = $departmentRepository;
    }
    
    public function showWeb()
    {
        return view('admin.member');
    }

    public function getUserList(Request $request){
    	$query_string = [];
        $query_data = [];
        $orderBy=['updated_at' =>'desc'];
        $where=[];
        $with=['role','department'];
        $module_by = $this->userRepository;
        $module_by->useAppends(false);
        $search_fields = ['email', 'name', 'created_at'];
        $search_relation_fields = ['department.name', 'role.name'];
        $search = ""; 

        if($request->has('sort')){
            $orderBy=[];
            $order_column = $request->input('sort', false);
            $order = $request->input('order');
            if($order_column == 'department'){
                $orderBy['department_id'] = $order;
            }elseif($order_column == 'role'){
                $orderBy['role_id'] = $order;
            }else if($order_column != ''){
                $orderBy[$order_column] = $order;
            }
        }


        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search');
        }

        $module_total = $module_by->whereBy($where)->toCount();
        $module_total_filtered = $module_by->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->toCount();
        
        $module = $module_by->searchBy( $search_fields, $search, $search_relation_fields)->toWith($with)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $module_total_filtered,
            "totalNotFiltered" => $module_total,
            "rows" => $module
        ];
        return response()->json($data);
    }

    public function userChecked(Request $request){
    	$ids=$request->input('ids');
    	$this->userRepository->userChecked($ids);
    	return response()->json(['response'=>'ok']);
    }

    public function userEdit($id)
    {
        $user = $this->userRepository->getUser($id);
        $allUsers = $this->userRepository->getsWith([],['id.<>'=>$user->id]);
        $roles = Role::get();
        $user_data=[
            'id'=> $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'storage'=>$user['storage'],
            'remark'=> $user['remark'],
            'role'=> $user['role'],
            'checked'=> $user['checked'],
            'auto_transfer' => $user['auto_transfer'],
            'auto_report' => $user['auto_report'],
            'holters' => $user->holters()->get(),
            'users'=> $user->users()->get(),
            'allUsers'=>$allUsers,
            'department_id'=>$user['department_id'],
            'departments'=>$this->departmentRepository->gets(),
            'roles'=>$roles,
            'expiration_date'=> $user['expiration_date']
            ];
        return view('admin.userregister',$user_data);
    }

    public function userSave(Request $request)
    {   
        $update_data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'storage' => $request->input('storage'),
            'remark' => $request->input('remark'),
            'checked' => $request->input('checked'),
            'auto_transfer' => $request->input('auto_transfer', 0),
            'auto_report' => $request->input('auto_report', 0),
            'role_id' => $request->input('role_id'),
            'department_id' => $request->input('department_id'),
            'expiration_date' => $request->input('expiration_date') ? $request->input('expiration_date') : null
        ];
        $holters = $request->has('holter')? $request->input('holter'):[];
        $uers_ids = $request->input('user_ids',[]);
        $user_data=$this->userRepository->updateUser($request->input('id'),$update_data,$holters);
        $user = $this->userRepository->get($request->input('id'));
        $user->users()->sync($uers_ids);
        return redirect('/admin/member');
    }
    public function userDelete(Request $request,$id = 0)
    {
        if(!$id){
            $del_ids= $request->input('ids');
            $del_success_count=0;
            foreach ($del_ids as $key => $id) {
                $this->userRepository->deleteUser($id);
                $del_success_count++;
            }
            return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
        }
        $this->userRepository->deleteUser($id);
        return redirect('/admin/member');
        
    }
}
