<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ProgramController extends Controller
{
    public function __construct()
    {

    }
    
    public function index(Request $request)
    {
        $z2b_report_program = app_path('Http/Controllers/Analysis/z2b_report');
        $qrs_redetect_program = app_path('Http/Controllers/File/qrs_redetect');
    	$data = [
    		'data'=>null,
    		'programs'=>[
    			"z2b_report"=>file_exists($z2b_report_program),
                "qrs_redetect"=>file_exists($qrs_redetect_program)
    		]
    	];

        return view('admin.program.list', $data);
    }

    public function save(Request $request)
    {
    	if($request->hasFile('z2b_report')){
    		$program = $request->file('z2b_report');
            $program->move(app_path('Http/Controllers/Analysis'),'z2b_report');
            exec("chmod -R 777 ".app_path('Http/Controllers/Analysis/z2b_report'));
    	}

        if($request->hasFile('qrs_redetect')){
            $program = $request->file('qrs_redetect');
            $program->move(app_path('Http/Controllers/File'), 'qrs_redetect');
            exec("chmod -R 777 ".app_path('Http/Controllers/File/qrs_redetect'));
        }
    	return redirect()->back();
    }
}
