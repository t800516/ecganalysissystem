<?php

namespace App\Http\Controllers\Analysis;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ECG\Services\ECGService;

class BatchPeakController extends Controller
{

	protected $ecgService;
	
	public function __construct(ECGService $ecgService)
	{
		$this->ecgService = $ecgService;
	}

	public function showWeb(Request $request, $ecg_id)
	{
		$user = Auth::user();
		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg(null, $ecg_id, true);
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecg_id);
		}

		if ($ecg) {
			$recorded_at = strtotime($ecg->recorded_at);
			return view('analysis.batchpeak', ['ecg_id' => $ecg_id,'filename'=>$ecg->filename,'filename_ext'=>$ecg->filename_ext,'recorded_at'=>$recorded_at]);
		} else {
			return redirect('/ecg/manager');
		}
	}
	public function setPeaks(Request $request, $ecg_id)
	{
		$user = Auth::user();
		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg(null, $ecg_id, true);
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecg_id);
		}
		$peaks_time = $request->input('peakstime');
		$peak= $request->input('peak');
		if($this->ecgService->setPeaksToFile($user->id, $ecg_id, $peaks_time, $peak)){
			return response()->json(["response" => "ok"]);
		}else{
			return response()->json(["response" => "ok","error"=>1]);
		}
	}
}
