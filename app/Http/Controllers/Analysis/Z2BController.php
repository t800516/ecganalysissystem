<?php

namespace App\Http\Controllers\Analysis;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ECG\Services\ECGService;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReportFile;
use App\Mail\QTCReport;
use Storage;
use App\Jobs\RunReport;
use App\Analysis\ReportHandler;
use App\Department\Repositories\DepartmentRepository;
class Z2BController extends Controller
{

	protected $ecgService;
	
	public function __construct(ECGService $ecgService)
	{
		$this->ecgService = $ecgService;
	}

	public function getReport(Request $request, $ecgID)
	{
		$user = $request->user();
		if($user->can('user_ecg_manager')){
			$ecg = $this->ecgService->get_ecg(null, $ecgID, true);
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID);
		}
		$userID = $user->id;
		$type = $request->input('type',0);
		$department = $request->input('department',false);
		$comment = $request->input('comment',null);
		if($ecg){
			$update_data =  ['report_type'=>$type];
			if($request->has('comment')){
				$update_data['comment'] = $comment;
			}
			$ecg->update($update_data);
			if($type==3){
				$ecg->update(['report_status'=>2]);
	    		return response()->view('errors.report');
			}
			$reportHandler = new ReportHandler($ecg);
			$ecg->update(['report_status'=>3]);
			if($department == "QTC"){
				RunReport::dispatch($reportHandler, $department)->onConnection('sync');
            	$report_file_path = public_path("uploads/{$userID}/qt_report/".$ecg->filename);
	        	if(file_exists($report_file_path.'.pdf')){
	        		$new_report_file_path = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
	        		if($department == 'QTC'){
		            	$new_report_file_path.='_QTC';
	        		}
	        		if($user->can('report_origin_name')){
	        			return response()->file($report_file_path.'.pdf', [
						    'Content-Type' => 'application/pdf',
						    'Cache-Control'=>'no-store,no-cache',
						    'Content-Disposition' => 'inline; filename="'.$new_report_file_path.'"'
						]);
	        		}
	        		return response()->download($report_file_path.'.pdf', $new_report_file_path.'.pdf', [
						    'Content-Type' => 'application/pdf',
						    'Cache-Control'=>'no-store,no-cache'
						]);
	        	}else{
	        		$ecg->update(['report_status'=>0]);
	    			return response()->view('errors.report');
	        	}
			}else if($user->department && in_array($user->department->name, ['HRV', 'HRVMED'])){
                RunReport::dispatch($reportHandler, $department)->onQueue('priority_report');
            }else{
                RunReport::dispatch($reportHandler, $department)->onQueue('manual_report');
            }
            return response()->json(['id'=>$ecg->id, 'report_status'=>3]);
			
		}else{
			$ecg->update(['report_status'=>0]);
			return response()->view('errors.report');
		}
	}
	
	public function downloadReport(Request $request, DepartmentRepository $departmentRepository, $ecgID)
	{
		$user = $request->user();
		$user_role = $user->role_id;
		if($user->can('user_ecg_manager')){
			$ecg = $this->ecgService->get_ecg(null, $ecgID, true);
			$user = $ecg->user;

		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID);
		}
		$userID = $user->id;
		$type = $request->input('type',0);
		$department = $request->input('department',false);
		if($department == 'QTC'){
			$department_data = $departmentRepository->getBy(['name'=>$department],['report_types']);
		}else{
			$department_data = $user->department()->with(['report_types'])->first();
			$department = $department_data ? $department_data->name : '';
		}
		if($ecg){
			if($department == 'QTC'){
				$report_file_path = public_path("uploads/{$userID}/qt_report/".$ecg->filename);
			}else if(in_array($department, ["TWDHA", "TFDA", "TFDAHF"])){
				$data_mode = $user_role == 7 ;
				if($data_mode){
					$report_file_path = public_path("uploads/{$userID}/report/".$ecg->filename.'_TOTC');
				}else{
					$report_file_path = public_path("uploads/{$userID}/report/".$ecg->filename.'_TPOC');
				}
			}else{
				$report_file_path = public_path("uploads/{$userID}/report/".$ecg->filename);
			}

        	if(file_exists($report_file_path.'.pdf')){
        		$new_report_file_path = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
        		if($department == 'QTC'){
	            	$new_report_file_path.='_QTC';
        		}
        		if($user->can('report_origin_name')){
        			return response()->file($report_file_path.'.pdf', [
					    'Content-Type' => 'application/pdf',
					    'Cache-Control'=>'no-store,no-cache',
					    'Content-Disposition' => 'inline; filename="'.$new_report_file_path.'"'
					]);
        		}
        		return response()->download($report_file_path.'.pdf', $new_report_file_path.'.pdf', [
					    'Content-Type' => 'application/pdf',
					    'Cache-Control'=>'no-store,no-cache'
					]);
	    	}else{
	    		return response()->view('errors.report');
	    	}
		}else{
			return response()->view('errors.report');
		}
	}
	public function reportExists(Request $request, DepartmentRepository $departmentRepository, $ecgID){
		
		$user = $request->user();
		$user_role = $user->role_id;
		if($user->can('user_ecg_manager')){
			$ecg = $this->ecgService->get_ecg(null, $ecgID, true);
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID);
		}
		$userID = $user->id;

		$department = $request->input('department', false);
		if($department == 'QTC'){
			$department_data = $departmentRepository->getBy(['name'=>$department],['report_types']);
		}else{
			$department_data = $user->department()->with(['report_types'])->first();
			$department = $department_data ? $department_data->name : '';
		}

		if($ecg){
			$report_type = explode(",", $ecg->report_type);
			$template = view('analysis.report_type_inputs')->with('comment',$ecg->comment)->with('department',$department_data)->with('report_types',$report_type)->render();
			if($department == 'QTC'){
				$report_file_path = public_path("uploads/{$userID}/qt_report/".$ecg->filename);
			}else if(in_array($department, ["TWDHA", "TFDA", "TFDAHF"])){
				$data_mode = $user_role == 7;
				if($data_mode){
					$report_file_path = public_path("uploads/{$userID}/report/".$ecg->filename.'_TOTC');
				}else{
					$report_file_path = public_path("uploads/{$userID}/report/".$ecg->filename.'_TPOC');
				}
			}else{
				$report_file_path = public_path("uploads/{$userID}/report/".$ecg->filename);
			}
        	if(file_exists($report_file_path.'.pdf')){
	    		return response()->json(['exists'=>1, 'report_type_inputs'=>$template]);
        	}
	    	return response()->json(['exists'=>0, 'report_type_inputs'=>$template]);
        }

	}
	public function getReportTypeInputs(Request $request,DepartmentRepository $departmentRepository, $ecgID){
		$user = Auth::user();
		if($user->can('user_ecg_manager')){
			$ecg = $this->ecgService->get_ecg(null, $ecgID, true);
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID);
		}
		$userID = $user->id;
		$department_type = $request->input('department',false);
		if($department_type =='QTC'){
			$department = $departmentRepository->getBy(['name'=>$department_type],['report_types']);
		}else{
			$department = $user->department()->with(['report_types'])->first();
		}

		return view('analysis.report_type_inputs',["department_type"=>$department_type, 'department'=>$department, "comment"=>$ecg->comment]);
	}

	public function getQTCReport(Request $request, $ecgID)
	{
		$user = $request->user();
		if($user->can('user_ecg_manager')){
			$ecg = $this->ecgService->get_ecg(null, $ecgID, true);
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID);
		}
		$userID = $user->id;
		$department = "QTC";
		if($ecg){
			$reportHandler = new ReportHandler($ecg);
			RunReport::dispatch($reportHandler, $department)->onConnection('sync');
            $report_file_path = public_path("uploads/{$userID}/qt_report/".$ecg->filename);
	        if(file_exists($report_file_path.'.pdf')){
	        	$new_report_file_path = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
	        	$new_report_file_path.='_QTC';

	        	if($user->can('report_origin_name')){
	        		return response()->file($report_file_path.'.pdf', [
						'Content-Type' => 'application/pdf',
						'Cache-Control'=>'no-store,no-cache',
						'Content-Disposition' => 'inline; filename="'.$new_report_file_path.'"'
					]);
	        	}

	        	return response()->download($report_file_path.'.pdf', $new_report_file_path.'.pdf', [
						    'Content-Type' => 'application/pdf',
						    'Cache-Control'=>'no-store,no-cache'
					]);
	        }else{
	    		return response()->view('errors.report');
	        }
		}else{
			return response()->view('errors.report');
		}
	}
	public function sendReport(Request $request)
	{
		$this->validate($request, [
	        'ecg_id' => 'required|exists:ecgs,id',
	        'report_email' => 'required|email',
	    ]);
		$report_email = $request->input('report_email');
		$ecg_id = $request->input('ecg_id');
		$user = Auth::user();
		if($user->can('user_ecg_manager')){
			$ecg = $this->ecgService->get_ecg(null, $ecg_id, true);
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecg_id);
		}
		if($ecg){
			$report_file_path = public_path("uploads/".$ecg->user_id."/report/".$ecg->filename.".pdf");
			if(file_exists($report_file_path)){
				Mail::to($report_email)->send(new ReportFile($ecg));
				return response()->json(['send'=>1,'report'=>$ecg->filename]);
			}
			return response()->json(['send'=>0,'msg'=>$report_file_path.' reprot is not exists']);
		}
		return response()->json(['send'=>0,'msg'=>'ecg is not exists']);
	}
}
