<?php

namespace App\Http\Controllers\Analysis;

use Auth;
use Validator;
use Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;

class HRVController extends Controller
{


	protected $ecgRepository;

	public function __construct(ECGRepository $ecgRepository)
	{
		$this->ecgRepository = $ecgRepository;
	}

	public function showWeb(Request $request, $ecg_id)
	{
		if (isset($ecg_id)) {
			$user = Auth::user();
			$user_id = $user->id;
			if($user->can('manager_type')){
				$ecg = $this->ecgRepository->get($ecg_id);
				$user = $ecg->user;
				$user_id = $user->id;
			}else{
				$ecg = $this->ecgRepository->getECG($user_id, $ecg_id);
			}
			return view('analysis.hrv', ['ecg_id' => $ecg_id,'filename'=>$ecg->filename]);
		} else {
			return redirect('/ecg/manager');
		}
	}

	public function runAnalysis($ecg_id, $interval){
		if (isset($ecg_id)) {
			$user = Auth::user();
			$user_id = $user->id;
			if($user->can('manager_type')){
				$ecg = $this->ecgRepository->get($ecg_id);
				$user = $ecg->user;
				$user_id = $user->id;
			}else{
				$ecg = $this->ecgRepository->getECG($user_id, $ecg_id);
			}
			if(Auth::user()->cannot('hrv_interval_input')){
				$interval=5;
			}
			if($ecg){
				$execPath = dirname(__FILE__);
                $rr_suffix = '_rr_edited.txt';
                $hrv_suffix = '_hrv.txt';
                $bin_suffix = '.bin';

				$ecg_file_path = "uploads/{$user_id}/ecg/";
				$rr_file_path = "uploads/{$user_id}/rr/";
				$hrv_file_path = "uploads/{$user_id}/hrv/";
				if (!is_dir(public_path($hrv_file_path))) {
	                $oldmask = umask(0);
            		mkdir(public_path($hrv_file_path), 0777, true);
            		umask($oldmask);
	            }
				$rr_file_name=$rr_file_path.$ecg->filename.$rr_suffix;
				$hrv_file_name=$hrv_file_path.$ecg->filename.'_'.$interval.$hrv_suffix;
				$bin_file_name=$ecg_file_path.$ecg->filename.$bin_suffix;
				
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
					$exeFileExt='.exe';
				} else {
					$exeFileExt='';
				}
				$sample_rate = $ecg->sample_rate != 0 ? $ecg->sample_rate:'';
				$execpath="\"$execPath/HRV$exeFileExt\" \"$rr_file_name\" \"$hrv_file_name\" \"$interval\" \"$bin_file_name\" \"$sample_rate\"";
	            exec($execpath);
	            
	            $hrv_result = $this->get_hrv_data($user, $ecg->filename,$ecg->filename_ext,$interval);
	            
	            return response('{"response":"ok", "hrv_data":'.$hrv_result.'}')
					->header('Content-Type', "application/json");
			}else{
				return response('{"response":"ok", "error":"HRV data is not exist"}')
					->header('Content-Type', "application/json");
			}
		}

	}

	private function get_hrv_data($user, $filename, $filename_ext,$interval)
	{
		$user_id = $user->id;
		$hrv_suffix = '_hrv.txt';
		$hrv_file_path = "uploads/{$user_id}/hrv/";
		$hrv_file_name = $hrv_file_path . $filename .'_'. $interval . $hrv_suffix;
		//$hrv_file_name = public_path('uploads\\' . $user_id . '\\hrv\\' . $filename .'_'.$interval. $hrv_suffix);
		if(!file_exists($hrv_file_name)){
			return "[]";
		}
		
		$file_handle = fopen($hrv_file_name, "r");
		
		if ($file_handle) {
			$i=0;
			$hrv_data="";
			while (($line = fgets($file_handle)) !== false) {
				if($line == "=====\n"){
					break;
				}
				$hrv_line = explode("\t", trim($line));
				$line_data ='';
				foreach ($hrv_line as $key => $value) {
					if($key<12|| $key >16){
						$line_data .= ($key!=0?",":"")."\"{$value}\"";
					}else{
						if($user->can('hrv_excolumn')){
							$line_data .= ($key!=0?",":"")."\"{$value}\"";
							if($key==13){
								if($i==0){
									$line_data .= ($key!=0?",":"")."\"{$hrv_line[12]}/{$value}\"";
								}else{
									$line_data .= ($key!=0?",":"")."\"".(($value!='0')? number_format((float)$hrv_line[12]/(float)$value,2) : 0)."\"";
								}
							}
						}
					}
				}
				$hrv_data .= ($i!=0? ",":"")."[{$line_data}]";
				$i++;
			}
			fclose($file_handle);
			return "[{$hrv_data}]";
		} else {
			return "[]";
		}
	}

	public function export(Request $request)
	{
		$user = $request->user();
		$user_id = $user->id;
		Validator::make($request->all(), [
            'id' => 'required',
            'type' => 'required',
            'interval' => 'required'
        ])->validate();
        $interval = $request->input('interval');
        $ecg_id = $request->input('id');
        $type = $request->input('type');
		if($user->can('manager_type')){
			$ecg = $this->ecgRepository->get($ecg_id);
			$user = $ecg->user;
			$user_id = $user->id;
		}else{
				$ecg = $this->ecgRepository->getECG($user_id, $ecg_id);
		}
		if($ecg){
            $hrv_suffix = '_hrv';
			$hrv_file_name = $ecg->filename.'_'.$interval.$hrv_suffix;
			
			$hrv_result=json_decode($this->get_hrv_data($user, $ecg->filename,$ecg->filename_ext,$interval),true);
			Excel::create($hrv_file_name, function($excel) use($hrv_result) {
				$excel->sheet('SheetHRV', function($sheet) use($hrv_result) {
					foreach ($hrv_result as $key => $value) {
						$sheet->row(($key+1),$value);
					}
				});
			})->export('xls');
		}
	}
}
