<?php

namespace App\Http\Controllers\Analysis;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ECG\Services\ECGService;

class SleepController extends Controller
{

	protected $ecgService;
	
	public function __construct(ECGService $ecgService)
	{
		$this->ecgService = $ecgService;
	}

	public function showWeb(Request $request, $ecg_id)
	{
		$user_id = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($user_id, $ecg_id);
		if ($ecg) {
            $sleep_file_path = public_path("uploads/{$user_id}/sleep/");
            $sleep_filename = $sleep_file_path.$ecg->filename.'_2_Arousal_1Hz_edited.txt';
            $report_file_path = $sleep_file_path.$ecg->filename.'.pdf';
            
			$data = [
				'ecg_id' => $ecg_id,
				'filename'=> $ecg->filename,
				'filename_ext'=> $ecg->filename_ext,
				'recorded_at' => $ecg->recorded_at,
                'report_status' => file_exists($report_file_path) ? 1:0,
                'analysis_status'=> file_exists($sleep_filename) ? 1:0
			];
			return view('analysis.sleep', $data);
		} else {
			return redirect('/ecg/manager');
		}
	}

	function runSleep($ecgID)
	{
		$userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);

		if($ecg){
			$execPath = dirname(__FILE__);
            $sleep_folder_path = public_path("uploads/{$userID}/sleep/");
			if (!is_dir($sleep_folder_path)) {
                $oldmask = umask(0);
	            mkdir($sleep_folder_path, 0777, true);
                umask($oldmask);
	        }
            $exec_path = app_path('Http/Controllers/Analysis/sleep_detect');
            $rr_file_path = public_path("uploads/{$userID}/rr/".$ecg->filename."_rr_edited.txt");
            $ecg_file_path = public_path("uploads/{$userID}/ecg/".$ecg->filename);
            $sleep_file_path = $sleep_folder_path.$ecg->filename;
            $command_str = "\"".$exec_path."\" \"".$rr_file_path."\" \"".$ecg_file_path."\" \"".$sleep_file_path."\"";
            $retArr =[];
            $start_time = microtime(true);
            exec($command_str, $retArr);
            $end_time = microtime(true);

            copy($sleep_file_path.'_2_Arousal_1Hz.txt', $sleep_file_path.'_2_Arousal_1Hz_edited.txt');
            copy($sleep_file_path.'_Tag_1Hz.txt',$sleep_file_path.'_Tag_1Hz_edited.txt');

            $ecg->update(["sleep_analysis_status"=>2]);
			return response()->json(["response" => "ok"]);
		}else{
            $ecg->update(["sleep_analysis_status"=>3]);
			return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
		}
	}
	public function getReport($ecgID)
	{
		$userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);

		if($ecg){
            $report_file_path = public_path("uploads/{$userID}/sleep/".$ecg->filename);
            if(file_exists($report_file_path.'.pdf')){
                return response()->download($report_file_path.'.pdf');
            }else{
                return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
            }
		}else{
			return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
		}
	}
    public function createReport($ecgID)
    {
        $userID = Auth::user()->id;
        $ecg = $this->ecgService->get_ecg($userID, $ecgID);

        if($ecg){
            $report_file_path = public_path("uploads/{$userID}/sleep/".$ecg->filename);
            $ecg_file_path = public_path("uploads/{$userID}/sleep/".$ecg->filename);
            $info_file_path = public_path("uploads/{$userID}/ecg/".$ecg->filename."_info.txt");
            
            if (!is_dir(public_path("uploads/{$userID}/report"))) {
                $oldmask = umask(0);
                mkdir(public_path("uploads/{$userID}/report"), 0777, true);
                umask($oldmask);
            }

            $mcr_path=env('Z2B_REPORT_MCR_PATH','/usr/local/MATLAB/MATLAB_Runtime/v91');
            $exec_path = app_path('Http/Controllers/Analysis/sleep_report.sh');
            $command_str = "\"".$exec_path."\" \"".$mcr_path."\" \"".$ecg_file_path."\" \"".$info_file_path."\" \"".$report_file_path."\"";
            $set_charset = 'export LANG=en_US.UTF-8;';

            $retArr =[];
            $start_time = microtime(true);
            exec($set_charset.$command_str, $retArr);
            $end_time = microtime(true);

            if(file_exists($report_file_path.'.pdf')){
                return response()->download($report_file_path.'.pdf');
            }else{
                return response()->json(['response' => 'ok', 'errorcode' => 4, 'msg' => 'create report failed']);
            }
        }else{
            return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
        }
    }

	public function getResult($ecgID, $type = 0)
	{
		$userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);

		if($ecg){
            $sleep_file_path = public_path("uploads/{$userID}/sleep/");
            $tests = [
                    $ecg->filename.'_1_Stage_30sec.txt',
                    $ecg->filename.'_2_Arousal_1Hz_edited.txt',
                    $ecg->filename.'_3_SPO2_1Hz.txt',
                    $ecg->filename.'_4_PPG_128Hz.bin',
                    $ecg->filename.'_5_ECG_128Hz.bin',
                    $ecg->filename.'_6_HR_PTT_RespR_1Hz.txt',
                    $ecg->filename.'_6_HR_PTT_RespR_1Hz.txt',
                    $ecg->filename.'_6_HR_PTT_RespR_1Hz.txt',
                    $ecg->filename.'_7_Position_10sec.txt',
                    $ecg->filename.'_10_AirFlow_32Hz.txt'];
	        $data = '[]';
			$sleep_filename = $sleep_file_path.$tests[$type];

			if($type==6){
				$column = 1;
			}else if($type==7){
				$column = 2;
			}else{
				$column = 0;
			}
			if (file_exists($sleep_filename)) {
				$result = $this->getFile($sleep_filename, $column, pathinfo($sleep_filename)['extension']=='bin');
				$data = $result;
			}
			return response()->json(["response" => "ok",'data' => $data]);
		}else{
			return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
		}
	}
	public function getTag($ecgID, $type=0)
	{
		$userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);

		if($ecg){
            $sleep_file_path = public_path("uploads/{$userID}/sleep/");
            $tests = [$ecg->filename.'_Tag_1Hz_edited.txt'];
            
	        $data = '[]';
			$sleep_filename = $sleep_file_path.$tests[$type];
			
			if (file_exists($sleep_filename)) {
				$result = $this->getFile($sleep_filename, [0,1], pathinfo($sleep_filename)['extension']=='bin');
				$data = $result;
			}
			return response()->json(["response" => "ok",'data' => $data]);
		}else{
			return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
		}
	}

	private function getFile($filename, $column=0, $binary = false){
		$mode = $binary? "rb":"r";
		if (!$file_handle = fopen($filename, $mode)) {
            return '[]';
        }
        if($binary){
        	$filesize = filesize($filename);
        	$data_str = '';
            for($position = 0 ; $position < $filesize; $position+=4){
                if(fseek($file_handle, $position) == -1){
                    break;
                }
                $contents = fread($file_handle, 4);
                $line_array = unpack("i*", $contents);
                $data_str .= ($position!=0 ? "," : "").$line_array[1];
            }
            $result = '['.$data_str.']';
    	}else{
    		$line = fgets($file_handle);
	        $dataLine = explode("\t", trim($line));
	        if(is_array($column)){
	        	$datarow = '';
	        	foreach ($column as $key => $col) {
	        		if(isset($dataLine[$col])){
		        			$datarow .= ($key == 0 ? '':',').$dataLine[$col];
	            		}
	        	}
	        	$data = '['.$datarow.']';
	        }else{
	        	$data = $dataLine[$column];
			}
	        while ($line = fgets($file_handle)) {
	        	$dataLine = explode("\t", trim($line));
	            if(is_array($column)){
	            	$datarow = '';

	            	foreach ($column as $key => $col) {
	            		if(isset($dataLine[$col])){
	            			$datarow .= ($key == 0 ? '':',').$dataLine[$col];
	            		}
		        	}
		        	$data .= ',['.$datarow.']';
		        }else{
		        	$data .= ','.$dataLine[$column];
				}
	        }
	        $result = '['.$data.']';
    	}
        fclose($file_handle);
        return $result;
	}
	public function addPeak(Request $request, $ecgID)
    {
        $userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);
		$insertPeak = $request->input('peak');
		if($ecg){
			$sleep_file_path = public_path("uploads/{$userID}/sleep/");
            
            $testfile = $ecg->filename.'_2_Arousal_1Hz_edited.txt';
            
            $fileHandle = fopen($sleep_file_path.$testfile, "r+");

            $newContent = "";
            if ($fileHandle) {
                // Store the content till the insert point
                $peaktime = 0;
                while ($line = fgets($fileHandle)) {
                    if ($peaktime < $insertPeak) {
                        $newContent .= $line;
                    } else {
                        break;
                    }
                	$peaktime++;
                }

                $newContent .= "1" . "\n";
               
               // Store the remaining content
                while ($line = fgets($fileHandle)) {
                    $newContent .= $line;
                }

                // Clear the file before writing the new content
                ftruncate($fileHandle, 0);
                rewind($fileHandle);

                // Write the new content
                fwrite($fileHandle, $newContent);

                // Close file
                fclose($fileHandle);
                return response()->json(["response" => "ok"]);
            }else{
            	return response()->json(["response" => "error"]);
            }
        }else{
            return response()->json(["response" => "error"]);
		
        }
    }

    public function delPeak(Request $request, $ecgID)
    {
        $userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);
		$startPeak = $request->input('start');
		$endPeak = $request->input('end');
		if($ecg){
			$sleep_file_path = public_path("uploads/{$userID}/sleep/");
            
            $testfile = $ecg->filename.'_2_Arousal_1Hz_edited.txt';
            
            $fileHandle = fopen($sleep_file_path.$testfile, "r+");

            $newContent = "";
            if ($fileHandle) {
                // Store the content till the insert point
                $peaktime = 0;
                while ($line = fgets($fileHandle)) {
                    if ($peaktime < $startPeak) {
                        $newContent .= $line;
                    } else {
                    	$newContent .= "0" . "\n";
                        break;
                    }
                	$peaktime++;
                }
                for ($i = $startPeak+1; $i <= $endPeak  ; $i++) { 
                	fgets($fileHandle);
                	$newContent .= "0" . "\n";
                }
                
               // Store the remaining content
                while ($line = fgets($fileHandle)) {
                    $newContent .= $line;
                }

                // Clear the file before writing the new content
                ftruncate($fileHandle, 0);
                rewind($fileHandle);

                // Write the new content
                fwrite($fileHandle, $newContent);

                // Close file
                fclose($fileHandle);
                return response()->json(["response" => "ok",'data'=>$newContent]);
            }else{
            	return response()->json(["response" => "error"]);
            }
        }else{
            return response()->json(["response" => "error"]);
		
        }
    }
    public function addTag(Request $request, $ecgID)
    {
        $userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);
		$startTag = $request->input('start');
		$endTag = $request->input('end');
		if($ecg){
			$sleep_file_path = public_path("uploads/{$userID}/sleep/");
            
            $testfile = $ecg->filename.'_Tag_1Hz_edited.txt';
            
            $fileHandle = fopen($sleep_file_path.$testfile, "r+");

            $newContent = "";
            if ($fileHandle) {
                // Store the content till the insert point
               	$tagtime = 0;
                while ($line = fgets($fileHandle)) {
                    if ($tagtime < $startTag) {
                        $newContent .= $line;
                    } else {
                    	$newContent .= "1" . "\n";
                        break;
                    }
                	$tagtime++;
                }
                for ($i = $startTag+1; $i <= $endTag  ; $i++) { 
                	fgets($fileHandle);
                	$newContent .= "1" . "\n";
                }
               
               // Store the remaining content
                while ($line = fgets($fileHandle)) {
                    $newContent .= $line;
                }

                // Clear the file before writing the new content
                ftruncate($fileHandle, 0);
                rewind($fileHandle);

                // Write the new content
                fwrite($fileHandle, $newContent);

                // Close file
                fclose($fileHandle);
                return response()->json(["response" => "ok"]);
            }else{
            	return response()->json(["response" => "error"]);
            }
        }else{
            return response()->json(["response" => "error"]);
		
        }
    }

    public function delTag(Request $request, $ecgID)
    {
        $userID = Auth::user()->id;
		$ecg = $this->ecgService->get_ecg($userID, $ecgID);
		$startTag = $request->input('start');
		$endTag = $request->input('end');
		if($ecg){
			$sleep_file_path = public_path("uploads/{$userID}/sleep/");
            
            $testfile = $ecg->filename.'_Tag_1Hz_edited.txt';
            
            $fileHandle = fopen($sleep_file_path.$testfile, "r+");

            $newContent = "";
            if ($fileHandle) {
                // Store the content till the insert point
                $tagtime = 0;
                while ($line = fgets($fileHandle)) {
                    if ($tagtime < $startTag) {
                        $newContent .= $line;
                    } else {
                    	$newContent .= "0" . "\n";
                        break;
                    }
                	$tagtime++;
                }
                for ($i = $startTag+1; $i <= $endTag  ; $i++) { 
                	fgets($fileHandle);
                	$newContent .= "0" . "\n";
                }
                
               // Store the remaining content
                while ($line = fgets($fileHandle)) {
                    $newContent .= $line;
                }

                // Clear the file before writing the new content
                ftruncate($fileHandle, 0);
                rewind($fileHandle);

                // Write the new content
                fwrite($fileHandle, $newContent);

                // Close file
                fclose($fileHandle);
                return response()->json(["response" => "ok",'data'=>$newContent]);
            }else{
            	return response()->json(["response" => "error"]);
            }
        }else{
            return response()->json(["response" => "error"]);
		
        }
    }
}
