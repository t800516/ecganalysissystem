<?php

namespace App\Http\Controllers\Analysis;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ECG\Services\ECGService;
Use App\ECG\Repositories\ECGRepository;
use iio\libmergepdf\Merger;
use Storage;
use PDF;

class ReportController extends Controller
{

	protected $ecgService; 
	protected $ecgRepository;
	
	public function __construct(ECGService $ecgService, ECGRepository $ecgRepository)
	{
		$this->ecgService = $ecgService;
		$this->ecgRepository=$ecgRepository;
	}

	public function showWeb(Request $request, $ecg_id)
	{
		$user = $request->user();
		$user_id =$user->id;
		$ecg_info = $this->ecgService->get_ecg($user_id, $ecg_id, true);
		// $ecg_data = $this->ecgService->get_ecg_data($user_id, $ecg_id);
		// $rr_data = $this->ecgService->get_rr_data($user_id, $ecg_id);
		$ecg_summary = $this->ecgService->get_report_summary($ecg_info->user_id, $ecg_id);
		$ecg_general_profile = $this->ecgService->get_general_profile($ecg_info->user_id, $ecg_id,60*20);
		if ($ecg_info) {
			return view('analysis.report', [
				'ecg_id' => $ecg_id,
				'fileName'=>$ecg_info->filename,
				'ecgInfo'=>$ecg_info,
				'summary'=>$ecg_summary,
				// 'ecgData'=>$ecg_data,
				// 'rrData'=>$rr_data,
				'generalProfile'=>$ecg_general_profile
				]);
		} else {
			return redirect('/ecg/manager');
		}
	}

    public function getSummary(Request $request, $ecg_id)
    {
        $user_id = Auth::user()->id;

        $info = $this->ecgService->get_report_summary($user_id, $ecg_id);
        if ($info) {
            return response('{"response":"ok", "data":'.json_encode($info).'}')
                ->header('Content-Type', "application/json");
        } else {
            return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
        }
    }

    public function reportExport(Request $request, $id)
	{
		$time = time();
	    $user = $request->user();
        $user_id = $user->id;
	    $request_data = $request->only(['patient_name', 'age', 'sex', 'comment', 'hrbmp', 'vpbbmp', 'svpbbmp']);
	    $svgs = $request->input('svgs', []);
	    $svg_info = $request->input('svg_info', []);
	    $ecg_info = $this->ecgService->get_ecg($user_id, $id, true);
		$ecg_summary = $this->ecgService->get_report_summary($ecg_info->user_id, $id);
		$ecg_general_profile = $this->ecgService->get_general_profile($ecg_info->user_id, $id, 60*20);
		$ecg_info->patient_name = $request_data['patient_name'];
		$ecg_info->age = $request_data['age'];
		$ecg_info->sex = $request_data['sex'];
		$ecg_info->comment = $request_data['comment'];
		$data = [
			'ecgInfo'=>$ecg_info, 
			'summary'=>$ecg_summary,
			'generalProfile'=>$ecg_general_profile,
			'hrbmp'=>$request_data['hrbmp'],
			'vpbbmp'=>$request_data['vpbbmp'],
			'svpbbmp'=>$request_data['svpbbmp'],
			'svgs'=>$svgs,
			'svg_info'=>$svg_info
		];
	    $export_path = 'report_tmp/'.$user->id;
	    if(!Storage::disk('public')->has($export_path)){
			Storage::disk('public')->makeDirectory($export_path, 0755, true);
	    }
	    $temp_file = $export_path.'/'.$time.'.pdf';
	    $temp_path = Storage::disk('public')->path($temp_file);
		Pdf::setOptions([]);
		// return view('analysis.report_pdf', $data);
	   	
	   	$pdf = Pdf::loadView('analysis.report_pdf', $data)->setPaper('a4', 'portrait');
   		$pdf->save($temp_path);
   		// $merger->addfile($temp_path);
		// foreach ($ecg_reports as $key => $ecg_report) {
        //     $merger->addfile($ecg_report);
        // }
        // if(count($ecg_reports)){
	    //     $createdPdf = $merger->merge();
	    //     Storage::disk('public')->put($temp_file, $createdPdf);
		// }
		$pdf_filename = "{$ecg_info->filename}_report.pdf";
	   	return  response()->download($temp_path, $pdf_filename, [
						    'Content-Type' => 'application/zip',
						    'Cache-Control'=>'no-store,no-cache'
						]);
	}

    public function download(Request $request){
    	$user = $request->user();
    	$ids = $request->input('ids', []);
    	$poc_id = $request->input('poc', false);
    	$user_role = $user->role_id;
		$department_data = $user->department()->first();
		$department = $department_data ? $department_data->name : '';
		$ecgs = $this->ecgRepository->getsWith([],['id.in'=>$ids, 'user_id'=>$user->id]);
		$file_paths = [];
		foreach ($ecgs as $key => $ecg) {
			if(in_array($department, ["TWDHA", "TFDA", "TFDAHF"])){
				$data_mode = $user_role == 7;
				if($data_mode){
					$report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.'_TOTC');
				}else{
					$report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.'_TPOC');
				}
			}else{
	    		$report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.'.pdf');
	    	}

	    	if(file_exists($report_file_path)){
		    	$new_report_file_path = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
		    	if($user->can('report_origin_name')){
		    		array_push($file_paths,['origin'=> $report_file_path, 'filename'=>$ecg->filename.'.pdf', 'file'=>$ecg->filename, 'recorded_at'=>$ecg->recorded_at]);
		    	}else{
		    		array_push($file_paths,['origin'=> $report_file_path, 'filename'=>$new_report_file_path.'.pdf', 'file'=>$ecg->filename, 'recorded_at'=>$ecg->recorded_at]);
		    	}
	    	}
		}
		$file_paths = collect($file_paths);
		
		$group_files = $file_paths->groupBy(function($d){
			list($holter_str, $datetime_str) = explode('_', $d['file'], 2);
			$date = date('Ymd', strtotime($d['recorded_at']));
			return $holter_str.'_'.$date;
		});

		if($group_files->count()){
			$export_path = 'pdf_merge_tmp/'.$user->id;
		    if(!Storage::disk('public')->has($export_path)){
		         Storage::disk('public')->makeDirectory($export_path, 0755, true);
		    }

			$zip_filename = date('YmdHis').'_report.zip';
		 	$zip_file = public_path("uploads/{$user->id}/zip");
		 	if (!is_dir($zip_file)) {
		        $oldmask = umask(0);
		        mkdir($zip_file, 0777, true);
		        umask($oldmask);
		    }
		    $zip = new \ZipArchive();
			if($zip->open($zip_file.'/'.$zip_filename, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)){

				foreach ($group_files as $holter_str => $group_file) {
					if(count($group_file) > 1){
						$merger = new Merger;
						foreach ($group_file as $key => $file_path){
							$merger->addfile($file_path['origin']);
						}
						$merged_pdf = $merger->merge();

			    		$merge_file = $export_path.'/'.$holter_str.'.pdf';
		        		Storage::disk('public')->put($merge_file, $merged_pdf);
		        		$merge_path = Storage::disk('public')->path($merge_file);
						$zip->addFile($merge_path, $holter_str.'.pdf');
					}else if(count($group_file) == 1){
						$zip->addFile($group_file[0]['origin'], $group_file[0]['filename'] );
					}
				}
			    $zip->close();
			    return response()->download($zip_file.'/'.$zip_filename, $zip_filename, [
							    'Content-Type' => 'application/zip',
							    'Cache-Control'=>'no-store,no-cache'
							]);
		    }
		}
    	return response()->json(['response' => 'ok', 'status' => 'error', 'errorcode' => 1, 'msg' => 'no exist']);

    }
}
