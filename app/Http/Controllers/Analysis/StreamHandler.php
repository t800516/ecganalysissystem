<?php

namespace App\Http\Controllers\Analysis;
class StreamHandler
{
    private $streamFolder;
    private $dataFolder;
    private $wavePath;
    private $dcPath;
    private $hrsPath;
    private $waveStorePath;
    private $dcStorePath;
    private $hrsStorePath;
    private $resultPath;
    private $realtimeWavePath;
    private $timestampPath;
    private $statusPath;
    private $userID;
    private $holterIDNumber;
    private $execPath;
    private $execution;
    private $exeFileExt;
    private $timestamp;
    public function __construct($holterIDNumber)
    {
        //$this->userID = $userID;
        $this->holterIDNumber = $holterIDNumber;

        $this->streamFolder = "uploads/holter/{$holterIDNumber}/";
        if (!is_dir(public_path($this->streamFolder))) {
            $oldmask = umask(0);
            mkdir(public_path($this->streamFolder), 0777, true);
            umask($oldmask);
        }
        $this->dataFolder = $this->streamFolder.'data/';
        if (!is_dir(public_path($this->dataFolder))) {
            $oldmask = umask(0);
            mkdir(public_path($this->dataFolder), 0777, true);
            umask($oldmask);
        }
        $this->wavePath = $this->streamFolder.$holterIDNumber.'_wave.txt';
        $this->dcPath = $this->streamFolder.$holterIDNumber.'_dc.txt';
        $this->hrsPath = $this->streamFolder.$holterIDNumber.'_hrs.txt';
        $this->resultPath = $this->streamFolder.$holterIDNumber.'_result.txt';
        $this->realtimeWavePath = $this->streamFolder.$holterIDNumber.'_realtime_wave';
        $this->timestampPath = $this->streamFolder.$holterIDNumber.'_timestamp';
        $this->statusPath = $this->streamFolder.$holterIDNumber.'_status';
        $this->waveStorePath = $holterIDNumber.'_wave.txt';
        $this->dcStorePath = $holterIDNumber.'_dc.txt';
        $this->hrsStorePath = $holterIDNumber.'_hrs.txt';
        $this->resultStorePath = $holterIDNumber.'_result.txt';

        $this->execPath = dirname(__FILE__);
        $this->execution = 'vitalsign_decode';
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $this->exeFileExt='.exe';
        } else {
            $this->exeFileExt='';
        }
    }
    public function heartbeat(){
        $time = time();
        file_put_contents($this->statusPath, $time);
        return $time;
    }

    public function clearcache(){
        file_put_contents($this->wavePath, '');
        file_put_contents($this->dcPath, '');
        file_put_contents($this->hrsPath, '');
        file_put_contents($this->realtimeWavePath, '');
        return true;
    }

    public function clearheartbeat(){
        file_put_contents($this->statusPath, '');
        return true;
    }
    private function cacheWave($data){
        $row = '';
        $storeRow = '';
        $file_count = file_exists($this->wavePath) ? count(file($this->wavePath)) : 0;
        $ecg_l1_array = json_decode($data['ecg_l1']);
        $ecg_l2_array = json_decode($data['ecg_l2']);
        $ecg_l3_array = json_decode($data['ecg_l3']);
        $ppg_r_array = json_decode($data['ppg_r']);
        $ppg_ir_array = json_decode($data['ppg_ir']);
        if(!is_array($ecg_l1_array)){
            return false;
        }
        $row_count = count($ecg_l1_array);
        foreach ($ecg_l1_array as $key => $value) {
            $row .= $ecg_l1_array[$key]."\t".$ecg_l2_array[$key]."\t".$ecg_l3_array[$key]."\t".$ppg_ir_array[$key]."\t".$ppg_r_array[$key]."\n";
            $storeRow .= $this->timestamp."\t".$ecg_l1_array[$key]."\t".$ecg_l2_array[$key]."\t".$ecg_l3_array[$key]."\t".$ppg_ir_array[$key]."\t".$ppg_r_array[$key]."\n";
        }
        file_put_contents($this->realtimeWavePath, $data['ecg_l1']."\n");
        file_put_contents($this->realtimeWavePath, $data['ecg_l2']."\n", FILE_APPEND);
        file_put_contents($this->realtimeWavePath, $data['ecg_l3']."\n", FILE_APPEND);
        file_put_contents($this->realtimeWavePath, $data['ppg_r']."\n", FILE_APPEND);
        file_put_contents($this->realtimeWavePath, $data['ppg_ir']."\n", FILE_APPEND);

        if(($file_count + $row_count) > 3840){
            $fileHandle = fopen($this->wavePath, "r+");
            $file_content = '';
            if ($fileHandle) {
                $line_count=0;
                while ($line = fgets($fileHandle)) {
                    $line_count++;
                    if($row_count >= $line_count){
                        continue;
                    }else{
                        $file_content .= $line;
                    }

                }

                fclose($fileHandle);
            }
            file_put_contents($this->wavePath, $file_content.$row);
        }else{
            file_put_contents($this->wavePath, $row, FILE_APPEND);
        }
        //$this->storeData($storeRow, $this->waveStorePath);
    }

    private function cacheDc($data){
        $row = '';
        $storeRow = '';
        $file_count = file_exists($this->dcPath) ? count(file($this->dcPath)) : 0;
        $r_dc_array = json_decode($data['r_dc']);
        $ir_dc_array = json_decode($data['ir_dc']);
        $row_count = count($r_dc_array);
        if(!is_array($r_dc_array)){
            return false;
        }
        foreach ($r_dc_array as $key => $value) {
            $row .= $ir_dc_array[$key]."\t".$r_dc_array[$key]."\n";
            $storeRow .= $this->timestamp."\t".$ir_dc_array[$key]."\t".$r_dc_array[$key]."\n";
        }
        if(($file_count + $row_count) > 480){
            $fileHandle = fopen($this->dcPath, "r+");
            $file_content = '';
            if ($fileHandle) {
                $line_count=0;
                while ($line = fgets($fileHandle)) {
                    $line_count++;
                    if($row_count >= $line_count){
                        continue;
                    }else{
                        $file_content .= $line;
                    }

                }

                fclose($fileHandle);
            }
            file_put_contents($this->dcPath, $file_content.$row);
        }else{
            file_put_contents($this->dcPath, $row, FILE_APPEND);
        }
        
        //$this->storeData($storeRow, $this->dcStorePath);
    }

    private function cacheHrs($data){
        $row = '';
        $storeRow = '';
        $file_count = file_exists($this->hrsPath) ? count(file($this->hrsPath)) : 0;
        $data_keys = ['name','ID','bed_no','hr_up','hr_down','hr','respr_up','respr_down','respr','spo2_down','spo2'];
        foreach($data_keys as $key => $data_key){
            if($key){
                $row .= "\t";
            }
            $row .= $data[$data_key];
        }
        file_put_contents($this->hrsPath, $row);
        $storeRow = $row;
        $this->storeData($this->timestamp."\t".$storeRow."\n", $this->hrsStorePath);
    }

    public function cache($data, $timestamp=0){
        $this->timestamp = $timestamp;
        file_put_contents($this->timestampPath, $timestamp);
        $this->cacheWave($data);
        $this->cacheHrs($data);
        // $this->cacheDc($data);
    }

    public function storeData($row, $filePath){
        if(!file_exists($this->dataFolder.$filePath) ){
            file_put_contents($this->dataFolder.$filePath, $row, FILE_APPEND);
        }else{
            if($this->isNewFile($this->dataFolder.$filePath)){
                rename($this->dataFolder.$filePath, $this->dataFolder.date('YmdHis',$this->timestamp)."_".$filePath);
            }
            file_put_contents($this->dataFolder.$filePath, $row, FILE_APPEND);
        }
    }
    private function isNewFile($filePath){
        $newFile = false;
        $fileHandle = fopen($filePath, "r");
        if ($fileHandle) {
            if ($line = fgets($fileHandle)) {
                $line_array = explode("\t", $line);
                $prev_datetime = $line_array[0];
                if($this->timestamp - $prev_datetime > 60*60 ){
                    $newFile = true;
                }
            }
            fclose($fileHandle);
        }
        return $newFile;
    }

    public function run(){
        $ouptput = [];
        exec("\"".$this->execPath."/".$this->execution.$this->exeFileExt."\" \"".$this->wavePath."\" \"".$this->dcPath."\"", $ouptput);
        file_put_contents($this->resultPath, $ouptput);
        $result = count($ouptput) ? $ouptput[0]:"0\t0";
        $this->storeData($this->timestamp."\t".$result."\n", $this->resultStorePath);
        return $result;
    }
    
    public function getRealtimeWaveData(){
        $result = ['ecg_l1'=>'[]','ecg_l2'=>'[]','ecg_l3'=>'[]','ppg_r'=>'[]','ppg_ir'=>'[]'];
        $result_mapping = ['ecg_l1','ecg_l2','ecg_l3','ppg_r','ppg_ir'];
        $fileHandle = fopen($this->realtimeWavePath, "r");
        if ($fileHandle) {
            $line_count = 0;
            while ($line = fgets($fileHandle)) {
                $result[$result_mapping[$line_count]] = str_replace("\n", "", $line);
                $line_count++;
            }
            fclose($fileHandle);
        }
        return $result;
    }
    // 3,       4,          5,  6,          7,          8,      9,          10
    // 'hr_up','hr_down','hr','respr_up','respr_down','respr','spo2_down','spo2'
    public function getHrsBoundaryData(){
        if(!file_exists($this->hrsPath)){
            return ['hr_up'=>"0",'hr_up'=>"0", 'hr_down'=>"0", 'respr_up'=>"0", 'respr_down'=>"0", 'spo2_down'=>"0"];
        }
        $result = file_get_contents($this->hrsPath);

        $data = explode("\t", $result);

        return ['hr_up'=>$data[3],'hr_up'=>$data[3], 'hr_down'=>$data[4], 'respr_up'=>$data[6], 'respr_down'=>$data[7], 'spo2_down'=>$data[9]];

    }
    public function getHrsExData(){
        if(!file_exists($this->hrsPath)){
            return ['ID'=>"","bed_no"=>"","name"=>""];
        }
        $result = file_get_contents($this->hrsPath);

        $data = explode("\t", $result);

        return ['ID'=>$data[1],"bed_no"=>$data[2],"name"=>$data[0]];

    }
    public function getHrsData(){
        if(!file_exists($this->hrsPath)){
            return ['heart_rate'=>"-", 'spo2'=>"-", 'respiration_rate'=>"-"];
        }
        $result = file_get_contents($this->hrsPath);
        $data = explode("\t", $result);
        return ['heart_rate'=>$data[5], 'spo2'=>$data[10], 'respiration_rate'=>$data[8]];

    }
    public function getResultData(){
        if(!file_exists($this->resultPath)){
            return ['heart_rate'=>"-", 'spo2'=>"-", 'respiration_rate'=>"-"];
        }
        $result = file_get_contents($this->resultPath);
        $data = explode("\t", $result);
        return ['heart_rate'=>$data[0], 'spo2'=>$data[1], 'respiration_rate'=>"-"];

    }
    public function getTimestamp(){
        $result = file_get_contents($this->timestampPath);
        return $result;
    }

    public function islive(){
        if(!file_exists($this->statusPath)){
            return false;
        }
        $lasttime = (int)file_get_contents($this->statusPath);
        $time = time();
        return $time <= ($lasttime + 3);
    }

    public function hasHrs(){
        if(!file_exists($this->hrsPath)){
            return false;
        }
        $hrs = file_get_contents($this->hrsPath);
        return $hrs != '';
    }

    public function getHistoryHrsData($timestamp=false, $type=false){
        $type_mapping = ['hr'=>6,'respr'=>9,'spo2'=>11];
        $last_hrs = $this->getHrsExData();
        $last_ID = $last_hrs['ID'];
        $data_files = scandir($this->dataFolder);
        $data = [];
        foreach($data_files as $data_file) { 
            if( strpos($data_file, $this->hrsStorePath) !== false){
                preg_match('/^([0-9]+)_([^_]+)_hrs\.txt$/', $data_file, $matches);
                if(count($matches)){
                   $file_datetime = $matches[1];
                   if($timestamp && $timestamp > strtotime($file_datetime)){
                        continue;
                   }
                }
                $fileHandle = fopen($this->dataFolder.$data_file, 'r');
                if ($fileHandle) {
                    while ($line = fgets($fileHandle)) {
                        $line = str_replace("\n","",$line);
                        $row = explode("\t", $line);
                        if($row[2] == $last_ID){
                            if(!$timestamp || $timestamp <= $row[0]){
                                if($type){
                                    $data[] = [$row[0], $row[$type_mapping[$type]]];
                                }else{
                                    $data[] = [$row[0], $row[6], $row[9], $row[11]];
                                }
                            }
                        }
                    }
                    fclose($fileHandle);
                }
            }
        }
        return $data;
    }
}
