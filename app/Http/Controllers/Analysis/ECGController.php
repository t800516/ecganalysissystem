<?php

namespace App\Http\Controllers\Analysis;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\File\TransferHandler;
Use App\ECG\Repositories\ECGRepository;
use App\ECG\Services\ECGService;

use App\Jobs\RunReport;
use App\Analysis\ReportHandler;

class ECGController extends Controller
{

	protected $ecgService;
    protected $ecgRepository;
	
	public function __construct(ECGService $ecgService, ECGRepository $ecgRepository)
	{
		$this->ecgService = $ecgService;
        $this->ecgRepository=$ecgRepository;
	}

	public function showWeb(Request $request, $ecgID)
	{
		$user = Auth::user();
		$mode_data = $user->variables()->where('name', 'ecg_mode')->first();

		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$mode = 'one_lead';
			$user = $ecg->user;
		}else if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg(null, $ecgID, true);
			if($user->can('ecg_analysis_switch')){
				$mode = $mode_data ? $mode_data->data : 'one_lead';
			}else{
				$mode = 'holter';
			}
			$user = $ecg->user;
		}else{
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID);
			if($user->can('ecg_analysis_switch')){
				$mode = $mode_data ? $mode_data->data : 'holter';
			}else{
				$mode = 'holter';
			}
		}

		$department = $user->department()->with(['report_types'])->first();

		if ($ecg) {
			if($ecg->filename_ext!='z2b'){
				$mode = 'holter';
			}
			$ecg_report_types = explode(',', $ecg->report_type);
			$report_types = [];
			$has_v_s_peak = $this->ecgService->hasVSPeak($ecg);
			if(($has_v_s_peak & 1) == 1){
				if(!in_array('VPC', $ecg_report_types)){
					array_push($ecg_report_types, 'VPC');
	            }
			}

			if(($has_v_s_peak & 2) == 2){
				if(!in_array('APC', $ecg_report_types)){
					array_push($ecg_report_types, 'APC');
				}
			}
			foreach ($ecg_report_types as $key => $report_type) {
	            if($report_type == ''){
	                continue;
	            }
	            $report_type = strtolower($report_type);
	            if(in_array($report_type, ['0', 'n', 'normal sinus rhythm'])){
	            	if($has_v_s_peak != 0){
	            		continue;
	            	}
	                array_push($report_types, 'N');
	            }else if(in_array($report_type, ['apc', 'atrial premature contraction'])){
	                array_push($report_types, 'APC');
	            }else if(in_array($report_type, ['vpc', 'ventricular premature contraction'])){
	                array_push($report_types, 'VPC');
	            }else if(in_array($report_type, ['2','abnormal'])){
	                array_push($report_types, 'Other');
	            }else if(in_array($report_type, ['1', 'afib', 'atrial fibrillation'])){
	                array_push($report_types, 'Afib');
	            }else if(in_array($report_type, ['af', 'atrial flutter'])){
	                array_push($report_types, 'AF');
	            }else if(in_array($report_type, ['bbb', 'bundle branch block'])){
	                array_push($report_types, 'BBB');
	            }else if(in_array($report_type, ['av', 'av block'])){
	                array_push($report_types, 'AV');
	            }else if(in_array($report_type, ['bra', 'bradycardia'])){
	                array_push($report_types, 'Bra');
	            }else if(in_array($report_type, ['tac', 'tachycardia'])){
	                array_push($report_types, 'Tac');
	            }else if(in_array($report_type, ['avi', 'avii', 'av3', 'srvt', 'svt', 'ps'])){
	                array_push($report_types, strtoupper($report_type));
	            }else if(in_array($report_type, ['pause'])){
	                array_push($report_types, 'Pause');
	            }else{
	            	$av_matches = null;
	            	$tac_matches = null;
	            	$bra_matches = null;
	            	$afib_matches = null;
	            	preg_match('/^av([0-9]+)/', $report_type, $av_matches);
	            	preg_match('/^bra([0-9]+)/', $report_type, $bra_matches);
	            	preg_match('/^tac([0-9]+)/', $report_type, $tac_matches);
	            	preg_match('/^afib([0-9]+)/', $report_type, $afib_matches);
	            	if($av_matches){
	                	array_push($report_types, 'AV');
		            }else if($bra_matches){
	                	array_push($report_types, 'Bra60');
		            }else if($tac_matches){
		                array_push($report_types, 'Tac100');
		            }else if($afib_matches){
		                array_push($report_types, 'Afib');
		            }else{
		            	array_push($report_types, 'Other');
		            }
	        	}
	        }
			$types = [
				['name'=>'Normal sinus rhythm', 'value'=>'N'],
				['name'=>'Atrial Fibrillation', 'value'=>'Afib'],
				['name'=>'Atrial Flutter', 'value'=>'AF'],
				['name'=>'Atrial Premature Contraction', 'value'=>'APC'],
				['name'=>'Ventricular Premature Contraction', 'value'=>'VPC'],
				['name'=>'Bundle Branch Block', 'value'=>'BBB'],
				['name'=>'Bradycardia(<60 bpm)', 'value'=>'Bra60'],
				['name'=>'Tachycardia(>100 bpm)', 'value'=>'Tac100'],
				['name'=>'AV Block', 'value'=>'AV'],
				['name'=>'Others', 'value'=>'Other'],
				['name'=>'Poor Signal', 'value'=>'PS']
			];
			$types_2 = [
				['name'=>'Long Pause(≥ 3s)', 'value'=>'Pause'],
				['name'=>'Complete AV Block', 'value'=>'AV3'],
				['name'=>'Second degree AV block,Type 1', 'value'=>'AVI'],
				['name'=>'Second degree AV block,Type 2', 'value'=>'AVII'],
				['name'=>'Short run VT(≥3 triplet)', 'value'=>'SRVT'],
				['name'=>'Supraventricular tachycardia', 'value'=>'SVT']
			];
			return view('analysis.ecg', ['ecg_id' => $ecg->id,'filename'=>$ecg->filename,'filename_ext'=>$ecg->filename_ext,'sample_rate'=> $ecg->sample_rate,'department' => $department , 'ecg'=>$ecg, 'mode'=>$mode, 'ecg_report_types'=>$report_types, 'types'=>$types, 'types_2'=>$types_2]);
		} else {
			return redirect('/ecg/manager');
		}
	}

	public function getECGContent(Request $request, $ecgID)
	{
		$user = Auth::user();
		$sample_256 = $request->input('sample_256', true);
		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
		}
		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$user = $ecg->user;
		}
		// Construct JSON without using json_encode() to avoid exceeding memory limit
		$ecg_data = $this->ecgService->get_ecg_data($user->id, $ecgID);
		if($sample_256 === 'false'){
			$ecg_data_256 = "[]";
		}else{
			$ecg_data_256 = $this->ecgService->get_ecg_data_256($user->id, $ecgID);
		}
		if($ecg_data){
			return response('{"response": "ok", "ecgdata": ' . $ecg_data . ', "ecgdata256": ' . $ecg_data_256 . '}')
				->header('Content-Type', "application/json");
		} else {
			return response()->json(['response' => 'ok', 'ecgdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}

	public function getRRContent(Request $request, $ecgID)
	{
		$user = Auth::user();
		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
		}
		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$user = $ecg->user;
		}
		$rr_data = $this->ecgService->get_rr_data($user->id, $ecgID);
		
		if($rr_data){	
			return response('{"response":"ok", "rrdata":'.$rr_data.'}')
				->header('Content-Type', "application/json");
		} else {
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}
	public function getEventContent(Request $request, $ecgID)
	{
		$user = Auth::user();
		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
		}
		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$user = $ecg->user;
		}
		
		$event_data = $this->ecgService->get_event_data($user->id, $ecgID);
		
		if($event_data){	
			return response('{"response":"ok", "eventdata":'.$event_data.'}')
				->header('Content-Type', "application/json");
		} else {
			return response()->json(['response' => 'ok', 'eventdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}
	public function getInfo(Request $request, $ecgID)
	{
		$user = Auth::user();
		$ignore = false;
		$userID = $user->id;
		if($user->can('manager_type') || $user->can('doctor_type')){
			$ignore = true;
			$userID = null;
		}
		
		$info = $this->ecgService->get_analysis_info($userID, $ecgID, $ignore);
		if ($info) {
			return response('{"response":"ok", "data":'.$info.'}')
				->header('Content-Type', "application/json");
		} else {
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}
	public function checkPeakN(Request $request, $ecgID)
	{
		$user = Auth::user();
		$ignore = false;
		$userID = $user->id;
		if($user->can('manager_type') || $user->can('doctor_type')){
			$ignore = true;
			$userID = null;
		}
		
		$info = $this->ecgService->check_peakN($userID, $ecgID, $ignore);
		if ($info) {
			return response('{"response":"ok", "data":'.$info.'}')
				->header('Content-Type', "application/json");
		} else {
			return response()->json(['response' => 'ok', 'data' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}

	function addPeakRecord(Request $request, $ecgID)
	{
		$user = Auth::user();
		if($user->can('explorer_type')){
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 2, 'msg' => 'no permission']);
		}
		
		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true, true);
			$user = $ecg->user;
		}

		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
		}
		
		$addPeak = $this->ecgService->addPeakToFile($user->id, $ecgID, $request->input("insertPeak"), $request->input("nextPeak"));

		if ($addPeak) {
			return response()->json(["response" => "ok"]);
		} else {
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}

	function delPeakRecord(Request $request, $ecgID)
	{
		$user = Auth::user();
		if($user->can('explorer_type')){
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 2, 'msg' => 'no permission']);
		}
		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
		}

		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$user = $ecg->user;
		}
		
		$delPeak = $this->ecgService->delPeakToFile($user->id, $ecgID, $request->input("delPeak"), $request->input("nextPeak"));
		if ($delPeak) {
			return response()->json(["response" => "ok"]);
		} else {
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}
	function delPeaksRecord(Request $request, $ecgID)
	{
		$user = Auth::user();
		if($user->can('explorer_type')){
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 2, 'msg' => 'no permission']);
		}
		if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
		}
		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$user = $ecg->user;
		}
		
		$delPeak = $this->ecgService->delPeaksToFile($user->id, $ecgID, $request->input("delPeaks"), $request->input("nextPeak"));
		if ($delPeak) {
			return response()->json(["response" => "ok" , 'data'=>$request->all()]);
		} else {
			return response()->json(['response' => 'ok', 'rrdata' => [], 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}
	function runQRS($ecgID){
		$user = Auth::user();
		$userID = $user->id;
		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$user = $ecg->user;
			$userID = $user->id;
		}else if($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
			$userID = $user->id;
		}else{
			$ecg = $this->ecgService->get_ecg($userID, $ecgID);
		}

		if($ecg){
			$execPath = dirname(__FILE__);
			if($ecg->filename_ext == 'EKG' || $ecg->filename_ext == 'ekg') {
                $rr_suffix = '_rr.txt';
                $rr_edited_suffix = '_rr_edited.txt';
           	} else {
                $rr_suffix = '_rr.txt';
                $rr_edited_suffix = '_rr_edited.txt';
            }
			$rr_file_path = "uploads/{$userID}/rr/";
			
			$rr_file_name=$rr_file_path.$ecg->filename.$rr_suffix;
			$rr_edited_file_name=$rr_file_path.$ecg->filename.$rr_edited_suffix;
			
			if (copy($rr_file_name,$rr_edited_file_name)) {
				return response()->json(["response" => "ok"]);
			} else {
				return response()->json(['response' => 'ok', 'errorcode' => 2, 'msg' => 'QRS error']);
			}
		}else{
			return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
		}
	}
	function runRRAlign($ecgID){
		$user = Auth::user();
		$userID = $user->id;
		if($user->can('doctor_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, false, true);
			$user = $ecg->user;
			$userID = $user->id;
		}elseif($user->can('manager_type')){
			$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
			$user = $ecg->user;
			$userID = $user->id;
		}else{
			$ecg = $this->ecgService->get_ecg($userID, $ecgID);
		}

		if($ecg){
			$transfer_handler = new TransferHandler($userID, $ecg->filename.'.'.$ecg->filename_ext);
			//return response()->json(["response" => "ok",'data'=>$result]);
			if ($transfer_handler->run_rr_align(1, (int)($ecg->sample_rate))) {
				return response()->json(["response" => "ok"]);
			} else {
				return response()->json(['response' => 'ok', 'errorcode' => 2, 'msg' => 'RR Align error']);
			}
		}else{
			return response()->json(['response' => 'ok', 'errorcode' => 3, 'msg' => 'no exist']);
		}
	}
	function exportRR(Request $request, $id=0)
	{
		if(Auth::user()->cannot('rr_download')){
			return response()->json(["result" => "fail", "message" => "Access denied"]);
		}
		$user = Auth::user();
		$ecgIDs = $id ? [$id] : $request->input('ids',[]);
		$rrSuffix = "_rr_edited.txt";
		$zip_filename = date('YmdHis').'_rr.zip';
 		$zip_file = public_path("uploads/{$user->id}/zip");
		$zip = new \ZipArchive();

 		if (!is_dir($zip_file)) {
            $oldmask = umask(0);
            mkdir($zip_file, 0777, true);
            umask($oldmask);
        }
		if($zip->open($zip_file.'/'.$zip_filename, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)){
				foreach ($ecgIDs as $key => $ecgID) {
					$userID = $user->id;
					if($user->can('doctor_type')){
						$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true, true);
						$userID = $ecg->user_id;
					}else if($user->can('manager_type')){
						$ecg = $this->ecgService->get_ecg($user->id, $ecgID, true);
						$userID = $ecg->user_id;
					}else{
						$ecg = $this->ecgService->get_ecg($userID, $ecgID);
					}
		            $rrPath = public_path("uploads/" . $userID . "/rr/" . $ecg->filename . $rrSuffix);
					$zip->addFile($rrPath, $ecg->filename."_rr.txt");
				}
		    	$zip->close();
        		if(count($ecgIDs) > 1 ){
		    		return response()->download($zip_file.'/'.$zip_filename, $zip_filename, [
							    'Content-Type' => 'application/zip',
							    'Cache-Control'=>'no-store,no-cache'
							]);
		    	}else{
		    		if($ecg){
		    			return response()->download($rrPath, $ecg->filename."_rr.txt", [
							    'Content-Type' => 'application/zip',
							    'Cache-Control'=>'no-store,no-cache'
							]);
		    		}
		    	}
	    	}
		return response()->json(["result" => "fail", "message" => "Access denied"]);
	}

	public function checked(Request $request, $ecgID)
	{
		$user = $request->user();
		$ignore = false;
		$userID = $user->id;
		if($user->can('manager_type') || $user->can('doctor_type')){
			$ignore = true;
			$userID = null;
		}
		
		$info = $this->ecgService->setChecked($userID, $ecgID, $ignore);
		if ($info) {
			$this->ecgService->setChecker($userID, $ecgID, $ignore, $user->email);
			return response()->json(["response" => "ok" , 'status'=>"success"]);
		} else {
			return response()->json(['response' => 'ok', "status"=>"error", 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}
	public function unchecked(Request $request, $ecgID)
	{
		$user = $request->user();
		$ignore = false;
		$userID = $user->id;
		if($user->can('manager_type') || $user->can('doctor_type')){
			$ignore = true;
			$userID = null;
		}
		
		$info = $this->ecgService->setUnchecked($userID, $ecgID, $ignore);
		if ($info) {
			return response()->json(["response" => "ok" , 'status'=>"success"]);
		} else {
			return response()->json(['response' => 'ok', "status"=>"error", 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}

	public function mode(Request $request)
	{
		$user = Auth::user();
		$mode = $request->input('mode','holter');

		$mode_data = $user->variables()->where('name', 'ecg_mode')->first();
		if($mode_data){
			$mode_data->update(['data'=>$mode]);
		}else{
			$user->variables()->create(['name'=>'ecg_mode', 'data'=>$mode]);
		}
		return back();
	}

	public function confirmed(Request $request, $ecgID)
	{
		$user = $request->user();
		$ignore = false;
		$userID = $user->id;
		$assignee_type = false;
		if($user->can('manager_type')){
			$ignore = true;
			$userID = null;
		}

		if($user->can('doctor_type')){
			$ignore = true;
			$assignee_type = true;
		}

		$report_types = $request->input('report_types',[]);
		$comment = $request->input('comment','');
		$ecg = $this->ecgService->setConfirmed($userID, $ecgID, $ignore, $report_types, $comment, $assignee_type);
		if ($ecg) {
			$this->ecgService->setConfirmer($userID, $ecgID, $ignore, $user->email, $assignee_type);
			$reportHandler = new ReportHandler($ecg);
			$ecg->update(['report_status'=>3]);
			RunReport::dispatch($reportHandler, false)->onQueue('manual_report');
			return response()->json(["response" => "ok" , 'status'=>"success"]);
		} else {
			return response()->json(['response' => 'ok', "status"=>"error", 'errorcode' => 1, 'msg' => 'no exist']);
		}
	}

	public function allConfirmedCheck(Request $request)
	{
		$user = $request->user();

        if($user->can('doctor_type')){
            $where=['assignee'=>$user->id, 'confirm'=>1, 'confirm_time.>'=> date('Y-m-d 00:00:00', strtotime('-6 day')), 'confirm_check'=>0];
        }else{
            $where=['user_id'=>$user->id];
        }
		$this->ecgRepository->whereBy($where)->toUpdate(['confirm_check'=>1]);
		return redirect('/ecg/manager?confirmed=true');
	}
}
