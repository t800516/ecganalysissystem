<?php

namespace App\Http\Controllers\Apk;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apk\Repositories\ApkRepository;

class ApkController extends Controller
{
	protected $apkRepository;
    public function __construct(ApkRepository $apkRepository)
    {
    	$this->apkRepository = $apkRepository;
    }
    
    public function show($name)
    {   
        $apk = $this->apkRepository->getBy(['name'=>$name]);
        $data = [
            'apk'=>$apk
        ];
        return view('apk.download',$data);
    }
}
