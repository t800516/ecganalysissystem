<?php

namespace App\Http\Controllers\Data;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;
use App\User;
use DB;
use App\Traits\RelatedUser;
class ECGController extends Controller
{
    use RelatedUser;
    protected $ecgRepository;
    
    public function __construct(ECGRepository $ecgRepository)
    {
        $this->ecgRepository=$ecgRepository;
    }
    public function index(Request $request)
    {
        $user = $request->user();
        $allUser = DB::table('user_user')->get();
        $related_users = [$user->id];
        $this->getAllUsers($user->id, $related_users, $allUser);
        $users = User::with('department')->whereIn('id', $related_users)->get();
        foreach ($users as $key => $v) {
            if(!$v->department){
                $index = array_search($v->id, $related_users);
                if($index !== false){
                    array_splice($related_users, $index, 1);
                }
            }
        }
        $department = $user->department()->with(['report_types'])->first();
        $data=[
            'poc'=>null,
            'user'=>$request->user(),
            'related_users'=>$related_users,
            'mode'=>'all',
            'department' => $department 
        ];
        return view('data.ecglist', $data);
    }
    function allData(Request $request){
        $user = $request->user();
        $related_users = $request->input('related_users',[]);
        $allUser = User::whereIn('id', $related_users)->get();
        $where=['user_id.in'=>$related_users];

        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->ecgRepository;
        $search_fields = ['filename', 'patient_IDNumber'];
        $search_relation_fields = ['user.email'];
        $search = ""; 
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            if($order_column == 'user'){
                $orderBy['user_id'] = $order;
            }else{
                $orderBy[$order_column] = $order;
            }
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        
        $data_total = $repositoryBy->whereBy($where)->toCount();

        $tableData = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        //$data_total_filtered = $tableData->count();
        foreach ($tableData as $key => $v) {
            $ecg_user = $allUser->where('id', $v->user_id)->first();
            if($ecg_user){
                $v->user = ['email'=>$ecg_user->email];
            }else{
                $v->user = ['email'=>''];
            }
        }
        // $data_total = $repositoryBy->whereBy($where)->toCount();
        // $data_total_filtered = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->toCount();
        // $tableData = $repositoryBy->toWith(['user'])->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $data_total,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }

    function getECGsBySubUser($user, $wheres, $result_users){
        if($user->users->count()){
            $users  = $user->users()->with(['ecgs'])->get();
            foreach ($users as $key => $user_data) {
                if(!array_key_exists($user_data->id, $result_users) ){
                    $builder = $user_data->ecgs()->with('user');
                    foreach ($wheres as $key => $value) {
                        $field_array = explode('.', $key);
                        if(count($field_array)>1){
                            if($field_array[1] == 'in'){
                                $builder = $builder->whereIn($field_array[0], $value);
                            }else{
                                $builder = $builder->where($field_array[0], $field_array[1], $value);
                            }
                        }else{
                            $builder = $builder->where($key,$value);
                        }
                    }
                    $result_users[$user_data->id] =  $builder->get();
                    $result_users = $this->getECGsBySubUser($user_data, $wheres, $result_users);
                }
            }
        }
        return $result_users;
    }

    public function showWeb(Request $request, $poc_id = 0, $id = 0)
    {
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $user = $request->user()->users()->find($id);
            }else{
                $user = $request->user();
            }
        }else{
            $poc = $request->user()->users()->find($poc_id);
            $user = $poc->users()->find($id);
        }
        $department = $user ? $user->department()->with(['report_types'])->first() : null;
        $data=[
            'poc'=>$poc,
            'user'=>$user,
            'type'=>'manager',
            'mode'=>'normal',
            'department' => $department 
        ];
        return view('data.ecglist',$data);
    }
    
    public function data(Request $request, $poc_id = 0, $id = 0)
    {
        if(!$id){
            $id = $poc_id;
            $poc = null;
            $user = $request->user()->users()->find($id);
        }else{
            $poc = $request->user()->users()->find($poc_id);
            $user = $poc->users()->find($id);
        }
        $department = $user->department ? $user->department->name : '';
        $where=['user_id' => $user->id];
        $orderBy=[];
        $repositoryBy = $this->ecgRepository;
        $search_fields = ['filename', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            $orderBy[$order_column] = $order;
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('checked')){
            $where['checked'] = 1;
        }

        if($request->has('unconfirmed')){
            $where['confirm'] = 0;
        }

        if($request->has('abnormal')){
            $where['report_type.notIn'] = ['n', 'n,', 'N', 'N,', '0', '3', 'PS', 'PS,', 'ps', 'ps,'];
        }

        if($department == 'HOLTER'){
            $where['filename.notLike'] = '%_dup%';
        }

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
            if(strpos($search , '&') !== false){
               $expolode_search = explode('&', $search);
               $search = ['text'=>$expolode_search[0], 'created_at'=>$expolode_search[1]];
            }
        }
        
        $data_total = $repositoryBy->whereBy($where)->toCount();
        $data_total_filtered = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->toCount();
        $tableData = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $data_total_filtered,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }
    public function delete(Request $request, $poc_id = 0, $id = 0)
    {
        if(!$id){
            $id = $poc_id;
            $poc = null;
            $user = $request->user()->users()->find($id);
        }else{
            $poc = $request->user()->users()->find($poc_id);
            $user = $poc->users()->find($id);
        }
        $user_id = $user->id;
        Validator::make($request->all(), [
            'ecgs.*.id' => 'required'
        ])->validate();
        $del_ecgs= $request->input('ecgs');
        $del_success_count=0;
        foreach ($del_ecgs as $key => $ecg) {
            $ecg_data = $this->ecgRepository->getECG($user_id,$ecg['id']);
            if($this->del_file($user_id, $ecg_data->filename, $ecg_data->filename_ext)){
                $this->ecgRepository->delECG($ecg['id'],$user_id);
                $del_success_count++;
            }
        }
        return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
    }
    private function del_file($user_id,$filename,$filename_ext)
    {
        $del_path=public_path('uploads/'.$user_id.'/del/');

        $time=time();
        if (!is_dir($del_path.$time.'/ekg/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/ekg/', 0777, true);
            umask($oldmask);
        }
        if (!is_dir($del_path.$time.'/ecg/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/ecg/', 0777, true);
            umask($oldmask);
        }
        if (!is_dir($del_path.$time.'/rr/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/rr/', 0777, true);
            umask($oldmask);
        }
        if (!is_dir($del_path.$time.'/hrv/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/hrv/', 0777, true);
            umask($oldmask);
        }
        return $this->renameAllFile($user_id,$filename,$filename,$filename_ext,'/del/'.$time);
    }
    private function renameAllFile($user_id,$oldfilename,$filename,$filename_ext,$movepath='')
    {
        $result=true;

        $psg_suffix=['.'.$filename_ext];
        $ecg_suffix=['_32.txt','_128.txt','_256.txt','_'.strtolower($filename_ext).'_info.txt','_'.strtolower($filename_ext).'_event.txt','_1024.bin'];
        $rr_suffix=['_256_rr.txt','_256_rr_edited.txt','_256_Table201.12.1.101.3.txt'];
        $hrv_suffix=['_5_256_hrv.txt','_10_256_hrv.txt','_15_256_hrv.txt','_30_256_hrv.txt','_60_256_hrv.txt'];

        if(!$this->rename_files(public_path('uploads/'.$user_id.'/ekg/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/ekg/'.$filename),$psg_suffix)){
            //$result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/ecg/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/ecg/'.$filename),$ecg_suffix)){
            //$result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/rr/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/rr/'.$filename),$rr_suffix)){
           // $result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/hrv/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/hrv/'.$filename),$hrv_suffix)){
        }

        return $result;
    }
    private function rename_files($old_path,$current_path,$suffixs){
        $result=true;
        foreach ($suffixs as $suffix) {
            if(file_exists($old_path.$suffix)){
                rename($old_path.$suffix,$current_path.$suffix);
            }else{
                //$result=false;
            }
        }
        return $result;
    }
}
