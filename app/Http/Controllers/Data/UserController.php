<?php

namespace App\Http\Controllers\Data;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Repositories\UserRepository;
use App\Department\Repositories\DepartmentRepository;
class UserController extends Controller
{
    protected $userRepository;
    protected $departmentRepository;
    public function __construct(UserRepository $userRepository, DepartmentRepository $departmentRepository)
    {
        $this->userRepository = $userRepository;
        $this->departmentRepository = $departmentRepository;
    }
    
    public function showWeb(Request $request, $poc_id = 0)
    {
        $poc = $poc_id ? $this->userRepository->get($poc_id) : $poc_id;

        $data=[
            'poc'=>$poc
        ];

        return view('data.member',$data);
    }

    public function getlist(Request $request, $poc_id = 0){
        $user = $poc_id ? $request->user()->users()->find($poc_id) : $request->user();
    	$user_id = $user->id;
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $where=[];
        $has = ['masterUsers'=>function($query) use ($user_id) { $query->where('id',$user_id);}];
        $with=['users', 'department'];
        $module_by = $this->userRepository;
        $search_fields = ['email', 'name'];
        $search_relation_fields = ['department.name'];
        $search = ""; 

        if($request->has('sort')){
            $order_column = $request->input('sort', false);
            $order = $request->input('order');
            if($order_column){
                $orderBy[$order_column] = $order;
            }else{
                $orderBy=['created_at' =>'desc'];
            }
        }else{
            $orderBy=['created_at' =>'desc'];
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search');
        }

        $module_total = $module_by->hasBy($has)->whereBy($where)->toCount();
        $module_total_filtered = $module_by->hasBy($has)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->toCount();
        $module = $module_by->hasBy($has)->searchBy( $search_fields, $search, $search_relation_fields)->toWith($with)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        foreach ($module as $key => $userData) {
            $users = $userData->users;
            $nonCheckedCount = $userData->ecg_non_checked_count;
            foreach ($users as $key => $related_user) {
                $nonCheckedCount += $related_user->ecg_non_checked_count;
            }
            $userData->non_checked_count = $nonCheckedCount; 
            $userData->users_count = $users->count();
        }
        $data = [
            "total" => $module_total_filtered,
            "totalNotFiltered" => $module_total,
            "rows" => $module
        ];
        return response()->json($data);
    }

    public function edit(Request $request, $poc_id = 0, $id = 0)
    {
        if(!$id){
            $id = $poc_id;
            $poc = null;
        }else{
            $poc = $this->userRepository->get($poc_id);
        }
        $user = $this->userRepository->get($id);
        $user_data=[
            'poc'=>$poc,
            'id'=> $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'checked'=> $user['checked'],
            'patient_name'=> $user['patient_name'],
            'patient_IDNumber'=> $user['patient_IDNumber'],
            'sex'=> $user['sex'],
            'birthday'=> $user['birthday']
            ];
        return view('data.userForm',$user_data);
    }

    public function save(Request $request, $poc_id = 0)
    {   
        $id =  $request->input('id',0);
        $user = $poc_id ? $request->user()->users()->find($poc_id) : $request->user() ;

        if(!$id){
            $this->validator($request->all())->validate();
        }

        $update_data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'storage' => 10,
            'remark' => '',
            'checked' => $request->input('checked'),
            'role_id' => 7,
            'auto_transfer'=> 1,
            'auto_report'=> 1,
            'department_id' => $user->department_id
        ];
        if($id){
            if($request->input('name')){
                $update_data['patient_name'] = $request->input('name');
            }
            if($request->input('patient_IDNumber')){
                $update_data['patient_IDNumber'] = $request->input('patient_IDNumber');
            }
            if($request->input('sex')){
                $update_data['sex'] = $request->input('sex');
            }
            if($request->input('birthday')){
                $update_data['birthday'] = $request->input('birthday');
            }
            $user->users()->where('id',$id)->update($update_data);
        }else{
            $update_data['patient_name'] = $request->input('name');
            $update_data['patient_IDNumber'] = $request->input('patient_IDNumber');
            $update_data['sex'] = $request->input('sex') ? $request->input('sex') : '';
            $update_data['birthday'] = $request->input('birthday') ? $request->input('birthday') : '';
            $update_data['password'] = bcrypt($request->input('password'));
            $user->users()->create($update_data);
        }
        
        return $poc_id ? redirect('/data/pocs/'.$poc_id.'/users') : redirect('/data/users/manager');
    }
    public function delete(Request $request, $id = 0)
    {
        $user = $request->user();

        if(!$id){
            $del_ids = $request->input('ids',[]);
            $del_success_count=0;
            foreach ($del_ids as $key => $id) {
                $user->users()->where('id',$id)->delete();
                $del_success_count++;
            }
            return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
        }

        return redirect('/data/users/manager');
    }
    public function destroy(Request $request, $poc_id = 0 , $id = 0)
    {
        $user = $request->user();

        if(!$id){
            $del_ids = $request->input('ids',[]);
            $del_success_count=0;
            foreach ($del_ids as $key => $del_id) {
                $poc_user = $user->users()->find($poc_id);
                if($poc_user){
                    $poc_user->users()->where('id',$del_id)->delete();
                    $del_success_count++;
                }
            }
            return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
        }

        $poc_user = $user->users()->find($poc_id);
        $poc_user->users()->where('id',$id)->delete();

        return redirect('/data/pocs/'.$poc_id.'/users');
    }

    public function create(Request $request, $poc_id = 0)
    {
        $poc = $poc_id ? $this->userRepository->get($poc_id) : $poc_id;
        
        $data = [
            'poc'=>$poc
        ];
        
        return view('data.userForm', $data );
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
}
