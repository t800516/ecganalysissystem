<?php

namespace App\Http\Controllers\Data;

use Auth;
use Validator;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ECG\Services\ECGService;
use App\ECG\Repositories\ECGRepository;
use App\Traits\ServiceHandler;
use App\Traits\HolderAPI;
use iio\libmergepdf\Merger;
use Storage;
use PDF;

class ReportController extends Controller
{
    use ServiceHandler, HolderAPI;
	protected $ecgService; 
	protected $ecgRepository;
	
	public function __construct(ECGService $ecgService, ECGRepository $ecgRepository)
	{
		$this->ecgService = $ecgService;
		$this->ecgRepository=$ecgRepository;
	}

    public function download(Request $request, $id){
    	$user = $request->user();
    	$user_role = $user->role_id;
    	$ids = $request->input('ids', []);
		$department_data = $user->department()->first();
    	$department = $department_data ? $department_data->name : '';
		$ecgs = $this->ecgRepository->getsWith([],['id.in'=>$ids, 'user_id'=>$id]);

		$file_paths = [];
		foreach ($ecgs as $key => $ecg) {
			if(in_array($department, ["TWDHA", "TFDA", "TFDAHF"])){
				$data_mode = $user_role == 7;
				if($data_mode){
					$report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.'_TOTC.pdf');
				}else{
					$report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.'_TPOC.pdf');
				}
			}else{
    			$report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename.'.pdf');
    		}
    		if(file_exists($report_file_path)){
		    	if($user->can('report_origin_name')){
		    		array_push($file_paths,['origin'=> $report_file_path, 'filename'=>$ecg->filename.'.pdf', 'file'=>$ecg->filename, 'recorded_at'=>$ecg->recorded_at]);
		    	}else{
		    		$new_report_file_path = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
		    		array_push($file_paths,['origin'=> $report_file_path, 'filename'=>$new_report_file_path.'.pdf', 'file'=>$ecg->filename, 'recorded_at'=>$ecg->recorded_at]);
		    	}
	    	}
	   }
	   $file_paths = collect($file_paths);
		
		$group_files = $file_paths->groupBy(function($d){
			list($holter_str, $datetime_str) = explode('_', $d['file'], 2);
			$date = date('Ymd', strtotime($d['recorded_at']));
			return $holter_str.'_'.$date;
		});
		if($group_files->count()){
			$export_path = 'pdf_merge_tmp/'.$user->id;
		   if(!Storage::disk('public')->has($export_path)){
		      Storage::disk('public')->makeDirectory($export_path, 0755, true);
		   }

			$zip_filename = date('YmdHis').'_report.zip';
		 	$zip_file = public_path("uploads/{$user->id}/zip");
		 	if (!is_dir($zip_file)) {
		        $oldmask = umask(0);
		        mkdir($zip_file, 0777, true);
		        umask($oldmask);
		    }
		   $zip = new \ZipArchive();
			if($zip->open($zip_file.'/'.$zip_filename, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)){

				foreach ($group_files as $holter_str => $group_file) {
					if(count($group_file) > 1){
						$merger = new Merger;
						foreach ($group_file as $key => $file_path){
							$merger->addfile($file_path['origin']);
						}
						$merged_pdf = $merger->merge();

			    		$merge_file = $export_path.'/'.$holter_str.'.pdf';
		        		Storage::disk('public')->put($merge_file, $merged_pdf);
		        		$merge_path = Storage::disk('public')->path($merge_file);
						$zip->addFile($merge_path, $holter_str.'.pdf');
					}else if(count($group_file) == 1){
						$zip->addFile($group_file[0]['origin'], $group_file[0]['filename'] );
					}
				}
			   $zip->close();
			   if(file_exists($zip_file.'/'.$zip_filename)){
		    		return response()->download($zip_file.'/'.$zip_filename, $zip_filename, [
							    'Content-Type' => 'application/zip',
							    'Cache-Control'=>'no-store,no-cache'
							]);
		    	}
		   }
		}
    	return response()->json(['response' => 'ok', 'status' => 'error', 'errorcode' => 1, 'msg' => 'no exist']);

    }

	public function summary(Request $request, $id)
	{
	   $user = $request->user();
	   $user_role = $user->role_id;
	   $ids = $request->input('ids', []);
		$ecgs = $this->ecgRepository->getsWith([],['id.in'=>$ids, 'user_id'=>$id], ['recorded_at'=>'asc']);
		$p_user = User::find($id);
		$reports = [];
		$report_ids = '';
		$record_normal = 0;
		$record_abnormal = 0;
		$record_noisy = 0;
		foreach ($ecgs as $key => $ecg) {
			$recorded_date = date('Ymd', strtotime($ecg->recorded_at));
				$department_data = $ecg->user->department()->first();
	    		$department = $department_data ? $department_data->name : '';
				if(in_array($department, ["TWDHA", "TFDA", "TFDAHF"])){
					$data_mode = $user_role == 7;
					if($data_mode){
						$report_file_path = "uploads/{$ecg->user_id}/report/".$ecg->filename.'_TOTC.pdf';
					}else{
						$report_file_path = "uploads/{$ecg->user_id}/report/".$ecg->filename.'_TPOC.pdf';
					}
				}else{
	    			$report_file_path = "uploads/{$ecg->user_id}/report/".$ecg->filename.'.pdf';
	    		}
	    		$report_ids .= ($key !== 0 ? ',':'') . $ecg->id;
	    		$reports[] = ['id'=>$ecg->id, 'path'=>public_path($report_file_path), 'url'=>url($report_file_path), 'report_name'=>$ecg->filename.'.pdf'];
	    		if($ecg->comment && $ecg->comment!=''){
	    			if(strtolower(trim($ecg->comment)) == 'nsr'){
		    			$record_normal++;
		    		}else if(strtolower(trim($ecg->comment)) == 'artifact'){
		    			$record_noisy++;
		    		}else{
		    			$record_abnormal++;
		    		}
	    		}
	    	}
	   $start_time = strtotime($ecgs->min('recorded_at'));
	   $end_time = strtotime($ecgs->max('recorded_at'));
	   $start_date = date('Y/m/d', $start_time);
		$end_date  = date('Y/m/d', $end_time);
	   $test_days = floor((strtotime($end_date) - strtotime($start_date))/ (60*60*24)) + 1;
	   $finding = (
	   	count($reports) > 0 ? 
		   	"Total ".count($reports)." event".(count($reports) > 1 ? 's':'')." within ".$test_days." day".($test_days > 1 ? 's':'').".\n"
		   	:
		   	""
		  	).
	   	($record_normal > 0 ?
	   		$record_normal." event".($record_normal> 1 ? 's':'')." were NSR.\n"
	   		:
	   		"").
	   	($record_noisy > 0 ? 
	   		$record_noisy." event".($record_noisy>1 ? 's':'')." were artifact.":"");

	   $data = [
	    	'id'=>$id,
	    	'report_ids'=>$report_ids,
	    	'reports' =>$reports,
	    	'name'=>$p_user? $p_user->name:'',
			'test_date'=> $start_date.(($end_date && $start_date != $end_date) ? ('~'.$end_date):''),
			'min_hr'=>$ecgs->filter(function($d){return $d->HR != '-999' && $d->HR != '';})->min('HR') ?? '',
			'max_hr'=>$ecgs->filter(function($d){return $d->HR != '-999' && $d->HR != '';})->max('HR') ?? '',
			'record_normal'=>$record_normal,
			'record_noisy'=>$record_noisy,
			'record_abnormal'=>$record_abnormal,
			'test_days'=>$test_days,
			'finding' =>$finding
	    ];
	    return view('data.summary', $data);
	}

	public function summaryExport(Request $request, $id)
	{
		$time = time();
		$merger = new Merger;
	    $user = $request->user();
	    $user_role = $user->role_id;
	    $request_data = $request->only(['hospital', 'hospital_en', 'physician', 'indications', 'report_number', 'finding', 'test_date', 'record_normal', 'record_abnormal', 'record_noisy','min_hr' ,'max_hr']);
	   $ids = explode(',', $request->input('ids', ''));
		$ecgs = $this->ecgRepository->getsWith([],['id.in'=>$ids, 'user_id'=>$id], ['recorded_at'=>'asc']);
		$service = null;
		$service_ecg = null;
		$ecg_reports = [];
		foreach ($ecgs as $key => $ecg) {
			if(!$service){
				$service = $ecg->service;
			}
			if(!$service_ecg){
				$service_ecg = $ecg;
			}
	    	$department_data = $ecg->user->department()->first();
	    	$department = $department_data ? $department_data->name : '';
			if($ecg->report_status == 2)
            {
            	if(in_array($department, ["TWDHA", "TFDA", "TFDAHF"])){
						$data_mode = $user_role == 7;
						if($data_mode){
							$report_file_path = "uploads/{$ecg->user_id}/report/".$ecg->filename.'_TOTC.pdf';
						}else{
							$report_file_path = "uploads/{$ecg->user_id}/report/".$ecg->filename.'_TPOC.pdf';
						}
					}else{
		    			$report_file_path = "uploads/{$ecg->user_id}/report/".$ecg->filename.'.pdf';
		    		}
                $report_file_path = public_path($report_file_path);
                if(file_exists($report_file_path)){
                    array_push($ecg_reports, $report_file_path);
                }
            }
		}
		$serial_number = $service ? $service->serial_number : ($service_ecg ? $service_ecg->service_SN : '');
		// $serial_number = "HAN_E000006";

	   $profile = $this->getHolderProfile($serial_number);
	   if(!$profile){
	   	return redirect()->back()->withErrors(['msg' => '個人資料取得失敗，請重新嘗試。'])->withInput();
	   }

      $export_path = 'report_tmp/'.$user->id;
      if(!Storage::disk('public')->has($export_path)){
         Storage::disk('public')->makeDirectory($export_path, 0755, true);
      }
      $temp_file = $export_path.'/'.$time.'.pdf';
      $temp_path = Storage::disk('public')->path($temp_file);
      
      $data_user = User::find($id);
		$data_department = $data_user->department()->first();
		// $data = $this->summaryECGs($ecgs, $data_department);
		$data = $profile;
		$data['sn'] = $serial_number;
		$data['height'] = '';
		$data['weight'] = '';
		$data['record_num'] = $ecgs->count();

		$data['age'] = $data['age'] != '' ? $data['age'] : ($data['birthday'] != '' ? date_diff(date_create($data['birthday']), date_create('today'))->y :'');

		$data = array_merge($data, $request_data);
		// Pdf::setOptions([]);
		// return view('data.summary_pdf', $data);
	   $pdf = Pdf::loadView('data.summary_pdf', $data)->setPaper('a4', 'portrait');
   	$pdf->save($temp_path);
   	$merger->addfile($temp_path);
		foreach ($ecg_reports as $key => $ecg_report) {
            $merger->addfile($ecg_report);
        }
        if(count($ecg_reports)){
	        $createdPdf = $merger->merge();
	        Storage::disk('public')->put($temp_file, $createdPdf);
		}
		// $pdf_filename = ($request_data['hospital'] ? $request_data['hospital'].'_'.$patient_name : $patient_name).'.pdf';
		$pdf_filename = "{$data['report_number']}.pdf";
	   return  response()->download($temp_path, $pdf_filename, [
						    'Content-Type' => 'application/zip',
						    'Cache-Control'=>'no-store,no-cache'
						]);
	}

	function birthdayHandler($date)
    {
        if(strlen($date)==6){
           return ((int)substr($date, 0,2)+1911).'-'.substr($date, 2,2).'-'.substr($date, 4,2);
        }else if(strlen($date)==7){
           return ((int)substr($date, 0,3)+1911).'-'.substr($date, 2,2).'-'.substr($date, 4,2);
        }else if(strlen($date)<=5){
           return '';
        }else if(strlen($date) > 10){
           return '';
        }else if(!$date){
           return '';
        }
        return $date;
    }
}