<?php

namespace App\Http\Controllers\Data;

use Auth;
use Illuminate\Http\Request;
use App\Jobs\RunTransfer;
use App\Http\Controllers\Controller;
use App\Http\Controllers\File\UploadHandler;
Use App\ECG\Repositories\ECGRepository;
Use App\Holter\Repositories\HolterRepository;

class UploadController extends Controller
{
    protected $ecgRepository;
    protected $holterRepository;

    public function __construct(ECGRepository $ecgRepository, HolterRepository $holterRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->holterRepository = $holterRepository;
    }

    public function showWeb(Request $request, $poc_id=0, $id=0)
    {
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $user = $request->user()->users()->find($id);
            }else{
                $user = $request->user();
            }
        }else{
            $poc = $request->user()->users()->find($poc_id);
            $user = $poc->users()->find($id);
        }
        $data=[
            'poc'=>$poc,
            'user'=>$user,
            'type'=>'manager'
        ];
        return view('file.upload',$data);
    }

    public function save(Request $request, $poc_id=0, $id=0)
    {
        if(!$id){
            if($poc_id){
                $id = $poc_id;
                $poc = null;
                $user = $request->user()->users()->find($id);
            }else{
                $user = $request->user();
            }
        }else{
            $poc = $request->user()->users()->find($poc_id);
            $user = $poc->users()->find($id);
        }
        if(!$user){
            return response()->json(['error'=>'error']);
        }
        $holter_number = $user->holters()->count();
        $holters = $user->holters;
        $storage = $user->storage;
        $file_size =  $request->file('files')[0]->getClientSize();
        $user_id = $user->id;
        $file_total_size=$this->ecgRepository->getECGTotalSize($user_id);
        if(($holter_number*15+$storage)*1024*1024*1024 < $file_total_size + $file_size){
            return response()->json(['files'=>[['name'=> $request->file('files')[0]->getClientOriginalName(),'error'=>'儲存空間不足。']]]);
        }else{
            $upload_handler = new UploadHandler(['user_id'=>$user_id]);

            if (!isset($upload_handler->response["files"][0]->error)) {
               $ecg_data = [
    				"user_id" => $user_id,
    				"filename" => $upload_handler->getFileName(),
                    "filename_ext" => $upload_handler->getFileNameEXT(),
                    "filesize" => $upload_handler->getFileSize(),
    				"description" => ($request->input('description') != null) ? $request->input('description') : '',
                    "analysis_status" => 0,
                    "report_type"=>"-1"
    			];
    			$ecg = $this->ecgRepository->insertECG($ecg_data);
                if( $user->auto_transfer == 1){
                    RunTransfer::dispatch($ecg->id)->onQueue('transfer');
                }
    		}
        }
    }
}
