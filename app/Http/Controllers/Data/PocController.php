<?php

namespace App\Http\Controllers\Data;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User\Repositories\UserRepository;
use App\Department\Repositories\DepartmentRepository;
class PocController extends Controller
{
    protected $userRepository;
    protected $departmentRepository;
    public function __construct(UserRepository $userRepository, DepartmentRepository $departmentRepository)
    {
        $this->userRepository = $userRepository;
        $this->departmentRepository = $departmentRepository;
    }
    public function index(Request $request){
        $user = $request->user();
        $data = [
            'type'=>'MGR',
            'users'=>$user->users
        ];
        return view('data.manager', $data );
    }
    public function showWeb(Request $request)
    {   
        if(Auth::user()->can('data_manager_model')){
            session(['manager_model' => 'data']);
        }
        $data = [
            'type'=>'MGR',
            'user_manager_type' => Auth::user()->role_id != 4
        ];
        return view('data.member', $data );
    }

    public function getlist(Request $request){
        $user = $request->user();
        $user_id = $user->id;
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $where=[];
        $has = ['masterUsers'=>function($query) use ($user_id) { $query->where('id',$user_id);}];
        $with=['users', 'department'];
        $module_by = $this->userRepository;
        $search_fields = ['email', 'name'];
        $search_relation_fields = ['department.name'];
        $search = ""; 

        if($request->has('sort')){
            $order_column = $request->input('sort', false);
            $order = $request->input('order');
            if($order_column){
               if($order_column == 'department'){
                    $with['department'] = function($query)use($order){
                        $query->orderBy('name',$order);
                    };
                }else{
                    $orderBy[$order_column] = $order;
                }
            }else{
                $orderBy=['created_at' =>'desc'];
            }
        }else{
            $orderBy=['created_at' =>'desc'];
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search');
        }

        $module_total = $module_by->hasBy($has)->whereBy($where)->toCount();
        $module_total_filtered = $module_by->hasBy($has)->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->toCount();
        $module = $module_by->hasBy($has)->searchBy( $search_fields, $search, $search_relation_fields)->toWith($with)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        foreach ($module as $key => $userData) {
            $users = $userData->users;
            $nonCheckedCount = $userData->ecg_non_checked_count;
            foreach ($users as $key => $related_user) {
                $nonCheckedCount += $related_user->ecg_non_checked_count;
            }
            $userData->non_checked_count = $nonCheckedCount;
            $userData->users_count = $users->count();
        }
        $data = [
            "total" => $module_total_filtered,
            "totalNotFiltered" => $module_total,
            "rows" => $module
        ];
        return response()->json($data);
    }

    public function edit($id)
    {
        $user = $this->userRepository->get($id);
        $user_data=[
            'type'=>'MGR',
            'id'=> $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'checked'=> $user['checked'],
            ];
        return view('data.userForm', $user_data);
    }

    public function save(Request $request)
    {   
        $id =  $request->input('id',0);
        $user = $request->user();
        if(!$id){
            $this->validator($request->all())->validate();
        }
        $otc_department = $this->departmentRepository->getBy(['name'=>'OTC']);
        $update_data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'storage' => 10,
            'remark' => '',
            'checked' => $request->input('checked'),
            'role_id' => 8,
            'auto_transfer'=> 1,
            'auto_report'=> 1,
            'department_id' =>$user->department_id
        ];
        if($id){
            $user->users()->where('id',$id)->update($update_data);
        }else{
            $update_data['password'] = bcrypt($request->input('password'));
            $user->users()->create($update_data);
        }
        return redirect('/data/pocs/manager');
    }
    public function delete(Request $request,$id = 0)
    {
        $user = $request->user();
        if(!$id){
            $del_ids= $request->input('ids');
            $del_success_count=0;
            foreach ($del_ids as $key => $id) {
                $poc_user = $user->users()->find($id);
                if($poc_user){
                    //$del_success_count+=$poc_user->users->count();
                    //$poc_user->users()->delete();
                    $poc_user->delete();
                    $del_success_count++;
                }
            }
            return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
        }

        $user->users()->where('id',$id)->delete();

        return redirect('/data/pocs/manager');
        
    }
    public function create()
    {
        $data = [
            'type'=>'MGR'
        ];
        return view('data.userForm', $data );
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
}
