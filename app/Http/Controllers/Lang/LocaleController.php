<?php

namespace App\Http\Controllers\Lang;
use App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class LocaleController extends Controller
{
    public function __construct()
    {

    }
    
    public function setLang(Request $request,$locale)
    {
		$request->session()->put('locale',$locale);
		return redirect()->back();
    }
    
}
