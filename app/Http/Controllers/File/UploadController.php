<?php

namespace App\Http\Controllers\File;

use Auth;
use App\Traits\SlugTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;
Use App\Holter\Repositories\HolterRepository;
use App\Jobs\RunTransfer;

class UploadController extends Controller
{
    use SlugTrait;
    protected $ecgRepository;
    protected $holterRepository;

    public function __construct(ECGRepository $ecgRepository, HolterRepository $holterRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->holterRepository = $holterRepository;
    }

    public function showWeb()
    {
        return view('file.upload');
    }

    public function save(Request $request)
    {   
        $user = $request->user();
        $holter_number = $user->holters()->count();
        $holters = $user->holters;
        $storage = $user->storage;
        $file_size =  $request->file('files')[0]->getClientSize();
        $user_id = $user->id;
        $department_name = $user->department ? $user->department->name : '';
        $file_total_size=$this->ecgRepository->getECGTotalSize($user_id);
        if(($holter_number*15+$storage)*1024*1024*1024 < $file_total_size + $file_size){
            return response()->json(['files'=>[['name'=> $request->file('files')[0]->getClientOriginalName(),'error'=>'儲存空間不足。']]]);
        }else{
            $upload_handler = new UploadHandler(['user_id'=>$user_id]);

            if (!isset($upload_handler->response["files"][0]->error)) {
               $ecg_data = [
    				"user_id" => $user_id,
    				"filename" => $upload_handler->getFileName(),
                    "filename_ext" => $upload_handler->getFileNameEXT(),
                    "filesize" => $upload_handler->getFileSize(),
    				"description" => ($request->input('description') != null) ? $request->input('description') : '',
                    "analysis_status" => 0,
                    "report_type"=>"-1",
                    "priority"=>0,
    			];
                if(in_array($department_name, ['TZUCHI'])){
                    $ecg_data["slug"] = $this->generateSlug();
                }

    			$ecg = $this->ecgRepository->insertECG($ecg_data);
                
                if( $user->auto_transfer == 1){
                    RunTransfer::dispatch($ecg->id)->onQueue('transfer');
                }
    		}
        }
    }
}
