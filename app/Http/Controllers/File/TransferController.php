<?php

namespace App\Http\Controllers\File;

use Auth;
use App\Analysis\ReportHandler;
use App\Jobs\RunTransfer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;
Use App\Holter\Repositories\HolterRepository;

class TransferController extends Controller
{
    protected $ecgRepository;
    protected $holterRepository;

    public function __construct(ECGRepository $ecgRepository, HolterRepository $holterRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->holterRepository = $holterRepository;
    }

    public function run(Request $request)
    {
        $user = Auth::user();
        $holters = $user->holters;
        $user_id = $user->id;
        $ecg_ids = $request->input('ecg_ids',[]);
        $result = [];
        foreach ($ecg_ids as $key => $ecg_id) { 
            if($user->can('manager_type')){
                $ecg = $this->ecgRepository->get($ecg_id);
                $user = $ecg->user;
                $user_id = $user->id;
                $holters = $user->holters;
            }else{
                $ecg = $this->ecgRepository->getECG($user_id, $ecg_id);
            }
            $ecg_data = [
                        "analysis_status" => 93,
                    ];
            $this->ecgRepository->setInfo($ecg_id, $ecg_data);
            $this->output_msg($ecg->id, $ecg_data);
            RunTransfer::dispatch($ecg->id)->onQueue('manual_transfer');
            
            array_push($result, ['id'=>$ecg->id, 'status'=>$ecg_data["analysis_status"]]);
            
        }
        return response()->json($result);
    }
    function output_msg($id, $data){
        echo json_encode(['id'=>$id, 'status'=>$data["analysis_status"], 'data'=>$data]);
        ob_flush(); 
        flush();
        sleep(1);
    }
}
