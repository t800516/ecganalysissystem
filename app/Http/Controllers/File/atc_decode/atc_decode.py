import argparse
from atc.atc_decode import atcdecode
def main():
	argparser = argparse.ArgumentParser()
	argparser.add_argument("file_path", help ="atc path", nargs='?', default = None)
	argparser.add_argument("output_path", help ="output path", nargs='?', default = None)
	argparser.add_argument("rr_path", help ="rr path", nargs='?', default = None)
	args = argparser.parse_args()

	atcdecode(args.file_path, args.output_path, args.rr_path)

if __name__ == '__main__':
	main()