# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 16:44:19 2025

@author: BOX
"""

import numpy as np
import scipy.signal as signal
import struct
import os
from datetime import datetime

from atc import atc_reader
from atc.atc_reader import ATCReader


def process_ecg_data(reader, output_path):
    """
    讀取 ECG 訊號，進行重新取樣、濾波，並存成 .bin 和 .txt 檔案。

    參數：
        reader      - ATCReader 物件 (已開啟 .atc 檔)
        output_path  - 完整的輸出路徑（包含檔名，不含副檔名）
    """
    if reader.status() != atc_reader.READ_SUCCESS:
        print("讀取 ATC 檔案失敗！")
        return
    
    # 讀取導聯數量 & 取樣率
    num_leads = reader.num_leads()
    original_fs = reader.sample_rate_hz()
    target_fs = 256  # 目標取樣率    

    #print(f"導聯數量: {num_leads}, 原始取樣率: {original_fs} Hz")

    # 嘗試讀取最多 6 個導聯
    lead_signals = []
    for i in range(1, min(num_leads, 6) + 1):  # 確保最多讀取 6 個
        lead_signals.append(reader.get_ecg_samples(i))
        
    # === 2. 內插函式（線性內插）===
    def resample_ecg(signal_data, original_fs, target_fs):
        original_time = np.linspace(0, len(signal_data) / original_fs, num=len(signal_data), endpoint=False)
        new_time = np.linspace(0, original_time[-1], num=int(len(signal_data) * target_fs / original_fs), endpoint=False)
        resampled_signal = np.interp(new_time, original_time, signal_data)
        return resampled_signal
   
    # 重新取樣所有可用的導聯
    lead_resampled = [resample_ecg(lead, original_fs, target_fs) for lead in lead_signals]

    # === 3. Butterworth 濾波器 (0.5 ~ 35 Hz) ===
    def butterworth_filter(signal_data, lowcut=0.5, highcut=35, fs=256, order=2):
        nyquist = 0.5 * fs  # 計算 Nyquist 頻率
        low = lowcut / nyquist
        high = highcut / nyquist
        b, a = signal.butter(order, [low, high], btype='band')  # 設定 Butterworth 濾波器
        filtered_signal = signal.filtfilt(b, a, signal_data)  # 前向 + 反向濾波
        return filtered_signal
    # 濾波所有導聯
    lead_filtered = [butterworth_filter(lead) for lead in lead_resampled]
    
    # === 4. 確保存成固定 5 個通道 ===
    relen = len(lead_filtered[0])  # 使用第一個導聯的長度作為基準
    ecg_bin_data = np.zeros((relen, 5), dtype=np.int32)  # 固定存 5 通道


    # === 5. 填充前 5 個導聯的數據 ===
    for i in range(min(num_leads, 5)):  # 只存前 5 個導聯
        ecg_bin_data[:, i] = (lead_filtered[i] *reader.resolution() /1000).astype(np.int32)  # 轉換為 uV

    
    # 確保輸出目錄存在
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
    # 設定輸出檔案名稱
    bin_filename = output_path + ".bin"
    txt_filename = output_path + "_256Hz.txt"


    # === 6.1 存成二進制 .bin 檔 ===

    with open(bin_filename, "wb") as f_bin:
        for row in ecg_bin_data:
            f_bin.write(struct.pack("5i", *row))  # 5 通道，每行寫入 5 個 int32
    #print(f"ECG 二進制檔案已儲存: {bin_filename}")

    # === 6.2 額外存 ECG1 到 .txt 檔 ===

    with open(txt_filename, "w") as f_txt:
        for kk in range(relen):
            if num_leads == 1:
                f_txt.write(f"{ecg_bin_data[kk, 0]}\n")  # 只存 Lead I
            else:
                f_txt.write(f"{ecg_bin_data[kk, 1]}\n")  # 只存 Lead I
    
    return bin_filename, txt_filename


def save_info(reader, output_path):
    """
    儲存裝置資訊到 *_info.txt 檔案。
    
    參數：
        reader       - ATCReader 物件 (已開啟 .atc 檔)
        output_path  - 完整的輸出路徑（包含檔名，不含副檔名）

    回傳：
        info_filename - 存儲的 .txt 檔案完整路徑
    """
    if reader.status() != atc_reader.READ_SUCCESS:
        print("讀取 ATC 檔案失敗！")
        return None

    # 取得裝置序號、紀錄時間、取樣率
    device_sn = reader.device_data()  # 手環序號
    date_recorded = reader.date_recorded()  # 紀錄時間 (ISO 8601 格式)
    sampling_rate = 256  # reader.sample_rate_hz()  # 取樣率
    #print(date_recorded)
    # 轉換時間格式（從 ISO 8601 轉為 Unix Timestamp）
    # 嘗試解析日期格式
    date_formats = [
        "%Y-%m-%dT%H:%M:%S%z",       # 2025-01-15T23:42:48+0800
        "%Y-%m-%dT%H:%M:%S.%f%z",     # 2016-06-01T18:15:25.777-04:00
        "%Y-%m-%dT%H:%M:%S",          # 沒有時區
        "%Y-%m-%dT%H:%M:%S.%f",       # 沒有時區但有毫秒
        ]

    dt = None
    for fmt in date_formats:
        try:
            dt = datetime.strptime(date_recorded, fmt)
            break  # 成功解析則跳出迴圈
        except ValueError:
            continue  # 嘗試下一種格式
    #unix_timestamp = int(dt.timestamp())  # 轉換為 Unix 時間（秒）   
    # **依據輸入的時區，自動調整為 UTC 時間**
    if dt is None:
      print(f"無法解析日期格式: {date_recorded}")
      return None
    dt_adjusted = dt + dt.utcoffset()  # 轉換為 UTC 因為系統上為GMT
    #dt_adjusted = dt - dt.utcoffset()  # 合理轉換
    # 轉換為 Unix 時間戳記（秒）
    unix_time_adjusted = int(dt_adjusted.timestamp())

    # 確保輸出目錄存在
    os.makedirs(os.path.dirname(output_path), exist_ok=True)

    # 設定輸出檔案名稱（資訊檔案）
    info_filename = output_path + "_info.txt"

    # 寫入資料
    with open(info_filename, "w") as f_info:
        f_info.write(f"{device_sn}\n")
        f_info.write(f"{unix_time_adjusted}\n")
        f_info.write(f"{sampling_rate}\n")

    #print(f"裝置資訊已儲存: {info_filename}")

    return info_filename




def save_rr_annotations(reader, rrfilename):
    """
    儲存 RR 間期資訊到 *_rr.txt 檔案。
    
    參數：
        reader       - ATCReader 物件 (已開啟 .atc 檔)
        output_path  - 完整的輸出路徑（包含檔名，不含副檔名）

    回傳：
        rr_filename - 存儲的 .txt 檔案完整路徑
    """
    # Beat Type
    # - 0 = Unknown
    # - 1 = Normal beat
    # - 2 = Ventricular ectopic beat
    # - 3 = Atrial ectopic beat (not used)

    offsets, beat_types = reader.get_annotations()
    if len(offsets) < 2:
        print("無足夠的標註數據來計算 RR 間期！")
        return None
    original_fs = reader.sample_rate_hz()  # 原始取樣率
    target_fs = 128  # 目標取樣率 (RR 計算)
    # 轉換 Beat Type
    beat_type_mapping = {0: "Q", 1: "N", 2: "V", 3: "S"}
    beat_type_labels = [beat_type_mapping.get(bt, "Unknown") for bt in beat_types]
    # 計算 RR 間期 (轉換到 128Hz)
    rr_intervals = [(offsets[i] - offsets[i - 1]) / original_fs * 1000 for i in range(1, len(offsets))]

    # 判斷 AF (如果有 AF，設為 1，否則 0)
    af_flags = [0]* (len(offsets) - 1)
    # 轉換 offsets 到 128Hz 的取樣位置
    new_offsets = [int(offset * target_fs / original_fs) for offset in offsets]

    beat_summary = set(beat_type_labels)
    if beat_summary == {"N"}:
        rhythm_label = "N,"
    elif "V" in beat_summary and "S" in beat_summary:
        rhythm_label = "VPC,APC,"
    elif "V" in beat_summary:
        rhythm_label = "VPC,"
    elif "S" in beat_summary:
        rhythm_label = "APC,"
    else:
        rhythm_label = "N,"
    # 確保輸出目錄存在
    os.makedirs(os.path.dirname(rrfilename), exist_ok=True)

    # 寫入資料
    with open(rrfilename, "w") as f_rr:
        for i in range(1, len(offsets)):
            if i == len(offsets) - 1:  # 在最後一行加入節律類型
                f_rr.write(f"{new_offsets[i]} {beat_type_labels[i]} {int(rr_intervals[i-1])} {rhythm_label}\n")
            else:
                f_rr.write(f"{new_offsets[i]} {beat_type_labels[i]} {int(rr_intervals[i-1])} {af_flags[i-1]}\n")

    #print(f"RR 間期資訊已儲存: {rrfilename}")

    return rrfilename
        
def atcdecode(filename, output_path, rrfilename):
    
    reader = ATCReader(filename)  # 載入 .atc 檔案  
    if reader.status() != atc_reader.READ_SUCCESS:
        print("讀取 ATC 檔案失敗！")
        return None

    bin_file, txt_file = process_ecg_data(reader, output_path)
    info_filename = save_info(reader, output_path)
    save_rr_annotations(reader, rrfilename)
    

    