<?php

namespace App\Http\Controllers\File;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;
use App\Traits\ECGDelete;
class ManagerController extends Controller
{
    use ECGDelete;
    protected $ecgRepository;
    
    public function __construct(ECGRepository $ecgRepository)
    {
        $this->ecgRepository=$ecgRepository;
    }

    public function showWeb(Request $request)
    {
        $user = $request->user();
        if($user->can('ecg_manager_model')){
            session(['manager_model' => 'ecg']);
        }
        $holter_number = $user->holters()->count();
        $storage = $user->storage;
        $department = $user->department()->with(['report_types'])->first();
        $ecgs_storage = $user->ecgs()->sum('filesize');
        $use_storage = $ecgs_storage/(1024*1024);
        $total_storage = $holter_number*15+$storage;
        $data=[
            'use_storage'=>round($use_storage, 2),
            'use_storage_rate'=>$total_storage == 0 ? 0 : round(($ecgs_storage/($total_storage*1024*1024*1024))*100, 2),
            'storage' =>$total_storage,
            'department' => $department 
        ];
        if($user->can('doctor_type')){
            $data['confirmed'] = $request->input('confirmed', false);
            return view('doctor.manager',$data);
        }
        return view('file.manager',$data);
    }

    public function getECGs()
    {
        $user_id = Auth::user()->id;
        $ecgs = $this->ecgRepository->getECGs($user_id);
        return response()->json(['response'=>'ok','ECGs'=>$ecgs]);
    }

    public function data(Request $request)
    {
        $user = $request->user();

        $confirm = $request->input('confirmed', 'false') != 'false' ? 1 : 0; 
        if($user->can('doctor_type')){
            $where=['assignee'=>$user->id, 'confirm'=>$confirm, 'confirm_check'=>0];
            if($confirm){
                $where['confirm_time.>'] = date('Y-m-d 00:00:00', strtotime('-6 day'));
            }
        }else{
            $where=['user_id'=>$user->id];
        }
        $department = $user->department ? $user->department->name : '';
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->ecgRepository;
        $search_fields = ['filename', 'patient_IDNumber'];
        $search_relation_fields = [];
        $search = ""; 

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            $orderBy[$order_column] = $order;
        }else{
            if($confirm){
                $orderBy['confirm_time'] = 'DESC';
            }else{
                $orderBy['created_at'] = 'DESC';
            }
        }



        if($department == 'HOLTER'){
            $where['filename.notLike'] = '%_dup%';
        }

        $offset = $request->input('offset',0);
        $limit = $request->input('limit',100);

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
            if(strpos($search , '&') !== false){
               $expolode_search = explode('&', $search);
               $search = ['text'=>$expolode_search[0], 'created_at'=>$expolode_search[1]];
            }
        }
        
        $data_total = $repositoryBy->whereBy($where)->toCount();
        $data_total_filtered = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->toCount();
        $tableData = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->limit($offset, $limit)->toGets();
        
        $data = [
            "total" => $data_total_filtered,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }

    public function del(Request $request)
    {
        $user_id=Auth::user()->id;
        Validator::make($request->all(), [
            'ecgs.*.id' => 'required'
        ])->validate();
        $del_ecgs= $request->input('ecgs');
        $del_success_count=0;
        foreach ($del_ecgs as $key => $ecg) {
            $ecg_data = $this->ecgRepository->getECG($user_id,$ecg['id']);
            if($this->del_file($user_id, $ecg_data->filename, $ecg_data->filename_ext)){
                $this->ecgRepository->delECG($ecg['id'],$user_id);
                $del_success_count++;
            }
        }
        return response()->json(['response'=>'ok','del_success_count'=>$del_success_count]);
    }
    
    public function setFilename(Request $request)
    {
        $user_id=Auth::user()->id;
        Validator::make($request->all(), [
            'id' => 'required',
            'filename' => 'required'
        ])->validate();
        $filename = $request->input('filename');
        if(preg_match("/^[A-Za-z0-9.\-_]+$/",$filename)!=1){
            return response()->json(['response'=>'error','error_code'=>2,'msg'=>'只允許英數字、底線(_)、破折號(-)與句號(.)為檔名！']);
        }
        $ecg_id = $request->input('id');
        if($this->ecgRepository->checkFilename($ecg_id,$filename,$user_id)){
            $ecg_data = $this->ecgRepository->getECG($user_id,$ecg_id);
            $oldfilename=$ecg_data->filename;
            $filerename=$this->renameAllFile($user_id,$oldfilename,$filename,$ecg_data->filename_ext);
            $this->ecgRepository->setFilename($ecg_id,$filename);
            return response()->json(['response'=>'ok','filename'=>$filename]);
        }else{
            return response()->json(['response'=>'error','error_code'=>1,'filename'=>$filename,'msg'=>'filename is exist']);
        }
    }
    public function setInfo(Request $request)
    {
        $user_id=Auth::user()->id;

        Validator::make($request->all(), [
            'id' => 'required',
        ])->validate();
        $update_data=[
            'description'=> $request->input('description') ? $request->input('description','') : '',
            'patient_name'=> $request->input('patient_name'),
            'patient_IDNumber'=> $request->input('patient_IDNumber'),
            'birthday'=> $request->input('birthday'),
            'sex'=> $request->input('sex'),
            'comment'=>$request->input('comment') ?  $request->input('comment','') : ''
            ];
        $ecg_id = $request->input('id');
        if($this->ecgRepository->checkOwner($ecg_id,$user_id) || Auth::user()->can('manager_type')){
            $this->ecgRepository->setInfo($ecg_id,$update_data);
            return response()->json(['response'=>'ok']);
        }else{
            return response()->json(['response'=>'error','error_code'=>1,'msg'=>'permission denied']);
        }
    }
}
