<?php

namespace App\Http\Controllers\File;
use Storage;
class TransferHandler
{
    private $userID;
    private $ecgFolder;
    private $rrFolder;
    private $execPath;
    private $executionECG;
    private $executionRR;
    private $txtFilename;
    private $txtFilenameD16;
    private $txtFilenameD8;
    private $txtFilenameD4;
    private $exeFileExt;
    private $filename;
    private $filename_ext;
    public function __construct($userID,$filename)
    {
        $this->userID = $userID;
        $this->ekgFolder = public_path("uploads/{$userID}/ekg/");
        $this->ecgFolder = "uploads/{$userID}/ecg/";//public_path("uploads/{$userID}/ecg/");
        $this->rrFolder = "uploads/{$userID}/rr/";//public_path("uploads/{$userID}/rr/");
        $this->execPath = dirname(__FILE__);
        if (!is_dir(public_path($this->ecgFolder))) {
            $oldmask = umask(0);
            mkdir(public_path($this->ecgFolder), 0777, true);
            umask($oldmask);
        }

        if (!is_dir(public_path($this->rrFolder))) {
            $oldmask = umask(0);
            mkdir(public_path($this->rrFolder), 0777, true);
            umask($oldmask);
        }
        preg_match("/([^.\/]+)(\.|\/)(psg|z2b|xml|fat|atc)$/i", $filename, $filename_ext);

        $this->filename_ext = $filename_ext[3];
        $this->filename = $filename_ext[1];
        
        if( $this->filename_ext=='atc'){
            $this->executionECG = 'atc_decode.sh';
        }elseif( $this->filename_ext=='fat'){
            $this->executionECG = 'z2b_decode';
        }else{
             $this->executionECG = $this->filename_ext.'_decode';
        }

        $this->output_ext = 'txt';
       
        $this->txtFilename = $this->ecgFolder . $this->filename;
        $this->txtFilenameD16 = $this->ecgFolder . $this->filename . "_32.".$this->output_ext;
        $this->txtFilenameD8 = $this->ecgFolder . $this->filename . "_128.".$this->output_ext;
        $this->txtFilenameD4 = $this->ecgFolder . $this->filename . "_256.".$this->output_ext;
        $this->binFilename = $this->ecgFolder . $this->filename . ".bin";
        $this->executionRR = 'qrs_detect';
        $this->executionReRR = 'qrs_redetect';
        $this->executionRRAlign = 'peak_align';
        $this->rrFilename = $this->rrFolder . $this->filename . "_rr.txt";
        $this->rrTableFilename = $this->rrFolder . $this->filename . "_256_Table201.12.1.101.3.txt";
        $this->rrEditedFilename = $this->rrFolder . $this->filename . "_rr_edited.txt";
        $this->rrEditedMaxFilename = $this->rrFolder . $this->filename . "_rr_editedMax.txt";

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $this->exeFileExt='.exe';
        } else {
            $this->exeFileExt='';
        }
    }
    public function run_to_txt($file_path){

        $start_time = microtime(true);
        exec("\"".$this->execPath."/".$this->executionECG.$this->exeFileExt."\" \"".$file_path."\" \"".public_path($this->txtFilename)."\"", $resArr);
        $end_time = microtime(true);
        
        //rename($this->txtFilename.'_event.txt', $this->txtFilename.'_'.strtolower($this->filename_ext).'_event.txt');
        //rename($this->txtFilename.'_info.txt', $this->txtFilename.'_'.strtolower($this->filename_ext).'_info.txt');

        return $end_time - $start_time;
    }
    public function run_atc_decode($file_path){

        Storage::append('transfer_debug', "\"".$this->execPath."/".$this->executionECG."\" \"".$file_path."\" \"".public_path($this->txtFilename)."\" \"".public_path($this->rrFilename)."\"");
        $start_time = microtime(true);
        exec("\"".$this->execPath."/".$this->executionECG."\" \"".$file_path."\" \"".public_path($this->txtFilename)."\" \"".public_path($this->rrFilename)."\"", $resArr);
        $end_time = microtime(true);

        // Copy a RR file for user editing
        copy(public_path($this->rrFilename), public_path($this->rrEditedFilename));
        exec("chmod -R 777 ".public_path($this->rrTableFilename));
        exec("chmod -R 777 ".public_path($this->rrFilename));
        exec("chmod -R 777 ".public_path($this->rrEditedFilename));

        return $end_time - $start_time;
    }

    public function getPath($file_path){

        return "\"".$this->execPath."/".$this->executionECG.$this->exeFileExt."\" \"".$file_path."\" \"".$this->txtFilename."\"";
    }
    public function run_to_rr($fix_path=false){
        
        // Convert TXT to RR
        $start_time = microtime(true);
        $output='';
        
        exec("\"".$this->execPath."/".$this->executionRR.$this->exeFileExt."\" \"".($fix_path ? $fix_path.'/'.$this->txtFilenameD4 : $this->txtFilenameD4)."\" \"".($fix_path ? $fix_path.'/'.$this->rrFilename : $this->rrFilename)."\"", $output);

        $end_time = microtime(true);

        // Copy a RR file for user editing
        copy(public_path($this->rrFilename), public_path($this->rrEditedFilename));
        exec("chmod -R 777 ".public_path($this->rrTableFilename));
        exec("chmod -R 777 ".public_path($this->rrFilename));
        exec("chmod -R 777 ".public_path($this->rrEditedFilename));

        return $end_time - $start_time;
        
    }
    public function run_rr_align($signal, $sample_rate){
        
        $start_time = microtime(true);
        exec("\"".$this->execPath."/".$this->executionRRAlign.$this->exeFileExt."\" \"".$this->rrEditedFilename."\" \"".$this->binFilename."\" \"".$signal."\" \"".$sample_rate."\" \"".$this->rrEditedMaxFilename."\"");
        $end_time = microtime(true);
        exec("chmod -R 777 ".public_path($this->rrEditedMaxFilename));
        copy(public_path($this->rrEditedMaxFilename), public_path($this->rrEditedFilename));
        exec("chmod -R 777 ".public_path($this->rrEditedFilename));
        return $end_time - $start_time;
    }

    public function run_to_rr_second($fix_path=false){
        
        // Convert TXT to RR
        $start_time = microtime(true);
        
        exec("\"".$this->execPath."/".$this->executionReRR.$this->exeFileExt."\" \"".($fix_path ? $fix_path.'/'.$this->rrFilename:$this->rrFilename)."\" \"".($fix_path ? $fix_path.'/'.$this->txtFilenameD4:$this->txtFilenameD4)."\"", $output);
        $end_time = microtime(true);
        
        // Copy a RR file for user editing
        copy(public_path($this->rrEditedFilename), public_path($this->rrFilename));
        exec("chmod -R 777 ".public_path($this->rrEditedFilename));
        return $end_time - $start_time;
    }

    public function run_predict($file_path, $sample_rate=250){
        $output=null;
        $start_time = microtime(true);
        exec("\"".$this->execPath."/"."predict.sh\" -n \"".$file_path."\" -f \"250\"", $output);
        $end_time = microtime(true);
        try {
            if(count($output)){
                $result = trim(explode(':', $output[count($output)-1])[1]);
            }else{
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
        return $result;
    }

    public function checkHolterID($holters , $holter_id_pass = false)
    {
        if (!$file_handle = fopen(public_path($this->txtFilename.'_info.'.$this->output_ext), "r")) {
            return false;
        }
        $pass=false;
        $result=[];
        $line = fgets($file_handle);
        $infoLine = explode("\t", trim($line));
        $holter_id = $infoLine[0];
        $result['holter_id'] = $holter_id; 
        foreach ($holters as $key => $holter) {
            if($holter_id==$holter['IDNumber']){
                $pass=true;
                break;
            }
        }

        if(!$pass && !$holter_id_pass){
            fclose($file_handle);
            return false;
        }

        $line = fgets($file_handle);
        $infoLine = explode("\t", trim($line));
        $result['recorded_at']=$infoLine[0];

        $line = fgets($file_handle);
        $infoLine = explode("\t", trim($line));
        $result['sample_rate']=$infoLine[0];

        fclose($file_handle);
        return $result;
    }

    public function get_report_type(){

        if (!$file_handle = fopen(public_path($this->rrEditedFilename), "r")) {
            return -1;
        }
        $isN = false;
        while ($line = fgets($file_handle)){
            $infoLine = explode(" ", trim($line));
            if(count($infoLine)>3){
                $isN = isset($infoLine[3]) ? $infoLine[3] : -1;
            }
        }
        fclose($file_handle);
        return $isN;
    }

    public function info_file_exists(){
        return file_exists(public_path($this->txtFilename.'_info.'.$this->output_ext)) && filesize(public_path($this->txtFilename.'_info.'.$this->output_ext))!=0;
    }

    public function failureFile(){
        $result=true;
        $filename_ext = $this->filename_ext;
        $user_id =  $this->userID;
        $oldfilename = $filename = $this->filename;
        $time = time();
        $movepath = '/failure/'. $time ;
        $failure_path = public_path('uploads/'.$user_id.'/failure/');
        if (!is_dir($failure_path.$time.'/ekg/')) {
            mkdir($failure_path.$time.'/ekg/', 0755, true);
        }
        if (!is_dir($failure_path.$time.'/ecg/')) {
            mkdir($failure_path.$time.'/ecg/', 0755, true);
        }
        if (!is_dir($failure_path.$time.'/rr/')) {
            mkdir($failure_path.$time.'/rr/', 0755, true);
        }
        
        $psg_suffix=['.'.$filename_ext];
        $ecg_suffix=['_32.txt','_128.txt','_256.txt','_info.txt','_event.txt','_1024.bin'];
        $rr_suffix=['_256_rr.txt','_256_rr_edited.txt','_256_Table201.12.1.101.3.txt'];

        if(!$this->rename_files(public_path('uploads/'.$user_id.'/ekg/'.$oldfilename), public_path('uploads/'.$user_id.$movepath.'/ekg/'.$filename),$psg_suffix)){
            $result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/ecg/'.$oldfilename), public_path('uploads/'.$user_id.$movepath.'/ecg/'.$filename),$ecg_suffix)){
            $result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/rr/'.$oldfilename), public_path('uploads/'.$user_id.$movepath.'/rr/'.$filename),$rr_suffix)){
            $result=false;
        }
    }
    private function rename_files($old_path,$current_path,$suffixs){
        $result=true;
        foreach ($suffixs as $suffix) {
            if(file_exists($old_path.$suffix)){
                rename($old_path.$suffix,$current_path.$suffix);
            }else{
                //$result=false;
            }
        }
        return $result;
    }
}
