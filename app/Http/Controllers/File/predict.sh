#!/bin/sh
exe_name=$0
exe_dir=`dirname "$0"`
args=
while [ $# -gt 0 ]; do
	token=$1
	args="${args} \"${token}\"" 
	shift
done
eval "python3.9 \"${exe_dir}/predict.py\"" $args