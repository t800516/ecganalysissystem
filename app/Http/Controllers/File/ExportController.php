<?php

namespace App\Http\Controllers\File;

use Auth;
use Validator;
use Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\ECG\Repositories\ECGRepository;
class ExportController extends Controller
{
    
    protected $ecgRepository;
    
    public function __construct(ECGRepository $ecgRepository)
    {
        $this->ecgRepository=$ecgRepository;
    }

    public function showWeb(Request $request)
    {
        $user = $request->user();
        $data = [];
        return view('file.export',$data);
    }

    public function getECGs(Request $request)
    {
        $user = $request->user();
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        $where = [
            "user_id" => $user->id
        ];
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }
        $ecgs = $this->ecgRepository->getsBy($where);

        return response()->json(['response'=>'ok','ECGs'=>$ecgs]);
    }

    public function export(Request $request)
    {
        $user = $request->user();
        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        $where = [
            "user_id" => $user->id,
        ];
        if($request->has('start')){
            $where["created_at.>="] = $start;
        }
        if($request->has('end')){
            $where["created_at.<"] = $end;
        }
        $ecgs = $this->ecgRepository->getsBy($where);
        $filename = date('Ymd').'_ecg';
        $ecg_data = [];
        $fields = [];
        foreach ($ecgs as $key => $ecg) {
            if($key == 0){
                $ecg->makeHidden('upload_date');
                $fields = array_keys($ecg->toArray());
                array_push($ecg_data, $fields);
            }
            $row = [];
            foreach ($fields as $key2 => $field) {
                array_push($row, $ecg->{$field});
            }
            array_push($ecg_data, $row);
        }
        Excel::create($filename, function($excel) use ($ecg_data) {
                $excel->sheet('SheetECG', function($sheet) use($ecg_data) {
                    $sheet->rows($ecg_data);
                });
            })->export('csv');
    }
    function getAnalysisStatus($value){
        switch($value){default:
            case 0:return '尚未轉檔';
            case 9:return '開始轉檔...';
            case 1:return 'ECG轉檔成功';
            case 92:return '開始QRS分析...';
            case 82:return '開始QRS二次分析...';
            case 2:return 'QRS分析成功';
            case 10:return '轉檔發生錯誤，請嘗試重新上傳!';
            case 11:return 'Holter ID 錯誤!';
        }
    }
    function getReportType($type){
        switch($type){
            default:return '尚未產生報表';
            case -1 :return '尚未產生報表';
            case 0 :return 'Normal';
            case 1 :return 'Atrial';
            case 2 :return 'Abnormal';
            case 3 :return 'Bad signal';
        }
    }
    function getReportStatus($status){
        switch($status){
            default:return '尚未產生報告';
            case 0 :return '尚未產生報告';
            case 1 :return '正在產生報告';
            case 2 :return '報告完成';
        }
    }
}
