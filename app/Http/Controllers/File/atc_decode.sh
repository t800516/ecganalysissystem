#!/bin/sh
exe_name=$0
exe_dir=`dirname "$0"`
args=
while [ $# -gt 0 ]; do
	token=$1
	args="${args} \"${token}\"" 
	shift
done
eval "${exe_dir}/atc_decode/env/bin/python3 \"${exe_dir}/atc_decode/atc_decode.py\"" $args
# eval "python3 \"${exe_dir}/atc_decode/atc_decode.py\"" $args
