import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import argparse
import tensorflow
import numpy as np
from scipy.signal import butter, filtfilt
from scipy import interpolate


def butter_lowpass_filter(data, input_fs):
    # # Filter requirements.
    order = 4       # sin wave can be approx represented as quadratic
    # Get the filter coefficients 
    b, a = butter(order, [0.05, 40], fs = input_fs, btype='bandpass')
    y = filtfilt(b, a, data)
    return y

def seg_sgfilt(input_sig, input_fs):
    from scipy.signal import savgol_filter
    n = int(0.5*input_fs)
    k = 1
    f = int(0.5*input_fs-1) #window_length
    f= f-1 if (f%2!=1) else f #f must be an odd number
    M = int(0.5*input_fs) 
    M= M-1 if (M%2 !=1) else M # must be an odd number
    a = 1
    b = n
    est = np.zeros(len(input_sig))

    for _ in range(0, len(input_sig), n):
        est[a:b] = savgol_filter(input_sig[a:b], window_length=f, polyorder=k)
        a = 1+b
        b = b+n
        b = len(input_sig) if b>len(input_sig) else b

    # Moving average filter
    for _ in range(1):
        est = np.convolve(est, np.ones(M)/M, mode='same') 

    removed_BW = input_sig-est
    return removed_BW

def interpolation(input_sig, input_fs, output_fs):
    t_tmp = np.arange(0, len(input_sig)/input_fs, 1/input_fs)
    t_n = np.arange(0, len(input_sig)/input_fs, 1/output_fs)
    f = interpolate.interp1d(t_tmp, input_sig, fill_value="extrapolate")
    sig_interp = f(t_n)   # use interpolation function returned by `interp1d`
    return sig_interp
    
def filtNSeg(input_sig, input_fs):
    output_fs = 500

    filt_sig = butter_lowpass_filter(input_sig, input_fs)
    sg_filt = seg_sgfilt(filt_sig, input_fs)
    interp_sig = interpolation(sg_filt, input_fs, output_fs)
    seg_sig = interp_sig.copy()[:1238*12, np.newaxis].reshape(12, 1238)
    seg_sig = (seg_sig-seg_sig.mean(1)[:, np.newaxis])/seg_sig.std(1)[:, np.newaxis]
    return seg_sig


def main(fileanme, fs):
    current_path =  os.path.dirname(os.path.abspath(__file__))
    with open(fileanme, 'r') as file:
        inputsig = np.fromfile(file, dtype=float, sep=' ')
    input_data = filtNSeg(inputsig, int(fs))
    model = tensorflow.keras.models.load_model(current_path+'/I_model.h5')
    indep_prob = model.predict(input_data)
    print('Final prediction: ', indep_prob.mean())


if __name__ == '__main__':
    # Args parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--filename", type=str, help="Input filename with path (.z2b)", required=True
    )
    parser.add_argument(
        "-f", "--input_fs", type=int, help="Frequency of input signal", required=True
    )

    args = parser.parse_args()

    main(args.filename, args.input_fs)