<?php

namespace App\Http\Controllers\NotifiableModule;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ECG\Repositories\ECGRepository;
use App\Service\Repositories\ServiceRepository;
use DB;
use App\User;
use App\Department;
use App\Traits\RelatedUser;

class ManagerController extends Controller
{
    use RelatedUser;
    protected $ecgRepository;
    protected $serviceRepository;

    public function __construct(ECGRepository $ecgRepository, ServiceRepository $serviceRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->serviceRepository = $serviceRepository;
    }

    public function index(Request $request)
    {
        $user = $request->user();
        $allUser = DB::table('user_user')->get();
        $related_users = [$user->id];
        $this->getAllUsers($user->id, $related_users, $allUser);
        $users = User::with('department')->whereIn('id', $related_users)->get();
        foreach ($users as $key => $v) {
            if(!$v->department || $v->department->module_id == 0){
                $index = array_search($v->id, $related_users);
                if($index !== false){
                    array_splice($related_users, $index, 1);
                }
            }
        }
        $data = [
            'related_users'=>$related_users
        ];
        
        return view('notifiableModule.list', $data);
    }

    public function data(Request $request){
        $user = $request->user();
        $related_users = $request->input('related_users',[]);
        $allUser = User::whereIn('id', $related_users)->get();
        $allDepartment = Department::get();
        $where=['user_id.in'=>$related_users];
        $query_string = [];
        $query_data = [];
        $orderBy=[];
        $repositoryBy = $this->serviceRepository;
        $search_fields = ['serial_number', 'start_date', 'patient_name','patient_IDNumber'];
        $search_relation_fields = [/*'patient.email', 'patient.department.name'*/];
        $search = ""; 

        $start = date('Y-m-d H:i:s', strtotime($request->input('start', date('Y-m-d'))));
        $end = date('Y-m-d H:i:s', strtotime($request->input('end', date('Y-m-d')).' +1 day'));
        
        if($request->has('start')){
            $where["updated_at.>="] = $start;
        }

        if($request->has('end')){
            $where["updated_at.<"] = $end;
        }

        if($request->has('sort')){
            $order_column = $request->input('sort') != '' ? $request->input('sort') : 'created_at'; 
            $order = $request->input('sort') != '' ? $request->input('order') : 'DESC';
            $orderBy[$order_column] = $order;
        }else{
            $orderBy['created_at'] = 'DESC';
        }

        if($request->has('search') && $request->input('search','')!=''){
            $search = $request->input('search','');
        }

        $tableData = $repositoryBy->searchBy( $search_fields, $search, $search_relation_fields)->whereBy($where)->orderBy($orderBy)->toGets();
        $data_total = $tableData->count();
        $data_total_filtered = $data_total;
        foreach ($tableData as $key => $v) {
            $ecg_patient = $allUser->where('id', $v->patient_user_id)->first();
            if($ecg_patient){
                $department = $allDepartment->where('id', $ecg_patient->department_id)->first();
                $v->patient = ['email'=>$ecg_patient->email, 'department'=>$department ? ['name'=>$department->name]:null];

            }else{
                $v->patient = null;
            } 
            $ecg_user = $allUser->where('id', $v->user_id)->first();
            if($ecg_user){
                $department = $allDepartment->where('id', $ecg_user->department_id)->first();
                $v->user = ['email'=>$ecg_user->email, 'department'=>$department ? ['name'=>$department->name]:null];
            }else{
                $v->user = ['email'=>''];
            }
        }
        $data = [
            "total" => $data_total_filtered,
            "totalNotFiltered" => $data_total,
            "rows" => $tableData
        ];
        return response()->json($data);
    }
    public function notified(Request $request, $id){
        $service = $this->serviceRepository->get($id);
        $user =  $service->patient ? $service->patient : $service->user;
        $department =  $user->department;
        if($department->module){
            $moduleClass = 'App\\NotifiableModule\\Services\\'.$department->module->name.'NotifiableModule';
            $module = (new $moduleClass());
            $ecgs = $service->ecgs()->where('report_status',2)->where('checked',1)->where('notified',0)->get();
            $notifiable_records = $ecgs->count();
            $finish_count = 0;
            if(in_array($department->module->name, ['TZUCHI'])){
                $parse_data = $module->getSendData($ecgs);
                foreach ($parse_data as $row_data) {
                    $result = $module->send($row_data);
                    if(count($result)){
                        $finish_count += count($result);
                        $this->ecgRepository->whereBy(['id.in'=>$result])->toUpdate(['notified'=>1]);
                        $data = [
                            'total_count'=>$notifiable_records,
                            'finish_count'=>$finish_count,
                            'status'=>'progressing'
                        ];
                        $this->output_msg($id, $data);
                    }
                }
            }else{
                foreach ($ecgs as $key => $ecg) {
                    $result = $module->send($ecg);
                    if($result){
                        $finish_count++;
                        $data = [
                            'total_count'=>$notifiable_records,
                            'finish_count'=>$finish_count,
                            'status'=>'progressing'
                        ];
                        $this->ecgRepository->update($ecg->id,['notified'=>1]);
                        $this->output_msg($id, $data);
                    }
                }
            }
            $data = [
                        'total_count'=>$notifiable_records,
                        'finish_count'=>$finish_count,
                        'status'=>'complete'
                    ];
            return response()->json(['id'=>$id, 'data'=>$data]);
        } 
        $data = [
                    'total_count'=>0,
                    'finish_count'=>0,
                    'status'=>'error'
                ];
        return response()->json(['id'=>$id, 'data'=>$data]);
    }

    function output_msg($id, $data){
        echo json_encode(['id'=>$id, 'data'=>$data]);
        ob_flush(); 
        flush();
        sleep(1);
    }
}