<?php

namespace App\Http\Controllers\Survey;

use Validator;
use Illuminate\Http\Request;
use App\Survey\Repositories\SurveyRepository;
use App\User\Repositories\UserRepository;
use App\Http\Controllers\Controller;

class SurveyController extends Controller
{

    protected $surveyRepository;
    protected $userRepository;

    public function __construct(SurveyRepository $surveyRepository, UserRepository $userRepository)
    {
        $this->surveyRepository = $surveyRepository;
        $this->userRepository = $userRepository;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'max:255',
            'email' => 'email|max:255',
        ]);
    }

    protected function store(Request $request, $id) 
    {
        $this->validator($request->all());
        $insert_data=[
            'name' => $request->input('name','') ? $request->input('name',''):'',
            'email' => $request->input('email','')? $request->input('email',''):''
        ];
        $user = $this->userRepository->get($id);
        $survey = $user->surveys()->create($insert_data);
        return redirect('survey/result')->with('survey_id', $survey->id);
    }

    public function result()
    {
        $survey_id = session('survey_id');
        return view('survey.result', ['survey_id'=>$survey_id]);
    }

    public function showWeb($id)
    {
        $data = [
            'user_id'=>$id
        ];
        return view('survey.form', $data);
    }
}