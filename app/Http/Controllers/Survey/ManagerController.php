<?php

namespace App\Http\Controllers\Survey;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{
   
    public function __construct()
    {
    }
    public function showWeb(Request $request)
    {
    	$user = $request->user();
    	$data = [
    		'user_id'=>$user->id,
    		'surveys' =>$user->surveys()->orderBy('created_at', 'DESC')->get()->toJson()
    	];
        return view('survey.manager', $data);
    }
}
