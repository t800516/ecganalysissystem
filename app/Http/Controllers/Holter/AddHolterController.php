<?php

namespace App\Http\Controllers\Holter;

use App\User;
use App\Holter;
use Validator;
use App\Http\Controllers\Controller;

class AddHolterController extends Controller
{

    public function __construct()
    {

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'account' => 'required|max:255|unique:users',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {
        return Holter::create([
            'IDNumber' => $data['IDNumber'],
            'name' => $data['name'],
            'gender' => $data['gender'],
            'birthday' => $data['birthday'],
            'age' => $data['age'],
            'zipcode' => $data['zipcode'],
            'country' => $data['country'],
            'city' => $data['city'],
            'downtown' => $data['downtown'],
            'address' => $data['address'],
            'tel' => $data['tel'],
            'cel' => $data['cel'],
            'email' => $data['email']
        ]);
    }

    public function showWeb()
    {
        return view('holter.addholter');
    }
}