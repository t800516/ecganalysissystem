<?php

namespace App\Http\Controllers\Holter;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{
   
    public function __construct()
    {
    }
    public function showWeb()
    {
        return view('holter.manager');
    }
}
