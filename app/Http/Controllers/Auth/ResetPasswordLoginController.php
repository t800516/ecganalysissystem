<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResetPasswordLoginController extends Controller
{
    public function __construct()
    {
       
    }

    public function showResetForm()
    {
        return view('auth.resetpw');
    }

    public function reset(Request $request)
    {
        $this->validate($request, [
            'oldpassword' =>'required|min:6',
            'password' => 'required|confirmed|min:6'
        ]);

        $credentials=$this->credentials($request);
        if (Auth::once(["id"=>Auth::user()->id,"password"=> $credentials['oldpassword']])) {
            $this->resetPassword(User::find(Auth::user()->id), $credentials['password']);
            return redirect('/userprofile');
        }else{
            return redirect('/resetpw')->withErrors(['oldpassword'=>trans('physiolguard.password_error')]);
        }

    }

    protected function credentials(Request $request)
    {
        return $request->only(
            'oldpassword','password', 'password_confirmation'
        );
    }

    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();

        $this->guard()->login($user);
    }


    protected function guard()
    {
        return Auth::guard();
    }
}
