<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Session\Repositories\SessionRepository as LoginSession;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    protected $login_session = '/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LoginSession $login_session)
    {
        //$this->middleware('guest', ['except' => 'logout']);
        $this->login_session = $login_session;
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
    }
    public function login(Request $request)
    {
        $this->validateLogin($request);

     
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            if(Auth::user()->cannot('multiple_login')||true){
                $this->login_session->updateLoginSession(Session::getId(),Auth::user()->id);
            }
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    protected function sendLoginResponse(Request $request)
    {
        //$request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }


    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }
   
    public function username()
    {
        return 'name';
    }

    public function showChecking(){
        if (Auth::user()->checked==0){
            return view('auth.checking');
        }else{
            return redirect('/');
        }
    }

    public function authexpired(){
        return view('auth.expired');
    }
}
