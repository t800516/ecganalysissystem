<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class AuthExpiredController extends Controller
{

    public function __construct(LoginSession $login_session)
    {

    }

    public function authexpired(){
        return view('auth.checking');
    }
}
