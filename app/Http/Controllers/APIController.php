<?php

namespace App\Http\Controllers;
use App\Traits\ResponseFormatter;

class APIController extends Controller
{
    use ResponseFormatter;
}
