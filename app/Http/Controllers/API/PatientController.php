<?php

namespace App\Http\Controllers\API;
use App\Patient\Repositories\PatientRepository;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PatientController extends APIController
{
	protected $patientRepository;
	public function __construct(PatientRepository $patientRepository)
    {
        $this->patientRepository = $patientRepository;
    }
    public function index(Request $request){
    	$patients = $request->user()->patients->makeHidden(['user_id','created_at','updated_at']);
    	return $this->successResponse($patients);
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
    	$user = $request->user();
        $validator = $this->patientValidator($request->all());
        if($validator->fails()){
            return $this->validateErrorResponse($validator->errors()->all());
        }

        $request_data = $request->only(['IDNumber','name','comment','birthday','age','sex','height','weight']);
        $patient = $user->patients()->create($request_data);
        
        return $this->successResponse($patient?$patient:[]);
    }

    public function show(Request $request, $id)
    {
    	$user = $request->user();
    	$patient = $this->patientRepository->getBy(['user_id'=>$user->id,'id'=>$id]);
        if(!$patient){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        return $this->successResponse($patient->makeHidden(['user_id','created_at','updated_at']));
    }

    public function edit($id)
    {
      
    }

    public function update(Request $request, $id)
    {
    	$user = $request->user();
    	$patient = $this->patientRepository->getBy(['user_id'=>$user->id,'id'=>$id]);
        if(!$patient){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }

        $validator = $this->patientValidator($request->all());
        if($validator->fails()){
            return $this->validateErrorResponse($validator->errors()->all());
        }

        $request_data = $request->only(['IDNumber','name','comment','birthday','age','sex','height','weight']);
        $data = array_filter($request_data, function($item){return $item!=null;});

        $patient = $this->patientRepository->update($id,$data);

        return $this->successResponse($patient?$patient:[]);
    }

    public function destroy(Request $request, $id)
    {
    	$user = $request->user();
    	$patient = $this->patientRepository->getBy(['user_id'=>$user->id,'id'=>$id]);
        if(!$patient){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $this->patientRepository->delete($id);
        return $this->successResponse(['id'=>$id]);

    }

    protected function patientValidator(array $data)
    {
        return Validator::make($data, [
            'IDNumber' => 'required|max:255'
        ]);        
    }
}

