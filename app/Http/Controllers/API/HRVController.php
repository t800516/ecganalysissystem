<?php
namespace App\Http\Controllers\API;

use App\ECG\Repositories\ECGRepository;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\ECG\Services\ECGService;
use Illuminate\Support\Facades\Validator;
use App\Traits\HRV;
use Request as FacadesRequest;
class HRVController extends APIController
{
    use HRV;

	protected $ecgRepository;
    protected $ecgService;
	public function __construct(ECGRepository $ecgRepository, ECGService $ecgService)
    {
        $this->ecgRepository = $ecgRepository;
        $this->ecgService = $ecgService;
    }

    public function runAnalysis(Request $request, $ecg_id){
        $uri = $this->getUri();
        $actionMethod = $this->getMethod();
        $user  =  $request->user();
        $user_id = $user->id;
        $interval = $request->input('interval', 5);
        if (isset($ecg_id)) {
            if($user->can('manager_type')){
                $ecg = $this->ecgRepository->get($ecg_id);
                $user = $ecg->user;
                $user_id = $user->id;
            }else{
                $ecg = $this->ecgRepository->getECG($user_id, $ecg_id);
            }

            if($user->cannot('hrv_interval_input')){
                $interval=5;
            }

            if($ecg){
                if($ecg->analysis_status != 2){
                    return response('{"status":"error", "error":{"message":["The ECG was not converted"]}, "uri":"'.$uri.'", "method":"'.$actionMethod.'"}')
                    ->header('Content-Type', "application/json");
                }
                $execPath = dirname(dirname(__FILE__)).'/Analysis';
                $rr_suffix = '_rr_edited.txt';
                $hrv_suffix = '_hrv.txt';
                $bin_suffix = '.bin';

                $ecg_file_path = "uploads/{$user_id}/ecg/";
                $rr_file_path = "uploads/{$user_id}/rr/";
                $hrv_file_path = "uploads/{$user_id}/hrv/";
                if (!is_dir(public_path($hrv_file_path))) {
                    $oldmask = umask(0);
                    mkdir(public_path($hrv_file_path), 0777, true);
                    umask($oldmask);
                }
                $rr_file_name=$rr_file_path.$ecg->filename.$rr_suffix;
                $hrv_file_name=$hrv_file_path.$ecg->filename.'_'.$interval.$hrv_suffix;
                $bin_file_name=$ecg_file_path.$ecg->filename.$bin_suffix;
                
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                    $exeFileExt='.exe';
                } else {
                    $exeFileExt='';
                }
                $sample_rate = $ecg->sample_rate != 0 ? $ecg->sample_rate:'';
                $execpath="\"$execPath/HRV$exeFileExt\" \"$rr_file_name\" \"$hrv_file_name\" \"$interval\" \"$bin_file_name\" \"$sample_rate\"";
                exec($execpath);
                $hrv_data = $this->get_hrv_data($user, $ecg->filename,$ecg->filename_ext, $interval);
                
                if($hrv_data){
                    $hrv_result = $hrv_data;
                    return response('{"status":"success", "data":'.$hrv_result.', "uri":"'.$uri.'", "method":"'.$actionMethod.'"}')
                    ->header('Content-Type', "application/json");
                }else{
                    return response('{"status":"error", "error":{"message":["HRV analysis is error"]}, "uri":"'.$uri.'", "method":"'.$actionMethod.'"}')
                        ->header('Content-Type', "application/json");
                }
            }else{
                return response('{"status":"error", "error":{"message":["HRV data is not exist"]}, "uri":"'.$uri.'", "method":"'.$actionMethod.'"}')
                    ->header('Content-Type', "application/json");
            }
        }
        return response('{"status":"error", "error":{"message":["The ECG is not exist"]}, "uri":"'.$uri.'", "method":"'.$actionMethod.'"}');
    }
    function getResult(Request $request, $ecg_id){
        $uri = $this->getUri();
        $actionMethod = $this->getMethod();
        $user = $request->user();
        $user_id = $user->id;
        $interval = $request->input('interval', 5);
        if (isset($ecg_id)) {
            if($user->can('manager_type')){
                $ecg = $this->ecgRepository->get($ecg_id);
                $user = $ecg->user;
                $user_id = $user->id;
            }else{
                $ecg = $this->ecgRepository->getECG($user_id, $ecg_id);
            }
            if($user->cannot('hrv_interval_input')){
                $interval = 5;
            }
            $hrv_data = $this->get_hrv_data($user, $ecg->filename,$ecg->filename_ext, $interval);
            if($hrv_data){
                $hrv_result = $hrv_data;
                return response('{"status":"success", "data":'.$hrv_result.', "uri":"'.$uri.'", "method":"'.$actionMethod.'"}')
                    ->header('Content-Type', "application/json");
            }else{
                return response('{"status":"error", "error":"HRV analysis is error", "uri":"'.$uri.'", "method":"'.$actionMethod.'"}')
                    ->header('Content-Type', "application/json");
            }
        }else {
            return response('{"status":"error", "error":"HRV data is not exist", "uri":"'.$uri.'", "method":"'.$actionMethod.'"}')
                    ->header('Content-Type', "application/json");
        }
    }
     private function getUri(){
        return FacadesRequest::path();
    }
    private function getMethod(){
        return FacadesRequest::method();
    }
}

