<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\File\UploadHandler;
use App\ECG\Repositories\ECGRepository;
use App\User\Repositories\UserRepository;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Jobs\RunTransfer;
use App\Jobs\RunReport;
use App\Jobs\LinkService;
use App\Analysis\ReportHandler;
use App\ECG\Services\ECGService;
use Illuminate\Support\Facades\Validator;
use Storage;

class FileController extends APIController
{
	protected $ecgRepository;
	public function __construct(ECGRepository $ecgRepository, UserRepository $userRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->userRepository = $userRepository;
    }

    public function infoFile(Request $request, $id){
        $user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
        if( !$user ){
            return $this->failedResponse(['message'=>["User is not exists"]]);
        }
        $ecg = $user->ecgs()->find($id);
        $user_id = $user->id;
        if( !$ecg ){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $info_file_path = public_path("uploads/{$user_id}/ecg/".$ecg->filename);
        if(file_exists($info_file_path.'_info.txt')){
            return response()->download($info_file_path.'_info.txt');
        }else{
            return $this->failedResponse(['message'=>["File doesn't exist"]]);
        }
    }

    public function rrEditedFile(Request $request, $id){
        $user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
        if( !$user ){
            return $this->failedResponse(['message'=>["User is not exists"]]);
        }
        $ecg = $user->ecgs()->find($id);
        $user_id = $user->id;
        if( !$ecg ){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $info_file_path = public_path("uploads/{$user_id}/rr/".$ecg->filename);
        if(file_exists($info_file_path.'_rr_edited.txt')){
            return response()->download($info_file_path.'_rr_edited.txt');
        }else{
            return $this->failedResponse(['message'=>["File doesn't exist"]]);
        }
    }

    public function z2bFile(Request $request, $id){
        $user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
        if( !$user ){
            return $this->failedResponse(['message'=>["User is not exists"]]);
        }
        $ecg = $user->ecgs()->find($id);
        $user_id = $user->id;
        if( !$ecg ){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $z2b_file_path = public_path("uploads/{$user_id}/ekg/".$ecg->filename);
        if(file_exists($z2b_file_path.'.'.$ecg->filename_ext)){
            return response()->download($z2b_file_path.'.'.$ecg->filename_ext);
        }else{
            return $this->failedResponse(['message'=>["File doesn't exist"]]);
        }
    }
}

