<?php
namespace App\Http\Controllers\API;

use App\Traits\SlugTrait;
use App\Http\Controllers\File\UploadHandler;
use App\ECG\Repositories\ECGRepository;
use App\User\Repositories\UserRepository;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use App\Jobs\RunTransfer;
use App\Jobs\RunReport;
use App\Jobs\LinkService;
use App\Analysis\ReportHandler;
use App\ECG\Services\ECGService;
use Illuminate\Support\Facades\Validator;

class ECGController extends APIController
{
    use SlugTrait;
	protected $ecgRepository;
    protected $ecgService;
    protected $userRepository;
	public function __construct(ECGRepository $ecgRepository, ECGService $ecgService, UserRepository $userRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->ecgService = $ecgService;
        $this->userRepository = $userRepository;
    }
    public function index(Request $request){
    	$user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }

        $where_query = $user->ecgs();
            if($request->has('record_start')){
               $where_query = $where_query->where('recorded_at','>=',$request->input('record_start'));
            }
            if($request->has('record_end')){
                $record_end = date('Y-m-d H:i:s', strtotime($request->input('record_end').' +1 day'));
                $where_query = $where_query->where('recorded_at','<', $create_end);
            }
            if($request->has('create_start')){
               $where_query = $where_query->where('created_at','>=',$request->input('create_start'));
            }
            if($request->has('create_end')){
                $create_end = date('Y-m-d H:i:s', strtotime($request->input('create_end').' +1 day'));
                $where_query = $where_query->where('created_at','<', $create_end);
            }
            if($request->has('update_start')){
               $where_query = $where_query->where('updated_at','>=', $request->input('update_start'));
            }
            if($request->has('update_end')){
                $update_end = date('Y-m-d H:i:s', strtotime($request->input('update_end').' +1 day'));
                $where_query = $where_query->where('updated_at','<', $update_end);
            }
            if($request->has('checked')){
               $where_query = $where_query->where('checked',$request->input('checked'));
            }
        $ecgs = $where_query->get();
    	return $this->successResponse($ecgs->makeHidden(['user_id']));
    }
    public function allEcgs(Request $request){
        $user_emails = $request->input('user_emails',[]);
        $users = $this->userRepository->getsWith([],['email.in'=>$user_emails]);
        $user_ids = $users->map(function($d){return $d->id;})->toArray();
        $where_query = ['user_id.in'=>$user_ids];
        if($request->has('record_start')){
            $where_query['recorded_at.>='] = $request->input('record_start');
        }
        if($request->has('record_end')){
            $record_end = date('Y-m-d H:i:s', strtotime($request->input('record_end').' +1 day'));
            $where_query['recorded_at.<'] = $create_end;
        }
        if($request->has('create_start')){
            $where_query['created_at.>='] = $request->input('create_start');
        }
            
        if($request->has('create_end')){
            $create_end = date('Y-m-d H:i:s', strtotime($request->input('create_end').' +1 day'));
            $where_query['created_at.<'] = $create_end;
        }
        if($request->has('update_start')){
            $where_query['updated_at.>='] = $request->input('update_start');
            
        }
        if($request->has('update_end')){
            $update_end = date('Y-m-d H:i:s', strtotime($request->input('update_end').' +1 day'));
            $where_query['updated_at.<'] = $create_end;
        }

        if($request->has('checked')){
            $where_query['checked'] = $request->input('checked');
        }

        $ecgs = $this->ecgRepository->whereBy($where_query)->toGets();
        return $this->successResponse($ecgs->makeHidden(['user_id']));
    }
    public function create()
    {
        //
    }
    public function listById(Request $request, $id){
        $where_query = ['user_id'=>$id];
        if($request->has('queries')){
            $queries = json_decode($request->input('queries'), true);
            $where_query = array_merge($where_query, $queries);
        }
        $ecgs = $this->ecgRepository->getsWith([],$where_query);
        return $this->successResponse($ecgs);
    }

    public function store(Request $request)
    {
    	$user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
        if(!$user){
            return $this->failedResponse(['message'=>[trans('physiolguard.user_does_not_exist')]]);
        }
        $department_name = $user->department ? $user->department->name : '';
        
        $validator = $this->ecgValidator($request->all());
        if($validator->fails()){
            return $this->validateErrorResponse($validator->errors()->all());
        }

        $request_data = $request->only(['patient_IDNumber','patient_name','comment','age','sex', 'birthday','priority', 'service_SN', 'start_date', 'duration', 'site', 'sqi']);
        $sex = isset($request_data['sex']) ? $request_data['sex'] : 'M';
        $request_data['sex'] = in_array(strtoupper($sex), ['M', 'F']) ? strtoupper($sex) : ($sex == '男' ? 'M' : ($sex == '女' ? 'F' : $sex));
        $holter_number = $user->holters()->count();
        $holters = $user->holters;
        $storage = $user->storage;
        $file_size = isset($_FILES['files'])? $_FILES['files']['size']:0;//$request->file('files')->getClientSize();

        $user_id = $user->id;
        $_REQUEST['user_id'] = $user_id;
        $file_total_size=$this->ecgRepository->getECGTotalSize($user_id);
        if(($holter_number * 15 + $storage) * 1024 * 1024 * 1024 < $file_total_size + $file_size){
            return $this->failedResponse(['message'=>[trans('physiolguard.storage_out')]]);
        }else{
            $upload_handler = new UploadHandler(['user_id'=>$user_id, 'print_response'=>false]);

            if (!isset($upload_handler->response["files"][0]->error)){
                $request_data["filename"] = $upload_handler->getFileName();
                $request_data["filename_ext"] = $upload_handler->getFileNameEXT();
                $request_data["filesize"] = $upload_handler->getFileSize();
                $request_data["description"] = ($request->input('description') != null) ? $request->input('description') : '';
                $request_data["analysis_status"] = 0;
                $request_data["report_type"] = "-1";
                $request_data["priority"] = isset($request_data["priority"]) ? $request_data["priority"] : 0 ;
                if(in_array($department_name, ['TZUCHI'])){
                    $request_data["slug"] = $this->generateSlug();
                }
                $ecg = $user->ecgs()->create($request_data);
                
                if(isset($request_data['service_SN']) && $request_data['service_SN']!=''){
                    LinkService::dispatch($ecg->id)->onQueue('link_service');
                }

                if( $user->auto_transfer == 1){
                    RunTransfer::dispatch($ecg->id)->onQueue('transfer');
                }
                if($ecg){
                    $ecg->makeHidden(['user_id']);
                }

                return $this->successResponse($ecg?$ecg:[]);
            }
            return $this->failedResponse(['message'=>[trans('physiolguard.upload_failed')]]); 
        }
    }

    public function upload(Request $request)
    {
        $token = $request->bearerToken();
        if($token != 'AIzaSyBESFvIZRgc9V8-Y6LDbNpf-5ElkAhCjqo'){
            return $this->failedResponse(['message'=>['Unauthorized']]);
        }
        $validator = $this->ecgUploadValidator($request->all());
        if($validator->fails()){
            return $this->validateErrorResponse($validator->errors()->all());
        }
        $manager_user = $this->userRepository->getBy(['email'=>'eventmaster@holterholder.com']);

        if(!$manager_user){
            return $this->failedResponse(['message'=>['Unauthorized']]);
        }

        $user = $manager_user->users()->find($request->input('user_id',0));

        if(!$user){
            return $this->failedResponse(['message'=>['The user does not exist.']]);
        }

        $department_name = $user->department ? $user->department->name : '';
        
        $request_data = $request->only(['patient_IDNumber']);
        $request_data['service_SN'] = $request->input('serial_number');
        $base64_file = $request->input('files');
        $full_filename = $request->input('filename', uniqid().'_'.uniqid().'.z2b');
        $holter_number = $user->holters()->count();
        $holters = $user->holters;
        $storage = $user->storage;
        $user_id = $user->id;
        $file_size = (3 * (strlen($base64_file) / 4)) - substr_count($base64_file, '=');

        $file_total_size=$this->ecgRepository->getECGTotalSize($user_id);
        if(($holter_number * 15 + $storage) * 1024 * 1024 * 1024 < $file_total_size + $file_size){
            return $this->failedResponse(['message'=>[trans('physiolguard.storage_out')]]);
        }else{
            $file_content = base64_decode($base64_file);
            $this->upload_path = "uploads/{$user_id}/ekg/";
            $dir = public_path($this->upload_path);
            if (!is_dir($dir)) {
                $oldmask = umask(0);
                mkdir($dir, 0777, true);
                umask($oldmask);
            }
            $full_filename = $this->get_unique_filename($full_filename);
            $filename_ext = pathinfo($full_filename, PATHINFO_EXTENSION);
            $filename = pathinfo($full_filename, PATHINFO_FILENAME);
            $ifp = fopen( $dir.$full_filename, 'wb' ); 
            fwrite( $ifp, $file_content );
            fclose( $ifp ); 

            if (file_exists($dir.$full_filename)){
                $request_data["filename"] =  $filename;
                $request_data["filename_ext"] = $filename_ext;
                $request_data["filesize"] = $file_size;
                $request_data["analysis_status"] = 0;
                $request_data["report_type"] = "-1";
                $request_data["description"] = '';
                $request_data["priority"] = isset($request_data["priority"]) ? $request_data["priority"] : 0 ;
                
                $ecg = $user->ecgs()->create($request_data);
                if($ecg){
                    if(isset($request_data['service_SN']) && $request_data['service_SN']!=''){
                        LinkService::dispatch($ecg->id)->onQueue('link_service');
                    }

                    if( $user->auto_transfer == 1){
                        RunTransfer::dispatch($ecg->id)->onQueue('transfer');
                    }
                    $ecg->makeHidden(['user_id']);
                    return response()->json(['status'=>'Y', "desc"=>"上傳成功","ecg_id"=>$ecg->id]);
                }
            }
            return $this->failedResponse(['message'=>[trans('physiolguard.upload_failed')]]); 
        }
    }

    public function show(Request $request, $id)
    {
    	$user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
    	$ecg = $this->ecgRepository->getBy(['id'=>$id]);
        if(!$ecg){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $user_id = $ecg->user_id;

        if( !$ecg /*|| ($user_id != $user->id && ($user->cannot('data_type') || $user->users()->where('id',$user_id)->count()==0))*/){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        return $this->successResponse($ecg->makeHidden(['user_id','created_at', 'updated_at']));
    }

    public function edit($id)
    {
      
    }

    public function update(Request $request, $id)
    {
    	$user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
    	$prev_ecg = $this->ecgRepository->getBy(['user_id'=>$user->id,'id'=>$id]);
        if(!$prev_ecg){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
                 
        $request_data = $request->only(['patient_IDNumber','patient_name','comment','age','sex', 'birthday','priority','description', 'service_SN', 'start_date', 'duration', 'site','sqi']);
        foreach ($request_data as $key => $value) {
            if($value===null){
                if($key != 'priority' && $key != 'duration'){
                    $request_data[$key] = '';
                }else{
                    unset($request_data[$key]);
                }
            }
        }
        $data = $request_data;

        $ecg = $this->ecgRepository->update($id, $data);

        if(isset($request_data['description']) && $request_data['description']!='' && $prev_ecg->description != $request_data['description']){
            if($user->auto_report==1){
                $reportHandler = new ReportHandler($ecg);
                if($user->department && in_array($user->department->name, ['HRV', 'HRVMED'])){
                    RunReport::dispatch($reportHandler)->onQueue('priority_report');
                }else{
                    RunReport::dispatch($reportHandler)->onQueue('report');
                }
            }
        }

        return $this->successResponse($ecg?$ecg->makeHidden(['user_id','created_at', 'updated_at']):[]);
    }

    public function destroy(Request $request, $id)
    {
    	$user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
    	$ecg = $this->ecgRepository->getBy(['user_id'=>$user->id,'id'=>$id]);
        if(!$ecg){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $this->ecgRepository->delete($id);
        return $this->successResponse(['id'=>$id]);

    }
    public function report(Request $request, $id) 
    {
            $user = $request->user();
            if(!$user){
                $user = $this->userRepository->get($request->input('user_id',0));
            }
            $ecg = $this->ecgRepository->getBy(['user_id'=>$user->id,'id'=>$id]);
            if(!$ecg){
                return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
            }
            $type = $request->input('type',0);
            $ecg->update(['report_type'=>$type]);
            if($type==3){
                $ecg->update(['report_status'=>2]);
                return $this->successResponse(['id'=>$id, 'report_status'=>2]);
            }
            $reportHandler = new ReportHandler($ecg);
            $ecg->update(['report_status'=>3]);

            if($user->department && in_array($user->department->name, ['HRV', 'HRVMED'])){
                RunReport::dispatch($reportHandler)->onQueue('priority_report');
            }else{
                RunReport::dispatch($reportHandler)->onQueue('manual_report');
            }

            return $this->successResponse(['id'=>$id, 'report_status'=>3, 'url'=>url('user/ecgs/'.$id.'/report/download')]);
    }
    public function reportDownload(Request $request, $id){
        $user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
        $ecg = $this->ecgRepository->getBy(['id'=>$id]);
        $user_id = $ecg->user_id;

        if( !$ecg ||
            ($user_id != $user->id &&
             ( $user->cannot('data_type') || 
                $user->users()->where('id',$user_id)->count()==0)
                )
            ){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename);
        if(file_exists($report_file_path.'.pdf')){
            $new_filename = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
            return response()->download($report_file_path.'.pdf', $new_filename.'.pdf');
        }else{
            return $this->failedResponse(['message'=>["File doesn't exist"]]);
        }
    }

    public function openReportDownload(Request $request, $id){
        $ecg = $this->ecgRepository->getBy(['id'=>$id]);
        if( !$ecg ){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $user_id = $ecg->user_id;
        $report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename);
        if(file_exists($report_file_path.'.pdf')){
            $new_filename = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
            return response()->download($report_file_path.'.pdf', $new_filename.'.pdf');
        }else{
            return $this->failedResponse(['message'=>["File doesn't exist"]]);
        }
    }

    public function openReportDownloadBySlug(Request $request, $slug){
        $ecg = $this->ecgRepository->getBy(['slug'=>$slug]);
        if( !$ecg ){
            return $this->failedResponse(['message'=>[trans('auth.permission_denied')]]);
        }
        $user_id = $ecg->user_id;
        $report_file_path = public_path("uploads/{$ecg->user_id}/report/".$ecg->filename);
        if(file_exists($report_file_path.'.pdf')){
            $new_filename = str_replace($ecg->holter_IDNumber, $ecg->patient_IDNumber, $ecg->filename);
            return response()->file($report_file_path.'.pdf', [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$new_filename.'.pdf"'
            ]);
        }else{
            return $this->failedResponse(['message'=>["File doesn't exist"]]);
        }
    }

    protected function ecgValidator(array $data)
    {
        return Validator::make($data, [
            'files'=>'required|file',
        ]);        
    }

     protected function ecgUploadValidator(array $data)
    {
        return Validator::make($data, [
            'files'=>'required',
            'user_id'=>'required|numeric',
            'serial_number'=>'required',
            'patient_IDNumber'=>'required'
        ]);        
    }


    public function userData(Request $request, $id)
    {
        $user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
        if($user->cannot('data_type')){
            return $this->successResponse([]);
        }
        $d_user = $user->users()->find($id);
        if($d_user){
            $where_query = $d_user->ecgs();
            if($request->has('record_start')){
               $where_query = $where_query->where('recorded_at','>=',$request->input('record_start'));
            }
            if($request->has('record_end')){
                $record_end = date('Y-m-d H:i:s', strtotime($request->input('record_end').' +1 day'));
                $where_query = $where_query->where('recorded_at','<', $create_end);
            }
            if($request->has('create_start')){
               $where_query = $where_query->where('created_at','>=',$request->input('create_start'));
            }
            if($request->has('create_end')){
                $create_end = date('Y-m-d H:i:s', strtotime($request->input('create_end').' +1 day'));
                $where_query = $where_query->where('created_at','<', $create_end);
            }
            if($request->has('update_start')){
               $where_query = $where_query->where('updated_at','>=', $request->input('update_start'));
            }
            if($request->has('update_end')){
                $update_end = date('Y-m-d H:i:s', strtotime($request->input('update_end').' +1 day'));
                $where_query = $where_query->where('updated_at','<', $update_end);
            }
            $ecgs = $where_query->get();
        }else{
            $ecgs=false;
        }
        return $this->successResponse($ecgs ? $ecgs->makeHidden(['user_id']):[]);
    }
    function getResult(Request $request, $ecg_id){
        $user = $request->user();
        if(!$user){
            $user = $this->userRepository->getBy(['email'=>$request->input('user_email','')]);
        }
        $type = $request->input('type');
        if(!$type){
            return $this->failedResponse(['message'=>["Request had invalid type"]]);
        }
        if($type == 'rr'){
            $data = $this->ecgService->get_rr_data($user->id, $ecg_id);
        }else{
            $data = $this->ecgService->get_ecg_lead_data($user->id, $ecg_id, $type);
        }
        
        if($data){
            return $this->successResponse(json_decode($data));
        } else {
            return $this->failedResponse(['message'=>["Request had invalid ecg"]]);
        }
    }

    protected function upcount_name_callback($matches) {
        $index = isset($matches[1]) ? ((int)$matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return '_dup'.$index.$ext;
    }

    protected function upcount_name($name) {
        return preg_replace_callback(
            '/(?:(?:_dup([\d]+))?(\.[^.]+))?$/',
            array($this, 'upcount_name_callback'),
            $name,
            1
        );
    }

    protected function get_upload_path($name) {
        return public_path($this->upload_path.$name);
    }

    protected function get_unique_filename($name) {
        while(is_dir($this->get_upload_path($name))) {
           $name = $this->upcount_name($name);
        }
        while (is_file($this->get_upload_path($name))) {
            $name = $this->upcount_name($name);
        }
        return $name;
    }
}

