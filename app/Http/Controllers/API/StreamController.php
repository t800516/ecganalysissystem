<?php
namespace App\Http\Controllers\API;

use App\ECG\Repositories\ECGRepository;
use App\Http\Controllers\Analysis\StreamHandler;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;

class StreamController extends APIController
{
	protected $ecgRepository;
	public function __construct(ECGRepository $ecgRepository)
    {
        $this->ecgRepository = $ecgRepository;
    }
    public function start(Request $request)
    {
        $user = $request->user();
        
        $holter_IDNumber = $request->input('holter_IDNumber');
        $holter = $user->holters()->where('IDNumber', $holter_IDNumber)->first();

        if($holter){
            $streamHandler = new StreamHandler($holter_IDNumber, time());
            $timestamp = $streamHandler->heartbeat();
            return $this->successResponse(['user_id'=> $user->id, 'holter_id'=>$holter->id, 'status'=>'1']);
        }

        return $this->failedResponse(['message'=>['The holter is not exists']]); 
    }

    public function stream(Request $request)
    {
        //$user = $request->user();
        $holter_IDNumber = $request->input('holter_IDNumber');
        $timestamp = $request->input('timestamp');
        $streamHandler = new StreamHandler($holter_IDNumber);
        $streamHandler->heartbeat();

        $streamHandler->cache($request->only(['ecg_l1','ecg_l2','ecg_l3','ppg_r','ppg_ir', /*'r_dc', 'ir_dc',*/'name','ID','bed_no','hr_up','hr_down','hr','respr_up','respr_down','respr','spo2_down','spo2']), $timestamp);
        if($request->has("hr")){
            $result = $streamHandler->getHrsData();
            $heart_rate = $result["heart_rate"];
            $spo2 = $result["spo2"];
            $respiration_rate = $result["respiration_rate"];
        }else{
            $output = $streamHandler->run();
            $result = explode("\t", $output);
            $heart_rate = $result[0];
            $spo2 = $result[1];
            $respiration_rate = $result[2];
        }


        return $this->successResponse(['holter_IDNumber'=> $holter_IDNumber, 'timestamp'=> $timestamp , 'heart_rate'=>$heart_rate, 'spo2'=>$spo2,'respiration_rate'=>$respiration_rate]);
    }

    public function stop(Request $request)
    {
    	$user = $request->user();
        $holter_IDNumber = $request->input('holter_IDNumber');
        $holter = $user->holters()->where('IDNumber', $holter_IDNumber)->first();

        if($holter){
            $streamHandler = new StreamHandler($holter_IDNumber, time());
            $timestamp = $streamHandler->clearheartbeat();
            $streamHandler->clearcache();
            return $this->successResponse(['user_id'=> $user->id, 'holter_id'=>$holter->id, 'status'=>'0']);
        }

        return $this->failedResponse(['message'=>['The holter is not exists']]); 
    }
}

