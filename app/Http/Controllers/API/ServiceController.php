<?php

namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Http\Request;
use App\Service\Repositories\ServiceRepository;
use App\User\Repositories\UserRepository;
use App\Http\Controllers\APIController;

class ServiceController extends APIController
{

    protected $serviceRepository;
    protected $userRepository;

    public function __construct(ServiceRepository $serviceRepository, UserRepository $userRepository)
    {
        $this->serviceRepository = $serviceRepository;
        $this->userRepository = $userRepository;
    }
    public function index(Request $request)
    {
        $user = $request->user();
        $service = $user->services()->orderBy('created_at','desc')->orderBy('start_date','desc')->first();
        $userService = $user->userServices()->orderBy('created_at','desc')->orderBy('start_date','desc')->first();
        $resultService = null;
        if($service){
            $resultService = $service;
            if($userService){
                $resultService = $service->created_at > $userService->created_at ? $service : $userService;
            }
        }else{
            $resultService = $userService;
        }

        return $this->successResponse($resultService ? $resultService->makeHidden(['user_id', 'patient_user_id', 'created_at', 'updated_at']) : []);
    }
}