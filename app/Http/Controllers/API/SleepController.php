<?php
namespace App\Http\Controllers\API;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ECG\Services\ECGService;
use App\Http\Controllers\APIController;

class SleepController extends APIController
{

    protected $ecgService;
    
    public function __construct(ECGService $ecgService)
    {
        $this->ecgService = $ecgService;
    }

    public function getResult(Request $request, $ecgID)
    {
        $user = $request->user();
        $userID = $user->id;
        $ecg = $this->ecgService->get_ecg($userID, $ecgID);
        $type = $request->input('type');
        if(!$type){
            return $this->failedResponse(['message'=>["Request had invalid type"]]);
        }
        $types = ['stage', 'arousal', 'spo2', 'ppg', 'ecg', 'heart_rate', 'ppt_pb', 'respiration_rate', 'position'];
        $type = array_search($type, $types);
        if($ecg){
            $sleep_file_path = public_path("uploads/{$userID}/sleep/");
            $tests = [
                    $ecg->filename.'_1_Stage_30sec.txt',
                    $ecg->filename.'_2_Arousal_1Hz_edited.txt',
                    $ecg->filename.'_3_SPO2_1Hz.txt',
                    $ecg->filename.'_4_PPG_128Hz.bin',
                    $ecg->filename.'_5_ECG_128Hz.bin',
                    $ecg->filename.'_6_HR_PTT_RespR_1Hz.txt',
                    $ecg->filename.'_6_HR_PTT_RespR_1Hz.txt',
                    $ecg->filename.'_6_HR_PTT_RespR_1Hz.txt',
                    $ecg->filename.'_7_Position_10sec.txt'];
            $data = '[]';
            $sleep_filename = $sleep_file_path.$tests[$type];

            if($type==6){
                $column = 1;
            }else if($type==7){
                $column = 2;
            }else{
                $column = 0;
            }
            if (file_exists($sleep_filename)) {
                $result = $this->getFile($sleep_filename, $column, pathinfo($sleep_filename)['extension']=='bin');
                $data = $result;
            }
            return $this->successResponse(['data'=>json_decode($data), 'recorded_at'=>$ecg->recorded_at]);
        }else{
            return $this->failedResponse(['message'=>["Request had invalid ecg"]]);
        }
    }

    private function getFile($filename, $column=0, $binary = false){
        $mode = $binary? "rb":"r";
        if (!$file_handle = fopen($filename, $mode)) {
            return '[]';
        }
        if($binary){
            $filesize = filesize($filename);
            $data_str = '';
            for($position = 0 ; $position < $filesize; $position+=4){
                if(fseek($file_handle, $position) == -1){
                    break;
                }
                $contents = fread($file_handle, 4);
                $line_array = unpack("i*", $contents);
                $data_str .= ($position!=0 ? "," : "").$line_array[1];
            }
            $result = '['.$data_str.']';
        }else{
            $line = fgets($file_handle);
            $dataLine = explode("\t", trim($line));
            if(is_array($column)){
                $datarow = '';
                foreach ($column as $key => $col) {
                    if(isset($dataLine[$col])){
                            $datarow .= ($key == 0 ? '':',').$dataLine[$col];
                        }
                }
                $data = '['.$datarow.']';
            }else{
                $data = $dataLine[$column];
            }
            while ($line = fgets($file_handle)) {
                $dataLine = explode("\t", trim($line));
                if(is_array($column)){
                    $datarow = '';

                    foreach ($column as $key => $col) {
                        if(isset($dataLine[$col])){
                            $datarow .= ($key == 0 ? '':',').$dataLine[$col];
                        }
                    }
                    $data .= ',['.$datarow.']';
                }else{
                    $data .= ','.$dataLine[$column];
                }
            }
            $result = '['.$data.']';
        }
        fclose($file_handle);
        return $result;
    }
}
