<?php

namespace App\Http\Controllers\API;

use Auth;
use Illuminate\Http\Request;
use App\Jobs\RunTransfer;
use Request as RequestStatic;
use App\Http\Controllers\File\TransferHandler;
use App\Http\Controllers\APIController;
Use App\ECG\Repositories\ECGRepository;
Use App\Holter\Repositories\HolterRepository;

class TransferController extends APIController
{
    protected $ecgRepository;
    protected $holterRepository;

    public function __construct(ECGRepository $ecgRepository, HolterRepository $holterRepository)
    {
        $this->ecgRepository = $ecgRepository;
        $this->holterRepository = $holterRepository;
    }

    public function run(Request $request)
    {
        $user = $request->user();
        $holter_number = $user->holters()->count();
        $holters = $user->holters;
        $user_id = $user->id;
        $ecg_ids = $request->input('ecg_ids',[]);
        $ecg_ids = is_array($ecg_ids) ? $ecg_ids: [$ecg_ids];
        $result = [];
        foreach ($ecg_ids as $key => $ecg_id) {
            $ecg = $user->ecgs()->where('id',$ecg_id)->first();
            if(!$ecg){
                $ecg_data = [
                    "analysis_status" => 10
                ];
                $this->ecgRepository->setInfo($ecg_id, $ecg_data);
                array_push($result, ['id'=>$ecg_id, 'status'=>$ecg_data["analysis_status"]]);
                //$this->output_msg($ecg_id, $ecg_data);
                continue;
            }
            $ecg_data = [
                        "analysis_status" => 93,
                    ];
            $this->ecgRepository->setInfo($ecg_id, $ecg_data);
            //$this->output_msg($ecg->id, $ecg_data);
            RunTransfer::dispatch($ecg->id)->onConnection('sync');//->onQueue('manual_transfer');
            $analysis_ecg = $this->ecgRepository->get($ecg->id);
            if($analysis_ecg){
                array_push($result, ['id'=>$ecg->id, 'status'=>$analysis_ecg->analysis_status]);
            }

            /*
            $transfer_handler = new TransferHandler($user_id, $ecg->filename.'.'.$ecg->filename_ext);
            $file_path = public_path("uploads/{$user_id}/ekg/".$ecg->filename.'.'.$ecg->filename_ext);
            $ecg_data = [
                        "analysis_status" => 9,
                    ];
            $this->ecgRepository->setInfo($ecg_id, $ecg_data);
            $this->output_msg($ecg->id, $ecg_data);
            $ecganalysis_time = $transfer_handler->run_to_txt($file_path);
            
            if($transfer_handler->info_file_exists()){
                $check_result = $transfer_handler->checkHolterID($holters, Auth::user()->can('holter_id_pass'));
                if($check_result){
                    $recorded_at = $check_result['recorded_at'] < strtotime('1970-01-01') || $check_result['recorded_at'] >= strtotime('2037-01-01') ? null : $check_result['recorded_at'];
                    $ecg_data = [
                        "ecganalysis_time" => $ecganalysis_time,
                        "recorded_at" =>  date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', $recorded_at).' -8 hours')),
                        "sample_rate" => $check_result['sample_rate'],
                        "holter_IDNumber" => $check_result['holter_id'],
                        "analysis_status" => 1
                    ];
                    $this->ecgRepository->setInfo($ecg_id, $ecg_data);
                    $this->output_msg($ecg->id, $ecg_data);

                    $ecg_data = [
                        "analysis_status" => 92,
                    ];
                    $this->ecgRepository->setInfo($ecg_id, $ecg_data);
                    $this->output_msg($ecg->id, $ecg_data);

                    $rranalysis_time = $transfer_handler->run_to_rr();
                    $ecg_data = [
                        "rranalysis_time" => $rranalysis_time,
                        "analysis_status" => 82
                    ];
                    $this->ecgRepository->setInfo($ecg_id, $ecg_data);
                    $this->output_msg($ecg->id, $ecg_data);

                    $rerranalysis_time = $transfer_handler->run_to_rr_second();
                    $ecg_data = [
                        "rranalysis_time" => $rerranalysis_time,
                        "analysis_status" => 2
                    ];
                    $this->ecgRepository->setInfo($ecg_id, $ecg_data);
                    $this->output_msg($ecg->id, $ecg_data);
                }else{
                    $ecg_data = [
                        "ecganalysis_time" => $ecganalysis_time,
                        "analysis_status" => 11
                    ];
                    $this->ecgRepository->setInfo($ecg_id, $ecg_data);
                    $this->output_msg($ecg->id, $ecg_data); 
                }
            }else{
                $ecg_data = [
                    "analysis_status" => 10
                ];
                $this->ecgRepository->setInfo($ecg_id, $ecg_data);
                $this->output_msg($ecg->id, $ecg_data);
            }*/
        }
        return response()->json(['status'=>'success','data'=>$result, 'uri'=>$this->getUri(), 'method'=>$this->getMethod()]);
    }
    function output_msg($id, $data){
        $uri = $this->getUri();
        $actionMethod = $this->getMethod();
        echo json_encode(['status'=>'success','data'=>['id'=>$id, 'status'=>$data["analysis_status"], 'uri'=>$uri, 'method'=>$actionMethod]]);
        ob_flush(); 
        flush();
        sleep(1);
    }
    private function getUri(){
        return RequestStatic::path();
    }
    private function getMethod(){
        return RequestStatic::method();
    }
}
