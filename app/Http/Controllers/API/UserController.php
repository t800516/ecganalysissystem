<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User\Repositories\UserRepository;

class UserController extends APIController
{
	protected $userRepository;
	public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request){
        $user = $request->user();
        if($user->role_id != '4'){
            $this->successResponse([]);
    	}
        $users = $user->users()->with('department')->get()->makeHidden(['guide','checked','created_at','updated_at','sex', 'birthday','zipcode', 'country', 'city','downtown','address','tel','cel', 'role_id','storage','remark','department_id','pivot']);
    	return $this->successResponse($users);
    }

    public function show(Request $request){
        $email = $request->input('email');
        $user = $this->userRepository->getBy(['email'=>$email]);
        return $this->successResponse($user);
    }
}

