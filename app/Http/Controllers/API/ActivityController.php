<?php
namespace App\Http\Controllers\API;
use Storage;
use App\Activity\Repositories\ActivityRepository;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ActivityController extends APIController
{
	protected $activityRepository;
	public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository = $activityRepository;
        $this->columnHidden = ["user_id","path"];
    }
    public function index(Request $request){
    	$user = $request->user();
        $activities = $user->activities->makeHidden($this->columnHidden);
    	return $this->successResponse($activities);
    }

    public function store(Request $request)
    {
    	$user = $request->user();
        
        $validator = $this->activityValidator($request->all());
        if($validator->fails()){
            return $this->validateErrorResponse($validator->errors()->all());
        }

        $file = $request->file('file');
        $directory = ($user->id).'/activities';
        Storage::disk("upload")->makeDirectory($directory);
        $filename = $file->getClientOriginalName();
        while (Storage::disk("upload")->exists($directory.'/'.$filename)) {
            $filename = $this->upcount_name($filename);
        }
        $path = $file->storeAs($directory, $filename, "upload");

        $activity = $user->activities()->create(['path'=>$path, 'filename'=>$filename]);
        if($activity){
            $activity->makeHidden($this->columnHidden);
            return $this->successResponse($activity);
        }
        return $this->failedResponse(["message"=>"upload file failed"]);
    }

    public function show(Request $request, $id)
    {
    	$user = $request->user();
        $activity = $user->activities()->find($id);
        if($activity){
            $activity->makeHidden($this->columnHidden);
            return $this->successResponse($activity);
        }
        return $this->failedResponse(["message"=>"The activity is not exists"]);
    }

    public function update(Request $request, $id)
    {
    	$user = $request->user();
       
        return $this->successResponse();
    }

    public function destroy(Request $request, $id)
    {
        $user = $request->user();
    	
        $activity = $user->activities()->find($id);
        if($activity){
            Storage::disk("upload")->delete($activity->path);
            $activity->delete();
            $activity->makeHidden($this->columnHidden);
            return $this->successResponse(["id"=>$activity->id]);
        }
        return $this->failedResponse(["message"=>"The activity is not exists"]);

    }

    protected function activityValidator(array $data)
    {
        return Validator::make($data, [
            'file'=>'required|file',
        ]);        
    }
    protected function upcount_name_callback($matches) {
        $index = isset($matches[1]) ? ((int)$matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return '_dup'.$index.$ext;
    }

    protected function upcount_name($name) {
        return preg_replace_callback(
            '/(?:(?:_dup([\d]+))?(\.[^.]+))?$/',
            array($this, 'upcount_name_callback'),
            $name,
            1
        );
    }
}

