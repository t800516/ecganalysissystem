<?php

namespace App\Http\Controllers\API\Auth;
use Carbon\Carbon;
use Hash;
use App\User;
use App\Traits\OauthToken;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;
use Illuminate\Support\Facades\Validator;

class TokenController extends APIController
{
    use OauthToken;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    public function token(Request $request){
        $response = $this->clientCredentialsGrantToken($request);
        return $this->successResponse($response);
    }

    public function check(Request $request){
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return $this->validateErrorResponse($validator->errors()->all());
        }
        $email = $request->input('email');
        $password = $request->input('password');
        $user = User::where('email',$email)->first();
        if($user){
            if(Hash::check($request->input('password'), $user->getAuthPassword())){
                return $this->successResponse(['check'=>1]);
            }
        }
        return $this->successResponse(['check'=>0]);
    }

    public function accessToken(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return $this->validateErrorResponse($validator->errors()->all());
        }
        $email = $request->input('email');
        $password = $request->input('password');
        $user = User::where('email',$email)->first();
        if($user){
            if(($user->expiration_date && $user->expiration_date > date('Y-m-d'))){
               return $this->failedResponse(['message'=>['Your account is expired.']]);
            }
            if(Hash::check($request->input('password'), $user->getAuthPassword())){
                $user->touch();
            }
            if ($user->checked == 0) {
                return $this->failedResponse(['message'=>['Your account is under review.']]);
            }
        }else{
            return $this->failedResponse(['message'=>['Thw account is not exist.']]);
        }
        $response = $this->passwordGrantToken($request);
        $response['agreement'] = $user->agreement;
        return $this->successResponse($response);
    }
    public function refreshAccessToken(Request $request)
    {
        $validator = $this->refreshValidator($request->all());
        if ($validator->fails()) {
            return $this->validateErrorResponse($validator->errors()->all());
        }
        return $this->successResponse($this->refreshGrantToken($request));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|exists:users',
            'password' => 'required|string|min:6',
        ]);
    }
    protected function refreshValidator(array $data)
    {
        return Validator::make($data, [
            'refresh_token' => 'required',
        ]);
    }
    protected function credentials(Request $request){
        return $request->only(['email','password']);
    }
}
