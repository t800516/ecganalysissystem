<?php

namespace App\Http\Controllers\API\Auth;

use App\User;
use App\Traits\OauthToken;
use Illuminate\Http\Request;
use App\Http\Controllers\APIController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;

class RegisterController extends APIController
{
    use OauthToken;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function username(Request $request)
    {
        return $this->successResponse(['username'=>$request->user()->email]);
    }
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return $this->validateErrorResponse($validator->errors()->all());
        }
        event(new Registered($user = $this->create($request->all())));
        
        return $this->registered($request,$user);
    }

    protected function create(array $data)
    {
        $insert_data=[
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'checked' => 1,
            'storage'=>15,
            'role_id' => 2
        ];
        return User::create($insert_data);
    }

    protected function registered(Request $request,$user)
    {
        $token = $this->passwordGrantToken($request);
        //$token['user'] = $user;
        //$token['profile'] = $this->createProfile($request,$user);
        return $this->successResponse($token);
    }

    protected function createProfile(Request $request,$user){
        $store_data = $request->only(['nickname','name','sex','address','birthday','phone','company_id','invoice_title']);
        $profile = $user->profile()->create($store_data);
        return $profile;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'name' => 'required|string|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }

}
