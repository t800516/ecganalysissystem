<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User\Repositories\UserRepository;

class ProfileController extends APIController
{
	public function __construct(UserRepository $userRepository)
    {
       $this->userRepository = $userRepository;
    }

    public function show(Request $request)
    {
    	$user = $request->user();
    	$profile = $this->userRepository->getWith($user->id, ['department','department.report_types'])->makeHidden(['id','guide','checked','created_at','updated_at','sex', 'birthday','zipcode', 'country', 'city','downtown','address','tel','cel', 'role_id','storage','remark','department_id', 'agreed_at']);
        if($profile->department){
    		foreach($profile->department->report_types as $report_type){
    			$report_type->makeHidden(['id','department_id','created_at','updated_at','duration','duration_low', 'duration_up']);
    		}
    		$profile->department->makeHidden(['id','select_type','created_at','updated_at','duration','duration_low', 'duration_up']);
    	}
        $data = $profile ? $profile->toArray() : [];
        if($user){
            $data['user_id'] = $user->id;
        }
        return $this->successResponse($data);
    }

    public function update(Request $request)
    {
        $user = $request->user();
        $validator = $this->updateValidator($request->all());
        if ($validator->fails()) {
            return $this->validateErrorResponse($validator->errors()->all());
        }
        $agreement = $request->input('agreement', '0');
        $agreed_at = $agreement == '1' ? date("Y-m-d H:i:s") : null;
        $this->userRepository->update($user->id, ["agreed_at"=>$agreed_at]);
        return $this->successResponse(["agreement"=>$agreement]);
    }

    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'agreement' => 'in:0,1',
        ]);
    }
}

