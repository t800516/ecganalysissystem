<?php

namespace App\Http\Controllers\Message;

use Mail;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function __construct()
    {
      
    }

    public function send(Request $request){
        $this->validator($request->all())->validate();
        $data=$request->only(['firstname', 'lastname','email','msg','phone']);
        $maildata=[
            'from' => $data['email'],
            'name' => $data['firstname'].' '.$data['lastname'],
            'to' => 'service@physiolguard.com',
            'subject' => 'ECG System Contact',
            'msg' => $data['msg'],
            'phone' => $data['phone']
        ];
        $this->sendto($maildata,'emails.contactMsg');
        return redirect('/#contact_box');
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'captcha' => 'required|captcha',
            'msg' => 'required',
        ]);
    }

    protected function sendto(array $maildata, $view)
    {
        Mail::send($view, $maildata, function ($message) use ($maildata){
            $message->from($maildata['from'], $maildata['name']);
            $message->to($maildata['to'])->subject($maildata['subject']);
        });
    }
}