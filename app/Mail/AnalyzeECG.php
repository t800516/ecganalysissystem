<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnalyzeECG extends Mailable
{
    use Queueable, SerializesModels;
    protected $ecgs;
    protected $priority;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($priority, $ecgs)
    {
        $this->ecgs = $ecgs;
        $this->priority = $priority;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Sage心電圖分析網站經檢查後有尚未處理檔案
{$this->ecgs->count()}筆，檢查間隔為{$this->priority->period_hr}時{$this->priority->period_min}分")->from('no-reply@physiolguard.com','Physiol Guard')->view('mail.analysis')->with(['ecgs'=>$this->ecgs]);
    }
}
