<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QTCReport extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    protected $ecg;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ecg)
    {
        $this->ecg = $ecg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Sage心電圖分析QTC報表-".$this->ecg->filename)->from('no-reply@physiolguard.com','Physiol Guard')->view('mail.qtcReport')->with(['ecg'=>$this->ecg]);
    }
}
