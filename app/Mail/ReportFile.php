<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReportFile extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    protected $ecg;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ecg)
    {
        $this->ecg = $ecg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $report_file_path = public_path("uploads/".$this->ecg->user_id."/report/".$this->ecg->filename.".pdf");
        return $this->subject("Sage心電圖分析報表-".$this->ecg->filename)->from('no-reply@physiolguard.com','Physiol Guard')->view('mail.reportFile')->attach($report_file_path)->with(['ecg'=>$this->ecg]);
    }
}
