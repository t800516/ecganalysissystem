<?php

namespace App\Providers;

use App;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        Passport::routes();

        Passport::tokensExpireIn(now()->addDays(90));

        Passport::refreshTokensExpireIn(now()->addDays(100));

		// Avoid requesting a non-exist table problem which prevents 'artisan migrate' working
		if(App::runningInConsole()) { return; }

		$permissions = \App\Permission::with('roles')->get();
		foreach ($permissions as $permission) {
			$gate->define($permission->name, function ($user) use ($permission) {
				return $user->hasPermission($permission);
			});
		}
    }
}
