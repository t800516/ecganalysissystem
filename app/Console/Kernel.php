<?php

namespace App\Console;
use App\ECG;
use App\Priority;
use App\NotifiableModule;
use App\Mail\AnalyzeECG;
use App\Jobs\DeleteData;
use App\Http\Controllers\File\TransferHandler;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // if(Schema::hasTable('priorities') && Schema::hasTable('ecgs')){
        //     $priority = new Priority;
        //     $ecg = new ECG;
        //     $priorities = $priority->with(['emails'])->orderBy('value','ASC')->orderBy('updated_at', 'DESC')->get();
        //     foreach($priorities as $priority){
        //         $hr = $priority->period_hr==0 ? '*/1': '*/'.$priority->period_hr;
        //         $min = $priority->period_hr==0 ? ($priority->period_min==0? '*' : '*/'.$priority->period_min) : $priority->period_min;
                
        //         $schedule->call(function() use($priority, $ecg){
        //                 $ecgs = $ecg->whereNotIn('user_id', $priority->exception_users->map(function ($item, $key) {return $item->id;}))->where('analysis_status',0)->where('priority',$priority->value)->get();
        //                 foreach($priority->emails as $email){
        //                     if($ecgs->count()!=0){
        //                         Mail::to($email)->send(new AnalyzeECG($priority, $ecgs));
        //                     }
        //                 }
        //         })->cron($min.' '.$hr.' * * * *')->between($priority->start_hr.':'.$priority->start_min, $priority->end_hr.':'.$priority->end_min);
        //     }
        // }
        $run_at = ['00:00', '11:00', '17:00'];
        foreach($run_at as $daily_at){
            $schedule->call(function () {
                $now = date('Y-m-d 00:00:00', strtotime('-14 day'));
                $notifiableModules = NotifiableModule::with('departments')->get();
                foreach ($notifiableModules as $notifiableModule) {
                    $moduleClass = 'App\\NotifiableModule\\Services\\'.$notifiableModule->name.'NotifiableModule';
                    $module = (new $moduleClass());
                    foreach ( $notifiableModule->departments as $department) {
                        $users = $department->users;
                        foreach($users as $user){
                            $services = $user->userServices;
                            foreach ($services as $service) {
                                $ecgs = $service->ecgs()->where('report_status',2)->where('checked',1)->where('notified',0)->where('checked_time','>=', $now)->get();
                                if($ecgs->count()){
                                    echo $service->serial_number.':'.$ecgs->count().PHP_EOL;
                                    if(in_array($notifiableModule->name, ['TZUCHI'])){
                                        $parse_data = $module->getSendData($ecgs);
                                        foreach ($parse_data as $row_data) {
                                            $result = $module->send($row_data);
                                            $result = $ecgs->map(function($d){return $d->id;})->toArray();
                                            if(count($result)){
                                                ECG::whereIn('id', $result)->update(['notified'=>1]);
                                            }
                                        }
                                    }else{
                                        foreach ($ecgs as $ecg) {
                                            $result = $module->send($ecg);
                                            $result = true;
                                            if($result){
                                                ECG::where('id', $ecg->id)->update(['notified'=>1]);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }
                            }


                            $services = $user->services;
                            foreach ($services as $service) {
                                $ecgs = $service->ecgs()->where('report_status',2)->where('checked',1)->where('notified',0)->where('checked_time','>=', $now)->get();
                                if($ecgs->count()){
                                    echo $service->serial_number.':'.$ecgs->count().PHP_EOL;
                                    if(in_array($notifiableModule->name, ['TZUCHI'])){
                                        $parse_data = $module->getSendData($ecgs);
                                        foreach ($parse_data as $row_data) {
                                            $result = $module->send($row_data);
                                            $result = $ecgs->map(function($d){return $d->id;})->toArray();
                                            if(count($result)){
                                                ECG::whereIn('id', $result)->update(['notified'=>1]);
                                            }
                                        }
                                    }else{
                                        foreach ($ecgs as $ecg) {
                                            $result = $module->send($ecg);
                                            $result = true;
                                            if($result){
                                                ECG::where('id', $ecg->id)->update(['notified'=>1]);
                                            }
                                            sleep(1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })->dailyAt($daily_at);
        }
        $schedule->job(new DeleteData)->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
