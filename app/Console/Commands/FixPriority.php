<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Service\Repositories\ServiceRepository;

class FixPriority extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:priority';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send test data to TMUH api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ServiceRepository $serviceRepository)
    {
        $services = $serviceRepository->getsWith([],['site.null'=>true,'priority'=>0]);
        foreach ($services as $key => $service) {
           $ecg = $service->ecgs()->orderBy('created_at','desc')->first();
            if($ecg){
                $service->update(['priority'=>$ecg->priority]);
            }
        }
    }
}
