<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ECG\Repositories\ECGRepository;
use Storage;
class SendTMUHTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:TMUH';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send test data to TMUH api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ECGRepository $ecgRepository)
    {
        $dataFiles = ['A201079342.json', 'F103559391.json', 'J201889453.json'];
        foreach ($dataFiles as $key => $dataFile) {
            $ecgJsons = json_decode(Storage::get($dataFile));
            $ecg_ids = array_map(function($item){return $item->id;}, $ecgJsons);
            $ecgs = $ecgRepository->getsWith(['service'], ['id.in'=>$ecg_ids]);
            foreach ($ecgs as $key2 => $ecg) {
                echo $ecg->id.', '.$ecg->filename.PHP_EOL;
                $report_path = public_path('uploads/'.$ecg->user->id.'/report/'.$ecg->filename.'.pdf');
                if(file_exists($report_path)){
                    echo 'send...'.PHP_EOL;
                    $report_content = base64_encode(file_get_contents($report_path));
                    $data_fields = [
                        'ecg_id'=>$ecg->id,
                        'recorded_at'=>date('Y-m-d H:i:s', strtotime($ecg->recorded_at)),
                        'created_at'=>date('Y-m-d H:i:s', strtotime($ecg->created_at)),
                        'updated_at'=>date('Y-m-d H:i:s', strtotime($ecg->updated_at)),
                        'report_type'=>$ecg->report_type,
                        'report_kind'=>'2',
                        'report_content01'=> $report_content,
                        'report_content02'=> $report_content,
                        'report_result'=>'-',
                        'holter_idnumber'=>$ecg->holter_IDNumber.'TEST',
                        //'fee_no'=> ($ecg->service_SN && $ecg->service_SN != '') ? $ecg->service_SN : $ecg->service ? $ecg->service->serial_number : 'null',
                        'service_sn'=> ($ecg->service_SN && $ecg->service_SN != '') ? $ecg->service_SN : ($ecg->service ? $ecg->service->serial_number : '-'),
                        'start_date'=>($ecg->start_date && $ecg->start_date != '0000-00-00') ? $ecg->start_date : ($ecg->service ? $ecg->service->start_date : date('Y-m-d', strtotime($ecg->created_at))),
                        'duration'=>($ecg->duration && $ecg->start_date != '') ? $ecg->duration : ($ecg->service ? $ecg->service->duration : 1)
                    ];
                    $response = $this->sendRequest($data_fields);
                    $data_fields['report_content01'] = 'ignore...';
                    $data_fields['report_content02'] = 'ignore...';
                    Storage::append('TMUH_API.log',json_encode(["date"=>date('Y-m-d H:i:s'),'data'=>$data_fields, 'result'=>$response]));
                    sleep(2);
                }else{
                    echo 'no report'.PHP_EOL;
                    Storage::append('TMUH_API.log',json_encode(["date"=>date('Y-m-d H:i:s'),'data'=>['id'=>$ecg->id], 'result'=>'no report']));
                }
            }
        }
    }
    private function xml_data($data_fields){
        $data_xml = '';
        foreach($data_fields as $key => $value){
            $data_xml .='<'.$key.'>'.$value.'</'.$key.'>';      
        }
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                              <soap:Body>
                                <ECGFunc2 xmlns="http://tempuri.org/">
                                  '.$data_xml.' 
                                </ECGFunc2>
                              </soap:Body>
                            </soap:Envelope>';
        return $xml_post_string;
    }
    private function getResponseData($input_line){
        preg_match('/<ECGFunc2Result>(.*)<\/ECGFunc2Result>/', $input_line, $output_array);
        if(count($output_array)>1){
            return json_decode($output_array[1]);
        }
        return false;
    }
    private function sendRequest($data){
        $post_data = $this->xml_data($data);
        $apiHost = 'http://210.61.66.4:8082/';
        $http = new \GuzzleHttp\Client;
        $response = $http->request('post', $apiHost, [
                'headers'=>[
                    'Content-Type' => 'text/xml; charset=UTF8',
                    'SOAPAction' => 'http://tempuri.org/ECGFunc2',
                    //'Content-length'=> strlen($post_data)
                ],
                'body' => $post_data
            ]);
        return $this->getResponseData((string)$response->getBody());
    }
}
