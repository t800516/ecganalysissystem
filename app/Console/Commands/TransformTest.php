<?php

namespace App\Console\Commands;

use App\ECG;
use App\Permission;
use App\Http\Controllers\File\TransferHandler;
use Illuminate\Console\Command;
use App\ECG\Repositories\ECGRepository;
use App\Jobs\RunReport;
use App\Analysis\ReportHandler;

class TransformTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:transform {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'test transform';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ECGRepository $ecgRepository,Permission $permission)
    {
        
        $ecg_id = $this->argument('id');
        $ecg = ECG::find($ecg_id);
        if($ecg){
            $user = $ecg->user;
            $user_id = $user->id;
            $holters = $user->holters;
            $transfer_handler = new TransferHandler($user_id, $ecg->filename.'.'.$ecg->filename_ext);
            $file_path = public_path("uploads/{$user_id}/ekg/".$ecg->filename.'.'.$ecg->filename_ext);
            $ecg_data = [
                    "analysis_status" => 9,
                ];
            $ecgRepository->setInfo($ecg_id, $ecg_data);

            $ecganalysis_time = $transfer_handler->run_to_txt($file_path);

            if($transfer_handler->info_file_exists()){
                $holter_id_pass_permission = $permission->where('name','holter_id_pass')->first();
                $check_result = $transfer_handler->checkHolterID($holters, $user->hasPermission($holter_id_pass_permission));
                if($check_result){
                    $ecg_data = [
                            "ecganalysis_time" => $ecganalysis_time,
                            "recorded_at" => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', $check_result['recorded_at']).' -8 hours')),
                            "sample_rate" => $check_result['sample_rate'],
                            "holter_IDNumber" => $check_result['holter_id'],
                            "analysis_status" => 1
                    ];
                    $ecgRepository->setInfo($ecg_id, $ecg_data);
                       
                    $ecg_data = [
                            "analysis_status" => 92,
                    ];
                    $ecgRepository->setInfo($ecg_id, $ecg_data);
                        
                    $rranalysis_time = $transfer_handler->run_to_rr(public_path());
                    $ecg_data = [
                            "rranalysis_time" => $rranalysis_time,
                            "analysis_status" => 82
                    ];
                    $ecgRepository->setInfo($ecg_id, $ecg_data);
                        
                    $rerranalysis_time = $transfer_handler->run_to_rr_second(public_path());
                    $ecg_data = [
                        "rranalysis_time" => $rerranalysis_time,
                        "analysis_status" => 2
                    ];
                    $ecgRepository->setInfo($ecg_id, $ecg_data);

                    $predict_result = $transfer_handler->run_predict($file_path, $check_result['sample_rate']);
                    $ecg_data = [
                        "hf" => $predict_result
                    ];
                    if($predict_result !== null){
                        $ecgRepository->setInfo($ecg_id, $ecg_data);
                    }
                        
                    $report_type = $transfer_handler->get_report_type();
                    //if($run_report){
                    $ecg->report_type = $report_type;
                    $ecg->report_type_alg = $report_type;
                    $ecg->report_status = 0;
                    $ecg->save();
                    if($user->auto_report==1){
                        $reportHandler = new ReportHandler($ecg);
                        if($user->department && in_array($user->department->name, ['HRV', 'HRVMED'])){
                            RunReport::dispatch($reportHandler)->onQueue('priority_report');
                        }else{
                            RunReport::dispatch($reportHandler)->onQueue('report');
                        }
                    }
                    //}
                }else{
                    $ecg_data = [
                        "ecganalysis_time" => $ecganalysis_time,
                        "analysis_status" => 11
                    ];
                    $ecgRepository->setInfo($ecg_id, $ecg_data);
                }
            }else{
                $ecg_data = [
                        "analysis_status" => 10
                ];
                $ecgRepository->setInfo($ecg_id, $ecg_data);
            }
        }
    }
}
