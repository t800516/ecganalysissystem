<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ECG\Repositories\ECGRepository;

class FixECGRecordedAt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:recoredat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix data of recored at';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ECGRepository $ecgRepository)
    {
        $ecgs = $ecgRepository->getsWith([],['id.>='=>'176814']);
        foreach ($ecgs as $key => $ecg) {
            echo $ecg->id.' '.$ecg->created_at.' '.$ecg->recorded_at.' '.date('Y-m-d H:i:s', strtotime($ecg->recorded_at.' -8 hours')).PHP_EOL;
            $ecg->update(['recorded_at'=>date('Y-m-d H:i:s', strtotime($ecg->recorded_at.' -8 hours'))]);
        }
    }
}
