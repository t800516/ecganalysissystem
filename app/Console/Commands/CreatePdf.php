<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use iio\libmergepdf\Merger;
use PDF;
use Storage;

class CreatePdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $merger = new Merger;
        // $merger->addfile(public_path('uploads/806/report/WK0T0000909_20241023070033.pdf'));
        // $createdPdf = $merger->merge();
        // Storage::put('WK0T0000909_20241023070033.pdf', $createdPdf);


        $imagePath = app_path('Console/Commands/WK0T0000909_20241022064050.png');
        
        $data = [
            'imagePath' => $imagePath,
        ];

        $pdf = PDF::loadView('pdf/png_to_pdf', $data);
        $pdf->setPaper('A4', 'portrait');
        Storage::put('WK0T0000909_20241022064050.pdf',$pdf->output());
    }
}
