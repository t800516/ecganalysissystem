<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User\Repositories\UserRepository;
use App\ECG\Repositories\ECGRepository;
use Storage;
class SendTZUCHITest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:TZUCHI {--user_id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send test data to TZUCHI api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(UserRepository $userRepository, ECGRepository $ecgRepository)
    {
        $user_id = $this->option('user_id');
        if(!$user_id)return false;
        $user = $userRepository->get($user_id);
        $department = $user->department;
        if($department->module){
            $moduleClass = 'App\\NotifiableModule\\Services\\'.$department->module->name.'NotifiableModule';
            $module = (new $moduleClass());
            $ecgs = $user->ecgs()->where('report_status', 2)->where('checked', 1)->where('notified', 0)->orderBy('id','desc')->get();
            $parse_data = $module->getSendData($ecgs);
            $out_put = [];
            $finish_count = 0;
            $notifiable_records = $ecgs->count();
            foreach ($parse_data as $row_data) {
                $result = $module->send($row_data);
                if(count($result)){
                    $ecgRepository->whereBy(['id.in'=>$result])->toUpdate(['notified'=>1]);
                    $finish_count += count($result);
                    $out_put[] = [
                        'total_count'=>$notifiable_records,
                        'finish_count'=>$finish_count,
                        'status'=>'progressing'
                    ];
                }
            }
        }
    }
}
