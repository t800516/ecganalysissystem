<?php
namespace App\Patient\Repositories;
use App\Repositories\Repository;
use App\Patient;

class PatientRepository extends Repository
{
    public function __construct(Patient $patient)
    {
        $this->model = $patient;
    }
}