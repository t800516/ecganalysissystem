<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $fillable = ['value','start_hr','start_min','end_hr','end_min','period_hr','period_min','exception_users'];
    public function emails(){
        return $this->hasMany('App\Priority_email');
    }
    public function exception_users(){
        return $this->belongsToMany('App\User','priority_exceptions','user_id','priority_id');
    }
}
