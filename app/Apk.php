<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apk extends Model
{
    //
    protected $fillable = [
        'name','path','department_id'
    ];
    protected $appends = ['url'];
    public function department(){
    	return $this->belongsTo('App\Department');
    }
    public function getUrlAttribute(){
        return url($this->path);
    }
}
