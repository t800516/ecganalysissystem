<?php
namespace App\Survey\Repositories;
use App\Repositories\Repository;
use App\Survey;

class SurveyRepository extends Repository
{
    public function __construct(Survey $survey)
    {
        $this->model = $survey;
    }
}