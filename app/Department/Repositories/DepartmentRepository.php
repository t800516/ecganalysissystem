<?php
namespace App\Department\Repositories;
use App\Repositories\Repository;
use App\Department;

class DepartmentRepository extends Repository
{
    public function __construct(Department $department)
    {
        $this->model = $department;
    }
}