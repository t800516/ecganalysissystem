<?php
namespace App\Session\Repositories;

use App\Session;
class SessionRepository
{
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function updateLoginSession($session_id,$user_id){
      $this->session->where('id','<>',$session_id)->where('user_id',$user_id)->delete();//update(['user_id'=>0]);
    }
}