<?php
namespace App\Activity\Repositories;
use App\Repositories\Repository;
use App\Activity;

class ActivityRepository extends Repository
{
    public function __construct(Activity $model)
    {
        $this->model = $model;
        $this->condition = $model;
    }
}