<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = 'admins';
    protected $fillable = ['account', 'password', 'auth'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function hasPermission($permisson){
        return $this->hasRole($permisson->roles);
    }
}
