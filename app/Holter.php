<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holter extends Model
{
    //
    protected $fillable = [
        'IDNumber'
    ];
    protected $appends= ['stream_status'];
    public function users(){
    	return $this->belongsToMany('App\User');
    }
    public function getStreamStatusAttribute(){
    	return $this->islive($this->IDNumber) ? '1':'0';
    }
    private function islive($holter_IDNumber){
        //$statusPath = public_path("uploads/{$user_id}/stream/{$holter_id}_status");
        $statusPath = public_path("uploads/holter/{$holter_IDNumber}/{$holter_IDNumber}_status");
        if(!file_exists($statusPath)){
        	return false;
        }
        $lasttime = (int)file_get_contents($statusPath);
        $time = time();
        return $time <= ($lasttime + 3);
    }
}
