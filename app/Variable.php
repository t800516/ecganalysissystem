<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
     protected $fillable = [
        'user_id','name','data'
    ];
}
