<?php
namespace App\ECG\Repositories;

use App\Repositories\Repository;
use App\ECG;

class ECGRepository extends Repository
{
    protected $ecg;

    public function __construct(ECG $ecg)
    {
        $this->ecg = $ecg;
        $this->model = $ecg;
        $this->condition = $ecg;
    }

   	public function getECGs($user_id){
   		return $this->ecg->where('user_id',$user_id)->orderBy('created_at','DESC')->get();
   	}

    public function getsBy($where=[]){
      $model = $this->ecg;
      foreach ($where as $key => $value) {
        $column_array = explode('.', $key);
        if(count($column_array)>1){
          $model = $model->where($column_array[0],$column_array[1], $value);
        }else{
          $model = $model->where($key, $value);
        }
      }
      return $model->get();
    }

    public function getECGTotalSize($user_id){
      return $this->ecg->where('user_id',$user_id)->sum('filesize');
    }

    public function getECG($user_id,$id){
      return $this->ecg->where('user_id',$user_id)->where('id',$id)->first();
    }
    public function get($id){
      return $this->ecg->where('id',$id)->first();
    }

   	public function delECG($id,$user_id){
   		return $this->ecg->where('user_id',$user_id)->where('id',$id)->delete();
   	}
   	public function checkFilename($id,$filename,$user_id){
   		if($this->ecg->where('filename',$filename)->where('id',$id)->where('user_id',$user_id)->count()>0)
   			return false;
   		else
   			return true;
   	}
    public function checkOwner($id,$user_id){
      if($this->ecg->where('id',$id)->where('user_id',$user_id)->count()>0)
        return true;
      else
        return false;
    }
   	public function setFilename($id,$filename){
   		$this->ecg->where('id',$id)->update(['filename'=>$filename]);
   	}
   	public function insertECG($data){
   		return $this->ecg->create($data);
   	}
    public function setInfo($id,$data){
      $this->ecg->where('id',$id)->update($data);
    }
}