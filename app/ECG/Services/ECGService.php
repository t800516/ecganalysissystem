<?php
namespace App\ECG\Services;

use App\ECG\Repositories\ECGRepository;

class ECGService
{
    protected $ecgRepository;
  
    public function __construct(ECGRepository $ecgRepository)
    {
        $this->ecgRepository = $ecgRepository;
    }
    public function get_ecg($userID, $ecgID, $userIgnore=false, $assignee_type=false){
        if($assignee_type){
            $ecg = $this->ecgRepository->getBy(['assignee'=>$userID, 'id'=>$ecgID]);
        }else{
            $ecg = $userIgnore ? $this->ecgRepository->get($ecgID) : $this->ecgRepository->getECG($userID, $ecgID);
        }
        if(isset($ecg)){
            return $ecg;
        }else{
            return false;
        }
    }
    public function get_ecg_name($userID, $ecgID){
        $ecg = $this->ecgRepository->getECG($userID, $ecgID);
        if(isset($ecg)){
            return $ecg->filename;
        }else{
            return false;
        }
    }

    public function get_ecg_ext($userID, $ecgID){
        $ecg = $this->ecgRepository->getECG($userID, $ecgID);
        if(isset($ecg)){
            return $ecg->filename_ext;
        }else{
            return false;
        }
    }

    public function get_ecg_recorded_at($userID, $ecgID){
        $ecg = $this->ecgRepository->getECG($userID, $ecgID);
        if(isset($ecg)){
            return $ecg->recorded_at;
        }else{
            return false;
        }
    }

    public function get_ecg_sample_rate($userID, $ecgID){
        $ecg = $this->ecgRepository->getECG($userID, $ecgID);
        if(isset($ecg)){
            return $ecg->sample_rate;
        }else{
            return false;
        }
    }
    public function get_ecg_list($user_id){
        return  $this->ecgRepository->getECGs($user_id);
    }
    public function get_ecg_data($userID, $ecgID, $sample_rate = 8)
    {
        $ecg = $this->get_ecg($userID, $ecgID);
        if(!$ecg){
            return false;
        }
        $filename = $ecg->filename;
        $filename_ext = $ecg->filename_ext;
        $recorded_at = $ecg->recorded_at;
        $sample_rate = (int)($ecg->sample_rate/128);
        $sample_rate = $sample_rate == 0 ? 1 : $sample_rate;
        $ecgData = ["L1" => "", "L2" => "", "L3" => "", "IR"=>"", "R"=>""];
        if($filename){
            $ecg_suffix = ".bin";
            $ecg_path = public_path('uploads/' . $userID . '/ecg/' . $filename . $ecg_suffix);
            
            $mode = "rb";
            
            if (!$file_handle = fopen($ecg_path, $mode)) {
                return '[]';
            }
            $filesize = filesize($ecg_path);

            for($position = 0 ; $position < $filesize; $position+=20){
                if(($position/20) % $sample_rate!=0){
                    continue;
                }
                if(fseek($file_handle, $position) == -1){
                    break;
                }

                $contents = fread($file_handle, 20);
                $line_array = unpack("i*", $contents);
                $ecgData["L1"] .= ($position!=0 ? "," : "").$line_array[1];
                $ecgData["L2"] .= ($position!=0 ? "," : "").$line_array[2];
                $ecgData["L3"] .= ($position!=0 ? "," : "").$line_array[3];
                $ecgData["IR"] .= ($position!=0 ? "," : "").$line_array[4];
                $ecgData["R"]  .= ($position!=0 ? "," : "").$line_array[5];
            }

            fclose($file_handle);
            return '{"L1": [' . $ecgData["L1"] . '],' .
                    '"L2": [' . $ecgData["L2"] . '],' .
                    '"L3": [' . $ecgData["L3"] . '],' .
                    '"IR": [' . $ecgData["IR"] . '],' .
                    '"R": [' . $ecgData["R"] . '],' .
                    '"recorded_at": "' . $recorded_at . '",' .
                    '"filename_ext": "' . $filename_ext . '"}';
        }else{
            return false;
        }
    }
    public function get_ecg_data_256($userID, $ecgID)
    {
        $ecg = $this->get_ecg($userID, $ecgID);
        if(!$ecg){
            return false;
        }
        $sample_rate = (int)($ecg->sample_rate/256);
        $filename = $ecg->filename;
        $filename_ext = $ecg->filename_ext;
        if($filename){
            $ecg_data = '';
            $ecg_suffix = ".bin";
            $ecg_path = public_path('uploads/' . $userID . '/ecg/' . $filename . $ecg_suffix);
            
            $mode = "rb";
            if (!$file_handle = fopen($ecg_path, $mode)) {
                return '[]';
            }
            $filesize = filesize($ecg_path);

            for($position = 0 ; $position < $filesize; $position+=20){
                if(($position/20) % $sample_rate!=0){
                    continue;
                }
                if(fseek($file_handle, $position) == -1){
                    break;
                }

                $contents = fread($file_handle, 20);
                $line_array = unpack("i*", $contents);
                $ecg_data .= ($position!=0 ? "," : "").$line_array[1];
            }

            fclose($file_handle);
            return '['.$ecg_data.']';
        }else{
            return false;
        }
    }

    public function get_rr_data($userID, $ecgID)
    {
        $ecg = $this->get_ecg($userID, $ecgID);
        if(!$ecg){
            return false;
        }
        $filename = $ecg->filename;
        $filename_ext = $ecg->filename_ext;
        
        if($filename){
            $rr_suffix = '_rr_edited.txt';

            $rr_path = public_path('uploads/' . $userID . '/rr/' . $filename . $rr_suffix);

            if (!is_file($rr_path)) {
                return false;
            }

            if(!$file_handle = fopen($rr_path, "r")){
                return false; 
            }

            $line = fgets($file_handle);
            $rr_line = explode(' ', trim($line));
            $rr_data = '';
            if (count($rr_line)>=3){
                $rr_data .= '{"peak":"'.$rr_line[1].'","interval":'.$rr_line[2].',"time":'.$rr_line[0].'}';
            }
            while (($line = fgets($file_handle)) !== false) {
                $rr_line = explode(' ', trim($line));
                $rr_data .= ',{"peak":"'.$rr_line[1].'","interval":'.$rr_line[2].',"time":'.$rr_line[0].'}';
                //array_push($rr_data, ["peak" => $rr_line[1], "interval" => $rr_line[2], "time" => $rr_line[0]]);
            }
            fclose($file_handle);
            return '['.$rr_data.']';
        }else{
            return false;
        }
    }

    public function get_event_data($userID, $ecgID)
    {
        $filename = $this->get_ecg_name($userID, $ecgID);
        $filename_ext = $this->get_ecg_ext($userID, $ecgID);
        $recorded_at = strtotime($this->get_ecg_recorded_at($userID, $ecgID));
        //$mspp=0.008;
        $mspp=0.0078125;
        if($filename){
            $event_suffix = '_event.txt';
            $event_path = public_path('uploads/' . $userID . '/ecg/' . $filename . $event_suffix);
            if (!is_file($event_path)) {
                return false;
            }

            if(!$file_handle = fopen($event_path, "r")){
                return false; 
            }

            $line = fgets($file_handle);
            $event_line = explode(' ', trim($line));
            $event_data = '{"peak":"E","interval":0,"time":'.(((int)$event_line[0]-$recorded_at)/$mspp).'}';
            while (($line = fgets($file_handle)) !== false) {
                $event_line = explode(' ', trim($line));
                $event_data .= ',{"peak":"E","interval":0,"time":'.(((int)$event_line[0]-$recorded_at)/$mspp).'}';
            }
            fclose($file_handle);
            return '['.$event_data.']';
        }else{
            return false;
        }
    }

    public function get_analysis_info($userID, $ecgID, $ignore)
    {
        $ecg = $this->get_ecg($userID, $ecgID, $ignore);

        if($ecg){

            $filename = $ecg->filename;
            $filename_ext = $ecg->filename_ext;
            $rr_suffix = '_rr_edited.txt';
            $rr_path = public_path('uploads/' . $userID . '/rr/' . $filename . $rr_suffix);
            $result=[
                'peaks'=>[],
                'total_time'=>0
            ];
            if (!is_file($rr_path)) {
                return false; 
            }
            
            if(!$file_handle = fopen($rr_path, "r")){
                return false; 
            }

            while (($line = fgets($file_handle)) !== false) {
                $rr_line = explode(' ', trim($line));
                if(!array_key_exists ( $rr_line[1] , $result['peaks'])){
                    $result['peaks'][$rr_line[1]]=0;
                }
                $result['peaks'][$rr_line[1]]+=1;
                $result['total_time']=(float)$rr_line[0]/(256/2);
            }
            fclose($file_handle);
            return json_encode($result);
        }else{
            return false;
        }
    }
    public function addPeakToFile($userID, $ecgID, $insertPeak, $nextPeak)
    {
        $filename = $this->get_ecg_name($userID, $ecgID);
        $filename_ext = $this->get_ecg_ext($userID, $ecgID);

        if($filename){
            $rrSuffix = "_rr_edited.txt";
            $rrPath = public_path("uploads/" . $userID . "/rr/" . $filename . $rrSuffix);
            $fileHandle = fopen($rrPath, "r+");

            $newContent = "";
            if ($fileHandle) {
                // Store the content till the insert point
                while ($line = fgets($fileHandle)) {
                    $rrLine = explode(" ", trim($line));
                    if ($rrLine[0] < $insertPeak["time"]) {
                        $newContent .= $line;
                    } else {
                        break;
                    }
                }

                // Insert the new peak and the updated next peak into the content
                $newContent .= $insertPeak["time"] . " " . $insertPeak["type"] . " " . $insertPeak["interval"] . " " .((isset($rrLine[3])? $rrLine[3] : "0")). "\n";
                // Ignore the line of the old next peak since we updated it with the code above
                if($nextPeak) {
                    $newContent .= $nextPeak["time"] . " " . $nextPeak["type"] . " " . $nextPeak["interval"] . " " .(isset($rrLine[3])? $rrLine[3] : "0"). "\n";
                }
                
                // Store the remaining content
                while ($line = fgets($fileHandle)) {
                    $newContent .= $line;
                }

                // Clear the file before writing the new content
                ftruncate($fileHandle, 0);
                rewind($fileHandle);

                // Write the new content
                fwrite($fileHandle, $newContent);

                // Close file
                fclose($fileHandle);
                return true;
            }
            return false;
        }else{
            return false;
        }
    }

    public function delPeakToFile($userID, $ecgID, $delPeak, $nextPeak)
    {
        $filename = $this->get_ecg_name($userID, $ecgID);
        $filename_ext = $this->get_ecg_ext($userID, $ecgID);

        if($filename){
            $rrSuffix = "_rr_edited.txt";
            $rrPath = public_path("uploads/" . $userID . "/rr/" . $filename . $rrSuffix);
            $fileHandle = fopen($rrPath, "r+");

            $newContent = "";
            if ($fileHandle) {
                // Store the content till the delete point
                // The peak to be deleted will be discarded here
                while ($line = fgets($fileHandle)) {
                    $rrLine = explode(" ", trim($line));
                    if ($rrLine[0] == $delPeak["time"]) {
                        break;
                    } else {
                        $newContent .= $line;
                    }
                }

                // Ignore the line of the old next peak since we updated it with the code above
                if($line = fgets($fileHandle)){
                    $rrLine = explode(" ", trim($line));
                    if ($nextPeak["type"] != "") {
                        $newContent .= $nextPeak["time"] . " " . $nextPeak["type"] . " " . $nextPeak["interval"]. " " .(isset($rrLine[3])? $rrLine[3] : "0"). "\n";
                    }
                }

                // Store the remaining content
                while ($line = fgets($fileHandle)) {
                    $newContent .= $line;
                }

                // Clear the file before writing the new content
                ftruncate($fileHandle, 0);
                rewind($fileHandle);

                // Write the new content
                fwrite($fileHandle, $newContent);

                // Close file
                fclose($fileHandle);
                return true;
            }
            return false;
        }else{
            return false;
        }
    }
    public function delPeaksToFile($userID, $ecgID, $delPeaks, $nextPeak)
    {
        $filename = $this->get_ecg_name($userID, $ecgID);
        $filename_ext = $this->get_ecg_ext($userID, $ecgID);

        if($filename){
            $rrSuffix = "_rr_edited.txt";
            $rrPath = public_path("uploads/" . $userID . "/rr/" . $filename . $rrSuffix);
            $fileHandle = fopen($rrPath, "r+");

            $newContent = "";
            if ($fileHandle) {
                // Store the content till the delete point
                // The peak to be deleted will be discarded here
                while ($line = fgets($fileHandle)) {
                    $rrLine = explode(" ", trim($line));
                    if(count($delPeaks)>0){
                        if (in_array($rrLine[0], $delPeaks)) {
                            if($delPeaks[count($delPeaks)-1] == $rrLine[0]){
                                break;
                            }else{
                                continue;
                            }
                        } else {
                            $newContent .= $line;
                        }
                    } else {
                        $newContent .= $line;
                    }
                }

                if($line = fgets($fileHandle)){
                     // Insert the updated next peak into the content

                    $rrLine = explode(" ", trim($line));
                    if ($nextPeak["type"] != "") {
                        $newContent .= $nextPeak["time"] . " " . $nextPeak["type"] . " " . $nextPeak["interval"]. " ". (isset($rrLine[3])? $rrLine[3] : "0")."\n";
                    }
                }

                // Store the remaining content
                while ($line = fgets($fileHandle)) {
                    $newContent .= $line;
                }

                // Clear the file before writing the new content
                ftruncate($fileHandle, 0);
                rewind($fileHandle);

                // Write the new content
                fwrite($fileHandle, $newContent);

                // Close file
                fclose($fileHandle);
                return true;
            }
            return false;
        }else{
            return false;
        }
    }
    public function setPeaksToFile($userID, $ecgID, $peaksTime, $peak){
        $filename = $this->get_ecg_name($userID, $ecgID);
        $filename_ext = $this->get_ecg_ext($userID, $ecgID);

        if($filename){
            $rrSuffix = "_rr_edited.txt";
            $rrPath = public_path("uploads/" . $userID . "/rr/" . $filename . $rrSuffix);
            $fileHandle = fopen($rrPath, "r+");

            $newContent = "";
            $delinterval = 0;
            while ($line = fgets($fileHandle)) {
                $rrLine = explode(" ", trim($line));
                if (in_array($rrLine[0], $peaksTime)) {
                    if($peak!='D'){
                        $newContent .= $rrLine[0] . " " . $peak . " " . $rrLine[2] . " ". (isset($rrLine[3])? $rrLine[3] : "0") ."\n";
                    }else{
                        $delinterval += (int)$rrLine[2];
                    }
                } else {
                    $newContent .= $rrLine[0] . " " . $rrLine[1] . " " . ((int)$rrLine[2]+$delinterval)  . " ". (isset($rrLine[3])? $rrLine[3] : "0") ."\n";
                    $delinterval = 0;
                }
            }
            ftruncate($fileHandle, 0);
            rewind($fileHandle);

            // Write the new content
            fwrite($fileHandle, $newContent);

            // Close file
            fclose($fileHandle);
            return true;
        }else{
            return false;
        }
    }
    public function export_rr_data($userID, $ecgID){
        
        $filename = $this->get_ecg_name($userID, $ecgID);
        $filename_ext = $this->get_ecg_ext($userID, $ecgID);

        if($filename){
            $rrSuffix = "_rr_edited.txt";
            $rrPath = public_path("uploads/" . $userID . "/rr/" . $filename . $rrSuffix);
            //return response()->download($rrPath);

            if (!is_file($rrPath)) {
                return false;
            }

            $file_handle = fopen($rrPath, "r");

            if ($file_handle) {
                $line = fgets($file_handle);
                $rr_data = '';
                while (($line = fgets($file_handle)) !== false) {
                    $rr_line = explode(' ', trim($line));
                    $rr_data .= $rr_line[0] . " " . $rr_line[1] . " " . $rr_line[2] . (isset($rr_line[3])? " " . $rr_line[3]:"") . "\r\n";
                }
                fclose($file_handle);
                return ['content'=>$rr_data,'filename'=>$filename . $rrSuffix];
            }else{
                return false;
            }
        }
    }

    public function get_report_summary($userID, $ecgID){
        $filename = $this->get_ecg_name($userID, $ecgID);
        $filename_ext = $this->get_ecg_ext($userID, $ecgID);
        $ecg = $this->ecgRepository->getECG($userID, $ecgID);

        if($filename){
            
            $rr_suffix = '_rr_edited.txt';
            $sample_rate = 256/2;

            $rr_path = public_path('uploads/' . $userID . '/rr/' . $filename . $rr_suffix);
            $result=[
                'filename'=>'',
                'start_time'=>'',
                'total_time'=>0,
                'total_beats'=>0,
                'min_hr'=>200,
                'min_hr_time'=>0,
                'max_hr'=>0,
                'max_hr_time'=>0,
                'mean_hr'=>0,
                'V_total'=>0,
                'V_pairs'=>0,
                'S_total'=>0,
                'S_pairs'=>0
            ];
            $counter=[
                'prev_is_V'=>false,
                'prev_is_S'=>false
            ];

            if (!is_file($rr_path)) {
                return false;
            }

            if(!$file_handle = fopen($rr_path, "r")){
                return false;
            }

            while (($line = fgets($file_handle)) !== false) {
                $rr_line = explode(' ', trim($line));

                if($rr_line[1] == 'N') {
                    if($counter['prev_is_V'] || $counter['prev_is_S']) {
                        $counter['prev_is_V'] = false;
                        $counter['prev_is_S'] = false;
                    }
                }
                if($rr_line[1] == 'V') {
                    $result['V_total']+=1;
                    if($counter['prev_is_V']) {
                        $result['V_pairs']+=1;
                    } else {
                        $counter['prev_is_V'] = true;
                        $counter['prev_is_S'] = false;
                    }
                }
                if($rr_line[1] == 'S') {
                    $result['S_total']+=1;
                    if($counter['prev_is_S']) {
                        $result['S_pairs']+=1;
                    } else {
                        $counter['prev_is_S'] = true;
                        $counter['prev_is_V'] = false;
                    }
                }

                if($result['total_beats'] != 0) {
                    $hr = 60/($rr_line[2]/($sample_rate*8));
                    if($hr < $result['min_hr']) {
                        $result['min_hr'] = $hr;
                        $result['min_hr_time'] = $rr_line[0];
                    }
                    if($hr > $result['max_hr']) {
                        $result['max_hr'] = $hr;
                        $result['max_hr_time'] = $rr_line[0];
                    }
                    $result['mean_hr']+=$hr;

                }

                $result['total_time']=(float)$rr_line[0]/$sample_rate;
                $result['total_beats']+=1;
            }
            $result['mean_hr'] = $result['mean_hr'] / $result['total_beats'];
            $result['filename'] = $ecg->filename;
            $result['start_time'] = $ecg->recorded_at;
            fclose($file_handle);
            return $result;
        }else{
            return false;
        }
    }

    public function get_general_profile($userID, $ecgID, $interval=60){
        $ecg = $this->get_ecg($userID, $ecgID);

        if($ecg){
            
            $rr_suffix = '_rr_edited.txt';
            $sample_rate = 256/2;
            $rr_path = public_path('uploads/' . $userID . '/rr/' . $ecg->filename . $rr_suffix);
            

            if (!is_file($rr_path)) {
                return false;
            }

            if(!$file_handle = fopen($rr_path, "r")){
                return false;
            }
            $time_interval=0;

            $result=[
                'filename'=>$ecg->filename,
                'start_time'=>$ecg->recorded_at,
                'total_time'=>0,
                'total_beats'=>0,
                'min_hr'=> PHP_INT_MAX,
                'min_hr_time'=>0,
                'max_hr'=>0,
                'max_hr_time'=>0,
                'mean_hr'=>0,
                'V_total'=>0,
                'V_pairs'=>0,
                'V_single'=>0,
                'S_total'=>0,
                'S_pairs'=>0,
                'S_single'=>0,
                'interval_result'=>[]
            ];
            $interval_init_result=[
                'start_time'=>'',
                'end_time'=>'',
                'total_time'=>0,
                'total_beats'=>0,
                'min_hr'=> PHP_INT_MAX,
                'min_hr_time'=>0,
                'max_hr'=>0,
                'max_hr_time'=>0,
                'mean_hr'=>0,
                'V_total'=>0,
                'V_pairs'=>0,
                'V_single'=>0,
                'S_total'=>0,
                'S_pairs'=>0,
                'S_single'=>0,
            ];

            $interval_result = $interval_init_result;
            $last_end_time = 0;
            $counter=[
                'prev_is_V'=>false,
                'V_continuous'=>false,
                'prev_is_S'=>false,
                'S_continuous'=>false,
            ];
            $i=0;

            if(fgets($file_handle))
            while (($line = fgets($file_handle)) !== false) {
                $rr_line = explode(' ', trim($line));
                $peak_time = (float)$rr_line[0]/$sample_rate;
                $peak = $rr_line[1];
                $hr = 60/($rr_line[2]/($sample_rate*8));
                if($i==0){
                    $interval_result['start_time'] = $peak_time;

                }
                if((int)($peak_time/$interval) != $time_interval){
                    $interval_result['mean_hr'] = $interval_result['mean_hr'] / $interval_result['total_beats'];
                    $last_end_time = $interval_result['end_time'];
                    array_push($result['interval_result'],$interval_result);
                    $interval_result = $interval_init_result;
                    $time_interval = (int)($peak_time/$interval);
                    $interval_result['start_time'] = $peak_time;
                }
                $interval_result['mean_hr'] += $hr;
                $interval_result['end_time'] = $peak_time;
                $interval_result['total_time'] = $peak_time - $last_end_time;
                $interval_result['total_beats'] += 1;
                if( $hr < $interval_result['min_hr']){
                    $interval_result['min_hr'] = $hr;
                    $interval_result['min_hr_time'] = $peak_time;
                }

                if( $hr > $interval_result['max_hr']) {
                    $interval_result['max_hr'] = $hr;
                    $interval_result['max_hr_time'] = $peak_time;
                }

                switch ($peak) {
                    case 'V':case 'v':
                        $interval_result['V_total']+=1;
                        if($counter['prev_is_V']) {
                            if(!$counter['V_continuous']){
                                $interval_result['V_single']-=1;
                            }
                            $interval_result['V_pairs']+=1;
                            $counter['V_continuous'] = true;
                        } else {
                            $interval_result['V_single']+=1;
                            $counter['prev_is_V'] = true;
                            $counter['prev_is_S'] = false;
                        }
                        $counter['S_continuous'] = false;
                        break;
                    case 'S':case 's':
                        $interval_result['S_total']+=1;
                        if($counter['prev_is_S']) {
                            if(!$counter['S_continuous']){
                                $interval_result['S_single']-=1;
                            }
                            $interval_result['S_pairs']+=1;
                            $counter['S_continuous'] = true;
                        } else {
                            $interval_result['S_single']+=1;
                            $counter['prev_is_S'] = true;
                            $counter['prev_is_V'] = false;
                        }
                        $counter['V_continuous'] = false;
                        break;
                    default:
                        $counter['prev_is_V'] = false;
                        $counter['prev_is_S'] = false;
                        $counter['V_continuous'] = false;
                        $counter['S_continuous'] = false;
                        break;
                }
                $i++;
            }
            $interval_result['mean_hr'] = $interval_result['total_beats']==0 ? 0 : $interval_result['mean_hr'] / $interval_result['total_beats'];
            array_push($result['interval_result'],$interval_result);
            fclose($file_handle);
            return $result['interval_result'];
        }else{
            return false;
        }
    }
    public function check_peakN($userID, $ecgID, $ignore = False)
    {
        $ecg = $this->get_ecg($userID, $ecgID, $ignore);
        if($ecg){
            
            $rr_suffix = '_rr_edited.txt';
            $sample_rate = 256/2;
            $rr_path = public_path('uploads/' . $ecg->user_id . '/rr/' . $ecg->filename . $rr_suffix);
            $result=[
                'peak_AN'=>0,
            ];
            if (!is_file($rr_path)) {
                return false; 
            }
            
            if(!$file_handle = fopen($rr_path, "r")){
                return false; 
            }

            while (($line = fgets($file_handle)) !== false) {
                $rr_line = explode(' ', trim($line));
                if($rr_line[1]=='V' || $rr_line[1]=='S'){
                    $result['peak_AN']=1;
                }
            }
            fclose($file_handle);
            return json_encode($result);
        }
    }
    public function get_ecg_lead_data($user_id, $ecg_id, $lead='l1')
    {
        $ecg = $this->get_ecg($user_id, $ecg_id);
        if(!$ecg){
            return false;
        }
        $filename = $ecg->filename;
        $filename_ext = $ecg->filename_ext;
        $recorded_at = $ecg->recorded_at;
        $sample_rate = (int)($ecg->sample_rate/128);
        $sample_rate = $sample_rate == 0 ? 1 : $sample_rate;
        if($filename){
            $ecg_suffix = ".bin";
            $ecg_path = public_path('uploads/' . $ecg->user_id . '/ecg/' . $filename . $ecg_suffix);
            
            $mode = "rb";
            
            if (!$file_handle = fopen($ecg_path, $mode)) {
                return '[]';
            }
            $filesize = filesize($ecg_path);

            for($position = 0 ; $position < $filesize; $position+=20){
                if(($position/20) % $sample_rate!=0){
                    continue;
                }
                if(fseek($file_handle, $position) == -1){
                    break;
                }

                $contents = fread($file_handle, 20);
                $line_array = unpack("i*", $contents);
                if( $position == 0){
                    $ecgData = ["L1" => "", "L2" => "", "L3" => "", "IR"=>"", "R"=>""];
                }
                $ecgData["L1"] .= ($position!=0 ? "," : "").$line_array[1];
                $ecgData["L2"] .= ($position!=0 ? "," : "").$line_array[2];
                $ecgData["L3"] .= ($position!=0 ? "," : "").$line_array[3];
                $ecgData["IR"] .= ($position!=0 ? "," : "").$line_array[4];
                $ecgData["R"]  .= ($position!=0 ? "," : "").$line_array[5];
            }

            fclose($file_handle);
            if($lead=='l1'){
                return '{"data": [' . $ecgData["L1"] . '],' .
                    '"sample_rate": ' . $ecg->sample_rate . ','.
                    '"recorded_at": "' . $recorded_at . '"}';
            }else if($lead=='l2'){
                return '{"data": [' . $ecgData["L2"] . '],' .
                    '"sample_rate": ' . $ecg->sample_rate . ','.
                    '"recorded_at": "' . $recorded_at . '"}';
            }else if($lead=='l3'){
                return '{"data": [' . $ecgData["L3"] . '],' .
                    '"sample_rate": ' . $ecg->sample_rate . ','.
                    '"recorded_at": "' . $recorded_at . '"}';
            }
            return false;
        }else{
            return false;
        }
    }
     public function setChecked($userID, $ecgID, $ignore, $assignee_type=false){
        $ecg = $this->get_ecg($userID, $ecgID, $ignore, $assignee_type);

        if($ecg){
            $ecg->update(['checked'=>1]);
        }

        return $ecg;
    }
    public function setUnchecked($userID, $ecgID, $ignore, $assignee_type=false){
        $ecg = $this->get_ecg($userID, $ecgID, $ignore, $assignee_type);
        if($ecg){
            $ecg->update(['checked'=>0]);
        }
        return $ecg;
    }

    public function setChecker($userID, $ecgID, $ignore, $email, $assignee_type=false){
        $ecg = $this->get_ecg($userID, $ecgID, $ignore, $assignee_type);
        if($ecg){
            $ecg->update(['checked_author'=>$email, 'checked_time'=>date('Y-m-d H:i:s')]);
        }
        return $ecg;
    }

    public function setConfirmed($userID, $ecgID, $ignore, $report_types, $comment, $assignee_type=false){
        $ecg = $this->get_ecg($userID, $ecgID, $ignore, $assignee_type);
        if($ecg){
            $user = $ecg->user;
            $department_report_types = $user->department->report_types;
            $select_type = $user->department->select_type;
            $d_report_types = [];
            foreach ($department_report_types as $key => $department_report_type) {
                $d_report_types[strtolower($department_report_type->value)] = $department_report_type->value;
            }
            $report_type_tag_str = '';
            $ecg_report_type = [];

            foreach ($report_types as $key => $report_type) {
                if(strtolower($report_type)=='n'){
                    $num_report_type = '0';
                }else if(strtolower($report_type)=='afib'){
                    $num_report_type = '1';
                }else if(strtolower($report_type)=='ps'){
                    $num_report_type = '3';
                }else{
                    $num_report_type = '2';
                }

                if(isset($d_report_types[strtolower($report_type)])){
                    $ecg_report_type[strtolower($d_report_types[strtolower($report_type)])] = $d_report_types[strtolower($report_type)];
                    $report_type_tag_str .= '#'.$report_type;
                }else if(($num_report_type==0 || $num_report_type==1) && isset($d_report_types[$num_report_type])){
                    $ecg_report_type[$d_report_types[$num_report_type]] = $d_report_types[$num_report_type];
                    $report_type_tag_str .= '#'.$report_type;
                }else if($report_type=='Afib'){
                    $hit = false;
                    foreach ($department_report_types as $key => $department_report_type) {
                        $afib_matches = null;
                        preg_match('/^afib([0-9]+)/', strtolower($department_report_type->value), $afib_matches);
                        if($afib_matches){
                            $hit = true;
                            $ecg_report_type[strtolower($department_report_type->value)] = $department_report_type->value;
                        }
                    }
                    if($hit){
                        $report_type_tag_str .= '#'.$report_type;
                    }
                }else if($report_type=='AV'){
                    $hit = false;
                    foreach ($department_report_types as $key => $department_report_type) {
                        $av_matches = null;
                        preg_match('/^av([0-9]+)/', strtolower($department_report_type->value), $av_matches);
                        if($av_matches){
                            $hit = true;
                            $ecg_report_type[strtolower($department_report_type->value)] = $department_report_type->value;
                        }
                    }
                    if($hit){
                        $report_type_tag_str .= '#'.$report_type;
                    }
                }else if($report_type=='Bra'){
                    $hit = false;
                    foreach ($department_report_types as $key => $department_report_type) {
                        $bra_matches = null;
                        preg_match('/^bra([0-9]+)/', strtolower($department_report_type->value), $bra_matches);
                        if($bra_matches){
                            $hit = true;
                            $ecg_report_type[strtolower($department_report_type->value)] = $department_report_type->value;
                        }
                    }

                    if($hit){
                        $report_type_tag_str .= '#'.$report_type;
                    }
                }else if($report_type=='Tac'){
                    $hit = false;
                    foreach ($department_report_types as $key => $department_report_type) {
                        $tac_matches = null;
                        preg_match('/^tac([0-9]+)/', strtolower($department_report_type->value), $tac_matches);
                        if($tac_matches){
                            $hit = true;
                            $ecg_report_type[strtolower($department_report_type->value)] = $department_report_type->value;
                        }
                    }
                    if($hit){
                        $report_type_tag_str .= '#'.$report_type;
                    }
                }else if($num_report_type==2 && isset($d_report_types[$num_report_type])){
                    $ecg_report_type[$d_report_types[$num_report_type]] = $d_report_types[$num_report_type];
                    $report_type_tag_str .= '#'.$report_type;
                }else{
                    foreach ($department_report_types as $key => $department_report_type) {
                        if(strtolower($department_report_type->value) == 'other' || 
                            strtolower($department_report_type->value) == 'others'){
                            $ecg_report_type[strtolower($department_report_type->value)] = $department_report_type->value;
                        }
                    }
                }
            }
            $lower_report_types = array_map(function($d){return strtolower($d);}, $report_types);
            if($select_type==0){
                if(count($lower_report_types)==1 && $lower_report_types[0]=='n'){
                    $ecg_report_type = [0];
                }else if(in_array('ps',$lower_report_types)){
                    $ecg_report_type = [3];
                }else if(in_array('afib',$lower_report_types)){
                    $ecg_report_type = [1];
                }else{
                    $ecg_report_type = [2];
                }
            }
            $report_type_tag_str .= "\n";
            $ecg_comment = $report_type_tag_str.$comment;
            $update_data = ['confirm'=>1, 'comment'=>$ecg_comment, 'report_type'=>join(",",array_values($ecg_report_type))];
            if($ecg->confirm == 0){
                $update_data['report_type_raw'] = $ecg->report_type;
            }

            $ecg->update($update_data);
        }

        return $ecg;
    }


    public function setConfirmer($userID, $ecgID, $ignore, $email, $assignee_type=false){
        $ecg = $this->get_ecg($userID, $ecgID, $ignore, $assignee_type);
        if($ecg){
            $ecg->update(['confirm_author'=>$email, 'confirm_time'=>date('Y-m-d H:i:s')]);
        }
        return $ecg;
    }

    public function hasVSPeak($ecg)
    {
        $userID = $ecg->user_id;
        $filename = $ecg->filename;
        $filename_ext = $ecg->filename_ext;
        $has_t_peak = 0;
        if($filename){
            $rr_suffix = '_rr_edited.txt';

            $rr_path = public_path('uploads/' . $userID . '/rr/' . $filename . $rr_suffix);

            if (!is_file($rr_path)) {
                return 0;
            }

            if(!$file_handle = fopen($rr_path, "r")){
                return 0; 
            }

            $line = fgets($file_handle);
            $rr_line = explode(' ', trim($line));
            $rr_data = '';
            if (count($rr_line)>=3){
                if($rr_line[1] == 'V'){
                    $has_t_peak = $has_t_peak | 1;
                }else if($rr_line[1] == 'S'){
                    $has_t_peak = $has_t_peak | 2;
                }
            }
            while (($line = fgets($file_handle)) !== false) {
                $rr_line = explode(' ', trim($line));
                if($rr_line[1] == 'V'){
                    $has_t_peak = $has_t_peak | 1;
                }else if($rr_line[1] == 'S'){
                    $has_t_peak = $has_t_peak | 2;
                }
                if($has_t_peak == 3){
                    break;
                }
            }
            fclose($file_handle);
        }
        return $has_t_peak;
    }
}