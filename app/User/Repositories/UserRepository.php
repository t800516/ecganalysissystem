<?php
namespace App\User\Repositories;

use App\Repositories\Repository;
use App\User;

class UserRepository extends Repository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->model = $user;
        $this->condition = $user;
    }
    public function useAppends($use){
        $this->user->useAppends($use);
        $this->model->useAppends($use);
        $this->condition->useAppends($use);
    }
    
   	public function getUsers(){
   		return $this->user->with(['role','department'])->orderBy('checked', 'ASC')->orderBy('id', 'ASC')->get();
   	}
    public function getUser($id){
        return $this->user->with('role')->where('id',$id)->first();
    }
    public function deleteUser($id){
        return $this->user->where('id',$id)->delete();
    }
    public function createUser($data,$holter){
        $result = $this->user->create($data);
        $result->holters()->attach($holter);
        return $result;
    }    
    public function updateUser($id,$data,$holter){
        $result = $this->user->where('id',$id)->update($data);
        //$this->user->holters()->detach();
        //$this->user->holters()->attach($holter);
        $this->user->find($id)->holters()->sync($holter);
        return $result;
    }
    public function setUserGuide($id,$data){
        return $this->user->where('id',$id)->update(['guide'=>$data]);
    }
   	public function userChecked($ids){
   		$this->user->whereIn('id',$ids)->update(['checked'=>1]);
   	}
}