<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';
    protected $appends =["url"];
    protected $fillable = ['user_id', 'filename', 'path'];
   
    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function getUrlAttribute(){
        return url('uploads/'.$this->path);
    }
}
