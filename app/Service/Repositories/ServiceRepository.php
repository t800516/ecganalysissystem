<?php
namespace App\Service\Repositories;
use App\Repositories\Repository;
use App\Service;

class ServiceRepository extends Repository
{
    public function __construct(Service $model)
    {
        $this->model = $model;
        $this->condition = $model;
    }

    public function getsByUser($user_id, $patient_user_id, $patient_id)
    {
        return $this->model->where(function($query) use ($user_id, $patient_user_id){
        		return $query->where('user_id', $user_id)->where(
        	'patient_user_id', $patient_user_id);
        	})->orWhere(function($query) use ($patient_id){
        		return $query->where('user_id', $patient_id);
        	})->orderBy('created_at','desc')->orderBy('start_date','desc')->get();
    }
    public function userBy($user_id, $patient_user_id, $patient_id)
    {
        $this->condition = $this->condition->where(function($query) use ($user_id, $patient_user_id, $patient_id){
                return $query->where(function($query) use ($user_id, $patient_user_id){
                    return $query->where('user_id', $user_id)->where(
                'patient_user_id', $patient_user_id);
                })->orWhere(function($query) use ($patient_id){
                    return $query->where('user_id', $patient_id);
                });
            });
        return $this;
    }

     public function getByUser($user_id, $patient_user_id, $patient_id)
    {
        return $this->model->where(function($query) use ($user_id, $patient_user_id){
                return $query->where('user_id', $user_id)->where(
            'patient_user_id', $patient_user_id);
            })->orWhere(function($query) use ($patient_id){
                return $query->where('user_id', $patient_id);
            })->orderBy('created_at','desc')->orderBy('start_date','desc')->first();
    }
}