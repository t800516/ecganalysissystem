<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority_email extends Model
{
	protected $fillable = ['email'];
    
    protected $table="priority_emails";
}
