<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['user_id', 
        'serial_number', 'start_date', 'duration', 'site', 'priority', 'patient_name', 'sex', 'patient_IDNumber', 'birthday','patient_user_id', 'created_by'
    ];
    protected $appends = ['gender','notifiable_records', 'unchecked_records'];
    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function patient(){
        return $this->belongsTo('App\User', 'patient_user_id', 'id');
    }
    public function ecgs(){
    	return $this->hasMany('App\ECG');
    }
    public function creator(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }


    public function getGenderAttribute(){
        return $this->sex;
    }
    public function getBirthdayAttribute($value){
		return $value == null ? '' : $value;
    }

    public function getNotifiableRecordsAttribute(){
        return $this->ecgs()->where('checked',1)->where('notified',0)->count();
    }

    public function getUncheckedRecordsAttribute(){
        return $this->ecgs()->where('checked',0)->count();
    }
}
