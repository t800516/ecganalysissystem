<?php
namespace App\Apk\Repositories;
use App\Repositories\Repository;
use App\Apk;

class ApkRepository extends Repository
{
    public function __construct(Apk $apk)
    {
        $this->model = $apk;
    }
}