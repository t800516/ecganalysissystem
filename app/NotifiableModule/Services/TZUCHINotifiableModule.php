<?php
namespace App\NotifiableModule\Services;
use Storage;

class TZUCHINotifiableModule extends NotifiableModule
{
    protected $payload_type = 'json';
 
    protected function report_type_mapping($report_type){
        switch (strtolower($report_type)) {
            case '1':case 1:case 'n':return 1;
            case '2':case 2:case 'vpc':return 2;
            case '3':case 3:case 'apc':return 3;
            case '4':case 4:case 'sa':return 4;
            case '5':case 5:case 'afib':return 5;
            case '6':case 6:case 'af':return 6;
            case '7':case 7:case 'bra':return 7;
            case '8':case 8:case 'pause':return 8;
            case '9':case 9:case 'ps':return 9;
            case '10':case 10:case 'other':return 10;
            default:return 10;
        }
    }
    public function __construct()
    {
        $this->moduleName = 'TZUCHI';
        $this->apiHost = 'https://dcharity.tzuchi.org.tw/hcp/Wbhk/LineMsg/Post';
        $this->requestHeaders = [
            'X-TzuChi-Signature' => 'TzuChi',
        ];
    }
    public function getSendData($ecgs)
    {
        return $this->dataHandler($ecgs);
    }

    public function send($row)
    {
        $result = [];
        $response = $this->sendRequest($row);
        if($this->responseHandler($response)){
            foreach ($row['RptCtx'] as $ecg) {
                $result[] = $ecg['RptId'];
            }
        }
        return $result;
    }

    protected function dataHandler($ecgs)
    {
        $data = [];
        foreach ($ecgs as $key => $ecg) {
            if(!isset($data[$ecg->patient_name])){
                $data[$ecg->patient_name] = [
                    "abid"=>"RptUpd",
                    "RecKey"=>$ecg->patient_name,
                    "RptCtx"=>[]
                ];
            }
            // $report_path = public_path('uploads/'.$ecg->user->id.'/report/'.$ecg->filename.'.pdf');
            // $report_content = file_exists($report_path) ? base64_encode(file_get_contents($report_path)) : null;
            $report_types = explode(',', $ecg->report_type);

            $data[$ecg->patient_name]["RptCtx"][] = [
                "RptOrd"=>$key,
                "RptId"=>(string)$ecg->id,
                "RptNm"=>$ecg->slug,
                "MsrRslt"=>$this->report_type_mapping($report_types[0] ?? ''),
                // "RptBs64"=>$report_content
            ];
        }
        return array_values($data);
    }

    protected function responseHandler($input_line){
        $result = json_decode($input_line , true);
        Storage::append($this->moduleName.'.resp.log','['.date('Y-m-d H:i:s').']:'.$input_line);
        if($result){
            if(isset($result['Result']) && $result['Result']){
                return true;
            }
        }
        return false;
    }

    protected function dataRequest($data_fields){
        Storage::append($this->moduleName.'.req.log','['.date('Y-m-d H:i:s').']:'.json_encode($data_fields));
        return $data_fields;
    }
}