<?php
namespace App\NotifiableModule\Services;
use Storage;
class NotifiableModule
{
    protected $apiHost;
    protected $requestHeaders;
    protected $payload_type = 'body';

    public function __construct()
    {
        
    }
    protected function dataHandler($ecg)
    {
        $data_fields = $ecg;
        return $data_fields;
    }

    public function send($ecg)
    {
        $data_fields = $this->dataHandler($ecg);
        $response = $this->sendRequest($data_fields);
        return $this->responseHandler($response);
    }

    protected function dataRequest($data){
        return $data;
    }
    protected function sendRequest($data){
        $post_data = $this->dataRequest($data);
        $http = new \GuzzleHttp\Client;
        $payload = [
            'headers'=>$this->requestHeaders
        ];
        $payload[$this->payload_type] = $post_data;
        $response = $http->request('POST', $this->apiHost, $payload);
        return (string)$response->getBody();
    }
    protected function responseHandler($response){
        return $response;
    }
}