<?php
namespace App\NotifiableModule\Services;
use Storage;

class TMUHNotifiableModule extends NotifiableModule
{
  
    public function __construct()
    {
        $this->moduleName = 'TMUH';
        $this->apiHost = 'http://210.61.66.4:8082/';
        $this->requestHeaders = [
                    'Content-Type' => 'text/xml; charset=UTF8',
                    'SOAPAction' => 'http://tempuri.org/ECGFunc2'
                ];
    }
    
    protected function dataHandler($ecg)
    {
        $report_path = public_path('uploads/'.$ecg->user->id.'/report/'.$ecg->filename.'.pdf');
        $raw_path = public_path('uploads/'.$ecg->user->id.'/ecg/'.$ecg->filename.'_report.txt');
        $report_content = file_exists($report_path) ? base64_encode(file_get_contents($report_path)) : '-';
        $raw_data = file_exists($raw_path) ? str_replace("\n", ',', file_get_contents($raw_path)) : '-';
        $data_fields = [
                    'ecg_id'=>$ecg->id,
                    'recorded_at'=>date('Y-m-d H:i:s', strtotime($ecg->recorded_at)),
                    'created_at'=>date('Y-m-d H:i:s', strtotime($ecg->created_at)),
                    'updated_at'=>date('Y-m-d H:i:s', strtotime($ecg->updated_at)),
                    'report_type'=>$ecg->report_type,
                    'report_kind'=>'2',
                    'report_content01'=> $report_content,
                    'report_content02'=> '-',//$report_content,
                    'raw_data'=> $raw_data,
                    'report_result'=>'-',
                    'holter_idnumber'=>$ecg->holter_IDNumber,
                    'service_sn'=> $ecg->patient_IDNumber,
                    'fee_no'=> ($ecg->service_SN && $ecg->service_SN != '') ? $ecg->service_SN : ($ecg->service ? $ecg->service->serial_number : '-'),
                    'start_date'=>($ecg->start_date && $ecg->start_date != '0000-00-00') ? $ecg->start_date : ($ecg->service ? $ecg->service->start_date : date('Y-m-d', strtotime($ecg->created_at))),
                    'duration'=>($ecg->duration && $ecg->start_date != '') ? $ecg->duration : ($ecg->service ? ($ecg->service->duration ? $ecg->service->duration :14) : 14)
        ];
        Storage::disk("public")->put("TMUH_debug.txt", json_encode($data_fields));
        return $data_fields;
    }

    protected function responseHandler($input_line){
        preg_match('/<ECGFunc2Result>(.*)<\/ECGFunc2Result>/', $input_line, $output_array);
        if(count($output_array)>1){
            $responseData = json_decode($output_array[1], true);
            if($responseData[0]['EcgADDResultCode']=='9999'){
                return true;
            }else{
                Storage::append($this->moduleName.'.log','['.date('Y-m-d H:i:s').']:'.$output_array[1]);
                return false;
            }
        }
        return false;
    }

    protected function dataRequest($data_fields){
        $data_xml = '';
        foreach($data_fields as $key => $value){
            $data_xml .='<'.$key.'>'.$value.'</'.$key.'>';      
        }
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                              <soap:Body>
                                <ECGFunc2 xmlns="http://tempuri.org/">
                                  '.$data_xml.' 
                                </ECGFunc2>
                              </soap:Body>
                            </soap:Envelope>';
        return $xml_post_string;
    }
}