<?php
namespace App\NotifiableModule\Repositories;
use App\Repositories\Repository;
use App\NotifiableModule;

class NotifiableModuleRepository extends Repository
{
    public function __construct(NotifiableModule $model)
    {
        $this->model = $model;
    }
}