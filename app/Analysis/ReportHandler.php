<?php

namespace App\Analysis;
use Storage;
class ReportHandler
{
    protected $ecg;
    protected $report_file_path;
    protected $ecg_file_path;
    protected $info_file_path;
    protected $rr_file_path;
    protected $mcr_path;
    protected $exec_path;
    protected $department;
    protected $report_result=['QT'=>'','QTc'=>'', 'HR'=>'', 'min_HR'=>'', 'max_HR'=>'', 'comment'=>''];
    public function __construct($ecg)
    {
        $this->ecg = $ecg;
        $user = $ecg->user;
        $userID = $user->id;
        $this->report_file_path = public_path("uploads/{$userID}/report/".$ecg->filename);
        $this->qt_report_file_path = public_path("uploads/{$userID}/qt_report/".$ecg->filename);
        $this->ecg_file_path = public_path("uploads/{$userID}/ekg/".$ecg->filename.'.'.$ecg->filename_ext);
        $this->info_file_path = public_path("uploads/{$userID}/ecg/".$ecg->filename."_info.txt");
        $this->rr_file_path = public_path("uploads/{$userID}/rr/".$ecg->filename."_rr_edited.txt");
        
        if (!is_dir(public_path("uploads/{$userID}/report"))) {
            $oldmask = umask(0);
            mkdir(public_path("uploads/{$userID}/report"), 0777, true);
            umask($oldmask);
        }

        if (!is_dir(public_path("uploads/{$userID}/qt_report"))) {
            $oldmask = umask(0);
            mkdir(public_path("uploads/{$userID}/qt_report"), 0777, true);
            umask($oldmask);
        }

        $this->mcr_path = env('Z2B_REPORT_MCR_PATH','/usr/local/MATLAB/MATLAB_Runtime/v91');
        $this->exec_path = app_path('Http/Controllers/Analysis/z2b_report.sh');
        
        $this->department = $user->department ? $user->department->name:'';
        
        $this->output_ext = 'pdf';
    }
    public function get_ecg(){
        return $this->ecg;
    }
    public function run_report($type=false, $department = false, $qt_report = false){
        $retArr =[];
        if(!$type){
            $type = $this->ecg->report_type;
        }

        if(!$department){
            $department = $this->department;
        }

        if($department=='QTC'){
            $report_file_path = $this->qt_report_file_path;
        }else{
            $report_file_path = $this->report_file_path;
        }

        $command_str = "\"".$this->exec_path."\" \"".$this->mcr_path."\" \"".$this->ecg_file_path."\" \"".$this->info_file_path."\" \"".$this->rr_file_path."\" \"".$this->ecg->patient_IDNumber."\" \"".$type."\" \"".$report_file_path."\" \"".$department."\" \"".$this->ecg->patient_name."\" \"".$this->ecg->sex."\" \"".$this->ecg->birthday."\" \"".$this->ecg->description."\" \"".$this->ecg->comment."\" \"".$this->ecg->hf."\"";
        $set_charset = 'export LANG=en_US.UTF-8;';
        $start_time = microtime(true);
        exec($set_charset.$command_str, $retArr);
        $end_time = microtime(true);

        if(file_exists($this->report_file_path.'.'.$this->output_ext)){
            exec("chmod -R 777 ".$this->report_file_path.'.'.$this->output_ext);
        }
        if(count($retArr)){
            $result_array = explode("\t", $retArr[count($retArr)-1]);
            if(count($result_array)>2){
                // $this->report_result['QT'] = $result_array[0];
                // $this->report_result['QTc'] = $result_array[1];
                // $this->report_result['HR'] = $result_array[2];
                
                $this->report_result['HR'] = $result_array[0];
                $this->report_result['min_HR'] = $result_array[1];
                $this->report_result['max_HR'] = $result_array[2];
                $this->report_result['QT'] = $result_array[3] =='-999' ? '' :$result_array[3];
                $this->report_result['QTc'] = $result_array[4] =='-999' ? '' :$result_array[4];
                $this->report_result['comment'] = $result_array[5];
            }
        }
        if(in_array($department, ["TWDHA", "TFDA", "TFDAHF"])){
            return file_exists($this->report_file_path.'_TPOC'.'.'.$this->output_ext) && file_exists($this->report_file_path.'_TOTC'.'.'.$this->output_ext); 
        }
        return file_exists($this->report_file_path.'.'.$this->output_ext);
    }

    public function getResult(){
        return $this->report_result;
    }
}
