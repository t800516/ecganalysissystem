<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ECG extends Model
{
    protected $table = 'ecgs';
    protected $appends =['upload_date', 'confirm_date'];
    protected $fillable = ['user_id','patient_id','filename','filename_ext','filesize','sample_rate','description','patient_name','sex','comment','recorded_at','ecganalysis_time','rranalysis_time','analysis_status','patient_IDNumber','holter_IDNumber','report_status', 'report_type', 'birthday','priority','sleep_analysis_status', 'service_id', 'service_SN', 'start_date', 'duration', 'site', 'checked', 'checked_author', 'checked_time', 'QT', 'QTc','HR','confirm','confirm_author', 'confirm_time', 'assignee', 'slug', 'report_type_raw','confirm_check','sqi', 'report_type_alg', 'hf', 'min_HR', 'max_HR'];
    protected $casts=[
        'sqi'=>'integer'
    ];
    public function getUploadDateAttribute(){
    	return date('Y-m-d', strtotime($this->created_at));
    }
    public function getConfirmDateAttribute(){
        return date('Y-m-d', strtotime($this->confirm_time));
    }
    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function service(){
    	return $this->belongsTo('App\Service');
    }
}
