<?php
namespace App\Admin\Repositories;

use App\Repositories\Repository;
use App\Admin;
use Hash;
class AdminRepository extends Repository
{

    public function __construct(Admin $model)
    {
        $this->model = $model;
    }
   	public function check($credentials)
    {
      $admin_data = $this->model->where('account',$credentials['account'])->first();
      if($admin_data && Hash::check($credentials['password'],$admin_data->password))
      {
        if(Hash::needsRehash($admin_data->password)){
          $hashed = Hash::make($credentials['password']);
          update_password($admin_data->id,$hashed);
        }
        return true;
      }
      else
      {
        return false;
      }
   	}
    protected function update_password($id,$hashpwd){
      $this->model->where('id',$id)->update(['password'=>$hashpwd]);
    }
}