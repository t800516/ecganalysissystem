<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $fillable = [
        'name','email','phone','select_type', 'module_id'
    ];
    public function users(){
    	return $this->hasMany('App\User');
    }

    public function module(){
        return $this->belongsTo('App\NotifiableModule');
    }
    public function report_types(){
    	return $this->hasMany('App\Report_type');
    }
}
