<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','storage','remark','role_id','checked','department_id','auto_transfer', 'patient_name', 'patient_IDNumber', 'sex', 'birthday','auto_report', 'expiration_date', 'agreed_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'agreement'
    ];
    public function useAppends($useAppends){
        if($useAppends){
            $this->appends = [
                "ecg_non_trans_count",
                "ecg_non_trans_count_days",
                "ecg_non_report_count",
                "ecg_non_checked_count",
                "users_count",
                "agreement"
            ];
        }else{
            $this->appends = ['agreement'];
        }
    }
 
    public function ecgs(){
        return $this->hasMany('App\ECG');
    }
 
    public function assginedEcgs(){
        return $this->belongsToMany('App\ECG');
    }

    public function role(){
        //return $this->belongsToMany('App\Holter', 'user_holter', 'user_id', 'holter_id');
        return $this->belongsTo('App\Role');
    }

    public function holters(){
        //return $this->belongsToMany('App\Holter', 'user_holter', 'user_id', 'holter_id');
        return $this->belongsToMany('App\Holter');
    }

    public function patients(){
        return $this->hasMany('App\Patient');
    }
    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function hasRole($role){
        return $role->contains('id', $this->role->id);
    }

    public function hasPermission($permisson){
        return $this->hasRole($permisson->roles);
    }
    
    public function users(){
        return $this->belongsToMany('App\User','user_user','user_id','d_user_id');
    }

    public function masterUsers(){
        return $this->belongsToMany('App\User', 'user_user', 'd_user_id', 'user_id');
    }
    public function surveys(){
        return $this->hasMany('App\Survey');
    }
    public function services(){
        return $this->hasMany('App\Service');
    }

    public function userServices(){
        return $this->hasMany('App\Service', 'patient_user_id', 'id');
    }
 
    public function activities(){
        return $this->hasMany('App\Activity');
    }

    public function variables(){
        return $this->hasMany('App\Variable');
    }

    public function getEcgNonTransCountAttribute(){
        return $this->ecgs()->where('analysis_status',0)->count();
    }
    public function getEcgNonTransCountDaysAttribute(){
        return $this->ecgs()->where('analysis_status',0)->where('created_at','>=',date('Y-m-d H:i:s',strtotime(date('Y-m-d').' -3 day')))->count();
    }
    public function getEcgNonReportCountAttribute(){
        return $this->ecgs()->where('report_status',0)->count();
    }
    public function getEcgNonCheckedCountAttribute(){
        $nonCheckedCount = $this->ecgs()->where('checked', 0)->count();
        return $nonCheckedCount;
    }
    public function getUsersCountAttribute(){
        return $this->users()->count();
    }
    public function getAgreementAttribute(){
        return $this->agreed_at!=0 ? 1 : 0;
    }
}
