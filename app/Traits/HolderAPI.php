<?php 
namespace App\Traits;
use Exception;
use Storage;
trait HolderAPI
{
    protected function getHolderProfile($serial_number){
        $token = $this->getAPIToken();
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->post('https://holterholder.com/HolterAPI/api/CaseQuery/query', [
                'headers'=>[
                    'Authorization'=>$token,
                    'Content-Type' => 'application/json',
                    'Accept' =>'application/json'
                ],
                'json' => [
                    "SerialNumber"=>$serial_number, 
                    "ECGId"=>''
                ],
            ]); 
        } catch (Exception $e) {
            Storage::append('holterholder.resp.log','['.date('Y-m-d H:i:s').']:CaseQuery:'.$e->getMessage());
        }
        Storage::append('holterholder.resp.log','['.date('Y-m-d H:i:s').']:CaseQuery:'.(string) $response->getBody());
        $result = json_decode((string) $response->getBody(), true);
        if($result){
            if($result['Status']=='Y'){
                return [
                    'patient_IDNumber'=>isset($result["PatientIDNumber"]) ? $result["PatientIDNumber"] : '',
                    'birthday'=>isset($result["Birthday"]) ? str_replace('-', '/', $result["Birthday"]) : '',//(isset($result["Age"]) ? date('Y/m/d',strtotime('-'.$result["Age"])):''),
                    'age'=>isset($result["Age"]) ? $result["Age"] : '',
                    'sex'=>isset($result["Sex"]) ? $result["Sex"] : '',
                    'patient_name'=>isset($result["PatientName"]) ? $result["PatientName"] : '',
                  ];
            }
        }
        return null;
    }
    protected function getAPIToken(){
        $http = new \GuzzleHttp\Client;
        try {
            $response = $http->post('https://holterholder.com/HolterAPI/api/auth/login', [
                'headers'=>[
                    'Content-Type' => 'application/json',
                    'Accept' =>'application/json'
                ],
                'json' => [
                    'Username' => 'holterA100',
                    'Password' => 'hhNyQZ4G2aVuoho+kv7vJNxGlF8hI3JtU+e31BqVYoU=',
                ],
            ]);
        } catch (Exception $e) {
            Storage::append('holterholder.resp.log','['.date('Y-m-d H:i:s').']:auth/login:'.$e->getMessage());
            return null;
        }
        Storage::append('holterholder.resp.log','['.date('Y-m-d H:i:s').']:auth/login:'.(string) $response->getBody());
        $result = json_decode((string) $response->getBody(), true);
        return $result && isset($result['Token']) ? $result['Token'] : null;
    }
}
