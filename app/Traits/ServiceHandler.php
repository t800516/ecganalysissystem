<?php 
namespace App\Traits;
trait ServiceHandler
{
    private function cal_summary($rr_path, $recorded_at){
        $hours = [];
        $v_peak_num = 0;
        $s_peak_num = 0;
        $peak_num = 0;
        $pause = 0;
        $pause_sec = 0;
        $ecg_rr = [];

        for ($i=0; $i <24 ; $i++) { 
            $hours[$i]=['max_heart'=>0,'mean_heart'=>0, 'min_heart'=>0,'abnormal_num'=>0, 'recored_num'=>0 ,'peak_num'=>0, 'rr'=>[]];
        }

        if (!is_file($rr_path)) {
            return ['hours'=>$hours, 'v_peak_num'=>$v_peak_num, 's_peak_num'=>$s_peak_num, "ecg_rr"=>$ecg_rr, "pause"=>$pause, "pause_sec"=>$pause_sec];
        }

        if(!$file_handle = fopen($rr_path, "r")){
            return ['hours'=>$hours, 'v_peak_num'=>$v_peak_num, 's_peak_num'=>$s_peak_num, "ecg_rr"=>$ecg_rr, "pause"=>$pause, "pause_sec"=>$pause_sec];
        }
        $line = fgets($file_handle); // skip first data
        while (($line = fgets($file_handle)) !== false) {

            $rr_line = explode(' ', trim($line));
            $s = (float)$rr_line[0] * 0.0078125;
            $hour = date('G', strtotime($recorded_at) + $s);
            $hour = $hour ? $hour-1 : 23;

            if(in_array(strtolower($rr_line[1]),['n'], true)){
                $hours[$hour]['abnormal_num'] = 1; 

                if($rr_line[1] == 'V'){
                    $v_peak_num++;
                }

                if($rr_line[1] == 'S'){
                    $s_peak_num++;
                }
            }

            $rr = (float)$rr_line[2];

            if($rr > 3500){
                continue;
            }

            if($rr > 2000){
                $pause+=1;
                $pause_sec = max($pause_sec, $rr);
            }

            array_push($ecg_rr, $rr);
            array_push($hours[$hour]["rr"], $rr);
            $hours[$hour]['peak_num'] = 1;
            $hours[$hour]['recored_num'] = 1;
            $peak_num++;
        }

        fclose($file_handle);

        return ['hours'=>$hours, 'v_peak_num'=>$v_peak_num, 's_peak_num'=>$s_peak_num,  "pause"=>$pause, "pause_sec"=>$pause_sec, "ecg_rr"=>$ecg_rr];
    }

    public function summaryData($service)
    {
        $department = $service->patient ? $service->patient->department : $service->user->department;

        $data = $this->summaryECGs($service->ecgs, $department);
        $data['service'] = $service;
        return $data;
    }

    function summaryECGs($origin_ecgs, $department)
    {
        $ecgs = $origin_ecgs->filter(function ($value, $key) {
            return substr($value->recorded_at,4) != '2015';
        })->sortBy('recorded_at');

        $begin_ecg = $ecgs->first();
        $last_ecg = $ecgs->last();
        $record_num = 0;
        $department_report_types = $department->report_types;
        
        $duration_day_map = [];
        $duration_days = 0;
        $normal_num = 0;
        $abnormal_num = 0;
        $abnormal_detail = [];
        $unfinished_num = 0;
        $report_type_array = [];
        $holters = [];
        $hours = [];
        $v_peak_num = 0;
        $s_peak_num = 0;

        $total_pause = 0;
        $pause = 0;
        $pause_sec = 0;
        $tac = 0;
        $tac100 = 0;
        $bra = 61;
        $ecgs_min_rr = null;
        $ecgs_max_rr = null;

        for ($i=0; $i <24 ; $i++) { 
            $hours[$i]=['max_heart'=>0,'mean_heart'=>0, 'min_heart'=>0,'abnormal_num'=>0, 'recored_num'=>0 ,'peak_num'=>0, 'min_heart_rate' => 0, 'max_heart_rate' => 0, 'mean_heart_rate' => 0, 'ecg_max_heart'=>[], 'ecg_min_heart'=>[], 'ecg_rr'=>[]];
        }

        foreach ($department_report_types as $key => $department_report_type) {
            $report_type = strtolower($department_report_type->value);
            if(!in_array($report_type, [0,1,2,3, '0', '1', '2', '3', 'normal sinus rhythm', 'n', 'poor signal', 'ps'], true)){

                if(in_array($report_type, ['atrial fibrillation', 'atrial flutter', 'afib', 'af'], true)){
                    $report_type_array['afib'] = ["name"=>'Atrial Fibrillation/Atrial Flutter', "value"=>0, 'times'=>0];
                }else{
                    $report_type_array[$report_type] = ["name"=>$department_report_type->name, "value"=>0, 'times'=>0];
                }

            }
        }

        foreach ($ecgs as $key => $ecg) {
            if($this->isPoorSignalRecord($ecg->report_type)){
                $holters[$ecg->holter_IDNumber] = 1;
                ++$unfinished_num;
                ++$record_num;
                continue;
            }
            $rr_suffix = '_rr_edited.txt';
            $rr_path = public_path('uploads/' . $ecg->user->id . '/rr/' . $ecg->filename . $rr_suffix);
            $result = $this->cal_summary($rr_path, $ecg->recorded_at);
            $ecg_hour = $result['hours'];

            $duration_day_map[date('Ymd', strtotime($ecg->recorded_at))] = $ecg->recorded_at;

            $v_peak_num += $result['v_peak_num'];
            $s_peak_num += $result['s_peak_num'];

            $total_pause += $result['pause'];
            $pause = $pause < $result['pause'] ? $result['pause'] : $pause;
            $pause_sec = $pause_sec < $result['pause_sec'] ? $result['pause_sec'] : $pause_sec;

            $min_ecg_rr = count($result['ecg_rr']) ? min($result['ecg_rr']) : 0;
            if($ecgs_min_rr==null){
                $ecgs_min_rr = $min_ecg_rr;
            }else{
                $ecgs_min_rr = $min_ecg_rr < $ecgs_min_rr ? $min_ecg_rr :$ecgs_min_rr;
            }

            $max_ecg_rr = count($result['ecg_rr']) ? max($result['ecg_rr']) : 0;
            if($ecgs_max_rr==null){
                $ecgs_max_rr = $max_ecg_rr;
            }else{
                $ecgs_max_rr = $max_ecg_rr > $ecgs_max_rr ? $max_ecg_rr :$ecgs_max_rr;
            }
            
            $tac100 = array_filter($result['ecg_rr'], function($d){return $d ? (60000/$d >=100):false;});
            $bra60 = array_filter($result['ecg_rr'], function($d){return $d ?  (60000/$d >=50 && 60000/$d < 60):false;});
            $bra50 = array_filter($result['ecg_rr'], function($d){return $d ?  (60000/$d >=40 && 60000/$d < 50):false;});
            $bra40 = array_filter($result['ecg_rr'], function($d){return $d ?  (60000/$d < 40):false;});
            
            $tac = $tac < $min_ecg_rr ? $min_ecg_rr : $tac;
            $bra = $bra > $max_ecg_rr ? $max_ecg_rr : $bra;

            for ($i=0; $i <24 ; $i++) {
                // $hours[$i]['abnormal_num'] += $ecg_hour[$i]['abnormal_num'];
                // $hours[$i]['recored_num'] += $ecg_hour[$i]['recored_num'];
                $hours[$i]['peak_num'] += $ecg_hour[$i]['peak_num'];
                $hours[$i]['ecg_rr'] = array_merge($hours[$i]['ecg_rr'], $ecg_hour[$i]['rr']);
            }
            for ($i=0; $i <24 ; $i++) {
                $hours[$i]["max_heart"] = count($hours[$i]['ecg_rr']) ? max($hours[$i]['ecg_rr']) : 0;
                $hours[$i]["min_heart"] = count($hours[$i]['ecg_rr']) ? min($hours[$i]['ecg_rr']) : 0;
                $hours[$i]["mean_heart"] = count($hours[$i]["ecg_rr"]) ? array_sum($hours[$i]["ecg_rr"])/count($hours[$i]["ecg_rr"]): 0;
            }

            $is_between_data= ['min_heart_rate' => false, 'max_heart_rate' => false, 'mean_heart_rate' => false];

            for ($i=0; $i <24 ; $i++) {
                $hours[$i]['min_heart_rate'] = $hours[$i]['max_heart'] ? 60000 / $hours[$i]['max_heart'] : 0 ;
                if($hours[$i]['min_heart_rate'] != 0){
                    if($is_between_data['min_heart_rate']){
                        for ($h = $is_between_data['min_heart_rate']+1; $h < $i-1 ; $h++) {
                            $hours[$h]['min_heart_rate'] = $hours[$is_between_data['min_heart_rate']]['min_heart_rate'];
                        }
                        $is_between_data['min_heart_rate'] = false;
                    }else{
                        $is_between_data['min_heart_rate'] = $i;
                    }
                }

                $hours[$i]['max_heart_rate'] = $hours[$i]['min_heart'] ? 60000 / $hours[$i]['min_heart'] : 0 ;
                if($hours[$i]['max_heart_rate'] != 0){
                    if($is_between_data['max_heart_rate']){
                        for ($h = $is_between_data['max_heart_rate']+1; $h < $i-1 ; $h++) {
                            $hours[$h]['max_heart_rate'] = $hours[$is_between_data['max_heart_rate']]['max_heart_rate'];
                        }
                        $is_between_data['max_heart_rate'] = false;
                    }else{
                        $is_between_data['max_heart_rate'] = $i;
                    }
                }
                $hours[$i]['mean_heart_rate'] = $hours[$i]['mean_heart'] ? 60000 / $hours[$i]['mean_heart'] : 0 ;

                if($hours[$i]['mean_heart_rate'] != 0){
                    if($is_between_data['mean_heart_rate']){
                        for ($h = $is_between_data['mean_heart_rate']+1; $h < $i-1 ; $h++) {
                            $hours[$h]['mean_heart_rate'] = $hours[$is_between_data['mean_heart_rate']]['mean_heart_rate'];
                        }
                        $is_between_data['mean_heart_rate'] = false;
                    }else{
                        $is_between_data['mean_heart_rate'] = $i;
                    }
                }
            }

            $ecg_report_types = array_filter(array_map(function($value) use ($department){
                $department_report_type = $department->report_types->where('value', $value)->first();
                return $department_report_type ? strtolower($department_report_type->value) : null;
            }, explode(',', $ecg->report_type)), function($value){
                return $value!==null;
            });

            $is_abnormal = false;

            if($ecg->report_status != 2){
                $unfinished_num += 1; 
            }else if(in_array(0, $ecg_report_types, true) || in_array('0', $ecg_report_types, true) || in_array('n', $ecg_report_types, true) || in_array('normal sinus rhythm', $ecg_report_types, true)){
                $normal_num += 1;
            }else{
                $is_abnormal = true;
                if(in_array('pause', $ecg_report_types, true) && isset($report_type_array['pause'])){
                    $report_type_array['pause']['value'] += 1;
                    $report_type_array['pause']['times'] += $total_pause;
                    $abnormal_detail['pause'] = ["name"=>$report_type_array['pause']['name'], 'data'=>'最多次數：'.$pause.'，最長間距：'.round($pause_sec,1).' 秒)'];
                }
                if(in_array('tac100', $ecg_report_types, true) && isset($report_type_array['tac100'])){
                    $report_type_array['tac100']['value'] += 1;
                    $report_type_array['tac100']['times'] += count($tac100);
                    $h_tac = $tac ?  60000 / $tac : 0;
                    $abnormal_detail['tac100'] = ["name"=>$report_type_array['tac100']['name'], 'data'=>'最快：'.round($h_tac).' bpm'];
                }
                if(in_array('bra60', $ecg_report_types, true) && isset($report_type_array['bra60'])){
                    $report_type_array['bra60']['value'] += 1;
                    $report_type_array['bra60']['times'] += count($bra60);
                    $h_bra = $bra ?  60000 / $bra : 0;
                    $abnormal_detail['bra60'] = ["name"=>$report_type_array['bra60']['name'], 'data'=>'最慢：'.round($h_bra).' bpm'];
                }
                if(in_array('bra50', $ecg_report_types, true) && isset($report_type_array['bra50'])){
                    $report_type_array['bra50']['value'] += 1;
                    $report_type_array['bra50']['times'] += count($bra50);
                    $h_bra = $bra ?  60000 / $bra : 0;
                    $abnormal_detail['bra50'] = ["name"=>$report_type_array['bra50']['name'], 'data'=>'最慢：'.round($h_bra).' bpm'];
                }
                if(in_array('bra40', $ecg_report_types, true) && isset($report_type_array['bra40'])){
                    $report_type_array['bra40']['value'] += 1;
                    $report_type_array['bra40']['times'] += count($bra40);
                    $h_bra = $bra ?  60000 / $bra : 0;
                    $abnormal_detail['bra40'] = ["name"=>$report_type_array['bra40']['name'], 'data'=>'最慢：'.round($h_bra).' bpm'];
                }
                if((in_array('afib', $ecg_report_types, true) || in_array('af', $ecg_report_types, true)) && isset($report_type_array['afib'])){
                    $report_type_array['afib']['value'] += 1;
                    $report_type_array['afib']['times'] = $report_type_array['afib']['value'];
                    $abnormal_detail['afib'] = ["name"=>$report_type_array['afib']['name'], 'data'=>''];
                }

                if((in_array('vpc', $ecg_report_types, true) || in_array('af', $ecg_report_types, true)) && isset($report_type_array['vpc'])){
                    $report_type_array['vpc']['value'] += 1;
                    $report_type_array['vpc']['times'] = $v_peak_num;
                    $abnormal_detail['vpc'] = ["name"=>$report_type_array['vpc']['name'], 'data'=>''];
                }
                
                if((in_array('apc', $ecg_report_types, true) || in_array('af', $ecg_report_types, true)) && isset($report_type_array['apc'])){
                    $report_type_array['apc']['value'] += 1;
                    $report_type_array['apc']['times'] = $s_peak_num;
                    $abnormal_detail['apc'] = ["name"=>$report_type_array['apc']['name'], 'data'=>''];
                }

                foreach ($ecg_report_types as $key2 => $report_type) {
                    if(!in_array($report_type, [0,1,2,3, '0','1','2','3','n','normal sinus rhythm', 'poor signal', 'ps', 'afib', 'bra40', 'bra50', 'bra60', 'tac100', 'pause', 'vpc', 'apc'], true)){
                        $report_type_array[$report_type]['value'] += 1;
                        $report_type_array[$report_type]['times'] = $report_type_array[$report_type]['value'];
                        $abnormal_detail[$report_type] = ["name"=>$report_type_array[$report_type]['name'], 'data'=>''];
                    }
                }
            }
            $record_hour = date('G', strtotime($ecg->recorded_at));
            $record_hour = $record_hour ? $record_hour-1 : 23;
            if($is_abnormal){
                $hours[$record_hour]['abnormal_num'] += 1;
                $abnormal_num += 1;
            }
            $hours[$record_hour]['recored_num'] += 1;

            $holters[$ecg->holter_IDNumber] = 1;
            ++$record_num;
        }
        $report_type_array = array_filter($report_type_array, function($d){return $d["value"];});
        $holter_IDNumbers = join(", ",array_keys($holters));
        $duration_days = count(array_keys($duration_day_map));

        for ($i=0; $i <24 ; $i++) {
            unset($hours[$i]["ecg_rr"]);
            unset($hours[$i]["ecg_max_heart"]);
            unset($hours[$i]["ecg_min_heart"]);
            unset($hours[$i]["ecg_mean_heart"]);
        }
        $data = [
            'last_recored_at'=>$last_ecg && $last_ecg->recorded_at ? $last_ecg->recorded_at : false,
            'begin_recored_at'=>$begin_ecg && $begin_ecg->recorded_at ? $begin_ecg->recorded_at : false,
            'normal_num'=>$normal_num,
            'abnormal_num'=>$abnormal_num,
            'unfinished_num'=>$unfinished_num,
            'duration_days' =>$duration_days,
            'record_num'=>$record_num,
            'report_type_array'=>$report_type_array,
            'abnormal_detail'=>$abnormal_detail,
            'holter_IDNumbers'=>$holter_IDNumbers,
            'hours' => $hours,
            'v_peak_num'=>$v_peak_num,
            's_peak_num'=>$s_peak_num,
            'max_heart_rate'=> $ecgs_min_rr ? round(60000/$ecgs_min_rr):'',
            'min_heart_rate'=> $ecgs_max_rr ? round(60000/$ecgs_max_rr):'',
        ];

        return $data;
    }

    function isNormalRecord($report_type){
        $ecg_report_types = explode(',', strtolower($report_type));
        $is_normal = false;
        if(in_array('n', $ecg_report_types, true) || in_array('normal sinus rhythm', $ecg_report_types, true)){
            $is_normal = true;
        }
        return $is_normal;
    }

    function isAbnormalRecord($report_type){
        $ecg_report_types = explode(',', $report_type);
        $is_abnormal = false;
        foreach ($ecg_report_types as $key => $report_type) {
            if($report_type == ''){
                continue;
            }
            $report_type = strtolower($report_type);
            if(!in_array($report_type, ['n','normal sinus rhythm', 'poor signal', 'ps'], true)){
                $is_abnormal = true;
            }
        }
        return $is_abnormal;
    }

    function isPoorSignalRecord($report_type){
        $ecg_report_types = explode(',', strtolower($report_type));
        $is_poor_signal = false;
        if( in_array(3, $ecg_report_types, true) || 
            in_array('3', $ecg_report_types, true) || 
            in_array('ps', $ecg_report_types, true) || 
            in_array('poor signal', $ecg_report_types, true)){
            $is_poor_signal = true;
        }
        return $is_poor_signal;
    }
}