<?php 
namespace App\Traits;
use Request;
trait HRV
{
    private function get_hrv_data($user, $filename, $filename_ext,$interval)
    {
        $user_id = $user->id;
        $hrv_suffix = '_hrv.txt';
        $hrv_file_path = "uploads/{$user_id}/hrv/";
        $hrv_file_name = $hrv_file_path . $filename .'_'. $interval . $hrv_suffix;
        //$hrv_file_name = public_path('uploads\\' . $user_id . '\\hrv\\' . $filename .'_'.$interval. $hrv_suffix);
        if(!file_exists($hrv_file_name)){
            return false;
        }
        
        $file_handle = fopen($hrv_file_name, "r");
        
        if ($file_handle) {
            $i=0;
            $j=0;
            $hrv_data="";
            $hrv_fields="";
            $hrv_result="";
            $rr="";
            $ar="";
            $section = 1;
            while (($line = fgets($file_handle)) !== false) {
                if($line == "=====\n"){
                    $section ++;
                    continue;
                }
                $hrv_line = explode("\t", trim($line));
                $line_data ='';
                if($section==1){
                    foreach ($hrv_line as $key => $value) {
                        if($key<12 || $key >16){
                            $line_data .= ($key!=0?",":"")."\"{$value}\"";
                        }else{
                            if($user->can('hrv_excolumn')){
                                $line_data .= ($key!=0?",":"")."\"{$value}\"";
                                if($key==13){
                                    if($i==0){
                                        $line_data .= ($key!=0?",":"")."\"{$hrv_line[12]}/{$value}\"";
                                    }else{
                                        $line_data .= ($key!=0?",":"")."\"".(($value!='0')? number_format((float)$hrv_line[12]/(float)$value,2) : 0)."\"";
                                    }
                                }
                            }
                        }
                    }
                    if($i==0){
                        $hrv_fields  .= "[{$line_data}]";
                    }else{
                        $hrv_data .= ($i!=1? ",":"")."[{$line_data}]";
                    }
                    $i++;
                }else if($section==2){
                    foreach ($hrv_line as $key => $value) {
                        $key_value = explode("=", trim($value));
                        if(count($key_value)>1){
                            $hrv_result .= ($j!=0? ",":"").'"'.$key_value[0]."\":\"".$key_value[1]."\"";
                        }
                        $j++;
                    }
                }else if($section==3){
                    foreach ($hrv_line as $key => $value) {
                        $key_value = explode("=", trim($value));
                        if(count($key_value)>1){
                            $rr = substr($key_value[1], -1, 1) == ',' ? substr_replace($key_value[1] ,"",-1) : $key_value[1];
                        }
                    }
                }else if($section==4){
                    foreach ($hrv_line as $key => $value) {
                        $key_value = explode("=", trim($value));
                        if(count($key_value)>1){
                            $ar = substr($key_value[1], -1, 1) == ',' ? substr_replace($key_value[1] ,"",-1) : $key_value[1];
                        }
                    }
                }
            }
            fclose($file_handle);
            return '{"fields":'. $hrv_fields .', "data":['.$hrv_data.'], "result":{'.$hrv_result.'}, "rr":['.$rr.'], "ar":['.$ar.']}';
        } else {
            return false;
        }
    }
}