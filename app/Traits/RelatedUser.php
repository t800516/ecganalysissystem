<?php 
namespace App\Traits;
trait RelatedUser
{
    private function getAllUsers($user_id, &$related_users, $allUser)
    {
        $users = $allUser->where('user_id',$user_id)->all();
        if(count($users)){
            foreach ($users as $key => $user) {
                if(!in_array($user->d_user_id, $related_users)){
                    $related_users[] = $user->d_user_id;
                    $this->getAllUsers($user->d_user_id, $related_users, $allUser);
                }
            }
        }
    }
}