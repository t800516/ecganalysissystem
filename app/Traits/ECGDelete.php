<?php 
namespace App\Traits;
trait ECGDelete
{
   private function del_file($user_id, $filename, $filename_ext)
    {
        $del_path=public_path('uploads/'.$user_id.'/del/');

        $time=time();
        if (!is_dir($del_path.$time.'/ekg/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/ekg/', 0777, true);
            umask($oldmask);
        }
        if (!is_dir($del_path.$time.'/ecg/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/ecg/', 0777, true);
            umask($oldmask);
        }
        if (!is_dir($del_path.$time.'/rr/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/rr/', 0777, true);
            umask($oldmask);
        }
        if (!is_dir($del_path.$time.'/hrv/')) {
            $oldmask = umask(0);
            mkdir($del_path.$time.'/hrv/', 0777, true);
            umask($oldmask);
        }
        return $this->renameAllFile($user_id,$filename,$filename,$filename_ext,'/del/'.$time);
    }
    private function renameAllFile($user_id,$oldfilename,$filename,$filename_ext,$movepath='')
    {
        $result=true;

        $psg_suffix=['.'.$filename_ext];
        $ecg_suffix=['_32.txt','_128.txt','_256.txt','_'.strtolower($filename_ext).'_info.txt','_'.strtolower($filename_ext).'_event.txt','_1024.bin'];
        $rr_suffix=['_256_rr.txt','_256_rr_edited.txt','_256_Table201.12.1.101.3.txt'];
        $hrv_suffix=['_5_256_hrv.txt','_10_256_hrv.txt','_15_256_hrv.txt','_30_256_hrv.txt','_60_256_hrv.txt'];

        if(!$this->rename_files(public_path('uploads/'.$user_id.'/ekg/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/ekg/'.$filename),$psg_suffix)){
            //$result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/ecg/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/ecg/'.$filename),$ecg_suffix)){
            //$result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/rr/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/rr/'.$filename),$rr_suffix)){
           // $result=false;
        }
        if(!$this->rename_files(public_path('uploads/'.$user_id.'/hrv/'.$oldfilename),public_path('uploads/'.$user_id.$movepath.'/hrv/'.$filename),$hrv_suffix)){
        }

        return $result;
    }
    private function rename_files($old_path,$current_path,$suffixs){
        $result=true;
        foreach ($suffixs as $suffix) {
            if(file_exists($old_path.$suffix)){
                rename($old_path.$suffix,$current_path.$suffix);
            }else{
                //$result=false;
            }
        }
        return $result;
    }
}