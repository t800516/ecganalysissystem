<?php

use Illuminate\Database\Seeder;

class NotifiableModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names=[
            'TMUH' //1
        ];

        
        foreach ($names as $value) {
            if(DB::table('notifiable_modules')->where('name',$value)->count()==0){
                DB::table('notifiable_modules')->insert([
                    'name' => $value
                ]);
            }
        }
    }
}
