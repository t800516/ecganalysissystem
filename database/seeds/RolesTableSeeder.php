<?php

use Illuminate\Database\Seeder;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_names=[
            '測試型',      //1
            '基本型',      //2
            '進階型',      //3
            '資料型',      //4
            '管理型',      //5
            '瀏覽型',      //6
            'FDAOTC',     //7  
            'FDAPOC',     //8  
            'ECG瀏覽型',   //9
            'FDAMGR',     //10
            '醫生型',      //11
            '簡易型',      //12
            '簡易管理型',   //13
        ];

        
        foreach ($role_names as $value) {
            if(DB::table('roles')->where('name',$value)->count()==0){
                DB::table('roles')->insert([
                    'name' => $value
                ]);
            }
        }
    }
}
