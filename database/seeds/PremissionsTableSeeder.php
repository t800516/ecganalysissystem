<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PremissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perssion_names=[
            'multiple_login',       // 1
            'hrv_interval_input',   // 2
            'hrv_excolumn',         // 3
            'hrv_chart',            // 4
            'rr_download',          // 5
            'holter_id_pass',       // 6
            'ecg_analysis',         // 7
            'ecg_table_conversion', // 8
            'ecg_table_priority',   // 9
            'explorer_type',        //10
            'data_type',            //11
            'manager_type',         //12
            'ecg_manager',          //13
            'ecg_manager_model',    //14
            'data_manager_model',   //15
            'sleep_analysis',       //16
            'analytic_information', //17
            'user_manager',         //18
            'user_ecg_manager',     //19
            'report_origin_name',   //20
            'send_report',          //21
            'rename_ecg',           //22
            'upload_ekg',           //23
            'ecg_explorer_type',    //24
            'edit_ecg_info',        //25
            'poc_manager',          //26
            'service_manager',      //27
            'otc_user_info',        //28
            'mgr_manager',          //29
            'prescreening_report',  //30
            'all_records',          //31
            'notifiable_module',    //32
            'user_list',            //33
            'ecg_transfer',         //34
            'ecg_analysis_page',    //35
            'ecg_list_option',      //36
            'poc_edit',             //37
            'ecg_data',             //38
            'otc_manager',          //39
            'new_poc',              //40
            'service_read',         //41
            'ecg_table_checked',    //42
            'edit_profile',         //43
            'service_export',       //44
            'QTC_column',           //45
            'QTC_report_download',  //46
            'OTC_hidden_column',    //47
            'ecg_table_confirm',    //48
            'ecg_table_type_filter',//49
            'ecg_analysis_switch',  //50
            'doctor_type',          //51
            'ecg_analysis_confirm', //52
            'ecg_assigned',         //53
            'report_download',      //54
            'report_summary',      //55
            'peak_overview',      //56
            'hrv_analysis',      //57,
            'report_type_by_comment',      //58
        ];

        foreach ($perssion_names as $value) {
            if(DB::table('permissions')->where('name',$value)->count()==0){
                DB::table('permissions')->insert([
            		'name' => $value
            	]);
            }
        }
    }
}
