<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PremissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //測試型
        App\Role::find(1)->permissions()->sync([1,2,3,4,5,6,7,8,9,13,16,17,21,22,23,25,27,30,36,41,43,54,56,57]);
        
        //基本型
        App\Role::find(2)->permissions()->sync([6,7,8,9,13,22,23,25,36,43,54,56,57]);
        
        //進階型
        App\Role::find(3)->permissions()->sync([1,2,3,4,5,6,7,8,9,13,16,17,21,22,23,25,36,43,54]);
        
        //資料型
        App\Role::find(4)->permissions()->sync([6,7,8,9,11,16,19,20,22,23,25,36,38,43,54,56,57]);
        
        //管理型
        App\Role::find(5)->permissions()->sync([1,2,3,4,5,6,7,8,9,12,18,19,22,23,25,26,28,29,30,31,32,33,34,35,36,38,41,42,43,44,48,49,50,52,53,54,56,57]);
        
        //瀏覽型
        App\Role::find(6)->permissions()->sync([6,8,9,10,13,25,27,36,41,43,54,56,57]);
        
        //FDAOTC
        App\Role::find(7)->permissions()->sync([6,10,13,23,25,36,54,56,57]);
        
        //FDAPOC
        App\Role::find(8)->permissions()->sync([2,3,4,6,7,12,13,14,15,18,19,23,25,27,28,35,36,37,38,39,41,43,46,54,56,57]);
        
        //ECG瀏覽型
        App\Role::find(9)->permissions()->sync([6,7,13,24,36,43,54,56,57]);
        
        //FDAMGR
        App\Role::find(10)->permissions()->sync([2,3,4,6,7,12,13,18,19,23,25,26,27,28,29,35,37,38,40,41,43,54,56,57]);
        
        //醫生型
        App\Role::find(11)->permissions()->sync([6,7,8,9,13,25,36,43,51,52,54]);
        //簡易型
        App\Role::find(12)->permissions()->sync([6,10,13,24,56,57]);

        //簡易管理型
        App\Role::find(13)->permissions()->sync([1,2,3,4,6,7,12,18,19,22,23,25,28,29,30,33,35,36,38,39,42,43,44,52,54,55,56,58]);
    }
}
