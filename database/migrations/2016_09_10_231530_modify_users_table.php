<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('sex',8);
            $table->date('birthday');
            $table->string('zipcode',6);
            $table->string('country');
            $table->string('city');
            $table->string('downtown');
            $table->string('address');
            $table->string('tel');
            $table->string('cel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['sex','birthday','zipcode','country','city','downtown','address','tel','cel']);
        });
    }
}
