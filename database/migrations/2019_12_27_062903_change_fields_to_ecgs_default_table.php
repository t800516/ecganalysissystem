<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsToEcgsDefaultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->string('service_SN')->default('')->nullable()->change();
            $table->string('duration')->default('')->nullable()->change();
            $table->string('site')->default('')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->string('service_SN')->nullable()->change();
            $table->string('duration')->nullable()->change();
            $table->string('site')->nullable()->change();
        });
    }
}
