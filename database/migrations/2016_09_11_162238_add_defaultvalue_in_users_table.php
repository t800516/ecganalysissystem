<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultvalueInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('sex',8)->default('')->change();
            $table->string('birthday',10)->default('0000-00-00')->change();
            $table->string('zipcode',6)->default('')->change();
            $table->string('country')->default('')->change();
            $table->string('city')->default('')->change();
            $table->string('downtown')->default('')->change();
            $table->string('address')->default('')->change();
            $table->string('tel')->default('')->change();
            $table->string('cel')->default('')->change();
        });
         Schema::table('holters', function (Blueprint $table) {
            $table->string('sex',8)->default('')->change();
            $table->string('birthday',10)->default('0000-00-00')->change();
            $table->string('zipcode',6)->default('')->change();
            $table->string('country')->default('')->change();
            $table->string('city')->default('')->change();
            $table->string('downtown')->default('')->change();
            $table->string('address')->default('')->change();
            $table->string('tel')->default('')->change();
            $table->string('cel')->default('')->change();
            $table->string('email')->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('sex',8)->change();
            $table->date('birthday')->change();
            $table->string('zipcode',6)->change();
            $table->string('country')->change();
            $table->string('city')->change();
            $table->string('downtown')->change();
            $table->string('address')->change();
            $table->string('tel')->change();
            $table->string('cel')->change();
        });
        Schema::table('holters', function (Blueprint $table) {
            $table->string('sex',8)->change();
            $table->date('birthday')->change();
            $table->string('zipcode',6)->change();
            $table->string('country')->change();
            $table->string('city')->change();
            $table->string('downtown')->change();
            $table->string('address')->change();
            $table->string('tel')->change();
            $table->string('cel')->change();
            $table->string('email')->change();
        });
    }
}
