<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToEcgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->string('patient_name')->nullable()->default('username')->change();
            $table->text('description')->nullable()->change();
            $table->string('sex',8)->nullable()->default('N')->change();
            $table->string('patient_IDNumber')->nullable()->default('A123456789')->change();
            $table->string('holter_IDNumber')->nullable()->default('')->change();
            $table->string('birthday')->nullable()->default('1900-00-00')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->string('patient_name')->nullable(0)->change();
            $table->text('description')->nullable(0)->change();
            $table->string('sex',8)->nullable(0)->change();
            $table->string('patient_IDNumber')->nullable(0)->change();
            $table->string('holter_IDNumber')->nullable(0)->change();
            $table->string('birthday')->nullable(0)->change();
        });
    }
}
