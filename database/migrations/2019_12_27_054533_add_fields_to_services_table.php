<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->string('patient_name')->default('')->nullable();
            $table->string('sex')->default('')->nullable();
            $table->string('patient_IDNumber')->default('')->nullable();
            $table->date('birthday')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('patient_name');
            $table->dropColumn('sex');
            $table->dropColumn('patient_IDNumber');
            $table->dropColumn('birthday');
        });
    }
}
