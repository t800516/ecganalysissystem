<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportColumnToEcgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->string('min_HR')->default('');
            $table->string('max_HR')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->dropColumn('min_HR');
            $table->dropColumn('max_HR');
        });
    }
}
