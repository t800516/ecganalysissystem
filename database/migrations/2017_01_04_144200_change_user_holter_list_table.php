<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserHolterListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_holter', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
           
            $table->integer('holter_id')->unsigned();
            $table->foreign('holter_id')->references('id')->on('holters')->onDelete('cascade');
            $table->primary(['user_id', 'holter_id']);
        });
        Schema::drop('user_holter_list');
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_holter');
        Schema::create('user_holter_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('holter_id')->unsigned();
            $table->timestamps();
            $table->index('user_id');
            $table->index('holter_id');
        });
    }
}
