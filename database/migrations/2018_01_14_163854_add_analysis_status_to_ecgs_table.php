<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnalysisStatusToEcgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->integer('analysis_status')->default(2);
            $table->integer('sample_rate')->default(0)->change();
            $table->float('ecganalysis_time',8,2)->default(0)->change();
            $table->float('rranalysis_time',8,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->dropColumn('analysis_status');
            $table->integer('sample_rate')->change();
            $table->float('ecganalysis_time',8,2)->change();
            $table->float('rranalysis_time',8,2)->change();
        });
    }
}
