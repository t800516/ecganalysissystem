<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToEcgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->integer('service_id')->unsinged()->default(0);
            $table->string('service_SN')->nullable();
            $table->date('start_date')->nullable();
            $table->string('duration')->nullable();
            $table->string('site')->nullable();
            $table->integer('notified')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->dropColumn('service_id');
            $table->dropColumn('service_SN');
            $table->dropColumn('start_date');
            $table->dropColumn('duration');
            $table->dropColumn('site');
            $table->dropColumn('notified');
        });
    }
}
