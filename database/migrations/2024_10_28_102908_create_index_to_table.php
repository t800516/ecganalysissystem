<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->index(['filename']);
            $table->index(['patient_IDNumber']);
            $table->index(['created_at']);
            $table->index(['filename','patient_IDNumber'], 'ecgs_f_p_index');
            $table->index(['filename','created_at'], 'ecgs_f_c_index');
            $table->index(['patient_IDNumber','created_at'], 'ecgs_p_c_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            
            $table->dropIndex(['filename']);
            $table->dropIndex(['patient_IDNumber']);
            $table->dropIndex(['created_at']);
            $table->dropIndex('ecgs_f_p_index');
            $table->dropIndex('ecgs_f_c_index');
            $table->dropIndex('ecgs_p_c_index');
        });
    }
}
