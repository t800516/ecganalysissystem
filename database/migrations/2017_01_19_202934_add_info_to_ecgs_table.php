<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoToEcgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->dropColumn('holter_id');
            $table->string('patient_name')->after('filesize');
            $table->integer('age');
            $table->string('sex',8);
            $table->string('comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->integer('holter_id')->unsigned();
            $table->dropColumn(['patient_name','age','sex','comment']);
        });
    }
}
