<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcgUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecg_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('ecg_id')->unsigned();
            $table->primary(['user_id', 'ecg_id']);
        });

        Schema::table('ecgs', function (Blueprint $table) {
            $table->integer('assignee')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->dropColumn('assignee');
        });
        Schema::dropIfExists('ecg_user');
    }
}
