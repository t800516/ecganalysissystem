<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('IDNumber');
            $table->string('name');
            $table->string('sex',16);
            $table->date('birthday');
            $table->integer('age');
            $table->string('zipcode',6);
            $table->string('country');
            $table->string('city');
            $table->string('downtown');
            $table->string('address');
            $table->string('tel');
            $table->string('cel');
            $table->string('email');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('holters');
    }
}
