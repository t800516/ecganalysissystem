<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePatientNameDeflautEcgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->string('patient_name')->default('')->change();
            $table->integer('age')->default(0)->change();
            $table->string('sex',8)->default('')->change();
            $table->text('comment')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecgs', function (Blueprint $table) {
            $table->string('patient_name')->change();
            $table->integer('age')->change();
            $table->string('sex',8)->change();
            $table->string('comment')->change();
        });
    }
}
