<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeHoltersStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('holters', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('sex');
            $table->dropColumn('birthday');
            $table->dropColumn('age');
            $table->dropColumn('zipcode');
            $table->dropColumn('country');
            $table->dropColumn('city');
            $table->dropColumn('downtown');
            $table->dropColumn('address');
            $table->dropColumn('tel');
            $table->dropColumn('cel');
            $table->dropColumn('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holters', function (Blueprint $table) {
            $table->string('name');
            $table->string('sex',8);
            $table->date('birthday');
            $table->integer('age');
            $table->string('zipcode',6);
            $table->string('country');
            $table->string('city');
            $table->string('downtown');
            $table->string('address');
            $table->string('tel');
            $table->string('cel');
            $table->string('email');
        });
    }
}
