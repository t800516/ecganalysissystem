<?php
	return [
	    'default'   => [
	        'length'    => 5,
	        'width'     => 150,
	        'height'    => 40,
	        'quality'   => 90,
	        'math'      => false,  //Enable Math Captcha
	        'expire'    => 60,    //Stateless/API captcha expiration
	    ]
	];